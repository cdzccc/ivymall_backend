<?php
    include("../config.php");
    $category = filter_input(INPUT_GET, 'category');

    $sql_cmd = "select Category_Code, Category_Name from category where 
        Category_CodeGroup = 'Goods_Sub_Category' and deleted_at is null and Status = 1 
        and Parent_Category_Code = '".$category."'
        order by update_datetime asc";
    $rs_category = $db->query($sql_cmd);
    $result = [];
    while($row = $rs_category->fetchRow(MDB2_FETCHMODE_ASSOC)) {
        $result[] = array(
            "id" => urlencode($row['Category_Code']),
            "text" => urlencode($row['Category_Name']),
        );
    }
    echo json_encode($result);
?>