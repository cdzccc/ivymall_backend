<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("orders_edit");

$Order_ID               = filter_input(INPUT_POST, 'Order_ID');
$name                   = filter_input(INPUT_POST, 'name');
$email                  = filter_input(INPUT_POST, 'email');
$phone                  = filter_input(INPUT_POST, 'phone');
$city                   = filter_input(INPUT_POST, 'city');
$area                   = filter_input(INPUT_POST, 'area');
$address                = filter_input(INPUT_POST, 'address');
$Status                 = filter_input(INPUT_POST, 'Status');
$delivery_at            = filter_input(INPUT_POST, 'delivery_at');
$cancel_remark          = filter_input(INPUT_POST, 'cancel_remark');
$payment                = filter_input(INPUT_POST, 'Payment');
$Delivery_Name          = filter_input(INPUT_POST, 'Delivery_Name');
$Delivery_Mobile        = filter_input(INPUT_POST, 'Delivery_Mobile');
$Delivery_Phone         = filter_input(INPUT_POST, 'Delivery_Phone');
$Delivery_City          = filter_input(INPUT_POST, 'Delivery_City');
$Delivery_Area          = filter_input(INPUT_POST, 'Delivery_Area');
$Delivery_Addr          = filter_input(INPUT_POST, 'Delivery_Addr');
$Delivery_Company       = filter_input(INPUT_POST, 'Delivery_Company');
$delivery_remark        = filter_input(INPUT_POST, 'delivery_remark');
$delivery               = filter_input(INPUT_POST, 'delivery');
$store_name             = filter_input(INPUT_POST, 'store_name');
$store_addr             = filter_input(INPUT_POST, 'store_addr');
$delivery_time          = filter_input(INPUT_POST, 'Delivery_time');
$Delivery_No            = filter_input(INPUT_POST, 'Delivery_No');
$price                  = filter_input(INPUT_POST, 'price');
$discount_ticket        = filter_input(INPUT_POST, 'discount_ticket');
$discount_ticket_name   = filter_input(INPUT_POST, 'discount_ticket_name');
$discount_point         = filter_input(INPUT_POST, 'discount_point');
$discount_customer      = filter_input(INPUT_POST, 'discount_customer');
$discount_customer_name = filter_input(INPUT_POST, 'discount_customer_name');
$discount_purchase      = filter_input(INPUT_POST, 'discount_purchase');
$discount_purchase_name = filter_input(INPUT_POST, 'discount_purchase_name');
$discount_off           = filter_input(INPUT_POST, 'discount_off');
$discount_off_name      = filter_input(INPUT_POST, 'discount_off_name');
$Delivery_Price         = filter_input(INPUT_POST, 'Delivery_Price');
$total_price            = filter_input(INPUT_POST, 'total_price');
$point_get              = filter_input(INPUT_POST, 'point_get');
$Remark                 = filter_input(INPUT_POST, 'Remark');
$i_type                 = filter_input(INPUT_POST, 'i_type');
$i_title                = filter_input(INPUT_POST, 'i_title');
$i_tax_id_number        = filter_input(INPUT_POST, 'i_tax_id_number');
$i_address              = filter_input(INPUT_POST, 'i_address');
$i_status               = filter_input(INPUT_POST, 'i_status');
$i_number               = filter_input(INPUT_POST, 'i_number');

$Goods_ID               = filter_input(INPUT_POST, 'Goods_ID', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$Goods_Name             = filter_input(INPUT_POST, 'Goods_Name', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$remark                 = filter_input(INPUT_POST, 'remark', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$Goods_Price            = filter_input(INPUT_POST, 'Goods_Price', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$Goods_Discount_Price   = filter_input(INPUT_POST, 'Goods_Discount_Price', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$Goods_Qty              = filter_input(INPUT_POST, 'Goods_Qty', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$Total_Price            = filter_input(INPUT_POST, 'Total_Price', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$event                  = filter_input(INPUT_POST, 'event', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$order_item_status      = filter_input(INPUT_POST, 'order_item_status', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

if(empty($Order_ID)) {
    js_go_back_global("NOT_POST");
    exit;
}

$sql_cmd = "select a.*,b.Customer_Mail, b.level from `order` as a join customer as b on a.Customer_ID = b.Customer_ID where a.Order_ID = '".$Order_ID."'";
$rs = $db->query($sql_cmd);
$row_order = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);

$sql_array = array(
    "name"                   => array("2", checkinput_sql($name,50)),
    "email"                  => array("2", checkinput_sql($email,100)),
    "phone"                  => array("2", checkinput_sql($phone,20)),
    "city"                   => array("2", checkinput_sql($city,20)),
    "area"                   => array("2", checkinput_sql($area,20)),
    "address"                => array("2", checkinput_sql($address, 200)),
    "Status"                 => array("2", checkinput_sql($Status,2)),
    "cancel_remark"          => array("2", checkinput_sql($cancel_remark,200)),
    "payment"                => array("2", checkinput_sql($payment,50)),
    "Delivery_Name"          => array("2", checkinput_sql($Delivery_Name,20)),
    "Delivery_Mobile"        => array("2", checkinput_sql($Delivery_Mobile,20)),
    "Delivery_Phone"         => array("2", checkinput_sql($Delivery_Phone,20)),
    "Delivery_City"          => array("2", checkinput_sql($Delivery_City,20)),
    "Delivery_Area"          => array("2", checkinput_sql($Delivery_Area,20)),
    "Delivery_Addr"          => array("2", checkinput_sql($Delivery_Addr,100)),
    "delivery"               => array("2", checkinput_sql($delivery,9)),
    "store_name"             => array("2", checkinput_sql($store_name,50)),
    "store_addr"             => array("2", checkinput_sql($store_addr, 200)),
    "delivery_time"          => array("2", checkinput_sql($delivery_time,2)),
    "Delivery_No"            => array("2", checkinput_sql($Delivery_No,20)),
    "price"                  => array("2", checkinput_sql($price,6)),
    "discount_ticket"        => array("2", checkinput_sql($discount_ticket,6)),
    "discount_ticket_name"   => array("2", checkinput_sql($discount_ticket_name,200)),
    "discount_point"         => array("2", checkinput_sql($discount_point,5)),
    "discount_customer"      => array("2", checkinput_sql($discount_customer,5)),
    "discount_customer_name" => array("2", checkinput_sql($discount_customer_name,200)),
    "discount_purchase"      => array("2", checkinput_sql($discount_purchase,6)),
    "discount_purchase_name" => array("2", checkinput_sql($discount_purchase_name,200)),
    "discount_off"           => array("2", checkinput_sql($discount_off,6)),
    "discount_off_name"      => array("2", checkinput_sql($discount_off_name, 200)),
    "Delivery_Price"         => array("2", checkinput_sql($Delivery_Price,6)),
    "total_price"            => array("2", checkinput_sql($total_price,6)),
    "point_get"              => array("2", checkinput_sql($point_get,6)),
    "Remark"                 => array("2", checkinput_sql($Remark,200)),
    "i_type"                 => array("2", checkinput_sql($i_type,2)),
    "i_title"                => array("2", checkinput_sql($i_title,100)),
    "i_tax_id_number"        => array("2", checkinput_sql($i_tax_id_number,10)),
    "i_address"              => array("2", checkinput_sql($i_address,150)),
    "i_status"               => array("2", checkinput_sql($i_status,2)),
    "i_number"               => array("2", checkinput_sql($i_number,10)),
    "Delivery_Company"       => array("2", checkinput_sql($Delivery_Company,50)),
    "delivery_remark"        => array("2", checkinput_sql($delivery_remark,200)),
);
if(!empty($delivery_at)){
	$sql_array["delivery_at"]=array("2", checkinput_sql($delivery_at,50));
}
if($delivery_at==""){
	$sql_cmd = "UPDATE `order` SET `delivery_at` = NULL WHERE `order`.`Order_ID` = '".$Order_ID."'";
	$rs = $db->query($sql_cmd);
}

$sql_cmd = update("order", array("Order_ID", $Order_ID), $sql_array);
$rs = $db->query($sql_cmd);

$pear = new PEAR();
if ($pear->isError($rs))
{
    js_go_back_global("DB_EDIT_ERROR");
    exit;
}else{
    // order item
    foreach ($Goods_ID as $key => $value) {
        $item_number = explode(" - ", $Goods_ID[$key]);
        $sql_array = [
            "Goods_ID"             => array("2", $item_number[0]),
            "Magazine_item_number" => array("2", (!empty($item_number[1])?$item_number[1]:"")),
            "remark"               => array("2", $remark[$key]),
            "Goods_Price"          => array("2", $Goods_Price[$key]),
            "Goods_Discount_Price" => array("2", $Goods_Discount_Price[$key]),
            "Goods_Qty"            => array("2", $Goods_Qty[$key]),
            "Total_Price"          => array("2", $Total_Price[$key]),
            "event"                => array("2", $event[$key]),
            "Status"               => array("2", $order_item_status[$key]),
        ];
        $sql_cmd = update("order_item", array("Order_Item_ID", $key), $sql_array);
        $rs2 = $db->query($sql_cmd);
    }
    // 訂單轉付款成功得點
    /*if($row_order['Status'] == '1' && $order_status > '99') {
        // 紅利得點
        $get_point = $point_get;
        $sql_cmd = "select * from point_summary where Customer_ID = '".$row_order['Customer_ID']."'";
        $rs2 = $db->query($sql_cmd);
        if($rs2->numRows() > 0) {
            $row_point = $rs2->fetchRow(MDB2_FETCHMODE_ASSOC);
            $point_total = $row_point['total'] + $get_point;
            $point_in = $row_point['in'] + $get_point;
            $sql_cmd = "update point_summary set `in` = ".$point_in.",total = ".$point_total." where Customer_ID = '".$row_order['Customer_ID']."'";
            $rs2 = $db->query($sql_cmd);
        }
        else {
            $point_summary_id = get_id();
            $sql_array = array(
                "id"          => array("2", $point_summary_id),
                "Customer_ID" => array("2", $row_order['Customer_ID']),
                "in"          => array("2", $get_point),
                "out"         => array("2", 0),
                "total"       => array("2", $get_point),
            );
            $sql_cmd = insert("point_summary", $sql_array);
            $rs2 = $db->query($sql_cmd);
            $row_point['total'] = 0;
        }
        $point_list_id = get_id();
        $sql_array = array(
            "id"          => array("2", $point_list_id),
            "Customer_ID" => array("2", $row_order['Customer_ID']),
            "point"       => array("2", $get_point),
            "type"        => array("2", 1),
            "datetime"    => array("2", date("Y-m-d H:i:s")),
            "Order_ID"    => array("2", $row_order['Order_ID']),
            "left"        => array("2", $row_point['total']+$get_point),
            "mode"        => array("2", "bonus"),
        );
        $sql_cmd = insert("point_list", $sql_array);
        $rs2 = $db->query($sql_cmd);
    }*/

    if($row_order["Status"] != $Status && in_array($Status, [6,9,10,11,12,13])) {
        //加回庫存
        $sql_cmd = "select * from order_item where Order_ID = '".checkinput_sql($Order_ID, 19)."'";
        $rs = $db->query($sql_cmd);
        while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $sql_cmd = "update goods_item set Qty = (Qty+".$row['Goods_Qty'].") where Goods_Item_ID = '".$row['Goods_Item_ID']."'";
            $db->query($sql_cmd);
        }
        if($row_order['Pay_Status'] == 'y') {
            //歸還點數
            $sql_cmd = "select * from point_list where Order_ID = '".checkinput_sql($Order_ID, 19)."' and Customer_ID = '".$row_order['Customer_ID']."'";
            $rs = $db->query($sql_cmd);
            while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                $sql_cmd = "select * from point_summary where Customer_ID = '".$row_order['Customer_ID']."'";
                $rs_summary = $db->query($sql_cmd);
                $row_summary = $rs_summary->fetchRow(MDB2_FETCHMODE_ASSOC);
                if($row['type'] == 1) {
                    $type = 2;
                    $row_summary['out'] += $row['point'];
                    $row_summary['total'] -= $row['point'];
                }
                else {
                    $type = 1;
                    $row_summary['in'] += $row['point'];
                    $row_summary['total'] += $row['point'];
                }
                $sql_cmd = "update point_summary set `out` = ".$row_summary['out'].", `in` = ".$row_summary['in'].", total = ".$row_summary['total']." where Customer_ID = '".$row_order['Customer_ID']."'";
                $db->query($sql_cmd);
                $point_list_id = get_id();
                $left = $row['left'];
                $sql_array = array(
                    "id"          => array("2", $point_list_id),
                    "Customer_ID" => array("2", $row_order['Customer_ID']),
                    "point"       => array("2", $row['point']),
                    "type"        => array("2", $type),
                    "datetime"    => array("2", date("Y-m-d H:i:s")),
                    "Order_ID"    => array("2", checkinput_sql($Order_ID, 19)),
                    "left"        => array("2", $row_summary['total']),
                    "level"        => array("2", checkinput_sql($row_order['level'], 50)),
                );
                $sql_cmd = insert("point_list", $sql_array);
                $db->query($sql_cmd);
            }
        }
    }
	
    if(($Status == 3 || $Status == 4) && $row_order["Status"] != $Status && $row_order["store_update"] == null) {
        $sql_cmd = "update `order` set `store_update` = '0' where Order_ID ='".$row_order['Order_ID']."' and `status` not in (8)";
        $db->query($sql_cmd);
        if($row_order['delivery'] == 1) {
            if($row_order["Payment"] == 3) {
                $servicetype = "1";
            }
            else {
                $servicetype = "3";
            }
            preg_match("/^([^(]+)\(#?([^)]+)/", $row_order['store_name'], $store);
            $storeid = $store[2];
            $storename = $store[1];
            $ShipDate = date("Y-m-d");
            $ReturnDate = date("Y-m-d", strtotime($ShipDate."+7 days"));
            $sql_array = array(
                "crdate"      => array("2", date("Y-m-d H:i:s")),
                "customer_id" => array("3", $row_order['Customer_ID']),
                "order_id"    => array("3", $row_order['Order_ID']),
                "storepath"   => array("2", "A"),
                "storeid"     => array("3", $storeid),
                "storename"   => array("2", $storename),
                "storeaddr"   => array("2", $row_order['store_addr']),
                "servicetype" => array("2", $servicetype),
                "status"      => array("2", 30),
                "ShipDate"    => array("2", $ShipDate),
                "ReturnDate"  => array("2", $ReturnDate)
            );
            $sql_cmd = insert("store", $sql_array);
            $db->query($sql_cmd);
        }
        else if($row_order['delivery'] == 2) {
            if($row_order["Payment"] == 3) {
                $servicetype = "1";
            }
            else {
                $servicetype = "3";
            }
            preg_match("/^([^(]+)\(#?([^)]+)/", $row_order['store_name'], $store);
            $storeid = $store[2];
            $storename = $store[1];
            $ShipDate = date("Y-m-d");
            $ReturnDate = date("Y-m-d", strtotime($ShipDate."+7 days"));
            $sql_array = array(
                "crdate"      => array("2", date("Y-m-d H:i:s")),
                "customer_id" => array("3", $row_order['Customer_ID']),
                "order_id"    => array("3", $row_order['Order_ID']),
                "storepath"   => array("2", "A"),
                "storeid"     => array("3", $storeid),
                "storename"   => array("2", $storename),
                "storeaddr"   => array("2", $row_order['store_addr']),
                "servicetype" => array("2", $servicetype),
                "status"      => array("2", 30),
                "ShipDate"    => array("2", $ShipDate),
                "ReturnDate"  => array("2", $ReturnDate)
            );
            $sql_cmd = insert("store_cvs", $sql_array);
            $db->query($sql_cmd);
        }
    }
    
	if($Status == 7 && $row_order["Status"] != $Status) {
        //$sql_cmd = "update `order` set delivery_at = '".date("Y-m-d 00:00:00")."'  where `Status` = 7 and i_status = 1 and Order_ID = '".$Order_ID."'";
        //$db->query($sql_cmd);
    }
	
    if($Status == 8 && $row_order["Status"] != $Status && $row_order["is_finish"]==0) {
        //$sql_cmd = "update `customer` set order_count = (order_count + 1), year_consumption = (year_consumption + ".$row_order['total_price']."), total_consumption = (total_consumption + ".$row_order['total_price'].") where Customer_ID = '".$row_order['Customer_ID']."'";
        //$db->query($sql_cmd);
		//$sql_cmd = "update `order` set is_finish = '1' where Order_ID = '".$row_order['Order_ID']."'";
        //$db->query($sql_cmd);

		//加點數
		if($point_get > 0) {
            $sql_cmd = "select * from point_summary where Customer_ID = '".$row_order['Customer_ID']."'";
            $rs_summary = $db->query($sql_cmd);
            if($rs_summary->numRows() > 0) {
                $row_point = $rs_summary->fetchRow(MDB2_FETCHMODE_ASSOC);
                $point_total = $row_point['total'] + $point_get;
                $point_in = $row_point['in'] + $point_get;
                $point_out = $row_point['out'];
                $sql_cmd = "update point_summary set `in` = ".$point_in.", `out` = ".$point_out.", total = ".$point_total." where Customer_ID = '".$row_order['Customer_ID']."'";
				$rs2 = $db->query($sql_cmd);
            }
            else {
                $point_summary_id = get_id();
                $sql_array = array(
                    "id"          => array("2", $point_summary_id),
                    "Customer_ID" => array("2", $row_order['Customer_ID']),
                    "in"          => array("2", $point_get),
                    "out"         => array("2", 0),
                    "total"       => array("2", $point_get),
                );
                $sql_cmd = insert("point_summary", $sql_array);
                $rs2 = $db->query($sql_cmd);
                $row_point['total'] = 0;
            }
            $point_list_id = get_id();
            $enabled_date = date("Y-m-d H:i:s");
            $end_date = date("Y-m-d H:i:s", strtotime("+1 years"));
            $sql_array = array(
                "id"           => array("2", checkinput_sql($point_list_id, 19)),
                "Customer_ID"  => array("2", checkinput_sql($row_order['Customer_ID'], 200)),
                "point"        => array("2", checkinput_sql($row_order["point_get"], 200)),
                "mode"         => array("2", checkinput_sql(2 , 45)),
                "type"         => array("2", checkinput_sql(1,2)),
                "datetime"     => array("2", checkinput_sql(date("Y-m-d H:i:s"), 50)),
                "Order_ID"     => array("2", checkinput_sql($row_order['Order_ID'], 50)),
                "left"         => array("2", $row_point['total']+$point_get),
                "enabled_date" => array("2", checkinput_sql($enabled_date, 50)),
                "status"       => array("2", checkinput_sql(1, 50)),
                "end_date"     => array("2", checkinput_sql($end_date, 50)),
                "level"        => array("2", checkinput_sql($row_order['level'], 50)),
            );
            $sql_cmd = insert("point_list", $sql_array);
            $db->query($sql_cmd);
        }

        //首購
        $sql_cmd = "select * from `order` where Status in (8,12,13) and Order_ID != '".$row_order['Order_ID']."' and Customer_ID = '".$row_order['Customer_ID']."'";
        $rs = $db->query($sql_cmd);
        if($rs->numRows() == 0) {
            $sql_cmd = "select * from point_summary where Customer_ID = '".$row_order['Customer_ID']."'";
            $rs_summary = $db->query($sql_cmd);
            $point_get = 30;
            $row_point = $rs_summary->fetchRow(MDB2_FETCHMODE_ASSOC);
            $point_total = $row_point['total'] + $point_get;
            $point_in = $row_point['in'] + $point_get;
            $point_out = $row_point['out'];
            $sql_cmd = "update point_summary set `in` = ".$point_in.", `out` = ".$point_out.", total = ".$point_total." where Customer_ID = '".$row_order['Customer_ID']."'";
            $rs2 = $db->query($sql_cmd);
            $point_list_id = get_id();
            $enabled_date = date("Y-m-d H:i:s");
            $end_date = date("Y-m-d H:i:s", strtotime("+1 years"));
            $sql_array = array(
                "id"           => array("2", checkinput_sql($point_list_id, 19)),
                "Customer_ID"  => array("2", checkinput_sql($row_order['Customer_ID'], 200)),
                "point"        => array("2", checkinput_sql($row_order["point_get"], 200)),
                "mode"         => array("2", checkinput_sql(1 , 45)),
                "type"         => array("2", checkinput_sql(1,1)),
                "datetime"     => array("2", checkinput_sql(date("Y-m-d H:i:s"), 50)),
                "Order_ID"     => array("2", checkinput_sql($row_order['Order_ID'], 50)),
                "left"         => array("2", $row_point['total']+$point_get),
                "enabled_date" => array("2", checkinput_sql($enabled_date, 50)),
                "status"       => array("2", checkinput_sql(1, 50)),
                "end_date"     => array("2", checkinput_sql($end_date, 50)),
                "level"        => array("2", checkinput_sql($row_order['level'], 50)),
            );
            $sql_cmd = insert("point_list", $sql_array);
            $db->query($sql_cmd);
        }
		//給折價卷
		//print_r($Goods_ID);
		foreach ($Goods_ID as $key => $value) {
			if(!empty($event[$key])){
				$event =  json_decode($event[$key], true);
				if(!in_array($event[0]['id'],$event_group)){
					$event_group[] = $event[0]['id'];
					
					$sql_cmd = "select * from goods_event 
						where 
							type = 6 
							and id = ".$event[0]['id']."";
					$rs_event = $db->query($sql_cmd);
					$row_event = $rs_event->fetchRow(MDB2_FETCHMODE_ASSOC);
					if($row_event['type']==6){
						$sql_cmd = "select * from ticket where ticket_id = '".$row_event['ticket']."'";
						$rs_ticket = $db->query($sql_cmd);
						$row_ticket = $rs_ticket->fetchRow(MDB2_FETCHMODE_ASSOC);
						$sql_array = array(
							"Ticket_ID"       => array("2", checkinput_sql($row_ticket['ticket_id'], 19)),
							"CODE"            => array("2", ""),
							"Customer_ID"     => array("2", checkinput_sql($row_order['Customer_ID'], 50)),
							"price"           => array("2", checkinput_sql($row_ticket['price'], 5)),
							"date_start"      => array("2", checkinput_sql($row_ticket['date_start'], 50)),
							"date_end"        => array("2", checkinput_sql($row_ticket['date_end'], 50)),
							"status"          => array("2", 1),
							"create_datetime" => array("2", checkinput_sql(date("Y-m-d H:i:s"), 50)),
						);
						$sql_cmd = insert("ticket_list", $sql_array);
						$db->query($sql_cmd);
					}
				}
			}
		}
		
	
        /*echo $sql_cmd = "select * from goods_event 
            where 
                type = 6 
                and value1 >= ".$row_order['total_price']." 
                and status = 1 
                and sdate <= '".$row_order['create_datetime']."' 
                and edate >= '".$row_order['create_datetime']."'
                order by value1 desc limit 1";
        $rs_event = $db->query($sql_cmd);
        if($rs_event->numRows() > 0) {
			echo "給折價卷";
            $row_event = $rs_event->fetchRow(MDB2_FETCHMODE_ASSOC);
            $sql_cmd = "select * from ticket where ticket_id = '".$row_event['ticket_id']."'";
            $rs_ticket = $db->query($sql_cmd);
            $row_ticket = $rs_ticket->fetchRow(MDB2_FETCHMODE_ASSOC);
            $sql_array = array(
                "Ticket_ID"       => array("2", checkinput_sql($row_ticket['ticket_id'], 19)),
                "CODE"            => array("2", ""),
                "Customer_ID"     => array("2", checkinput_sql($row_order['Customer_ID'], 50)),
                "price"           => array("2", checkinput_sql($row_ticket['price'], 5)),
                "date_start"      => array("2", checkinput_sql($row_ticket['date_start'], 50)),
                "date_end"        => array("2", checkinput_sql($row_ticket['date_end'], 50)),
                "status"          => array("2", 1),
                "create_datetime" => array("2", checkinput_sql($create_datetime, 50)),
            );
            $sql_cmd = insert("ticket_list", $sql_array);
            $db->query($sql_cmd);
        }*/
		
		//  11/30之前的訂單加到累積B，之後的加到累積A
		if(strtotime($row_order['create_datetime']) < strtotime(date('Y').'-11-30')){
			$sql_cmd = "update `customer` set order_count = (order_count + 1), year_consumption = (year_consumption + ".$row_order['total_price']."),total_consumption = (total_consumption + ".$row_order['total_price'].") where Customer_ID = '".$row_order['Customer_ID']."'";
			$db->query($sql_cmd);
		}else{
			$sql_cmd = "update `customer` set order_count = (order_count + 1), year_consumption_b = (year_consumption_b + ".$row_order['total_price']."),total_consumption = (total_consumption + ".$row_order['total_price'].") where Customer_ID = '".$row_order['Customer_ID']."'";
			$db->query($sql_cmd);

		}
		$sql_cmd = "update `order` set is_finish = '1' where Order_ID = '".$row_order['Order_ID']."'";
        $db->query($sql_cmd);
		
		//會員升等
		$sql_cmd = "SELECT o.Customer_ID, 
						count(o.Customer_ID) as total_order, 
						sum(o.total_price) as  total_price,
						c.level
					from `order` as o 
					join customer as c
						on o.Customer_ID = c.Customer_ID
					WHERE 
						o.create_datetime between '".(date("Y")-1)."-12-01'
							and '".(date("Y"))."-11-30'
						and o.status in (7,8) 
					group by o.Customer_ID";
		$rs = $db->query($sql_cmd);
		while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
			$new_level = get_new_level($row['level'], $row['total_order'], $row['total_price']);
			if($new_level>$row['level']){
				update_customer_level($row['Customer_ID'], $new_level);
                level_up_gift($new_level, $row['Customer_ID']);
			}
		}


    }
	
    add_log('訂單管理','2');

    $db->disconnect();
    //js_repl_global( "./detail.php?Order_ID=".$Order_ID, "EDIT_SUCCESS");
    js_repl_global( "./list.php", "EDIT_SUCCESS");
    exit;
}


function get_new_level($level, $order, $total_price) {
	if($total_price >= '4500') {
		$new_level = 3;
	}
	else if($total_price >= '1500') {
		$new_level = 2;
	}
	else if($order > 0) {
		$new_level = 1;
	}
	return max($level,$new_level);
}

function update_customer_level($Customer_ID, $level) {
	global $db;
	if($level>=2)
		$sql_cmd = "update customer set level = '".$level."',level_sdate = '".date("Y-m-d H:i:s")."', `level_edate` = '".(date("Y")+1)."-12-31 23:59:59' where Customer_ID = '".$Customer_ID."'";
	else
		$sql_cmd = "update customer set level = '".$level."',level_sdate = '".date("Y-m-d H:i:s")."', `level_edate` = '' where Customer_ID = '".$Customer_ID."'";

	$db->query($sql_cmd);
}
