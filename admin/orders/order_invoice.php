<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("orders");
    $Order_ID = filter_input(INPUT_GET, 'id', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    $sql_cmd = "select * from `order` where Order_ID in ('". implode("','", $Order_ID) ."')";
    $rs = $db->query($sql_cmd);
    $str = [];
    $order = [];
    while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
        /*
        解析雜誌(商品系列名稱屬於 常春藤雜誌系列-解析) => 不開發票
        生活雜誌(商品系列名稱屬於 常春藤雜誌系列-生活) => 開免稅發票
        其他 => 開含稅發票
        */
        $url = "http://zipcode.mosky.tw/api/find?address=".urlencode($row['Delivery_City'].$row['Delivery_Area'].$row['Delivery_Addr']);
        $zipcode = json_decode(curling($url))->result;

        $sql_cmd = "select sum(oi.Total_Price) as sum_price,(case
                                when c.Category_Name = '常春藤雜誌系列-解析' then 1
                                when c.Category_Name = '常春藤雜誌系列-生活' then 2
                                else 3
                            end) as tax from order_item as oi
                    join goods as g 
                        on oi.goodsid = g.Goods_ID
                    join category as c
                        on g.sub_category = c.Category_Code
                    where oi.Order_ID = '".$row['Order_ID']."' 
                    group by (case
                                when c.Category_Name = '常春藤雜誌系列-解析' then 1
                                when c.Category_Name = '常春藤雜誌系列-生活' then 2
                                else 3
                            end)";
        $rs_tax = $db->query($sql_cmd);
        while($row_tax = $rs_tax->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $price = 0;
            $price_tax = 0;
            switch ($row_tax['tax']) {
                case '1':
                    $price = $row_tax['sum_price'];
                    $price_tax += $row_tax['sum_price'];
                    $number = "01500046";
                    $company = "普及美語雜誌社";
                    break;
                case '2':
                    $price = $row_tax['sum_price'];
                    $price_tax += $row_tax['sum_price'];
                    $number = "01500046";
                    $company = "普及美語雜誌社";
                    break;
                case '3':
                    $price = $row['total_price'] - $price_tax;
                    $number = "86145119";
                    $company = "常春藤有聲出版有限公司";
                    break;
            }
            if($row_tax['tax'] != 1 ) {
                $data = [
                    "M",    //主檔代號
                    $row['Order_ID'],   //訂單編號
                    "0",    //訂單狀態
                    date("Y/m/d", strtotime($row['create_datetime'])),    //訂單日期
                    date("Y/m/d", strtotime("+1 days")),    //預計出貨日
                    "0",    //稅率別
                    "",    //訂單金額(未稅)
                    "",    //訂單稅額
                    $price,    //訂單金額(含稅)
                    $number,    //賣方統一編號
                    $company,    //賣方廠編
                    $row['i_tax_id_number'],    //買方統一編號
                    $row['i_title'],    //買受人公司名稱
                    $row['Customer_ID'],    //會員編號
                    $row['name'],    //會員姓名
                    $zipcode,    //會員郵遞區號
                    $row['Delivery_City'].$row['Delivery_Area'].$row['Delivery_Addr'],    //會員地址
                    $row['Delivery_Phone'],    //會員電話
                    $row['Delivery_Mobile'],    //會員行動電話
                    $row['email'],    //會員電子郵件
                    "0",    //紅利點數折扣金額
                    "N",    //索取紙本發票
                    "",    //發票捐贈註記
                    "",    //訂單註記
                    "",    //付款方式
                    $row['Order_ID'],    //相關號碼 1(出貨單號)
                    "",    //相關號碼 2
                    "",    //相關號碼 3
                    "商品一批",    //主檔備註
                ];
                $str[$number][] = implode("|", $data);
                $order[$number][] = $row['Order_ID'];
                
                //$test=implode("|", $data);
                //echo $test;
            }
        }
        $str[$number][] = count($str);
    }
    $file_list = [];
    foreach ($str as $key => $value) {
        $content = implode("\r\n", $value);
        $filename = $key.'-O-'.date("Ymd-His").'.txt';
        $fp=fopen('./invoice_file/'.$filename,"w");
        fwrite($fp,$content);
        $file_list[] = DOCUMENT_ROOT.'/admin/orders/invoice_file/'.$filename;
    }

    // 建立 Archive_Zip 物件，及定義檔案名稱
    $archive_file_name = "invoice_".date("Ymd").".zip";
    $zipfile = New Archive_Zip($archive_file_name);

    // 建立 zip 檔案
    $p_params = array('remove_path' => DOCUMENT_ROOT.'/admin/orders/invoice_file/');
    $zipfile->create($file_list,$p_params);
    
    header("Content-type: application/zip"); 
    header("Content-Disposition: attachment; filename=$archive_file_name"); 
    header("Pragma: no-cache"); 
    header("Expires: 0"); 
    readfile($archive_file_name);
    foreach($file_list as $file) {
        unlink($file);
    }
    unlink($archive_file_name);

?>