<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("orders");

$type = filter_input(INPUT_GET, 'type');
if($type == "pick") {
    $Order_ID = filter_input(INPUT_GET, 'id', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

    $sql_cmd = "select o.*, c.level, c.Customer_Mail from `order` as o 
                join customer as c
                    on c.Customer_ID = o.Customer_ID
                    where o.Order_ID in ('". implode("','", $Order_ID) ."')";
    $rs = $db->query($sql_cmd);

    $ClassPDF = new ClassPDF();
    while ($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
        $sql_cmd = "select g.*,oi.Goods_Price, oi.Goods_Qty, oi.Total_Price, oi.remark,oi.gift_item_number from order_item as oi 
                    join goods as g 
                        on g.Goods_ID = oi.goodsid 
                    where oi.Order_ID = '".$row['Order_ID']."'";
        $rs_item = $db->query($sql_cmd);
        $row['item'] = [];
        while ($row_item = $rs_item->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            switch ($row_item['type']) {
                case '2':
                    $item_group = explode(",", $row_item['item_group']);
                    $sql_cmd = "select * from goods where Goods_ID in('".implode("','", $item_group)."')";
                    $rs_item_group = $db->query($sql_cmd);
                    $row_item['item_group'] = [];
                    while($row_item_group = $rs_item_group->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                        $row_item['item_group'][] = $row_item_group;
                    }
                    break;
                case '3':
                    $sql_cmd = "select * from goods_item where Goods_ID = '".$row_item['Goods_Item_ID']."'";
                    $rs_item_group = $db->query($sql_cmd);
                    $row_item['goods_item'] = [];
                    while($row_item_group = $rs_item_group->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                        $row_item['goods_item'][] = $row_item_group;
                    }
                    break;
            }
            if(!empty($row_item['gift_item_number'])) {
                $sql_cmd = "select * from gift where item_number = '".$row_item['gift_item_number']."'";
                $rs_gift = $db->query($sql_cmd);
                $row_gift = $rs_gift->fetchRow(MDB2_FETCHMODE_ASSOC);
                $row_item['gift_item'] = $row_gift;
            }
            $row['item'][] = $row_item;
        }
        $row["level"] = $ARRall['level'][$row['level']];
        $data[] = $row;
    }

    $ClassPDF->create_order_list($data);
    
}
