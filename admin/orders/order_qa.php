<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("orders");
$sql_where = "";
$order_by = " order by create_datetime desc ";
$Excel = false;
$order_id = $_GET['order_id'];

if(empty($order_id)) {
    js_go_back_global("NOT_POST");
    exit;
}
$sql_cmd = "select * from `order` where Order_ID = '".checkinput_sql($order_id, 19)."'";
$rs = $db->query($sql_cmd);
$row_order =  $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
$sql_cmd = "select * from order_qa where order_id = '".checkinput_sql($order_id, 19)."'";
$rs = $db->query($sql_cmd);
// while ($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {


?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php"); ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                訂單問答管理
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box">
                <div class="box-header">
                </div>
                <div class="box-body no-padding">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form class="form-horizontal" method="POST" action="./order_qa_edit.php" enctype="multipart/form-data">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">訂單編號</label>
                                <div class="col-sm-10 control-text">
                                    <?=$row_order['Order_ID']?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">會員ID</label>
                                <div class="col-sm-10 control-text">
                                    <?=$row_order['Customer_ID']?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">購買人姓名</label>
                                <div class="col-sm-10 control-text">
                                    <?=$row_order['name']?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">購買人聯絡Email</label>
                                <div class="col-sm-10 control-text">
                                    <?=$row_order['email']?>
                                </div>
                            </div>
                            <?php 
                                while ($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                            ?>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"><?=($row['type'] == 1)?"提出內容":"回覆紀錄"?></label>
                                <div class="col-sm-10 control-text" style="white-space: pre-line;"><?=$row['content']?></div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"><?=($row['type'] == 1)?"提出日期":"回覆日期"?></label>
                                <div class="col-sm-10 control-text">
                                    <?=$row['create_datetime']?>
                                </div>
                            </div>
                        <?php } ?>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">回覆內容</label>
                                <div class="col-sm-10">
                                    <textarea name="content" class="form-control"></textarea>
                                    <input type="hidden" name="Order_ID" value="<?=$row_order['Order_ID']?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"></label>
                                <div class="col-sm-10">
                                    <button type="submit" class="btn btn-primary">送出</button>
                                    <button type="button" class="btn btn-info" onclick="history.back()">回上一頁</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>
</body>
</html>