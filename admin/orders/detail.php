<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("orders");
$select2_json = "";
$action = "edit";
$Order_ID = filter_input(INPUT_GET, 'Order_ID');
if(empty($Order_ID)) {
    js_go_back_global("NOT_POST");
    exit;
}
$sql_cmd = "select o.*,m.Customer_Mail, level from `order` as o join customer as m on o.Customer_ID = m.Customer_ID
         where o.deleted_at is null and o.Order_ID = '".checkinput_sql($Order_ID,19)."'";
$rs = $db->query($sql_cmd);
$row_order = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);

$sql_cmd = "SELECT OI.*,GI.Name as Goods_Item_Name,G.Goods_Name, G.item_group, G.type from order_item as OI
            left join goods as G
            on G.Goods_ID = OI.goodsId
            left join goods_item as GI
            on OI.Goods_Item_ID = GI.Goods_Item_ID
             where OI.Order_ID = '".$Order_ID."'";
$rs_order_item = $db->query($sql_cmd);

?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php"); ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                訂單管理
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <!-- form start -->
            <form class="form-horizontal" method="POST" action="./<?=$action?>.php">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">訂單編號</label>
                            <div class="col-sm-10 control-text">
                                <?=$row_order['Order_ID']?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">訂單日期</label>
                            <div class="col-sm-10 control-text">
                                <?=$row_order['create_datetime']?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">商品明細列表</h3>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-sm-12">
                              
								<table class="table table-striped">
									<tbody>
									<tr>
										<th></th>
										<th>貨號</th>
										<th>商品名稱</th>
										<th>商品備註</th>
										<th width="100px">原價</th>
										<th width="100px">優惠價</th>
										<th width="100px">數量</th>
										<th width="100px">小計</th>
										<th>達成活動</th>
										<th>商品狀態</th>
									</tr>
									
									<?php 
										$i=1;
										while($row_order_item = $rs_order_item->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                                            if(!empty($row_order_item['Magazine_item_number']))
                                                $row_order_item['Goods_ID'] .= " - ".$row_order_item['Magazine_item_number'];
									?>
									<tr>
										<th>
										<?php 
										echo $i; 
										$name = [];
										$event = json_decode($row_order_item['event'], true);
										for($c=0;$c<count($event);$c++) {
											$name[]=$event[$c]['name'];
                                        }
										$event_name = implode(",",$name);
                                        if($row_order_item['is_additional'] == 1 && strpos($row_order_item['remark'], "加購品") === false) {
                                            $row_order_item['remark'] .= "加購品";
                                        }
                                        if($row_order_item['type'] == 3 && strpos($row_order_item['remark'], "雜誌期數") === false) {
                                            $row_order_item['remark'] .= $row_order_item['Magazine_spec']."-".$row_order_item['Magazine_Name']."期，起始月份：".$row_order_item['starting_months']."，雜誌期數：".$row_order_item['start_periods']."期";
                                        }
										?>
										</th>
										<th><input id="Goods_ID" name="Goods_ID[<?=$row_order_item['Order_Item_ID']?>]" type="text" class="form-control" placeholder="" value="<?=$row_order_item['Goods_ID']?>"></th>
										<th><input id="Goods_Name" name="Goods_Name[<?=$row_order_item['Order_Item_ID']?>]" type="text" class="form-control" placeholder="" value="<?=$row_order_item['Goods_Name']?>"></th>
										<th><input id="remark" name="remark[<?=$row_order_item['Order_Item_ID']?>]" type="text" class="form-control" placeholder="" value="<?=$row_order_item['remark']?>"></th>
										<th><input id="Goods_Price" name="Goods_Price[<?=$row_order_item['Order_Item_ID']?>]" type="text" class="form-control" placeholder="" value="<?=$row_order_item['Goods_Price']?>"></th>
										<th><input id="Goods_Discount_Price" name="Goods_Discount_Price[<?=$row_order_item['Order_Item_ID']?>]" type="text" class="form-control" placeholder="" value="<?=$row_order_item['Goods_Discount_Price']?>"></th>
										<th><input id="Goods_Qty" name="Goods_Qty[<?=$row_order_item['Order_Item_ID']?>]" type="text" class="form-control" placeholder="" value="<?=$row_order_item['Goods_Qty']?>"></th>
										<th><input id="Total_Price" name="Total_Price[<?=$row_order_item['Order_Item_ID']?>]" type="text" class="form-control" placeholder="" value="<?=$row_order_item['Total_Price']?>"></th>
										<th><input id="event" name="event_name[<?=$row_order_item['Order_Item_ID']?>]" type="text" class="form-control" placeholder="" value="<?=$event_name?>">
										<input id="event" name="event[<?=$row_order_item['Order_Item_ID']?>]" type="hidden" class="form-control" placeholder="" value='<?=$row_order_item['event']?>'></th>
										<th>
											<select name="order_item_status[<?=$row_order_item['Order_Item_ID']?>]" id="order_item_status" class="form-control input-sm">
                                                <?php foreach($ARRall['order_item_status'] as $key => $value) {
                                                ?>
                                                <option value="<?=$key?>" <?=($row_order_item['Status'] == $key)?"selected":""?>><?=$value?></option>

                                                <?php
                                                }
                                                ?>
                                            </select>
										</th>
									</tr>
                                    <?php
    										$i++;
                                            if(!empty($row_order_item['item_group'])) {
                                                $item_group = explode(",", $row_order_item['item_group']);
                                                $sql_cmd = "select * from goods where Goods_ID in ('".implode("','", $item_group)."')";
                                                $rs_group = $db->query($sql_cmd);
                                                while($row_group = $rs_group->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                                    ?>
                                    <tr>
                                        <th>
                                        </th>
                                        <th><input readonly id="Goods_ID" name="" type="text" class="form-control" placeholder="" value="<?=$row_group['Goods_ID']?>"></th>
                                        <th><input readonly id="Goods_Name" name="" type="text" class="form-control" placeholder="" value="<?=$row_group['Goods_Name']?>"></th>
                                        <th><input readonly id="remark" name="" type="text" class="form-control" placeholder="" value="套書"></th>
                                        <th><input readonly id="Goods_Price" name="" type="text" class="form-control" placeholder="" value="0"></th>
                                        <th><input readonly id="Goods_Discount_Price" name="Goods_Discount_Price[<?=$row_order_item['Order_Item_ID']?>]" type="text" class="form-control" placeholder="" value="0"></th>
                                        <th><input readonly id="Goods_Qty" name="" type="text" class="form-control" placeholder="" value="1"></th>
                                        <th><input readonly id="Total_Price" name="" type="text" class="form-control" placeholder="" value="0"></th>
                                        <th></th>
                                        <th>
                                        </th>
                                    </tr>                                    
                                    <?php 
                                                }
                                            }
                                            foreach ($event as $key => $value) {
                                                $sql_cmd = "select g.*
                                                 from goods_event as e
                                                 join gift as g on e.gift = g.item_number
                                                  where e.id = '".$value['id']."'";
                                                $rs_gift = $db->query($sql_cmd);
                                                while($row_gift = $rs_gift->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                                    ?>
                                    <tr>
                                        <th>
                                        </th>
                                        <th><input readonly id="" name="" type="text" class="form-control" placeholder="" value="<?=$row_gift['item_number']?>"></th>
                                        <th><input readonly id="" name="" type="text" class="form-control" placeholder="" value="<?=$row_gift['name']?>"></th>
                                        <th><input readonly id="" name="" type="text" class="form-control" placeholder="" value="贈品"></th>
                                        <th></th>
                                        <th></th>
                                        <th><input readonly id="" name="" type="text" class="form-control" placeholder="" value="<?=$row_order_item['gift_number']?>"></th>
                                        <th></th>
                                        <th><input readonly id="event" name="event[<?=$row_order_item['Order_Item_ID']?>]" type="text" class="form-control" placeholder="" value="<?=$event_name?>"></th>
                                        <th>
                                        </th>
                                    </tr>                                    
                                    <?php 
                                                }
                                            }
                                            if(!empty($row_order_item['gift_item_number'])) {
                                                $sql_cmd = "select * from gift where item_number = '".$row_order_item['gift_item_number']."'";
                                                $rs_gift = $db->query($sql_cmd);
                                                while($row_gift = $rs_gift->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                                    ?>
                                    <tr>
                                        <th>
                                        </th>
                                        <th><input readonly id="" name="" type="text" class="form-control" placeholder="" value="<?=$row_gift['item_number']?>"></th>
                                        <th><input readonly id="" name="" type="text" class="form-control" placeholder="" value="<?=$row_gift['name']?>"></th>
                                        <th><input readonly id="" name="" type="text" class="form-control" placeholder="" value="贈品"></th>
                                        <th></th>
                                        <th></th>
                                        <th><input readonly id="" name="" type="text" class="form-control" placeholder="" value="<?=$row_order_item['gift_number']?>"></th>
                                        <th></th>
                                        <th><input readonly id="event" name="event[<?=$row_order_item['Order_Item_ID']?>]" type="text" class="form-control" placeholder="" value="<?=$event_name?>"></th>
                                        <th>
                                        </th>
                                    </tr>                                    
                                    <?php 
                                                }
                                            }
                                            if($row_order_item['registered_use'] == 1) {

                                    ?>
                                    <tr>
                                        <th>
                                        </th>
                                        <th><input readonly id="" name="" type="text" class="form-control" placeholder="" value=""></th>
                                        <th><input readonly id="" name="" type="text" class="form-control" placeholder="" value="掛號費"></th>
                                        <th><input readonly id="" name="" type="text" class="form-control" placeholder="" value="<?=$row_order_item['Magazine_Name']?>期"></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th><input readonly id="Total_Price" name="Total_Price[<?=$row_order_item['Order_Item_ID']?>]" type="text" class="form-control" placeholder="" value="<?=$row_order_item['registered_total_Price']?>"></th>
                                        <th></th>
                                        <th>
                                        </th>
                                    </tr>                                    
                                    <?php 
                                                }
										} 
									?>
									</tbody>
									
								</table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">金額資訊</h3>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">購買小計</label>
                            <div class="col-sm-10">
                                <input id="price" name="price" type="text" class="form-control" placeholder=""
                                       value="<?=$row_order['price']?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">折價券折抵</label>
                            <div class="col-sm-10">
                                <input id="discount_ticket" name="discount_ticket" type="text" class="form-control" placeholder=""
                                       value="<?=$row_order['discount_ticket']?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">折價券名稱</label>
                            <div class="col-sm-10">
                                <input id="discount_ticket_name" name="discount_ticket_name" type="text" class="form-control" placeholder=""
                                       value="<?=$row_order['discount_ticket_name']?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">熊贈點折抵</label>
                            <div class="col-sm-10">
                                <input id="discount_point" name="discount_point" type="text" class="form-control" placeholder=""
                                       value="<?=$row_order['discount_point']?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">會員再折扣</label>
                            <div class="col-sm-10">
                                <input id="discount_customer" name="discount_customer" type="text" class="form-control" placeholder=""
                                       value="<?=$row_order['discount_customer']?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">會員再折扣活動名稱</label>
                            <div class="col-sm-10">
                                <input id="discount_customer_name" name="discount_customer_name" type="text" class="form-control" placeholder=""
                                       value="<?=$row_order['discount_customer_name']?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">滿額再折扣</label>
                            <div class="col-sm-10">
                                <input id="discount_purchase" name="discount_purchase" type="text" class="form-control" placeholder=""
                                       value="<?=$row_order['discount_purchase']?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">滿額再折扣活動名稱</label>
                            <div class="col-sm-10">
                                <input id="discount_purchase_name" name="discount_purchase_name" type="text" class="form-control" placeholder=""
                                       value="<?=$row_order['discount_purchase_name']?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">滿額折抵</label>
                            <div class="col-sm-10">
                                <input id="discount_off" name="discount_off" type="text" class="form-control" placeholder=""
                                       value="<?=$row_order['discount_off']?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">滿額折抵扣活動名稱</label>
                            <div class="col-sm-10">
                                <input id="discount_off_name" name="discount_off_name" type="text" class="form-control" placeholder=""
                                       value="<?=$row_order['discount_off_name']?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">運費</label>
                            <div class="col-sm-10">
                                <input id="Delivery_Price" name="Delivery_Price" type="text" class="form-control" placeholder=""
                                       value="<?=$row_order['Delivery_Price']?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">訂單總金額</label>
                            <div class="col-sm-10">
                                <input id="total_price" name="total_price" type="text" class="form-control" placeholder=""
                                       value="<?=$row_order['total_price']?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">可獲得熊贈點</label>
                            <div class="col-sm-10">
                                <input id="point_get" name="point_get" type="text" class="form-control" placeholder=""
                                       value="<?=$row_order['point_get']?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">訂單處理</h3>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">訂單狀態</label>
                            <div class="col-sm-10">
                                <select name="Status" id="Status" class="form-control input-sm">
                                    <?php foreach($ARRall['order_status'] as $key => $value) {
                                    ?>
                                    <option value="<?=$key?>" <?=($row_order['Status'] == $key)?"selected":""?> <?=($row_order['Status'] == 8)?"disabled":""?>><?=$value?></option>

                                    <?php
                                    }
                                    ?>
                                </select>
								<?php 
									if($row_order['Status'] == 8){
								?>
									<input type="hidden" name="Status" value="8">
								<?php
									}
								?>
                            </div>
                        </div>
						<div class="form-group">
                            <label class="col-sm-2 control-label">收貨日期</label>
                            <div class="col-sm-10">
                                <input type="text" name="delivery_at" class="form-control input-sm datetimepicker"
                                           value="<?=(!empty($row_order['delivery_at']))?$row_order['delivery_at']:""?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">退貨原因</label>
                            <div class="col-sm-10 control-text" style="white-space: pre-line;"><?=$row_order['cancel_reason']?></div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">取消或退貨原因</label>
                            <div class="col-sm-10">
                                <textarea name="cancel_reason" class="form-control"><?=$row_order['cancel_remark']?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">管理人員備註</label>
                            <div class="col-sm-10">
                                <input id="Remark" name="Remark" type="text" class="form-control" placeholder=""
                                       value="<?=$row_order['Remark']?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">訂單明細</h3>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">會員ID</label>
                            <div class="col-sm-10 control-text">
                                <?=$row_order['Customer_ID']?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">會員帳號</label>
                            <div class="col-sm-10 control-text">
                                <?=$row_order['Customer_Mail']?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">會員等級</label>
                            <div class="col-sm-10 control-text">
                                <?=$ARRall['level'][$row_order['Customer_level']]?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">購買人姓名</label>
                            <div class="col-sm-10">
                                <input id="name" name="name" type="text" class="form-control" placeholder=""
                                       value="<?=$row_order['name']?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">購買人聯絡Email</label>
                            <div class="col-sm-10">
                                <input id="email" name="email" type="text" class="form-control" placeholder=""
                                       value="<?=$row_order['email']?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">購買人電話</label>
                            <div class="col-sm-10">
                                <input id="phone" name="phone" type="text" class="form-control" placeholder=""
                                       value="<?=$row_order['phone']?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">購買人地址</label>
                            <div class="col-sm-10">
                                <div id="twzipcode">
                                    <div data-role="zipcode"
                                         data-name="zipcode"
                                         data-value=""
                                         data-style="zipcode-style form-control" class="col-sm-2 row">
                                    </div>
                                    <div data-role="county"
                                         data-name="city"
                                         data-value="<?=$row_order['city']?>"
                                         data-style="city-style form-control" class="col-sm-2 row">
                                    </div>
                                    <div data-role="district"
                                         data-name="area"
                                         data-value="<?=$row_order['area']?>"
                                         data-style="district-style form-control" class="col-sm-2 row">
                                    </div>
                                    <div class="col-sm-7 row">
                                        <input name="address" type="text" class="form-control"  value="<?=$row_order['address']?>"/>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">付款方式</label>
                            <div class="col-sm-10">
                                <select name="Payment" id="Payment" class="form-control input-sm">
                                    <?php foreach($ARRall['payment'] as $key => $value) {
                                    ?>
                                    <option value="<?=$key?>" <?=($row_order['Payment'] == $key)?"selected":""?>><?=$value?></option>

                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">收件人</label>
                            <div class="col-sm-10">
                                <input id="name" name="Delivery_Name" type="text" class="form-control" placeholder=""
                                       value="<?=$row_order['Delivery_Name']?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">收件人手機</label>
                            <div class="col-sm-10">
                                <input id="email" name="Delivery_Mobile" type="text" class="form-control" placeholder=""
                                       value="<?=$row_order['Delivery_Mobile']?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">收件人市話</label>
                            <div class="col-sm-10">
                                <input id="phone" name="Delivery_Phone" type="text" class="form-control" placeholder=""
                                       value="<?=$row_order['Delivery_Phone']?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">收件人地址</label>
                            <div class="col-sm-10">
                                <div id="twzipcode2">
                                    <div data-role="zipcode"
                                         data-name="zipcode"
                                         data-value=""
                                         data-style="zipcode-style form-control" class="col-sm-2 row">
                                    </div>
                                    <div data-role="county"
                                         data-name="Delivery_City"
                                         data-value="<?=$row_order['Delivery_City']?>"
                                         data-style="city-style form-control" class="col-sm-2 row">
                                    </div>
                                    <div data-role="district"
                                         data-name="Delivery_Area"
                                         data-value="<?=$row_order['Delivery_Area']?>"
                                         data-style="district-style form-control" class="col-sm-2 row">
                                    </div>
                                    <div class="col-sm-7 row">
                                        <input name="Delivery_Addr" type="text" class="form-control"  value="<?=$row_order['Delivery_Addr']?>"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">訂單備註</label>
                            <div class="col-sm-10 control-text" style="white-space: pre-line;"><?=$row_order['Note']?></div>
                        </div>
                    </div>
                </div>
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">物流資訊</h3>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">配送方式</label>
                            <div class="col-sm-10">
                                <select name="delivery" id="delivery" class="form-control input-sm">
                                    <?php foreach($ARRall['delivery'] as $key => $value) {
                                    ?>
                                    <option value="<?=$key?>" <?=($row_order['delivery'] == $key)?"selected":""?>><?=$value?></option>

                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">門市名稱</label>
                            <div class="col-sm-10">
                                <input id="store_name" name="store_name" type="text" class="form-control" placeholder=""
                                       value="<?=$row_order['store_name']?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">門市地址</label>
                            <div class="col-sm-10">
                                <input id="store_addr" name="store_addr" type="text" class="form-control" placeholder=""
                                       value="<?=$row_order['store_addr']?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">希望收件時段</label>
                            <div class="col-sm-10">
                                <select name="Delivery_time" id="Delivery_time" class="form-control input-sm">
                                    <?php foreach($ARRall['delivery_time'] as $key => $value) {
                                    ?>
                                    <option value="<?=$key?>" <?=($row_order['Delivery_time'] == $key)?"selected":""?>><?=$value?></option>

                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">物流名稱</label>
                            <div class="col-sm-10">
                                <input id="Delivery_No" name="Delivery_Company" type="text" class="form-control" placeholder=""
                                       value="<?=$row_order['Delivery_Company']?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">配送編號</label>
                            <div class="col-sm-10">
                                <input id="Delivery_No" name="Delivery_No" type="text" class="form-control" placeholder=""
                                       value="<?=$row_order['Delivery_No']?>">
                            </div>
                        </div>
                        <!--<div class="form-group">
                            <label class="col-sm-2 control-label">管理人員備註</label>
                            <div class="col-sm-10">
                                <textarea name="delivery_remark" class="form-control"><?//=$row_order['delivery_remark']?></textarea>
                            </div>
                        </div>-->
                    </div>
                </div>
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">發票資訊</h3>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">發票類型</label>
                            <div class="col-sm-10">
                                <select name="i_type" id="i_type" class="form-control input-sm">
                                    <?php foreach($ARRall['i_type'] as $key => $value) {
                                    ?>
                                    <option value="<?=$key?>" <?=($row_order['i_type'] == $key)?"selected":""?>><?=$value?></option>

                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">發票抬頭</label>
                            <div class="col-sm-10">
                                <input id="i_title" name="i_title" type="text" class="form-control" placeholder=""
                                       value="<?=$row_order['i_title']?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">統一編號</label>
                            <div class="col-sm-10">
                                <input id="i_tax_id_number" name="i_tax_id_number" type="text" class="form-control" placeholder=""
                                       value="<?=$row_order['i_tax_id_number']?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">發票地址</label>
                            <div class="col-sm-10">
                                <input id="i_address" name="i_address" type="text" class="form-control" placeholder=""
                                       value="<?=$row_order['i_address']?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">發票開立狀態</label>
                            <div class="col-sm-10">
                                <select name="i_status" id="i_status" class="form-control input-sm">
                                    <?php foreach($ARRall['i_status'] as $key => $value) {
                                    ?>
                                    <option value="<?=$key?>" <?=($row_order['i_status'] == $key)?"selected":""?>><?=$value?></option>

                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">發票號碼</label>
                            <div class="col-sm-10">
                                <input id="i_number" name="i_number" type="text" class="form-control" placeholder=""
                                       value="<?=$row_order['i_number']?>">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box box-primary">
                    <!-- /.box-body -->
                    <div class="box-footer text-right">
                        <input type="hidden" name="Order_ID" value="<?=$row_order['Order_ID']?>">
                        <a href="javascript:history.back()" class="btn btn-danger" style="margin-right: 10px;">取消，並返回上一層</a>
                        <button type="submit" class="btn btn-primary">儲存</button>
                        <input type="hidden" name="referer" value="<?=(!empty($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:"")?>">
                    </div>
                    <!-- /.box-footer -->
                </div>
            </form>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>