<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("orders");
$sql_where = "";
$order_by = " order by create_datetime desc ";
$payment = "";
$delivery = "";
$status = "";
$order_qa = "";
$remark = "";
$level = "";
$s_date  = filter_input(INPUT_GET, 's_date');
$e_date  = filter_input(INPUT_GET, 'e_date');
if(!empty($_GET['Order_ID'])) {
    $sql_where .= " and o.Order_ID like '%".$_GET["Order_ID"]."%'";
}
if(!empty($s_date))
    $sql_where .= " and o.create_datetime >= '".checkinput_sql($s_date." 00:00:00",19)."'";
if(!empty($e_date))
    $sql_where .= " and o.create_datetime <= '".checkinput_sql($e_date." 23:59:59",19)."'";
if(!empty($_GET['order_qa'])) {
    if($_GET['order_qa'] == 1)
        $sql_where .= " and (select count(*) from `order_qa` where Order_ID =o.Order_ID) > 0";
    else
        $sql_where .= " and (select count(*) from `order_qa` where Order_ID =o.Order_ID) = 0";
    $order_qa = $_GET['order_qa'];
}
if(!empty($_GET['item_number'])) {
    $sql_where .= " and (select count(*) from order_item as oi join goods as g on g.Goods_ID = oi.goodsid where oi.Order_ID = o.Order_ID and g.item_number like '%".$_GET['item_number']."%') > 0";
}
if(!empty($_GET['goods_name'])) {
    $sql_where .= " and (select count(*) from order_item as oi join goods as g on g.Goods_ID = oi.goodsid where oi.Order_ID = o.Order_ID and g.Goods_Name like '%".$_GET['goods_name']."%') > 0";
}
if(!empty($_GET['remark'])) {
    if($_GET['remark'] == 1)
        $sql_where .= " and o.Remark != '' and o.Remark is not null";
    else
        $sql_where .= " and (o.Remark = '' or o.Remark is null)";
    $remark = $_GET['remark'];
}
if(!empty($_GET['Customer_ID'])) {
    $sql_where .= " and o.Customer_ID like '%".$_GET["Customer_ID"]."%'";
}
if(!empty($_GET['email'])) {
    $sql_where .= " and m.Customer_Mail like '%".$_GET["email"]."%'";
}
if(!empty($_GET['level']) && $_GET['level'] != 'all') {
    $sql_where .= " and m.level = '".$_GET["level"]."'";
    $level = $_GET['level'];
}
if(!empty($_GET['name'])) {
    $sql_where .= " and o.name like '%".$_GET["name"]."%'";
}
if(!empty($_GET['phone'])) {
    $sql_where .= " and o.phone like '%".$_GET["phone"]."%'";
}
if(!empty($_GET['payment'])) {
    $sql_where .= " and o.Payment like '%".$_GET["payment"]."%'";
    $payment = $_GET['payment'];
}
if(!empty($_GET['Delivery_Name'])) {
    $sql_where .= " and o.Delivery_Name like '%".$_GET["Delivery_Name"]."%'";
}
if(!empty($_GET['Delivery_Mobile'])) {
    $sql_where .= " and o.Delivery_Mobile like '%".$_GET["Delivery_Mobile"]."%'";
}
if(!empty($_GET['delivery'])) {
    $sql_where .= " and o.delivery like '%".$_GET["delivery"]."%'";
    $delivery = $_GET['delivery'];
}
if(!empty($_GET['status'])) {
    $sql_where .= " and o.status = '".$_GET["status"]."'";
    $status = $_GET['status'];
}


//取出總筆數
$sql_cmd = "select count(*) from `order` as o join customer as m on m.Customer_ID = o.Customer_ID where  o.deleted_at is null ".$sql_where." ";
$rs = $db->query($sql_cmd);
$row = $rs->fetchRow();
$total = $row[0];

$mypage = new page;
$pages = $mypage->set_page($total, $PAGE_NUM);
//設定分頁的初始值
if (empty($_GET['num']))
{
    $num = 1;
}else{
    $num = intval($_GET['num']);
}


$sql_cmd = "select o.*,m.Customer_Mail from `order` as o join customer as m on m.Customer_ID = o.Customer_ID where  o.deleted_at is null ".$sql_where.$order_by.$pages[$num];
$rs_order = $db->query($sql_cmd);

$tmp = $_GET;
unset($tmp['num']);
$val = http_build_query($tmp);
$page = $mypage->multi($total , $PAGE_NUM, $num, "./list.php", "", $val);
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php"); ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                訂單管理
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box">
                <form action="./list.php" method="GET" class="form-inline" id="form">
                    <div class="box-header">
                        <div style="overflow: hidden; width:100%;">
                            <div style="float:left; display:inline-block; width:250px; padding-right:15px; padding-bottom:10px;">
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-primary btn-sm">訂單編號</button>
                                    </div>
                                    <input type="text" name="Order_ID" class="form-control input-sm"
                                           placeholder="" value="<?=(!empty($_GET['Order_ID']))?$_GET['Order_ID']:""?>">
                                </div>
                            </div>
                            <div style="float:left; display:inline-block; width:217px;">
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-primary btn-sm">訂單日期</button>
                                    </div>
                                    <input type="text" name="s_date" class="form-control input-sm datetimepicker"
                                           placeholder="起始日" value="<?=(!empty($_GET['s_date']))?$_GET['s_date']:""?>">
                                </div>
                            </div>
                            <div style="float:left; display:inline-block; width:250px; padding-right:15px; padding-bottom:10px;">
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-primary btn-sm">~</button>
                                    </div>
                                    <input type="text" name="e_date" class="form-control input-sm datetimepicker"
                                           placeholder="結束日" value="<?=(!empty($_GET['e_date']))?$_GET['e_date']:""?>">
                                </div>
                            </div>
                            <div style="float:left; display:inline-block; width:250px; padding-right:15px; padding-bottom:10px;">
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-primary btn-sm">訂單問答</button>
                                    </div>
                                    <select class="form-control input-sm" name="order_qa">
                                        <option value="">全部</option>
                                        <option value="1" <?=($order_qa == "1")?"selected":""?>>有</option>
                                        <option value="2" <?=($order_qa == "2")?"selected":""?>>無</option>
                                    </select>
                                </div>
                            </div>
                            <div style="float:left; display:inline-block; width:250px; padding-right:15px; padding-bottom:10px;">
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-primary btn-sm">貨號</button>
                                    </div>
                                    <input type="text" name="item_number" class="form-control input-sm"
                                           placeholder="" value="<?=(!empty($_GET['item_number']))?$_GET['item_number']:""?>">
                                </div>
                            </div>
                            <div style="float:left; display:inline-block; width:250px; padding-right:15px; padding-bottom:10px;">
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-primary btn-sm">商品名稱</button>
                                    </div>
                                    <input type="text" name="goods_name" class="form-control input-sm"
                                           placeholder="" value="<?=(!empty($_GET['goods_name']))?$_GET['goods_name']:""?>">
                                </div>
                            </div>
                            <div style="float:left; display:inline-block; width:250px; padding-right:15px; padding-bottom:10px;">
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-primary btn-sm">管理人員備註</button>
                                    </div>
                                    <select class="form-control input-sm" name="remark">
                                        <option value="">全部</option>
                                        <option value="1" <?=($remark == "1")?"selected":""?>>有</option>
                                        <option value="2" <?=($remark == "2")?"selected":""?>>無</option>
                                    </select>
                                </div>
                            </div>
                            <div style="float:left; display:inline-block; width:250px; padding-right:15px; padding-bottom:10px;">
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-primary btn-sm">會員ID</button>
                                    </div>
                                    <input type="text" name="Customer_ID" class="form-control input-sm"
                                           placeholder="" value="<?=(!empty($_GET['Customer_ID']))?$_GET['Customer_ID']:""?>">
                                </div>
                            </div>
                            <div style="float:left; display:inline-block; width:250px; padding-right:15px; padding-bottom:10px;">
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-primary btn-sm">會員帳號</button>
                                    </div>
                                    <input type="text" name="email" class="form-control input-sm"
                                           placeholder="" value="<?=(!empty($_GET['email']))?$_GET['email']:""?>">
                                </div>
                            </div>
                            <div style="float:left; display:inline-block; width:250px; padding-right:15px; padding-bottom:10px;">
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-primary btn-sm">會員等級</button>
                                    </div>
                                    <select class="form-control input-sm" name="level">
                                        <option value="all">全部</option>
                                        <option value="0" <?=($level == "0")?"selected":""?>>種子會員</option>
                                        <option value="1" <?=($level == "1")?"selected":""?>>幼苗會員</option>
                                        <option value="2" <?=($level == "2")?"selected":""?>>大樹會員</option>
                                        <option value="3" <?=($level == "3")?"selected":""?>>神木會員</option>
                                    </select>
                                </div>
                            </div>
                            <div style="float:left; display:inline-block; width:250px; padding-right:15px; padding-bottom:10px;">
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-primary btn-sm">購買人姓名</button>
                                    </div>
                                    <input type="text" name="name" class="form-control input-sm"
                                           placeholder="" value="<?=(!empty($_GET['name']))?$_GET['name']:""?>">
                                </div>
                            </div>
                            <div style="float:left; display:inline-block; width:250px; padding-right:15px; padding-bottom:10px;">
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-primary btn-sm">購買人電話</button>
                                    </div>
                                    <input type="text" name="phone" class="form-control input-sm"
                                           placeholder="" value="<?=(!empty($_GET['phone']))?$_GET['phone']:""?>">
                                </div>
                            </div>
                            <div style="float:left; display:inline-block; width:250px; padding-right:15px; padding-bottom:10px;">
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-primary btn-sm">付款方式</button>
                                    </div>
                                    <select class="form-control input-sm" name="payment">
                                        <option value="0">全部</option>
                                        <?php foreach ($ARRall['payment'] as $key => $value):?>
                                            <option value="<?=$key?>" <?=($payment == $key)?"selected":""?>><?=$value?></option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                            <div style="float:left; display:inline-block; width:250px; padding-right:15px; padding-bottom:10px;">
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-primary btn-sm">收件人</button>
                                    </div>
                                    <input type="text" name="Delivery_Name" class="form-control input-sm"
                                           placeholder="" value="<?=(!empty($_GET['Delivery_Name']))?$_GET['Delivery_Name']:""?>">
                                </div>
                            </div>
                            <div style="float:left; display:inline-block; width:250px; padding-right:15px; padding-bottom:10px;">
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-primary btn-sm">收件人手機</button>
                                    </div>
                                    <input type="text" name="Delivery_Mobile" class="form-control input-sm"
                                           placeholder="" value="<?=(!empty($_GET['Delivery_Mobile']))?$_GET['Delivery_Mobile']:""?>">
                                </div>
                            </div>
							<div style="float:left; display:inline-block; width:250px; padding-right:15px; padding-bottom:10px;">
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-primary btn-sm">配送方式</button>
                                    </div>
                                    <select class="form-control input-sm" name="delivery">
                                        <option value="0">全部</option>
                                        <option value="1" <?=($delivery == "1")?"selected":""?>>超商取貨：7-11</option>
                                        <option value="2" <?=($delivery == "2")?"selected":""?>>超商取貨：全家、OK、萊爾富</option>
                                        <option value="3" <?=($delivery == "3")?"selected":""?>>物流配送</option>
                                    </select>
                                </div>
                            </div>
							<div style="float:left; display:inline-block; width:250px; padding-right:15px; padding-bottom:10px;">
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-primary btn-sm">訂單狀態</button>
                                    </div>
                                    <select class="form-control input-sm" name="status">
                                        <option value="0">全部</option>
                                        <option value="1" <?=($status == "1")?"selected":""?>>等待處理</option>
                                        <option value="2" <?=($status == "2")?"selected":""?>>已確認付款，待商品出貨</option>
                                        <option value="3" <?=($status == "3")?"selected":""?>>出貨中</option>
                                        <option value="4" <?=($status == "4")?"selected":""?>>已出貨</option>
                                        <option value="5" <?=($status == "5")?"selected":""?>>已到店</option>
                                        <option value="6" <?=($status == "6")?"selected":""?>>自動取消訂單</option>
                                        <option value="7" <?=($status == "7")?"selected":""?>>已取貨，待產生發票</option>
                                        <option value="8" <?=($status == "8")?"selected":""?>>訂單完成，發票已產生</option>
                                        <option value="9" <?=($status == "9")?"selected":""?>>會員已取消</option>
                                        <option value="10" <?=($status == "10")?"selected":""?>>金流交易中斷</option>
                                        <option value="11" <?=($status == "11")?"selected":""?>>系統已取消</option>
                                        <option value="12" <?=($status == "12")?"selected":""?>>申請退貨中</option>
                                        <option value="13" <?=($status == "13")?"selected":""?>>已完成退貨</option>
                                    </select>
                                </div>
                            </div>
							<button type="button" class="btn btn-success btn-sm pull-right" onclick="$('form').attr('action','./list.php');$('form').submit();">送出</button>
							<a class="btn btn-danger btn-sm pull-right" href="./list.php" style="margin-right: 10px;">清除</a>
                        </div>
                        
                        <div style="float:left; display:inline-block; width:100%;">
                            <input class="pull-right btn btn-success btn-sm" type="button" value="刪除" onclick="if(confirm('是否刪除資料？刪除後無法刪除後無法還原！')) {$('form').attr('action','./delete.php');$('form').submit();}">
                            <input class="pull-right btn btn-success btn-sm" type="button" value="下載csv" onclick="$('form').attr('action','./order_csv.php');$('.type').val('pick');$('form').submit()" style="margin-right: 10px;">
							<input class="pull-right btn btn-success btn-sm" type="button" value="下載揀貨單" onclick="$('form').attr('action','./order_pdf.php');$('.type').val('pick');$('form').submit()" style="margin-right: 10px;">
							<input class="pull-right btn btn-success btn-sm" type="button" value="下載發票檔" onclick="$('form').attr('action','./order_invoice.php');$('.type').val('pick');$('form').submit()" style="margin-right: 10px;">
                            <input class="pull-right btn btn-success btn-sm" type="button" value="狀態變更" onclick="$('form').attr('action','./status.php');$('form').submit()" style="margin-right:10px">
                            <select class="form-control input-sm pull-right" name="order_status">
                                <option value="1">等待處理</option>
                                <option value="2">已確認付款，待商品出貨</option>
                                <option value="3">出貨中</option>
                                <option value="4">已出貨</option>
                                <option value="5">已到店</option>
                                <option value="6">自動取消訂單</option>
                                <option value="7">已取貨，待產生發票</option>
                                <option value="8">訂單完成，發票已產生</option>
                                <option value="9">會員已取消</option>
                                <option value="10">金流交易中斷</option>
                                <option value="11">系統已取消</option>
                                <option value="12">申請退貨中</option>
                                <option value="13">已完成退貨</option>
                            </select>
                        </div>
                    </div>
                    <div class="box-body no-padding table-responsive">
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <th style="min-width:100px;"><a href="javascript:void(0)" onclick="select_all('')">(全選)</a><input type="hidden" name="type" class="type"></th>
                                    <th style="min-width:140px;">訂單編號</th>
                                    <th style="min-width:180px;">訂單日期</th>
                                    
                                    <!--<th><input type="button" value="下載超商標籤" onclick="$('form').attr('action','./order_pdf.php?type=store');$('.type').val('store');$('form').submit()"><a href="javascript:void(0)" onclick="select_all('id_store')">(全選)</a></th>
                                    <th><input type="button" value="轉出" onclick="$('form').attr('action','./order_delivery.php');$('form').submit()"><a href="javascript:void(0)" onclick="select_all('id_out')">(全選)</a></th>-->
									
                                    <th style="min-width:100px;">訂單總金額</th>
                                    <th style="min-width:200px;">訂單狀態</th>
									
                                    <th style="min-width:100px;">購買人姓名</th>
                                    <th style="min-width:100px;">購買人電話</th>
                                    <th style="min-width:100px;">收件人</th>
                                    <th style="min-width:100px;">收件人手機</th>
									<th style="min-width:80px;">付款方式</th>
                                    <th style="min-width:180px;">配送方式</th>
                                    <th style="min-width:180px;">訂單備註</th>
									<th style="min-width:80px;">訂單問答</th>
                                    <th style="min-width:100px;">管理人員備註</th>
                                    <th>操作</th>
                                </tr>
                                <?php 
                                    while($row = $rs_order->fetchRow(MDB2_FETCHMODE_ASSOC)) { 
                                        $sql_cmd = "select * from `order_qa` where Order_ID = '".$row['Order_ID']."'";
                                        $rs_qa = $db->query($sql_cmd);
                                        $payment = "";
                                        if($row['delivery'] == 2) {
                                            if(strpos($row['store_name'], "F")) {
                                                $payment = "<br>(全家";
                                            }
                                            else if(strpos($row['store_name'], "L")) {
                                                $payment = "<br>(OK";
                                            }
                                            else {
                                                $payment = "<br>(萊爾富";
                                            }
                                            $payment .= ":".$row['Delivery_No'].")";
                                        }
                                        else if($row['Payment'] == 1) {
                                            $payment .= "<br>(".$row['Delivery_No'].")";
                                        }
                                ?>

                                <tr onClick="iCheckThisRow(this);">
                                    <td><input class="delete" type="checkbox" name="id[]" value="<?=$row["Order_ID"]?>"></td>
                                    <td><?=$row['Order_ID']?></td>
                                    <td><?=$row['create_datetime']?></td>
                                    <!--<td>
                                        <?php
                                            //if(in_array($row['delivery'], [1,2]))
                                                //echo "<input class='id_store' type=\"checkbox\" name=\"id_store[]\" value=\"".$row['Order_ID']."\">";
                                        ?>
                                    </td>-->
                                    <td><?=$row['total_price']?></td>
                                    <td><font style="color:<?=$ARRall['order_status_color'][$row['Status']]?>"><?=$ARRall['order_status'][$row['Status']]?></font></td>
                                    <td><?=$row['name']?></td>
                                    <td><?=$row['phone']?></td>
                                    <td><?=$row['Delivery_Name']?></td>
                                    <td><?=$row['Delivery_Mobile']?></td>
                                    <td><?=$ARRall['payment'][$row['Payment']]?></td>
                                    <td><?=$ARRall['delivery'][$row['delivery']]?><?=$payment?></td>
                                    <td><?=$row['Note']?></td>
                                    <td>
                                        <?php
                                            if($rs_qa->numRows() > 0)
                                                echo "<a class=\"btn btn-xs btn-primary\" href=\"./order_qa.php?order_id=".$row['Order_ID']."\">有</a>";
                                            else
                                                echo "";
                                        ?>
                                    </td>
                                    <td><?=(!empty($row['Remark']))?"有":""?></td>
                                    <td>
                                        <a class="btn btn-xs btn-primary" href="./review.php?action=edit&Order_ID=<?=$row['Order_ID']?>">檢視</a>
                                        <a class="btn btn-xs btn-primary" href="./detail.php?action=edit&Order_ID=<?=$row['Order_ID']?>">編輯</a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
            <?=$page?>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>
</body>

</html>