<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("orders");

$rs[] = [
        "訂單編號",
        "訂單日期",
        "貨號",
        "商品名稱",
        "商品備註",
        "原價",
        "優惠價",
        "數量",
        "小計",
        "達成活動",
        "商品狀態",
        "購買小計",
        "折價券折抵",
        "折價券名稱",
        "熊贈點折抵",
        "會員再折扣",
        "會員再折扣活動名稱",
        "滿額再折扣",
        "滿額再折扣活動名稱",
        "滿額折抵",
        "滿額折抵扣活動名稱",
        "運費",
        "訂單總金額",
        "可獲得熊贈點",
        "訂單狀態",
        "退貨原因",
        "取消或退貨原因",
        "管理人員備註",
        "會員ID",
        "會員帳號",
        "會員等級",
        "購買人姓名",
        "購買人聯絡Email",
        "購買人電話",
        "購買人地址",
        "付款方式",
        "收件人",
        "收件人手機",
        "收件人市話",
        "收件人地址",
        "訂單備註",
        "配送方式",
        "門市名稱",
        "門市地址",
        "門市位置",
        "希望收件時段",
        "物流名稱",
        "配送編號",
        "收貨日期",
        "發票類型",
        "發票抬頭",
        "統一編號",
        "發票地址",
        "發票開立狀態",
        "發票號碼",
    ];
$sql_where = "";
$order_by = " order by create_datetime desc ";
$payment = "";
$delivery = "";
$status = "";
$order_qa = "";
$remark = "";
$level = "";
$s_date  = filter_input(INPUT_GET, 's_date');
$e_date  = filter_input(INPUT_GET, 'e_date');
if(!empty($_GET['Order_ID'])) {
    $sql_where .= " and o.Order_ID like '%".$_GET["Order_ID"]."%'";
}
if(!empty($s_date))
    $sql_where .= " and o.create_datetime >= '".checkinput_sql($s_date." 00:00:00",19)."'";
if(!empty($e_date))
    $sql_where .= " and o.create_datetime <= '".checkinput_sql($e_date." 23:59:59",19)."'";
if(!empty($_GET['order_qa'])) {
    if($_GET['order_qa'] == 1)
        $sql_where .= " and (select count(*) from `order_qa` where Order_ID =o.Order_ID) > 0";
    else
        $sql_where .= " and (select count(*) from `order_qa` where Order_ID =o.Order_ID) = 0";
    $order_qa = $_GET['order_qa'];
}
if(!empty($_GET['item_number'])) {
    $sql_where .= " and (select count(*) from order_item as oi join goods as g on g.Goods_ID = oi.goodsid where oi.Order_ID = o.Order_ID and g.item_number like '%".$_GET['item_number']."%') > 0";
}
if(!empty($_GET['goods_name'])) {
    $sql_where .= " and (select count(*) from order_item as oi join goods as g on g.Goods_ID = oi.goodsid where oi.Order_ID = o.Order_ID and g.Goods_Name like '%".$_GET['goods_name']."%') > 0";
}
if(!empty($_GET['remark'])) {
    if($_GET['remark'] == 1)
        $sql_where .= " and o.Remark != '' and o.Remark is not null";
    else
        $sql_where .= " and (o.Remark = '' or o.Remark is null)";
    $remark = $_GET['remark'];
}
if(!empty($_GET['Customer_ID'])) {
    $sql_where .= " and o.Customer_ID like '%".$_GET["Customer_ID"]."%'";
}
if(!empty($_GET['email'])) {
    $sql_where .= " and m.Customer_Mail like '%".$_GET["email"]."%'";
}
if(!empty($_GET['level']) && $_GET['level'] != 'all') {
    $sql_where .= " and m.level = '".$_GET["level"]."'";
    $level = $_GET['level'];
}
if(!empty($_GET['name'])) {
    $sql_where .= " and o.name like '%".$_GET["name"]."%'";
}
if(!empty($_GET['phone'])) {
    $sql_where .= " and o.phone like '%".$_GET["phone"]."%'";
}
if(!empty($_GET['payment'])) {
    $sql_where .= " and o.Payment like '%".$_GET["payment"]."%'";
    $payment = $_GET['payment'];
}
if(!empty($_GET['Delivery_Name'])) {
    $sql_where .= " and o.Delivery_Name like '%".$_GET["Delivery_Name"]."%'";
}
if(!empty($_GET['Delivery_Mobile'])) {
    $sql_where .= " and o.Delivery_Mobile like '%".$_GET["Delivery_Mobile"]."%'";
}
if(!empty($_GET['delivery'])) {
    $sql_where .= " and o.delivery like '%".$_GET["delivery"]."%'";
    $delivery = $_GET['delivery'];
}
if(!empty($_GET['status'])) {
    $sql_where .= " and o.status = '".$_GET["status"]."'";
    $status = $_GET['status'];
}

$sql_cmd = "select o.*,m.Customer_Mail, m.level as Customer_level from `order` as o join customer as m on m.Customer_ID = o.Customer_ID where  o.deleted_at is null ".$sql_where.$order_by;
$rs_order = $db->query($sql_cmd);
while($row_order = $rs_order->fetchRow(MDB2_FETCHMODE_ASSOC)) {
    $sql_cmd = "SELECT OI.*,GI.Name as Goods_Item_Name,G.Goods_Name, G.item_group, G.type from order_item as OI
                left join goods as G
                on G.Goods_ID = OI.goodsId
                left join goods_item as GI
                on OI.Goods_Item_ID = GI.Goods_Item_ID
                 where OI.Order_ID = '".$row_order['Order_ID']."'";
    $rs_order_item = $db->query($sql_cmd);
    $row_order_item = $rs_order_item->fetchRow(MDB2_FETCHMODE_ASSOC);
    $name = [];
    $event = json_decode($row_order_item['event'], true);
    for($c=0;$c<count($event);$c++)
        $name[]=$event[$c]['name'];
    $event_name = implode(",",$name);
    if($row_order_item['is_additional'] == 1 && strpos($row_order_item['remark'], "加購品") === false) {
        $row_order_item['remark'] .= "加購品";
    }
    if($row_order_item['type'] == 3 && strpos($row_order_item['remark'], "雜誌期數") === false) {
        $row_order_item['remark'] .= $row_order_item['Magazine_spec']."-".$row_order_item['Magazine_Name']."期，起始月份：".$row_order_item['starting_months']."，雜誌期數：".$row_order_item['start_periods']."期";
    }

    $rs[] = [
        $row_order['Order_ID'],
        $row_order['create_datetime'],
        $row_order_item['Goods_ID'],
        $row_order_item['Goods_Name'],
        $row_order_item['remark'],
        $row_order_item['Goods_Price'],
        $row_order_item['Goods_Discount_Price'],
        $row_order_item['Goods_Qty'],
        $row_order_item['Total_Price'],
        $event_name,
        $ARRall['order_item_status'][$row_order_item['Status']],
        $row_order['price'],
        $row_order['discount_ticket'],
        $row_order['discount_ticket_name'],
        $row_order['discount_point'],
        $row_order['discount_customer'],
        $row_order['discount_customer_name'],
        $row_order['discount_purchase'],
        $row_order['discount_purchase_name'],
        $row_order['discount_off'],
        $row_order['discount_off_name'],
        $row_order['Delivery_Price'],
        $row_order['total_price'],
        $row_order['point_get'],
        $ARRall['order_status'][$row_order['Status']],
        $row_order['cancel_reason'],
        $row_order['cancel_remark'],
        $row_order['Remark'],
        " ".$row_order['Customer_ID']."\n",
        $row_order['Customer_Mail'],
        $ARRall['level'][$row_order['Customer_level']],
        $row_order['name'],
        $row_order['email'],
        " ".$row_order['phone']."\n",
        $row_order['city'].$row_order['area'].$row_order['address'],
        $ARRall['payment'][$row_order['Payment']],
        $row_order['Delivery_Name'],
        " ".$row_order['Delivery_Mobile']."\n",
        " ".$row_order['Delivery_Phone']."\n",
        $row_order['Delivery_City'].$row_order['Delivery_Area'].$row_order['Delivery_Addr'],
        $row_order['Note'],
        $ARRall['delivery'][$row_order['delivery']],
        $row_order['store_name'],
        $row_order['store_addr'],
        $ARRall['store_location'][$row_order['store_location']],
        $ARRall['delivery_time'][$row_order['Delivery_time']],
        $row_order['Delivery_Company'],
        $row_order['Delivery_No'],
        $row_order['delivery_at'],
        $ARRall['i_type'][$row_order['i_type']],
        $row_order['i_title'],
        $row_order['i_tax_id_number'],
        $row_order['i_address'],
        $ARRall['i_status'][$row_order['i_status']],
        $row_order['i_number'],
    ];

    if(!empty($row_order_item['item_group'])) {
        $item_group = explode(",", $row_order_item['item_group']);
        $sql_cmd = "select * from goods where Goods_ID in ('".implode("','", $item_group)."')";
        $rs_group = $db->query($sql_cmd);
        while($row_group = $rs_group->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $rs[] = [
                "",
                "",
                $row_group['Goods_ID'],
                $row_group['Goods_Name'],
                "套書",
                "0",
                "0",
                "1",
                "0",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
            ];
        }
    }
    foreach ($event as $key => $value) {
        $sql_cmd = "select g.*
         from goods_event as e
         join gift as g on e.gift = g.item_number
          where e.id = '".$value['id']."'";
        $rs_gift = $db->query($sql_cmd);
        while($row_gift = $rs_gift->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $rs[] = [
                "",
                "",
                $row_gift['item_number'],
                $row_gift['name'],
                "贈品",
                "0",
                "0",
                $row_order_item['gift_number'],
                "0",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
            ];
        }
    }
    if(!empty($row_order_item['gift_item_number'])) {
        $sql_cmd = "select * from gift where item_number = '".$row_order_item['gift_item_number']."'";
        $rs_gift = $db->query($sql_cmd);
        while($row_gift = $rs_gift->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $rs[] = [
                "",
                "",
                $row_gift['item_number'],
                $row_gift['name'],
                "贈品",
                "0",
                "0",
                $row_order_item['gift_number'],
                "0",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
            ];
        }
    }
    if($row_order_item['registered_use'] == 1) {
        $rs[] = [
            "",
            "",
            "",
            "掛號費",
            $row_order_item['Magazine_Name']."期",
            "",
            "",
            "",
            $row_order_item['registered_total_Price'],
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
        ];
    }

    while ($row_order_item = $rs_order_item->fetchRow(MDB2_FETCHMODE_ASSOC)) {
        $event = json_decode($row_order_item['event'], true);
        $name = [];
        for($c=0;$c<count($event);$c++)
            $name[]=$event[$c]['name'];
        $event_name = implode(",",$name);
        if($row_order_item['is_additional'] == 1)
            $row_order_item['remark'] .= "加購品";
        if($row_order_item['type'] == 3) {
            $row_order_item['remark'] .= $row_order_item['Magazine_spec']."-".$row_order_item['Magazine_Name']."期，起始月份：".$row_order_item['starting_months']."，雜誌期數：".$row_order_item['start_periods']."期";
        }

        $rs[] = [
            "",
            "",
            $row_order_item['Goods_ID'],
            $row_order_item['Goods_Name'],
            $row_order_item['remark'],
            $row_order_item['Goods_Price'],
            $row_order_item['Goods_Discount_Price'],
            $row_order_item['Goods_Qty'],
            $row_order_item['Total_Price'],
            $event_name,
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
        ];

        if(!empty($row_order_item['item_group'])) {
            $item_group = explode(",", $row_order_item['item_group']);
            $sql_cmd = "select * from goods where Goods_ID in ('".implode("','", $item_group)."')";
            $rs_group = $db->query($sql_cmd);
            while($row_group = $rs_group->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                $rs[] = [
                    "",
                    "",
                    $row_group['Goods_ID'],
                    $row_group['Goods_Name'],
                    "套書",
                    "0",
                    "0",
                    "1",
                    "0",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                ];
            }
        }

        foreach ($event as $key => $value) {
            $sql_cmd = "select g.*
             from goods_event as e
             join gift as g on e.gift = g.item_number
              where e.id = '".$value['id']."'";
            $rs_gift = $db->query($sql_cmd);
            while($row_gift = $rs_gift->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                $rs[] = [
                    "",
                    "",
                    $row_gift['item_number'],
                    $row_gift['name'],
                    "贈品",
                    "0",
                    "0",
                    $row_order_item['gift_number'],
                    "0",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                ];
            }
        }
        if(!empty($row_order_item['gift_item_number'])) {
            $sql_cmd = "select * from gift where item_number = '".$row_order_item['gift_item_number']."'";
            $rs_gift = $db->query($sql_cmd);
            while($row_gift = $rs_gift->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                $rs[] = [
                    "",
                    "",
                    $row_gift['item_number'],
                    $row_gift['name'],
                    "贈品",
                    "0",
                    "0",
                    $row_order_item['gift_number'],
                    "0",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                ];
            }
        }

        if($row_order_item['registered_use'] == 1) {
            $rs[] = [
                "",
                "",
                "",
                "掛號費",
                $row_order_item['Magazine_Name']."期",
                "",
                "",
                "",
                $row_order_item['registered_total_Price'],
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
            ];
        }
    }
}
header('Content-Encoding: UTF-8');
header("Content-type: application/x-download");
header("Content-disposition: attachment; filename=orders.csv");
echo arr_to_csv($rs);