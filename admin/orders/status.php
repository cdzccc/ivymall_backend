<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("orders_edit");

$Order_ID = filter_input(INPUT_GET, 'id', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$Status = filter_input(INPUT_GET, 'order_status');


foreach ($Order_ID as $key => $value) {
	//echo $value."--";

	
	
	$sql_cmd = "select a.*,b.Customer_Mail, b.level from `order` as a join customer as b on a.Customer_ID = b.Customer_ID where a.Order_ID = '".$value."'";
	$rs = $db->query($sql_cmd);
	$row_order = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
			
	if($row_order["Status"] != $Status && in_array($Status, [6,9,10,11,12,13])) {
        //加回庫存
        $sql_cmd = "select * from order_item where Order_ID = '".$value."'";
        $rs = $db->query($sql_cmd);
        while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $sql_cmd = "update goods_item set Qty = (Qty+".$row['Goods_Qty'].") where Goods_Item_ID = '".$row['Goods_Item_ID']."'";
            $db->query($sql_cmd);
        }
        if($row_order['Pay_Status'] == 'y') {
            //歸還點數
            $sql_cmd = "select * from point_list where Order_ID = '".$value."' and Customer_ID = '".$row_order['Customer_ID']."'";
            $rs = $db->query($sql_cmd);
            while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                $sql_cmd = "select * from point_summary where Customer_ID = '".$row_order['Customer_ID']."'";
                $rs_summary = $db->query($sql_cmd);
                $row_summary = $rs_summary->fetchRow(MDB2_FETCHMODE_ASSOC);
                if($row['type'] == 1) {
                    $type = 2;
                    $row_summary['out'] += $row['point'];
                    $row_summary['total'] -= $row['point'];
                }
                else {
                    $type = 1;
                    $row_summary['in'] += $row['point'];
                    $row_summary['total'] += $row['point'];
                }
                $sql_cmd = "update point_summary set `out` = ".$row_summary['out'].", `in` = ".$row_summary['in'].", total = ".$row_summary['total']." where Customer_ID = '".$row_order['Customer_ID']."'";
                $db->query($sql_cmd);
                $point_list_id = get_id();
                $left = $row['left'];
                $sql_array = array(
                    "id"          => array("2", $point_list_id),
                    "Customer_ID" => array("2", $row_order['Customer_ID']),
                    "point"       => array("2", $row['point']),
                    "type"        => array("2", $type),
                    "datetime"    => array("2", date("Y-m-d H:i:s")),
                    "Order_ID"    => array("2", $value),
                    "left"        => array("2", $row_summary['total']),
                    "level"        => array("2", checkinput_sql($row_order['level'], 50)),
                );
                $sql_cmd = insert("point_list", $sql_array);
                $db->query($sql_cmd);
            }
        }
    }
	
    if(($Status == 3 || $Status == 4) && $row_order["Status"] != $Status && $row_order["store_update"] == null) {
        $sql_cmd = "update `order` set `store_update` = '0' where Order_ID ='".$row_order['Order_ID']."' and `status` not in (8)";
        $db->query($sql_cmd);

        if($row_order['delivery'] == 1) {
            if($row_order["Payment"] == 3) {
                $servicetype = "1";
            }
            else {
                $servicetype = "3";
            }
            preg_match("/^([^(]+)\(#?([^)]+)/", $row_order['store_name'], $store);
            $storeid = $store[2];
            $storename = $store[1];
            $ShipDate = date("Y-m-d");
            $ReturnDate = date("Y-m-d", strtotime($ShipDate."+7 days"));
            $sql_array = array(
                "crdate"      => array("2", date("Y-m-d H:i:s")),
                "customer_id" => array("3", $row_order['Customer_ID']),
                "order_id"    => array("3", $row_order['Order_ID']),
                "storepath"   => array("2", "A"),
                "storeid"     => array("3", $storeid),
                "storename"   => array("2", $storename),
                "storeaddr"   => array("2", $row_order['store_addr']),
                "servicetype" => array("2", $servicetype),
                "status"      => array("2", 30),
                "ShipDate"    => array("2", $ShipDate),
                "ReturnDate"  => array("2", $ReturnDate)
            );
            $sql_cmd = insert("store", $sql_array);
            $db->query($sql_cmd);
        }
        else if($row_order['delivery'] == 2) {
            if($row_order["Payment"] == 3) {
                $servicetype = "1";
            }
            else {
                $servicetype = "3";
            }
            preg_match("/^([^(]+)\(#?([^)]+)/", $row_order['store_name'], $store);
            $storeid = $store[2];
            $storename = $store[1];
            $ShipDate = date("Y-m-d");
            $ReturnDate = date("Y-m-d", strtotime($ShipDate."+7 days"));
            $sql_array = array(
                "crdate"      => array("2", date("Y-m-d H:i:s")),
                "customer_id" => array("3", $row_order['Customer_ID']),
                "order_id"    => array("3", $row_order['Order_ID']),
                "storepath"   => array("2", "A"),
                "storeid"     => array("3", $storeid),
                "storename"   => array("2", $storename),
                "storeaddr"   => array("2", $row_order['store_addr']),
                "servicetype" => array("2", $servicetype),
                "status"      => array("2", 30),
                "ShipDate"    => array("2", $ShipDate),
                "ReturnDate"  => array("2", $ReturnDate)
            );
            $sql_cmd = insert("store_cvs", $sql_array);
            $db->query($sql_cmd);
        }
    }

	if($Status == 7 && $row_order["Status"] != $Status) {
        //$sql_cmd = "update `order` set delivery_at = '".date("Y-m-d 00:00:00")."'  where `Status` = 7 and i_status = 1 and Order_ID = '".$Order_ID."'";
        //$db->query($sql_cmd);
    }

    if($Status == 8 && $row_order["Status"] != $Status && $row_order["is_finish"] == 0) {
		//加點數
        $point_get = $row_order["point_get"];
		if($point_get > 0) {
            $sql_cmd = "select * from point_summary where Customer_ID = '".$row_order['Customer_ID']."'";
            $rs_summary = $db->query($sql_cmd);
            if($rs_summary->numRows() > 0) {
                $row_point = $rs_summary->fetchRow(MDB2_FETCHMODE_ASSOC);
                $point_total = $row_point['total'] + $point_get;
                $point_in = $row_point['in'] + $point_get;
                $point_out = $row_point['out'];
                $sql_cmd = "update point_summary set `in` = ".$point_in.", `out` = ".$point_out.", total = ".$point_total." where Customer_ID = '".$row_order['Customer_ID']."'";
				$rs2 = $db->query($sql_cmd);
            }
            else {
                $point_summary_id = get_id();
                $sql_array = array(
                    "id"          => array("2", $point_summary_id),
                    "Customer_ID" => array("2", $row_order['Customer_ID']),
                    "in"          => array("2", $point_get),
                    "out"         => array("2", 0),
                    "total"       => array("2", $point_get),
                );
                $sql_cmd = insert("point_summary", $sql_array);
                $rs2 = $db->query($sql_cmd);
                $row_point['total'] = 0;
            }
            $point_list_id = get_id();
            $enabled_date = date("Y-m-d H:i:s");
            $end_date = date("Y-m-d H:i:s", strtotime("+1 years"));
            $sql_array = array(
                "id"           => array("2", checkinput_sql($point_list_id, 19)),
                "Customer_ID"  => array("2", checkinput_sql($row_order['Customer_ID'], 200)),
                "point"        => array("2", checkinput_sql($row_order["point_get"], 200)),
                "mode"         => array("2", checkinput_sql(2 , 45)),
                "type"         => array("2", checkinput_sql(1,2)),
                "datetime"     => array("2", checkinput_sql(date("Y-m-d H:i:s"), 50)),
                "Order_ID"     => array("2", checkinput_sql($row_order['Order_ID'], 50)),
                "left"         => array("2", $row_point['total']+$point_get),
                "enabled_date" => array("2", checkinput_sql($enabled_date, 50)),
                "status"       => array("2", checkinput_sql(1, 50)),
                "end_date"     => array("2", checkinput_sql($end_date, 50)),
                "level"        => array("2", checkinput_sql($row_order['level'], 50)),
            );
            $sql_cmd = insert("point_list", $sql_array);
            $db->query($sql_cmd);
        }
		
        // 首次購物回饋禮
        $sql_cmd = "select * from `order` where Status in (8,12,13) and Order_ID != '".$row_order['Order_ID']."' and Customer_ID = '".$row_order['Customer_ID']."'";
        $rs = $db->query($sql_cmd);
        if($rs->numRows() == 0) {
            $sql_cmd = "select * from point_summary where Customer_ID = '".$row_order['Customer_ID']."'";
            $rs_summary = $db->query($sql_cmd);
            $point_get = 30;
            $row_point = $rs_summary->fetchRow(MDB2_FETCHMODE_ASSOC);
            $point_total = $row_point['total'] + $point_get;
            $point_in = $row_point['in'] + $point_get;
            $point_out = $row_point['out'];
            $sql_cmd = "update point_summary set `in` = ".$point_in.", `out` = ".$point_out.", total = ".$point_total." where Customer_ID = '".$row_order['Customer_ID']."'";
            $rs2 = $db->query($sql_cmd);
            $point_list_id = get_id();
            $enabled_date = date("Y-m-d H:i:s");
            $end_date = date("Y-m-d H:i:s", strtotime("+1 years"));
            $sql_array = array(
                "id"           => array("2", checkinput_sql($point_list_id, 19)),
                "Customer_ID"  => array("2", checkinput_sql($row_order['Customer_ID'], 200)),
                "point"        => array("2", checkinput_sql($point_get, 200)),
                "mode"         => array("2", checkinput_sql(1 , 45)),
                "type"         => array("2", checkinput_sql(1,1)),
                "datetime"     => array("2", checkinput_sql(date("Y-m-d H:i:s"), 50)),
                "Order_ID"     => array("2", checkinput_sql($row_order['Order_ID'], 50)),
                "left"         => array("2", $row_point['total']+$point_get),
                "enabled_date" => array("2", checkinput_sql($enabled_date, 50)),
                "status"       => array("2", checkinput_sql(1, 50)),
                "end_date"     => array("2", checkinput_sql($end_date, 50)),
                "level"        => array("2", checkinput_sql($row_order['level'], 50)),
            );
            $sql_cmd = insert("point_list", $sql_array);
            $db->query($sql_cmd);
        }	
		$sql_cmd = "SELECT OI.*,GI.Name as Goods_Item_Name,G.Goods_Name from order_item as OI
            join goods as G
            on G.Goods_ID = OI.goodsId
            left join goods_item as GI
            on OI.Goods_Item_ID = GI.Goods_Item_ID
            where OI.Order_ID = '".$value."'";
		$rs_order_item = $db->query($sql_cmd);
		
		
		//給折價卷
		while($row_order_item = $rs_order_item->fetchRow(MDB2_FETCHMODE_ASSOC)) {
			//echo $row_order_item['event'];
			if(!empty($row_order_item['event'])){
				$event = json_decode($row_order_item['event'], true);
				if(!in_array($event[0]['id'],$event_group)){
					$event_group[] = $event[0]['id'];
					
					$sql_cmd = "select * from goods_event 
							where 
							type = 6 
							and id = ".$event[0]['id']."";
					$rs_event = $db->query($sql_cmd);
					$row_event = $rs_event->fetchRow(MDB2_FETCHMODE_ASSOC);
					if($row_event['type']==6){
						$sql_cmd = "select * from ticket where ticket_id = '".$row_event['ticket']."'";
						$rs_ticket = $db->query($sql_cmd);
						$row_ticket = $rs_ticket->fetchRow(MDB2_FETCHMODE_ASSOC);
						$sql_array = array(
							"Ticket_ID"       => array("2", checkinput_sql($row_ticket['ticket_id'], 19)),
							"CODE"            => array("2", ""),
							"Customer_ID"     => array("2", checkinput_sql($row_order['Customer_ID'], 50)),
							"price"           => array("2", checkinput_sql($row_ticket['price'], 5)),
							"date_start"      => array("2", checkinput_sql($row_ticket['date_start'], 50)),
							"date_end"        => array("2", checkinput_sql($row_ticket['date_end'], 50)),
							"status"          => array("2", 1),
							"create_datetime" => array("2", checkinput_sql(date("Y-m-d H:i:s"), 50)),
						);
						$sql_cmd = insert("ticket_list", $sql_array);
						$db->query($sql_cmd);
					}
				}
			}
		}
		
		//  11/30之前的訂單加到累積B，之後的加到累積A
		if(strtotime($row_order['create_datetime']) < strtotime(date('Y').'-11-30')){
			$sql_cmd = "update `customer` set order_count = (order_count + 1), year_consumption = (year_consumption + ".$row_order['total_price']."),total_consumption = (total_consumption + ".$row_order['total_price'].") where Customer_ID = '".$row_order['Customer_ID']."'";
			$db->query($sql_cmd);
		}else{
			$sql_cmd = "update `customer` set order_count = (order_count + 1), year_consumption_b = (year_consumption_b + ".$row_order['total_price']."),total_consumption = (total_consumption + ".$row_order['total_price'].") where Customer_ID = '".$row_order['Customer_ID']."'";
			$db->query($sql_cmd);

		}
		$sql_cmd = "update `order` set is_finish = '1' where Order_ID = '".$row_order['Order_ID']."'";
        $db->query($sql_cmd);
		
		//會員升等
		$sql_cmd = "SELECT o.Customer_ID, 
						count(o.Customer_ID) as total_order, 
						sum(o.total_price) as  total_price,
						c.level
					from `order` as o 
					join customer as c
						on o.Customer_ID = c.Customer_ID
					WHERE 
						o.create_datetime between '".(date("Y")-1)."-12-01'
							and '".(date("Y"))."-11-30'
						and o.status in (7,8) 
					group by o.Customer_ID";
		$rs = $db->query($sql_cmd);
		while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
			$new_level = get_new_level($row['level'], $row['total_order'], $row['total_price']);
			if($new_level>$row['level']){
				update_customer_level($row['Customer_ID'], $new_level);
                level_up_gift($new_level, $row['Customer_ID']);
			}
		}


    }
	$sql_cmd = "update `order` set `status` = '".$Status."' where Order_ID ='".$value."' and `status` not in (8)";
	$rs = $db->query($sql_cmd);
	
    add_log('訂單管理','2');
    $db->disconnect();
	
	
}
js_repl_global( "./list.php", "EDIT_SUCCESS");

function get_new_level($level, $order, $total_price) {
	if($total_price >= '4500') {
		$new_level = 3;
	}
	else if($total_price >= '1500') {
		$new_level = 2;
	}
	else if($order > 0) {
		$new_level = 1;
	}
	return max($level,$new_level);
}

function update_customer_level($Customer_ID, $level) {
	global $db;
	if($level>=2)
		$sql_cmd = "update customer set level = '".$level."',level_sdate = '".date("Y-m-d H:i:s")."', `level_edate` = '".(date("Y")+1)."-12-31 23:59:59' where Customer_ID = '".$Customer_ID."'";
	else
		$sql_cmd = "update customer set level = '".$level."',level_sdate = '".date("Y-m-d H:i:s")."', `level_edate` = '' where Customer_ID = '".$Customer_ID."'";

	$db->query($sql_cmd);
}

