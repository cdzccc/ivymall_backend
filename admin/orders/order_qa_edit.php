<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("orders_edit");
$id = get_id();

$Order_ID        = filter_input(INPUT_POST, 'Order_ID');
$content         = filter_input(INPUT_POST, 'content');
$create_datetime = date("Y-m-d H:i:s");
$create_user     = $_SESSION[SESSION_VARIABLE."_user_id"];

if(empty($Order_ID)) {
    js_go_back_global("NOT_POST");
    exit;
}
if(empty($content)) {
    $err_msg = "欄位";
    if(empty($content))
        $err_field[] = "回覆內容";
    $err_msg .= "'".implode("','", $err_field)."'不可為空值";
    js_go_back_self($err_msg);
    exit;
}

$sql_array = array(
    "id"              => array("2", checkinput_sql($id,50)),
    "order_id"        => array("2", checkinput_sql($Order_ID,19)),
    "content"         => array("2", checkinput_sql($content,500)),
    "create_datetime" => array("2", checkinput_sql($create_datetime,20)),
    "create_user"     => array("2", checkinput_sql($create_user,20)),
    "type"            => array("2", 2),
);
$sql_cmd = insert("order_qa", $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
    js_go_back_global("DB_ADD_ERROR");
    exit;
}else{
    // order item
    $sql_cmd = "select * from `order` where Order_ID = '".$Order_ID."'";
    $rs = $db->query($sql_cmd);
    $row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
    $email = $row['email'];
    $sql_cmd = "select * from order_qa where type = 1 and order_id = '".$Order_ID."' order by id desc limit 1";
    $rs = $db->query($sql_cmd);
    $row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
    $question = $row['content'];
    $datas = [
        "title"    => "常春藤網路書城-訂單問題回覆通知",
        "type"     => "4",
        "Order_ID" => $Order_ID,
        "email"    => $email,
        "question" => $question,
        "answer"   => $content,
        "mail"     => $email,
    ];
    ClassMail::send_mail($datas);
    add_log('訂單問答管理','2');
    $db->disconnect();
    js_repl_global( "./order_qa.php?order_id=".$Order_ID, "ADD_SUCCESS");
    exit;
}

