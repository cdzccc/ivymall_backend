<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("customer_event");

$title    = filter_input(INPUT_GET, 'title');
$s_date_s = filter_input(INPUT_GET, 's_date_s');
$s_date_e = filter_input(INPUT_GET, 's_date_e');
$e_date_s = filter_input(INPUT_GET, 'e_date_s');
$e_date_e = filter_input(INPUT_GET, 'e_date_e');
$during_s = filter_input(INPUT_GET, 'during_s');
$during_e = filter_input(INPUT_GET, 'during_e');
$status   = filter_input(INPUT_GET, 'status');
$sql_where = "";
if(!empty($title)) {
    $sql_where .= " and title like '%".checkinput_sql($title,19)."%'";
}
if(!empty($s_date_s) && !empty($s_date_e)) {
    $s_date_s = date('Y/m/d H:i',strtotime($s_date_s));
    $s_date_e = date('Y/m/d H:i',strtotime($s_date_e));
    $sql_where .= " and sdate >= '".checkinput_sql($s_date_s,19)."' and sdate <= '".checkinput_sql($s_date_e,19)."'";
}
if(!empty($e_date_s) && !empty($e_date_e)) {
    $e_date_s = date('Y/m/d H:i',strtotime($e_date_s));
    $e_date_e = date('Y/m/d H:i',strtotime($e_date_e));
    $sql_where .= " and edate >= '".checkinput_sql($e_date_s,19)."' and edate <= '".checkinput_sql($e_date_e,19)."'";
}
if(!empty($during_s)) {
    $during_s = date('Y/m/d H:i',strtotime($during_s));
    $sql_where .= " and during_start >= '".checkinput_sql($during_s,19)."'";
}
if(!empty($during_e)) {
    $during_e = date('Y/m/d H:i',strtotime($during_e));
    $sql_where .= " and during_end <= '".checkinput_sql($during_e,19)."'";
}
if($status != '')
    $sql_where .= " and status = '".checkinput_sql($status, 2)."'";
//取出總筆數
$sql_cmd = "select count(*) from customer_event where deleted_at is null ".$sql_where." order by create_datetime DESC ";
$rs = $db->query($sql_cmd);
$row = $rs->fetchRow();
$total = $row[0];

$mypage = new page;
$pages = $mypage->set_page($total, $PAGE_NUM);

//設定分頁的初始值
if (empty($_GET['num']))
{
    $num = 1;
}else{
    $num = intval($_GET['num']);
}


$sql_cmd = "select * from customer_event where deleted_at is null ".$sql_where." order by create_datetime DESC ".$pages[$num];
$rs_dealer  = $db->query($sql_cmd);
// print_r($rs_dealer);
$tmp = $_GET;
unset($tmp['num']);
$val = http_build_query($tmp);
$page = $mypage->multi($total , $PAGE_NUM, $num, "./list.php", "", $val);
?>
<!DOCTYPE html>
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <?php include("../view/sidebar.php"); ?>
        <!-- /.sidebar -->
    </aside>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                會員活動管理
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box">
                <form action="./list.php" method="GET" name="search">
                    <div class="box-header">
                        <div style="float:left; display:inline-block; width:180px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">搜尋</button>
                                </div>
                                <input type="text" name="title" class="form-control input-sm"
                                       placeholder="名稱" value="<?=(!empty($_GET['title']))?$_GET['title']:""?>">
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:140px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">活動期間</button>
                                </div>
                                <input type="text" name="during_s" class="form-control input-sm datetimepicker"
                                       placeholder="起始日" value="<?=(!empty($_GET['during_s']))?$_GET['during_s']:""?>">
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:140px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">~</button>
                                </div>
                                <input type="text" name="during_e" class="form-control input-sm datetimepicker"
                                       placeholder="結束日" value="<?=(!empty($_GET['during_e']))?$_GET['during_e']:""?>">
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:140px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">上架時間</button>
                                </div>
                                <input type="text" name="s_date_s" class="form-control input-sm datetimepicker"
                                       placeholder="起始" value="<?=(!empty($_GET['s_date_s']))?$_GET['s_date_s']:""?>">
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:140px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">~</button>
                                </div>
                                <input type="text" name="s_date_e" class="form-control input-sm datetimepicker"
                                       placeholder="結束" value="<?=(!empty($_GET['s_date_e']))?$_GET['s_date_e']:""?>">
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:140px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">下架時間</button>
                                </div>
                                <input type="text" name="e_date_s" class="form-control input-sm datetimepicker"
                                       placeholder="起始" value="<?=(!empty($_GET['e_date_s']))?$_GET['e_date_s']:""?>">
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:140px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">~</button>
                                </div>
                                <input type="text" name="e_date_e" class="form-control input-sm datetimepicker"
                                       placeholder="結束" value="<?=(!empty($_GET['e_date_e']))?$_GET['e_date_e']:""?>">
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:150px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">狀態</button>
                                </div>
                                <select class="form-control input-sm" name="status">
                                    <option value="">全部</option>
                                    <option value="1" <?=($status == "1")?"selected":""?>>啟用</option>
                                    <option value="0" <?=($status == "0")?"selected":""?>>停用</option>
                                </select>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success btn-sm pull-right">送出</button>
                        <a class="btn btn-danger btn-sm pull-right" href="./list.php" style="margin-right: 10px;">清除</a>
                        <div style="float:left; display:inline-block; width:100%;margin-top: 10px;">
                            <a class="pull-right btn btn-success btn-sm" href="./detail.php?action=add">新增</a>
                            <input class="pull-right btn btn-success btn-sm" type="button" value="刪除" onclick="if(confirm('是否刪除資料？刪除後無法刪除後無法還原！')) {$('form').attr('action','./delete.php');$('form').submit();}"style="margin-right: 10px;">
                        </div>

                    </div>
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <th style="min-width:100px;"><a href="javascript:void(0)" onclick="select_all('')">(全選)</a></th>
                                    <th>文章標題</th>
                                    <th>活動期間</th>
                                    <th>上架時間</th>
                                    <th>下架時間</th>
                                    <th>排序</th>
                                    <th>狀態</th>
                                    <th>操作</th>
                                </tr>
                                <?php
                                if($rs_dealer->numRows() > 0) {
                                while($row = $rs_dealer->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                                ?>
                                <tr onClick="iCheckThisRow(this);">
                                    <th><input type="checkbox" name="id[]" value="<?=$row["id"]?>"></th>
                                    <th><?=$row['title']?></th>
                                    <th><?=$row['during_start']." - ".$row['during_end']?></th>
                                    <th><?=$row['sdate']?></th>
                                    <th><?=$row['edate']?></th>
                                    <th><?=$row['sort']?></th>
                                    <th><?=($row['status']==1)?"啟用":"停用"?></th>
                                    <th>
                                        <a class="btn btn-xs btn-primary" href="./detail.php?action=edit&id=<?=$row['id']?>">編輯</a>
                                    </th>
                                </tr>
                                <?php
                                } }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
		<?=$page?>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>
    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>