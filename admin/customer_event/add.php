<?php
include("../config.php");

ClassJscript::islogin();
ClassJscript::isadmino("customer_event_add");

$now = date('Y-m-d H:i:s');
$id = get_id();
$title           = filter_input(INPUT_POST, 'title');
$during_start    = filter_input(INPUT_POST, 'during_start');
$during_end      = filter_input(INPUT_POST, 'during_end');
$alt             = filter_input(INPUT_POST, 'alt');
$content         = filter_input(INPUT_POST, 'editor');
$meta_keyword    = filter_input(INPUT_POST, 'meta_keyword');
$meta_desc       = filter_input(INPUT_POST, 'meta_desc');
$sdate           = filter_input(INPUT_POST, 'sdate');
$edate           = filter_input(INPUT_POST, 'edate');
$sort            = filter_input(INPUT_POST, 'sort');
$status          = filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 1, 'default' => 1)));
$create_datetime = $now;
$update_datetime = $now;
$create_user     = $_SESSION[SESSION_VARIABLE."_user_id"];
$update_user     = $_SESSION[SESSION_VARIABLE."_user_id"];

if(empty($title) || empty($during_start) || empty($during_end) || empty($_FILES['pic']['name'])) {
    $err_msg = "欄位";
    if(empty($title))
        $err_field[] = "文章標題";
    if(empty($during_start) || empty($during_end))
        $err_field[] = "活動期間";
    if(empty($_FILES['pic']['name']))
        $err_field[] = "圖片";
    $err_msg .= "'".implode("','", $err_field)."'不可為空值";
    js_go_back_self($err_msg);
    exit;
}

if ($_FILES['pic']['name'] != "none" && is_uploaded_file($_FILES['pic']['tmp_name']))
{
    //重組檔名
    $type = explode(".", $_FILES['pic']['name']);
    $img = date("YmdHis").".".$type[1];
    @copy($_FILES['pic']['tmp_name'], DOCUMENT_ROOT."/upload/".$img);
}
else {
    js_go_back_global("IMG_ERROR");
    exit;
}

$sql_array = array(
    "id"              => array("2", checkinput_sql($id, 19)),
    "title"           => array("2", checkinput_sql($title, 200)),
    "during_start"    => array("2", checkinput_sql($during_start, 50)),
    "during_end"      => array("2", checkinput_sql($during_end, 50)),
    "pic"             => array("2", checkinput_sql($img, 200)),
    "alt"             => array("2", checkinput_sql($alt, 200)),
    "content"         => array("2", checkinput_sql($content, 9999999999)),
    "meta_keyword"    => array("2", checkinput_sql($meta_keyword, 200)),
    "meta_desc"       => array("2", checkinput_sql($meta_desc, 200)),
    "sdate"           => array("2", checkinput_sql($sdate, 30)),
    "edate"           => array("2", checkinput_sql($edate, 30)),
    "status"          => array("2", checkinput_sql($status, 2)),
    "sort"            => array("2", checkinput_sql($sort, 2)),
    "create_datetime" => array("2", checkinput_sql($create_datetime, 50)),
    "create_user"     => array("2", checkinput_sql($create_user, 50)),
    "update_datetime" => array("2", checkinput_sql($update_datetime, 50)),
    "update_user"     => array("2", checkinput_sql($update_user, 50)),
);

$sql_cmd = insert("customer_event", $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
   js_go_back_global("DB_ADD_ERROR");
   exit;
}else{
    add_log('會員活動管理','1');
    $db->disconnect();
    js_repl_global( "./list.php", "ADD_SUCCESS");
    exit;
}
?>
