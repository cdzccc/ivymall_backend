<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("customer_event");

if(isset($_GET['action']))
    $action = $_GET['action'];
else
    $action = "add";

$row["id"]           = "";
$row["title"]        = "";
$row["during_start"] = "";
$row["during_end"]   = "";
$row["pic"]          = "";
$row["alt"]          = "";
$row["content"]      = "";
$row["meta_keyword"] = "";
$row["meta_desc"]    = "";
$row["sdate"]        = "";
$row["edate"]        = "";
$row["sort"]         = "0";
$row["status"]       = "1";
$row["create_datetime"] = date('Y-m-d H:i:s');
$date_range = "";
if($action == "edit") {
    $sql_cmd = "select * from customer_event where id = '".$_GET['id']."'";
    $rs = $db->query($sql_cmd);
    $row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
}
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php"); ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                會員活動管理
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" method="POST" action="./<?=$action?>.php" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">文章標題</label>
                            <div class="col-sm-10">
                                <input id="dealer_name" name="title" type="text" class="form-control" placeholder=""
                                       value="<?=$row["title"]?>">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="col-sm-2 control-label">活動期間</label>
                            <div class="col-sm-10">
                                <div class="input-group">
                                    <input class="datetimepicker" name="during_start" type="text" class="form-control pull-right rangepicker" value="<?=$row['during_start']?>">
                                     -
                                    <input class="datetimepicker" name="during_end" type="text" class="form-control pull-right rangepicker" value="<?=$row['during_end']?>">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="pic" class="col-sm-2 control-label">圖片<br><span class="text-red">建議尺寸415 x 330</span></label>
                            <div class="col-sm-10">
                                <? if(!empty($row['pic'])): ?>
                                    <img src="<?=WEBSITE_URL?>upload/<?=$row['pic']?>" width="100" class="pic">
                                    <br><?=$row['pic']?>
                                     &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<a href='javascript:void(0);' class='btn btn-danger' onclick='$(".old_pic").val("");$(".pic").hide();$(this).hide()'>刪除圖片</a>
                                <? endif ?>
                                <input id="pic" name="pic" type="file">
                                <input name="old_pic" type="hidden" value='<?=$row['pic']?>' class="old_pic">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="alt" class="col-sm-2 control-label">ALT</label>
                            <div class="col-sm-10">
                                <input id="alt" name="alt" type="text" class="form-control" placeholder="" value="<?=$row['alt']?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="editor" class="col-sm-2 control-label">文章內容</label>
                            <div class="col-sm-10">
                                <textarea name="editor"><?=$row['content']?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="meta_keyword" class="col-sm-2 control-label">Meta Keywords</label>
                            <div class="col-sm-10">
                                <input id="meta_keyword" name="meta_keyword" type="text" class="form-control" placeholder="" value="<?=$row['meta_keyword']?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="meta_desc" class="col-sm-2 control-label">Meta Description</label>
                            <div class="col-sm-10">
                                <input id="meta_desc" name="meta_desc" type="text" class="form-control" placeholder="" value="<?=$row['meta_desc']?>">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="col-sm-2 control-label">上架時間</label>
                            <div class="col-sm-10">
                                <div class="input-group">
									<input class="datetimepicker" name="sdate" type="text" class="form-control pull-right rangepicker" value="<?=$row['sdate']?>">
                                     -
                                    <input class="datetimepicker" name="edate" type="text" class="form-control pull-right rangepicker" value="<?=$row['edate']?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">排序</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="sort" value="<?=$row['sort']?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">狀態</label>
                            <div class="col-sm-10 control-text">
                                <label>
                                    <input type="radio" name="status" value="0" <?php if($row["status"] == 0) {echo "checked";}?>>下架
                                </label>
                                <label>
                                    <input type="radio" name="status" value="1" <?php if($row["status"] == 1) {echo "checked";}?>>上架
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="f_path" class="col-sm-2 control-label">建立時間</label>
                            <div class="col-sm-10" style="line-height: 32px;">
                                <i class="fa fa-clock-o" aria-hidden="true"></i> <?=$row["create_datetime"]?>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-right">
                        <input type="hidden" name="id" value="<?=$row['id']?>">
                        <a href="javascript:history.back()" class="btn btn-danger" style="margin-right: 10px;">取消，並返回上一層</a>
                        <button type="button" class="btn btn-primary" onclick="check_file();">儲存</button>
                        <input type="hidden" name="referer" value="<?=(!empty($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:"")?>">
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>