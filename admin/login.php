<?php
    include("./config.php");
?>
<html style="height: auto;">
<?php include('./view/metalink.php'); ?>
<body class="skin-blue" style="height: auto;">
<div class="wrapper" style="height: auto;">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="background: rgb(236, 240, 245); margin-left:0px; min-height:95%">
        <!-- Main content -->
        <section class="content">
            <!-- Your Page Content Here -->

            <script type="text/javascript">
                function checkSubmit(obj) {
                    if (!checkValid(obj))
                        return false;
                    return true;
                }
            </script>

            <div class="login-box" style=" border: 1px solid #CCC">
                <div class="login-box-body">
                    <p class="login-box-msg">後台管理介面</p>
                    <form method="POST" action="./auth/login.php">
                        <div class="form-group has-feedback">
                            <input type="text" name="username" id="username" value="" maxlength="50" class="form-control" placeholder="帳號" notnull="true" detail="帳號">
                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="password" name="pwd" id="password" class="form-control" placeholder="密碼" notnull="true" detail="密碼">
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        </div>
                        <div class="row">
                            <div class="col-xs-8" style="padding-left:40px;">
                                <!--<div class="checkbox icheck">
                                    <label>
                                        <input type="checkbox" name="remember" value="1"> 記住我的帳號
                                    </label>
                                </div>-->
                            </div>
                            <div class="col-xs-4">
                                <button type="submit" class="btn btn-primary btn-block">登入</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Footer -->
    <?php include('./view/footer.php'); ?>

</div><!-- ./wrapper -->
<?php include('./view/js_css_include.php'); ?>
<style>
    .main-footer {
        margin-left: 0;
    }
</style>
</body>
</html>