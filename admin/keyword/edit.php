<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("keyword_edit");
$id = filter_input(INPUT_POST, 'id');

$keyword      = filter_input(INPUT_POST, 'keyword');
$status    = filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 1, 'default' => 0)));
$sort        = filter_input(INPUT_POST, 'sort', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 99999, 'default' => 0)));

if(empty($id)) {
    js_go_back_global("NOT_POST");
    exit;
}

if(empty($keyword)) {
    $err_msg = "欄位";
    if(empty($keyword))
        $err_field[] = "關鍵字";
    $err_msg .= "'".implode("','", $err_field)."'不可為空值";
    js_go_back_self($err_msg);
    exit;
}

$update_datetime = date("Y-m-d H:i:s");
$update_user     = $_SESSION[SESSION_VARIABLE."_user_id"];

$sql_array = array(
    "keyword"         => array("2", checkinput_sql($keyword, 100)),
    "status"          => array("2", checkinput_sql($status, 5)),
    "update_datetime" => array("2", checkinput_sql($update_datetime, 50)),
    "update_user"     => array("2", checkinput_sql($update_user, 50)),
    "sort"            => array("2", checkinput_sql($sort, 5)),
);
$sql_cmd = update("keyword", array("id", $id), $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
   js_go_back_global("DB_EDIT_ERROR");
   exit;
}else{
    add_log('搜尋關鍵字管理','2');
   $db->disconnect();
    $referer = filter_input(INPUT_POST, 'referer');
    $redirect = "./list.php?category=".$category;
    if(!empty($referer))
        $redirect = $referer;
    js_repl_global( $redirect, "EDIT_SUCCESS");

   exit;
}
?>
