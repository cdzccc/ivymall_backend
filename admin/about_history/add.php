<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("about_history_add");
$id = get_id();
$name         = filter_input(INPUT_POST, 'name');
$article_type = filter_input(INPUT_POST, 'type');
$sub_name     = filter_input(INPUT_POST, 'sub_name');
$desc2        = filter_input(INPUT_POST, 'editor');
$desc         = filter_input(INPUT_POST, 'alt');
$url          = filter_input(INPUT_POST, 'url');
$post_time    = filter_input(INPUT_POST, 'post_time');
$status       = filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 1, 'default' => 0)));
$sort         = filter_input(INPUT_POST, 'sort', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 99999, 'default' => 0)));

if(empty($name) || empty($article_type) || empty($desc2) || empty($post_time) || empty($_FILES['pic']['name'])) {
    $err_msg = "欄位";
    if(empty($name))
        $err_field[] = "名稱";
    if(empty($article_type))
        $err_field[] = "文章類型";
    if(empty($desc2))
        $err_field[] = "內容";
    if(empty($post_time))
        $err_field[] = "文章日期";
    if(empty($_FILES['pic']['name']))
        $err_field[] = "圖片";
    $err_msg .= "'".implode("','", $err_field)."'不可為空值";
    js_go_back_self($err_msg);
    exit;
}

if ($_FILES['pic']['name'] != "none" && is_uploaded_file($_FILES['pic']['tmp_name']))
{
    //重組檔名
    $type = explode(".", $_FILES['pic']['name']);
    $img = $_FILES['pic']['name'];
    if(file_exists(DOCUMENT_ROOT."/upload/".$img)) {
        // js_go_back_self("檔名重複!!");
        // exit;
    }
    @copy($_FILES['pic']['tmp_name'], DOCUMENT_ROOT."/upload/".$img);
}
else {
    js_go_back_global("IMG_ERROR");
    exit;
}

$create_datetime = date("Y-m-d H:i:s");
$create_user     = $_SESSION[SESSION_VARIABLE."_user_id"];
$update_datetime = date("Y-m-d H:i:s");
$update_user     = $_SESSION[SESSION_VARIABLE."_user_id"];


$sql_array = array(
    "id"              => array("2", checkinput_sql($id, 19)),
    "name"            => array("2", checkinput_sql($name, 255)),
    "sub_name"        => array("2", checkinput_sql($sub_name, 255)),
    "Category"        => array("2", "History"),
    "desc"            => array("2", checkinput_sql($desc, 100)),
    "desc2"           => array("2", checkinput_sql($desc2, 9999999)),
    "post_time"       => array("2", checkinput_sql($post_time, 30)),
    "status"          => array("2", checkinput_sql($status, 5)),
    "pic"             => array("2", checkinput_sql($img, 200)),
    "create_datetime" => array("2", checkinput_sql($create_datetime, 50)),
    "create_user"     => array("2", checkinput_sql($create_user, 50)),
    "update_datetime" => array("2", checkinput_sql($update_datetime, 50)),
    "update_user"     => array("2", checkinput_sql($update_user, 50)),
    "sort"            => array("2", checkinput_sql($sort, 5)),
    "type"            => array("2", checkinput_sql($article_type, 5)),
);
$sql_cmd = insert("article", $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
    js_go_back_global("DB_ADD_ERROR");
    exit;
}else{
    add_log('關於我們-企業沿革管理','1');
    $db->disconnect();
    js_repl_global( "./list.php", "ADD_SUCCESS");
    exit;
}
?>
