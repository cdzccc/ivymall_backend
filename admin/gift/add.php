<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("gift_add");
$id = get_id();
$name   = filter_input(INPUT_POST, 'name');
$item_type  = filter_input(INPUT_POST, 'type');
$stock  = filter_input(INPUT_POST, 'stock');
$item_number = filter_input(INPUT_POST, 'item_number', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$status = filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 1, 'default' => 0)));

if((empty($name) && $item_type == 3) || empty($item_type) || empty($item_number[$item_type])) {
    $err_msg = "欄位";
    if((empty($name) && $item_type == 3))
        $err_field[] = "贈品名稱";
    if(empty($item_type))
        $err_field[] = "贈品類型";
    if(empty($item_number[$item_type]))
        $err_field[] = "贈品貨號";
    $err_msg .= "'".implode("','", $err_field)."'不可為空值";
    js_go_back_self($err_msg);
    exit;
}

if ($_FILES['pic']['name'] != "none" && is_uploaded_file($_FILES['pic']['tmp_name']))
{
    //重組檔名
    $type = explode(".", $_FILES['pic']['name']);
    $pic = $_FILES['pic']['name'];
    if(file_exists(DOCUMENT_ROOT."/upload/".$pic)) {
        // js_go_back_self("檔名重複!!");
        // exit;
    }
    @copy($_FILES['pic']['tmp_name'], DOCUMENT_ROOT."/upload/".$pic);
}
else {
    $pic = "";
}


$create_datetime = date("Y-m-d H:i:s");
$create_user     = $_SESSION[SESSION_VARIABLE."_user_id"];
$update_datetime = date("Y-m-d H:i:s");
$update_user     = $_SESSION[SESSION_VARIABLE."_user_id"];

$sql_array = array(
    "id"              => array("2", checkinput_sql($id, 19)),
    "name"            => array("2", checkinput_sql($name, 200)),
    "type"            => array("2", checkinput_sql($item_type, 200)),
    "item_number"     => array("2", checkinput_sql($item_number[$item_type] , 45)),
    "pic"             => array("2", checkinput_sql($pic,200)),
    "status"          => array("2", checkinput_sql($status,2)),
    "stock"           => array("2", checkinput_sql($stock,10)),
    "create_datetime" => array("2", checkinput_sql($create_datetime, 50)),
    "create_user"     => array("2", checkinput_sql($create_user, 50)),
    "update_datetime" => array("2", checkinput_sql($update_datetime, 50)),
    "update_user"     => array("2", checkinput_sql($update_user, 50)),
);

$sql_cmd = insert("gift", $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
   js_go_back_global("DB_ADD_ERROR");
   exit;
}else{
    add_log('贈品上稿管理','1');
   $db->disconnect();
   js_repl_global( "./list.php", "ADD_SUCCESS");
   exit;
}
?>
