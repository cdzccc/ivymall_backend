<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("notice_mail");

//取出總筆數
$sql_cmd = "select * from var where category = 'MAIL' order by `sort` asc";
$rs = $db->query($sql_cmd);
while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
    $row_var[$row['type']]['val'] = $row['value'];
    $row_var[$row['type']]['note'] = $row['note'];
}
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php");?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                網站通知信管理
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box box-primary">
                <form class="form-horizontal" action="./edit.php" method="POST">
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-sm-12">

                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <th width="300px">名稱</th>
                                            <th width="300px">參數</th>
                                        </tr>
                                        <?php
                                            foreach ($row_var as $key => $value) {
                                        ?>
                                        <tr onClick="iCheckThisRow(this);">
                                            <th><?=$value['note']?></th>
                                            <th><input type="text" name="var[<?=$key?>]" value="<?=$value['val']?>" style="width:600px"></th>
                                        </tr>
                                        <?php
                                            }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer text-right">
                        <button type="submit" class="btn btn-primary">儲存</button>
                    </div>
                </form>

            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>