<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("event");

if(isset($_GET['action']))
    $action = $_GET['action'];
else
    $action = "add";

$row["id"] = "";
$row["name"] = "";
$row["desc"] = "";
$row["alt"] = "";
$row["note"] = "";
$row["type"] = "";
$row["value1"] = "";
$row["value2"] = "";
$row["option"] = "";
$row["gift"] = "";
$row["ticket"] = "";
$row["level"] = [];
$row["sdate"] = "";
$row["edate"] = "";
$row["status"] = "1";
$row['item_number'] = "";
$option = [];
if($action == "edit") {
    $sql_cmd = "select * from goods_event where deleted_at is null and id = '".checkinput_sql($_GET['id'],19)."'";
    $rs = $db->query($sql_cmd);
    $row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
}
$goods = [];
$sql_cmd = "select * from goods where type in (1,2,3) and deleted_at is null";
$rs = $db->query($sql_cmd);
$arr_select_goods = [];
while($row_select_goods = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
    $goods[] = $row_select_goods;
}
$sql_cmd = "select * from gift where deleted_at is null and status = 1";
$rs = $db->query($sql_cmd);
$arr_gift_goods = [];
while($row_gift_goods = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
    $arr_gift_goods[] = $row_gift_goods;
}
$sql_cmd = "select * from ticket where delete_at is null and status = 1";
$rs = $db->query($sql_cmd);
$ticket = [];
while($row_ticket = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
    $ticket[] = $row_ticket;
}
if(empty($row["note"])) {
    $row["note"] = '<ul class="attention-list">
                    <li><i class="ivy-icon ivy-icon-list_symbol"></i><span class="text">本活動商品為任選促銷活動商品，優惠價將以各單品之優惠折扣價四捨五入後之金額加總計算。同一筆訂單的所有商品將同時出貨，恕不分開出貨，亦不接受單獨取消或退貨。</span></li>
                    <li><i class="ivy-icon ivy-icon-list_symbol"></i><span class="text">若活動有贈送贈品時，贈品送完為止，且贈品每筆訂單不累送。</span></li>
                    <li><i class="ivy-icon ivy-icon-list_symbol"></i><span class="text">此活動商品無法使用信用卡分期付款，或以紅利扣抵。</span></li>
                    <li><i class="ivy-icon ivy-icon-list_symbol"></i><span class="text">若訂購因抵用單品折價券、E-coupon、購物金、滿額再折價金額，而造成未滿額，則將無法符合加購條件。</span></li>
                    <li><i class="ivy-icon ivy-icon-list_symbol"></i><span class="text">活動商品若同時參與單書折扣優惠（如每日66折），且折扣較本活動優惠更低，該商品建議您獨立結帳；若與本活動其他品項同時訂購，將以本活動折扣計算、不再享其他優惠折扣。</span></li>
                </ul>';
}
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php") ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                行銷活動設定管理
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" method="POST" action="./<?=$action?>.php" enctype="multipart/form-data">
                    <div class="box-header">
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label"><a style="color:red">*</a>活動名稱</label>
                            <div class="col-sm-10">
                                <input name="name" type="name" class="form-control" value="<?=$row["name"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="desc" class="col-sm-2 control-label">活動簡述</label>
                            <div class="col-sm-10">
                                <textarea name="desc" class="form-control"><?=$row["desc"]?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="pic" class="col-sm-2 control-label"><a style="color:red">*</a>活動banner<br><font class="text-red">建議尺寸:1900x400</font></label>
                            <div class="col-sm-10">
                                <?php
                                    if(!empty($row['banner'])) {
                                        echo "<img src='".WEBSITE_URL."upload/".$row['banner']."' width='80px' class='pic'><input type='hidden' class='old_pic' value='".$row['banner']."' name='old_pic'>";
                                        echo "<br>".$row['banner'];
                                        echo "<a href='javascript:void(0);' class='btn btn-danger' onclick='$(\".old_pic\").val(\"\");$(\".pic\").hide();$(this).hide()'>刪除圖片</a>";
                                    }
                                ?>
                                <input type="file" name="pic">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="alt" class="col-sm-2 control-label">alt</label>
                            <div class="col-sm-10">
                                <input name="alt" type="name" class="form-control" value="<?=$row["alt"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="editor" class="col-sm-2 control-label">注意事項</label>
                            <div class="col-sm-10">
                                <textarea name="editor" class="form-control"><?=$row["note"]?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="editor" class="col-sm-2 control-label"><a style="color:red">*</a>活動類型</label>
                            <div class="col-sm-10">
                                <div class="col-sm-12" >
                                    <input name="type" type="radio" <?=($row['type'] == 1)?"checked":""?> value="1" onclick="$('.type_item').hide();">
                                    單件折扣：單書/套書市售價乘以
                                    <input type="text" name="value1[1]" value="<?=($row['type'] == 1)?$row['value1']:""?>">
                                    <input type="hidden" name="value2[1]" value="0">
                                </div>
                                <div class="col-sm-12" >
                                    <input name="type" type="radio" <?=($row['type'] == 2)?"checked":""?> value="2" onclick="$('.type_item').hide();">
                                    滿件折扣：滿
                                    <input type="text" name="value1[2]" value="<?=($row['type'] == 2)?$row['value1']:""?>">
                                    件，市售價乘以
                                    <input type="text" name="value2[2]" value="<?=($row['type'] == 2)?$row['value2']:""?>">
                                </div>
                                <div class="col-sm-12" >
                                    <input name="type" type="radio" <?=($row['type'] == 3)?"checked":""?> value="3" onclick="$('.type_item').hide();">
                                    滿額再折扣：滿
                                    <input type="text" name="value1[3]" value="<?=($row['type'] == 3)?$row['value1']:""?>">
                                    元，整筆訂單再乘以
                                    <input type="text" name="value2[3]" value="<?=($row['type'] == 3)?$row['value2']:""?>">
                                </div>
                                <div class="col-sm-12" >
                                    <input name="type" type="radio" <?=($row['type'] == 4)?"checked":""?> value="4" onclick="$('.type_item').hide();">
                                    滿額折抵：滿
                                    <input type="text" name="value1[4]" value="<?=($row['type'] == 4)?$row['value1']:""?>">
                                    元，整筆訂單再折
                                    <input type="text" name="value2[4]" value="<?=($row['type'] == 4)?$row['value2']:""?>">
                                    元
                                </div>
                                <div class="col-sm-12" >
                                    <input name="type" type="radio" <?=($row['type'] == 5)?"checked":""?> value="5" onclick="$('.type_item').hide();$('.type_item_5').show();">
                                    滿額送好禮：滿
                                    <input type="text" name="value1[5]" value="<?=($row['type'] == 5)?$row['value1']:""?>">
                                    元，送贈品
                                    <input type="text" name="value2[5]" value="<?=($row['type'] == 5)?$row['value2']:""?>">
                                    樣
                                </div>
                                <div class="col-sm-12" >
                                    <input name="type" type="radio" <?=($row['type'] == 6)?"checked":""?> value="6" onclick="$('.type_item').hide();$('.type_item_6').show();">
                                    滿額送折價券：滿
                                    <input type="text" name="value1[6]" value="<?=($row['type'] == 6)?$row['value1']:""?>">
                                    元，送折價券
                                    <input type="hidden" name="value2[6]" value="0">
                                </div>
                                <div class="col-sm-12" >
                                    <input name="type" type="radio" <?=($row['type'] == 7)?"checked":""?> value="7" onclick="$('.type_item').hide();">
                                    會員折扣：整筆訂單再折
                                    <input name="value1[7]" value="<?=($row['type'] == 7)?$row['value1']:""?>" type="number" min="0.1" max="1" step="0.1">
                                    折
									<input type="hidden" name="value2[7]" value="0">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="type" class="col-sm-2 control-label">參與單元</label>
                            <div class="col-sm-10">
                                <?php
                                    foreach($ARRall['goods_category'] as $key => $goods_category) {
                                        $selected = "";
                                        $option = explode(",", $row['option']);
                                        if(in_array($key, $option))
                                            $selected = "checked";
                                        echo "<input type='checkbox' name='option[]' value='".$key."' ".$selected.">".$goods_category;
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="editor" class="col-sm-2 control-label">自選商品貨號</label>
                            <div class="col-sm-10">
                                <select name="item_number[]" class="form-control select2_ajax_category" style="width: 100%;" multiple>
                                    <?php
                                        foreach($goods as $value) {
                                            $checked = "";
                                            $item_number = explode(",", $row['item_number']);
                                            if(in_array($value['item_number'], $item_number))
                                                $checked = "selected";

                                            echo "<option value=".$value['item_number']." ".$checked.">[".$value['item_number']."]".$value['Goods_Name']."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group type_item type_item_5" <?=($row['type']==5)?"":"style='display:none;'"?>>
                            <label for="editor" class="col-sm-2 control-label"><a style="color:red">*</a>選擇贈品</label>
                            <div class="col-sm-10">
                                <select name="gift[]" class="form-control select2_ajax_category" style="width: 100%;" multiple>
                                        <option value="">請選擇</option>
                                    <?php
										$gift = explode(',',$row['gift']);
										
                                        foreach($arr_gift_goods as $value) {
                                            $checked = "";
                                            if(in_array($value['item_number'],$gift))
                                                $checked = "selected";
											if($value['name']==""){
												$sql_cmd = "select Goods_Name from goods where deleted_at is null and item_number = '".$value['item_number']."'";
												$rs = $db->query($sql_cmd);
												$goods = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
												echo "<option value=".$value['item_number']." ".$checked.">[".$value['item_number']."]".$goods['Goods_Name']."</option>";
											}else{
												echo "<option value=".$value['item_number']." ".$checked.">[".$value['item_number']."]".$value['name']."</option>";
											}
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group type_item type_item_6" <?=($row['type']==6)?"":"style='display:none;'"?>>
                            <label for="editor" class="col-sm-2 control-label"><a style="color:red">*</a>選擇折價券</label>
                            <div class="col-sm-10">
                                <select name="ticket" class="form-control select2_ajax_category" style="width: 100%;">
                                        <option value="">請選擇</option>
                                    <?php
                                        foreach($ticket as $value) {
                                            $checked = "";
                                            if($value['ticket_id'] == $row['ticket'])
                                                $checked = "selected";
                                            echo "<option value=".$value['ticket_id']." ".$checked.">".$value['name']."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">上架日期</label>
                            <div class="col-sm-10">
                                <div class="input-group">
                                    <input class="datetimepicker" name="sdate" type="text" class="form-control pull-right rangepicker" value="<?=$row['sdate']?>">
                                     - 
                                    <input class="datetimepicker" name="edate" type="text" class="form-control pull-right rangepicker" value="<?=$row['edate']?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="type" class="col-sm-2 control-label"><a style="color:red">*</a>參與等級</label>
                            <div class="col-sm-10 control-text">
                                <?php
                                    foreach($ARRall['level'] as $key => $level) {
                                        $level_val = [];
                                        if(!empty($row['level']) || $row['level'] == '0')
                                            $level_val = explode(",", $row['level']);
                                        $checked = "";
                                        if(in_array($key, $level_val))
                                            $checked = "checked";
                                        echo "<input type='checkbox' name='level[]' value='".$key."' ".$checked.">".$level;
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="sort" class="col-sm-2 control-label">排序</label>
                            <div class="col-sm-10">
                                <input id="sort" name="sort" type="text" class="form-control" value="<?=$row["sort"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">狀態</label>
                            <div class="col-sm-10 control-text">
                                <label>
                                    <input name="status" type="radio" <?=($row['status'] == 1)?"checked":""?>
                                       value="1">啟用
                                </label>
                                <label>
                                    <input name="status" type="radio" <?=($row['status'] == 0)?"checked":""?>
                                       value="0">停用
                                </label>
                            </div>
                        </div>
                        <?php if($action == "edit") {?>
                        <div class="form-group">
                            <label for="sort" class="col-sm-2 control-label">建立時間</label>
                            <div class="col-sm-10 control-text">
                                <?=$row['create_datetime']?>
                            </div>
                        </div>
                        <?php }?>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-right">
                        <input type="hidden" name="id" value="<?=$row['id']?>">
                        <a href="javascript:history.back()" class="btn btn-danger" style="margin-right: 10px;">取消，並返回上一層</a>
                        <button type="button" class="btn btn-primary" onclick="check_file();">儲存</button>
                        <input type="hidden" name="referer" value="<?=(!empty($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:"")?>">
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>