<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("event_add");
$id = get_id();
$name        = filter_input(INPUT_POST, 'name');
$desc        = filter_input(INPUT_POST, 'desc');
$alt         = filter_input(INPUT_POST, 'alt');
$note        = filter_input(INPUT_POST, 'editor');
$event_type  = filter_input(INPUT_POST, 'type');
$value1      = filter_input(INPUT_POST, 'value1', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$value2      = filter_input(INPUT_POST, 'value2', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$option      = filter_input(INPUT_POST, 'option', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$item_number = filter_input(INPUT_POST, 'item_number', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$gift        = filter_input(INPUT_POST, 'gift', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$ticket      = filter_input(INPUT_POST, 'ticket');
$sdate       = filter_input(INPUT_POST, 'sdate');
$edate       = filter_input(INPUT_POST, 'edate');
$level       = filter_input(INPUT_POST, 'level', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$status      = filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 1, 'default' => 0)));
$sort        = filter_input(INPUT_POST, 'sort', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 9999, 'default' => 0)));

if(empty($name) 
	|| empty($event_type)
	|| ($value1[$event_type]=="")
	|| ($value2[$event_type]=="")
	|| empty($level)
	|| ($event_type == 5 && empty($gift))
	|| ($event_type == 6 && empty($ticket))
	){
    $err_msg = "欄位";
    if(empty($name))
        $err_field[] = "活動名稱";
    if(empty($event_type))
        $err_field[] = "活動類型";
    if(empty($_FILES['pic']['name']))
        $err_field[] = "活動banner";
    if($value1[$event_type]=="")
        $err_field[] = "活動類型-輸入框內";
    if($value2[$event_type]=="")
        $err_field[] = "活動類型-輸入框內";
    if($event_type == 5
    && empty($gift))
        $err_field[] = "選擇贈品";
    if(($event_type == 6
    && (empty($ticket))))
        $err_field[] = "選擇折價券";
	if(empty($level))
        $err_field[] = "參與等級";
    $err_msg .= "'".implode("','", $err_field)."'不可為空值";
    js_go_back_self($err_msg);
    exit;
}

if($event_type == 7 && $value1[$event_type] > 1) {
    js_go_back_self("會員折扣必須小於1");
    exit;    
}

if ($_FILES['pic']['name'] != "none" && is_uploaded_file($_FILES['pic']['tmp_name']))
{
    //重組檔名
    $type = explode(".", $_FILES['pic']['name']);
    $pic = $_FILES['pic']['name'];
    if(file_exists(DOCUMENT_ROOT."/upload/".$pic)) {
        // js_go_back_self("檔名重複!!");
        // exit;
    }
    @copy($_FILES['pic']['tmp_name'], DOCUMENT_ROOT."/upload/".$pic);
}
else {
    js_go_back_global("IMG_ERROR");
    exit;
}

$create_datetime = date("Y-m-d H:i:s");
$create_user     = $_SESSION[SESSION_VARIABLE."_user_id"];
$update_datetime = date("Y-m-d H:i:s");
$update_user     = $_SESSION[SESSION_VARIABLE."_user_id"];
$sql_array = array(
    "id"              => array("2", checkinput_sql($id, 19)),
    "name"            => array("2", checkinput_sql($name, 100)),
    "desc"            => array("2", checkinput_sql($desc, 500)),
    "banner"          => array("2", checkinput_sql($pic , 200)),
    "alt"             => array("2", checkinput_sql($alt, 100)),
    "note"            => array("2", checkinput_sql($note, 9999999)),
    "type"            => array("2", checkinput_sql($event_type , 2)),
    "value1"          => array("2", checkinput_sql($value1[$event_type], 5)),
    "value2"          => array("2", checkinput_sql($value2[$event_type], 5)),
    "option"          => array("2", checkinput_sql(implode(",", $option) , 13)),
    "item_number"     => array("2", checkinput_sql(implode(",", $item_number), 255)),
    "gift"            => array("2", checkinput_sql(implode(",", $gift), 99999)),
    "ticket"          => array("2", checkinput_sql($ticket , 19)),
    "sdate"           => array("2", checkinput_sql($sdate, 20)),
    "edate"           => array("2", checkinput_sql($edate , 20)),
    "level"           => array("2", checkinput_sql(implode(",", $level) , 20)),
    "sort"            => array("2", checkinput_sql($sort,5)),
    "status"          => array("2", checkinput_sql($status,2)),
    "create_datetime" => array("2", checkinput_sql($create_datetime, 50)),
    "create_user"     => array("2", checkinput_sql($create_user, 50)),
    "update_datetime" => array("2", checkinput_sql($update_datetime, 50)),
    "update_user"     => array("2", checkinput_sql($update_user, 50)),
);
$sql_cmd = insert("goods_event", $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
   js_go_back_global("DB_ADD_ERROR");
   exit;
}else{
    add_log('行銷活動設定管理','1');
   $db->disconnect();
   js_repl_global( "./list.php", "ADD_SUCCESS");
   exit;
}
?>
