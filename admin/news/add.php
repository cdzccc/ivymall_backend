<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("news_add");
$id = get_id();
$name        = filter_input(INPUT_POST, 'name');
$header      = filter_input(INPUT_POST, 'header');
$url         = filter_input(INPUT_POST, 'url');
$status      = filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 1, 'default' => 0)));
$sort        = filter_input(INPUT_POST, 'sort', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 99999, 'default' => 0)));

if(empty($name)) {
    $err_msg = "欄位";
    if(empty($name))
        $err_field[] = "名稱";
    $err_msg .= "'".implode("','", $err_field)."'不可為空值";
    js_go_back_self($err_msg);
    exit;
}

$create_datetime = date("Y-m-d H:i:s");
$create_user     = $_SESSION[SESSION_VARIABLE."_user_id"];
$update_datetime = date("Y-m-d H:i:s");
$update_user     = $_SESSION[SESSION_VARIABLE."_user_id"];


$sql_array = array(
    "id"              => array("2", checkinput_sql($id, 19)),
    "name"            => array("2", checkinput_sql($name, 255)),
    "Category"        => array("2", "News"),
    "desc"            => array("2", ""),
    "url"             => array("2", checkinput_sql($url, 100)),
    "stime"           => array("2", ""),
    "etime"           => array("2", ""),
    "status"          => array("2", checkinput_sql($status, 5)),
    "pic"             => array("2", ""),
    "pic_m"           => array("2", ""),
    "desc2"           => array("2", ""),
    "create_datetime" => array("2", checkinput_sql($create_datetime, 50)),
    "create_user"     => array("2", checkinput_sql($create_user, 50)),
    "update_datetime" => array("2", checkinput_sql($update_datetime, 50)),
    "update_user"     => array("2", checkinput_sql($update_user, 50)),
    "sort"            => array("2", checkinput_sql($sort, 5)),
    "header"          => array("2", checkinput_sql($header, 5)),
);
$sql_cmd = insert("message", $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
   js_go_back_global("DB_ADD_ERROR");
   exit;
}else{
    add_log('即時消息管理','1');
   $db->disconnect();
   js_repl_global( "./list.php", "ADD_SUCCESS");
   exit;
}
?>
