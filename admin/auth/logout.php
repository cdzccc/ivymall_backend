<?php
include "../config.inc.php";
session_start();
unset($_SESSION[SESSION_VARIABLE."_user_s"]);
unset($_SESSION[SESSION_VARIABLE."_user_id"]);
unset($_SESSION[SESSION_VARIABLE."_priv_s"]);
session_unset($_SESSION[SESSION_VARIABLE."_user_s"]);
session_unset($_SESSION[SESSION_VARIABLE."_user_id"]);
session_unset($_SESSION[SESSION_VARIABLE."_priv_s"]);

header("location: /ivy_mall_backend/admin/login.php");

?>
