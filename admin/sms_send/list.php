<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("news");
$sql_where = "";

$title  = filter_input(INPUT_GET, 'title');
$status = filter_input(INPUT_GET, 'status');
$s_date  = filter_input(INPUT_GET, 's_date');
$e_date  = filter_input(INPUT_GET, 'e_date');

if(!empty($title))
    $sql_where .= " and title like '%".checkinput_sql($title,255)."%'";
if(!empty($s_date))
    $sql_where .= " and create_datetime >= '".checkinput_sql($s_date,19)."'";
if(!empty($e_date))
    $sql_where .= " and create_datetime <= '".checkinput_sql($e_date,19)."'";
if(in_array($status, ["0","1"]))
    $sql_where .= " and result = '".checkinput_sql($status,3)."'";

//取出總筆數
$sql_cmd = "select count(*) from sms_send where deleted_at is null ".$sql_where." ";
$rs = $db->query($sql_cmd);
// print_r($rs);
$row = $rs->fetchRow();
$total = $row[0];

$mypage = new page;
$pages = $mypage->set_page($total, $PAGE_NUM);

//設定分頁的初始值
if (empty($_GET['num']))
{
    $num = 1;
}else{
    $num = intval($_GET['num']);
}

$sql_cmd = "select * from sms_send where deleted_at is null ".$sql_where." order by id desc ".$pages[$num];
$rs_banner  = $db->query($sql_cmd);
$tmp = $_GET;
unset($tmp['num']);
$val = http_build_query($tmp);
$page = $mypage->multi($total , $PAGE_NUM, $num, "./list.php", "", $val);
?><!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php"); ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                簡訊發送管理
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box">
                <div class="box-header">
                    <form action="./list.php" method="GET">
                        <div style="float:left; display:inline-block; width:180px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">搜尋</button>
                                </div>
                                <input type="text" name="title" class="form-control input-sm"
                                       placeholder="名稱" value="<?=(!empty($_GET['title']))?$_GET['title']:""?>">
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:328px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">建立時間</button>
                                </div>
                                <input type="text" name="s_date" class="form-control input-sm datetimepicker"
                                       placeholder="起始日" value="<?=(!empty($_GET['s_date']))?$_GET['s_date']:""?>">
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:300px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">~</button>
                                </div>
                                <input type="text" name="e_date" class="form-control input-sm datetimepicker"
                                       placeholder="結束日" value="<?=(!empty($_GET['e_date']))?$_GET['e_date']:""?>">
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:180px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">發送結果</button>
                                </div>
                                <select class="form-control input-sm" name="status">
                                    <option value="all">請選擇</option>
                                    <option value="1" <?=($status == "1")?"selected":""?>>成功</option>
                                    <option value="0" <?=($status == "0")?"selected":""?>>失敗</option>
                                </select>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success btn-sm pull-right">送出</button>
                        <a class="btn btn-danger btn-sm pull-right" href="./list.php" style="margin-right: 10px;">清除</a>
                        <div style="float:left; display:inline-block; width:100%;margin-top: 10px;">
                            <a class="pull-right btn btn-success btn-sm" href="./detail.php?action=add">新增</a>
                        </div>
                    </form>
                </div>
                <div class="box-body no-padding">
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                <th>標題</th>
                                <th>發送結果</th>
                                <th>發送總筆數</th>
                                <th>建立時間</th>
                                <th>操作</th>
                            </tr>
                            <?php
                            if($rs_banner->numRows() > 0) {
                            while($row = $rs_banner->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                            ?>
                            <tr onClick="iCheckThisRow(this);">
                                <th><?=$row['title']?></th>
                                <th><?=($row['result']==0)?"失敗":"成功"?></th>
                                <th><?=$row['send_count']?></th>
                                <th><?=$row['create_datetime']?></th>
                                <th>
                                    <a class="btn btn-xs btn-primary" href="./detail.php?action=edit&id=<?=$row['id']?>">檢視</a>
                                </th>
                            </tr>
                            <?php }} ?>
                        </tbody>
                    </table>
                </div>
            </div>
		<?=$page?>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>