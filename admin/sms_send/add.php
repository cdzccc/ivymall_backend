<?php
include("../config.php");
require DOCUMENT_ROOT.'/vendor/autoload.php';

ClassJscript::islogin();
ClassJscript::isadmino("news_add");
$id = get_id();
$title  = filter_input(INPUT_POST, 'title');
$type   = filter_input(INPUT_POST, 'type');
$mobile = filter_input(INPUT_POST, 'mobile');
$content = filter_input(INPUT_POST, 'editor');

if(empty($title)
    || empty($type)
    || ($type == 1 && empty($mobile))
    || empty($content)) {
    $err_msg = "欄位";
    if(empty($title))
        $err_field[] = "標題";
    if(empty($type))
        $err_field[] = "個人/群組";
    if(($type == 1 && empty($mobile)))
        $err_field[] = "手機號碼";
    if(empty($content))
        $err_field[] = "發送內容";
    $err_msg .= "'".implode("','", $err_field)."'不可為空值";
    js_go_back_self($err_msg);
    exit;
}

$create_datetime = date("Y-m-d H:i:s");
$create_user     = $_SESSION[SESSION_VARIABLE."_user_id"];
$update_datetime = date("Y-m-d H:i:s");
$update_user     = $_SESSION[SESSION_VARIABLE."_user_id"];


$sql_array = array(
    "id"              => array("2", checkinput_sql($id, 19)),
    "title"           => array("2", checkinput_sql($title, 255)),
    "type"            => array("2", checkinput_sql($type, 2000)),
    "mobile"          => array("2", checkinput_sql($mobile, 2000)),
    "content"         => array("2", checkinput_sql($content, 9999999)),
    "result"          => array("2", "0"),
    "send_count"      => array("2", "0"),
    "create_datetime" => array("2", checkinput_sql($create_datetime, 50)),
    "create_user"     => array("2", checkinput_sql($create_user, 50)),
    "update_datetime" => array("2", checkinput_sql($update_datetime, 50)),
    "update_user"     => array("2", checkinput_sql($update_user, 50)),
);
$sql_cmd = insert("sms_send", $sql_array);
$rs = $db->query($sql_cmd);
// print_r($rs);
$pear = new PEAR();
if ($pear->isError($rs))
{
    js_go_back_global("DB_ADD_ERROR");
    exit;
}else{
    // send sms
    $mobile_list = [];
    if($type == 1) {
        // echo "------------------------------------------------";
        $mobile_list = explode(",", $mobile);
    } else {
        switch ($type) {
            case '2':
                $sql_where = "";
                // echo "------------------------------------------------1111";
                break;
            case '3':
                $sql_where = " and level = 0";
                break;
            case '4':
                $sql_where = " and level = 1";
                break;
            case '5':
                $sql_where = " and level = 2";
                break;
            case '6':
                $sql_where = " and level = 3";
                break;
        }
        $sql_cmd = "select * from customer where deleted_at is null".$sql_where;
        $rs = $db->query($sql_cmd);
        while ($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            if(! empty($row['Customer_Phone'])) {
                $mobile_list[] = $row['Customer_Phone'];
            }
        }
    }
    $content = str_replace(['<p>', '</p>'], '', $content);
    foreach ($mobile_list as $key => $mobile) {
        $mobile = preg_replace("/^0/", "886", $mobile);
        $client = new infobip\api\client\SendSingleTextualSms(
            new infobip\api\configuration\BasicAuthConfiguration('Ivyeducation','Ivy23317600')
        );
        $requestBody = new infobip\api\model\sms\mt\send\textual\SMSTextualRequest();
        $requestBody->setFrom('cqt');
        $requestBody->setTo($mobile);
        $requestBody->setText($content);
        $response = $client->execute($requestBody);
    }

    $sql_array = array(
        "result"     => array("2", "1"),
        "send_count" => array("2", count($mobile_list)),
    );
    $sql_cmd = update("sms_send", array("id", $id), $sql_array);
    $rs = $db->query($sql_cmd);
    // print_r($rs);


    add_log('簡訊發送管理','1');
    $db->disconnect();
    js_repl_global( "./list.php", "ADD_SUCCESS");
    exit;
}
?>
