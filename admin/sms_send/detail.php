<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("news");

if(isset($_GET['action']))
    $action = $_GET['action'];
else
    $action = "add";

$row["id"] = "";
$row["title"] = "";
$row["type"] = "";
$row["mobile"] = "";
$row["content"] = "";

if($action == "edit") {
    $sql_cmd = "select * from sms_send where deleted_at is null and id = '".checkinput_sql($_GET['id'],19)."'";
    $rs = $db->query($sql_cmd);
    $row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
}

?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php") ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                簡訊發送管理
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" method="POST" action="./<?=$action?>.php" enctype="multipart/form-data">
                    <div class="box-header">
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="title" class="col-sm-2 control-label">標題</label>
                            <div class="col-sm-10">
                                <input name="title" type="text" class="form-control" value="<?=$row["title"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="url" class="col-sm-2 control-label">個人/群組</label>
                            <div class="col-sm-10 control-text">
                                <label>
                                    <input type="radio" name="type" value="1" <?=($row['type']==1)?"checked":""?> onclick="$('.input_mobile').show();">特定對象
                                </label>
                                <label>
                                    <input type="radio" name="type" value="2" <?=($row['type']==2)?"checked":""?> onclick="$('.input_mobile').hide();">所有會員
                                </label>
                                <label>
                                    <input type="radio" name="type" value="3" <?=($row['type']==3)?"checked":""?> onclick="$('.input_mobile').hide();">種子會員
                                </label>
                                <label>
                                    <input type="radio" name="type" value="4" <?=($row['type']==4)?"checked":""?> onclick="$('.input_mobile').hide();">幼苗會員
                                </label>
                                <label>
                                    <input type="radio" name="type" value="5" <?=($row['type']==5)?"checked":""?> onclick="$('.input_mobile').hide();">大樹會員
                                </label>
                                <label>
                                    <input type="radio" name="type" value="6" <?=($row['type']==6)?"checked":""?> onclick="$('.input_mobile').hide();">神木會員
                                </label>
                            </div>
                        </div>
                        <div class="form-group input_mobile" style="<?=($row['type']==1)?"display:block;":"display:none;"?>">
                            <label for="mobile" class="col-sm-2 control-label">手機號碼</label>
                            <div class="col-sm-10">
                                <input name="mobile" type="text" class="form-control" value="<?=$row["mobile"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="sort" class="col-sm-2 control-label">發送內容</label>
                            <div class="col-sm-10 control-text">
                                <?php if($action == "edit"):?>
                                    <?=$row["content"]?>
                                <?php else:?>
                                    <textarea name="editor" class="form-control" <?=($action == "edit")?"readonly":""?>><?=$row["content"]?></textarea>
                                <?php endif;?>
                            </div>
                        </div>
                        <?php if($action == "edit") {
                        ?>
                        <div class="form-group">
                            <label for="mobile" class="col-sm-2 control-label">發送結果</label>
                            <div class="col-sm-10 control-text">
                                <?=($row['result']==0)?"失敗":"成功"?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="mobile" class="col-sm-2 control-label">發送總筆數</label>
                            <div class="col-sm-10 control-text">
                                <?=$row['send_count']?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="mobile" class="col-sm-2 control-label">建立時間</label>
                            <div class="col-sm-10 control-text">
                                <?=date("Y/m/d H:i:s", strtotime($row['create_datetime']))?>
                            </div>
                        </div>

                        <?php
                        }
                        ?>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-right">
                        <input type="hidden" name="id" value="<?=$row['id']?>">
                        <a href="javascript:history.back()" class="btn btn-danger" style="margin-right: 10px;">取消，並返回上一層</a>
                        <?php if($action == "add") {
                        ?>
                        <button type="submit" class="btn btn-primary">儲存</button>
                        <?php
                        }
                        ?>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>