<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("point");

if(isset($_GET['action']))
    $action = $_GET['action'];
else
    $action = "add";
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php") ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                熊贈點加點/扣點
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" method="POST" action="./<?=$action?>.php" enctype="multipart/form-data">
                    <div class="box-header">
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">個人/群組</label>
                            <div class="col-sm-10 control-text">
                                <?php
                                    foreach ($ARRall['send_to'] as $key => $value) {
                                        if($key == 1) 
                                            $show_customer = "$('.Customer_IDS').show();";
                                        else
                                            $show_customer = "$('.Customer_IDS').hide();";
                                ?>
                                <label>
                                    <input name="option" type="radio" value="<?=$key?>" onclick="<?=$show_customer?>"><?=$value?>
                                </label>
                                <?php
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="form-group Customer_IDS" style="display:none;">
                            <label for="Customer_IDS" class="col-sm-2 control-label">UID</label>
                            <div class="col-sm-10">
                                <input name="Customer_IDS" type="text" class="form-control" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="type" class="col-sm-2 control-label">類型</label>
                            <div class="col-sm-10 control-text">
                                <label>
                                    <input name="type" type="radio" value="1" onclick="$('.time').show();">系統贈點
                                </label>
                                <label>
                                    <input name="type" type="radio" value="2" onclick="$('.time').hide();">系統扣除
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="point" class="col-sm-2 control-label">點數</label>
                            <div class="col-sm-10">
                                <input name="point" type="text" class="form-control" value="">
                            </div>
                        </div>
                        <div class="form-group time">
                            <label for="enabled_date" class="col-sm-2 control-label">啟用時間</label>
                            <div class="col-sm-10">
                                <input name="enabled_date" type="text" class="form-control datetimepicker3" value="">
                            </div>
                        </div>
                        <div class="form-group time">
                            <label for="email" class="col-sm-2 control-label">到期時間</label>
                            <div class="col-sm-10">
                                <input name="end_date" type="text" class="form-control datetimepicker4" value="">
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-right">
                        <input type="hidden" name="id" value="<?=$row['id']?>">
                        <a href="./list.php" class="btn btn-danger" style="margin-right: 10px;">取消，並返回上一層</a>
                        <button type="submit" class="btn btn-primary" onclick="if(!confirm('送出後無法修改，是否發送？')){ return false;}">儲存</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>