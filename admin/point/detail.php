<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("point_record");

if(isset($_GET['action']))
    $action = $_GET['action'];
else
    $action = "add";
$sql_cmd = "select l.*,s.total from point_summary as s
        join point_list as l on s.Customer_ID = l.Customer_ID 
        where l.id = '".checkinput_sql($_GET['id'],19)."' ";
$rs = $db->query($sql_cmd);
$row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);

?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php") ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                熊贈點紀錄管理
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" method="POST" action="./<?=$action?>.php" enctype="multipart/form-data">
                    <div class="box-header">
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">UID</label>
                            <div class="col-sm-10 control-text">
                                <?=$row['id']?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">會員ID</label>
                            <div class="col-sm-10 control-text">
                                <?=$row['Customer_ID']?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">會員等級</label>
                            <div class="col-sm-10 control-text">
                                <?=$ARRall['level'][$row['level']]?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">類型</label>
                            <div class="col-sm-10 control-text">
                                <?=$ARRall['point_mode'][$row['mode']]?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">點數</label>
                            <div class="col-sm-10 control-text">
                                <?=$row['point']?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">現有總點數</label>
                            <div class="col-sm-10 control-text">
                                <?=$row['left']?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">生效時間</label>
                            <div class="col-sm-10 control-text">
                                <?=$row['enabled_date']?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">到期時間</label>
                            <div class="col-sm-10 control-text">
                                <?=$row['end_date']?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">狀態</label>
                            <div class="col-sm-10 control-text">
                                <?=($row['status']==0)?"失效":"有效"?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">訂單編號</label>
                            <div class="col-sm-10 control-text">
                                <?=$row['Order_ID']?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">建立時間</label>
                            <div class="col-sm-10 control-text">
                                <?=$row['datetime']?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">刪除時間</label>
                            <div class="col-sm-10 control-text">
                                <?=$row['deleted_at']?>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-right">
                        <input type="hidden" name="id" value="<?=$row['id']?>">
                        <a href="javascript:history.back()" class="btn btn-danger" style="margin-right: 10px;">取消，並返回上一層</a>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>