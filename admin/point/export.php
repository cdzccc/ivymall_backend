<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("point_record");
$sql_where = "";

$title  = filter_input(INPUT_GET, 'title');
$mode  = filter_input(INPUT_GET, 'mode');
$status = filter_input(INPUT_GET, 'status');
$s_date  = filter_input(INPUT_GET, 's_date');
$e_date  = filter_input(INPUT_GET, 'e_date');

if(!empty($title))
    $sql_where .= " and (l.Order_ID like '%".checkinput_sql($title,255)."%' or l.Customer_ID like '%".checkinput_sql($title,255)."%')";
if(in_array($status, ["0","1"]))
    $sql_where .= " and l.status = '".checkinput_sql($status,3)."'";
if(!empty($s_date))
    $sql_where .= " and l.datetime >= '".checkinput_sql($s_date,19)."'";
if(!empty($e_date))
    $sql_where .= " and l.datetime <= '".checkinput_sql($e_date,19)."'";
if(in_array($mode, ["1","2","3","4","5"]))
    $sql_where .= " and l.mode = '".checkinput_sql($mode,3)."'";

$sql_cmd = "select l.*,s.total from point_summary as s
        join point_list as l on s.Customer_ID = l.Customer_ID 
        where 1=1 ".$sql_where." ";
$rs_link  = $db->query($sql_cmd);
$rs[] = [
        "ID",
        "會員ID",
        "類型",
        "點數",
        "現有總點數",
        "生效時間",
        "到期時間",
        "狀態",
        "訂單編號",
        "建立時間",
        "刪除時間",
    ];
while($row = $rs_link->fetchRow(MDB2_FETCHMODE_ASSOC)) {
    $rs[] = [
        $row['id'],
        $row['Customer_ID'],
        $ARRall['point_mode'][$row['mode']],
        $row['point'],
        $row['total'],
        $row['enabled_date'],
        $row['end_date'],
        ($row["status"] == 1)?"生效":"失效",
        $row['Order_ID'],
        $row['datetime'],
        $row['deleted_at'],
    ];
}
header('Content-Encoding: UTF-8');
header("Content-type: application/x-download");
header("Content-disposition: attachment; filename=point.csv");
echo arr_to_csv($rs);
?>



