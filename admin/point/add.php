<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("point_record_add");
$id = get_id();
$Customer_IDS   = filter_input(INPUT_POST, 'Customer_IDS');
$type  = filter_input(INPUT_POST, 'type');
$point  = filter_input(INPUT_POST, 'point');
$option = filter_input(INPUT_POST, 'option');
$enabled_date = filter_input(INPUT_POST, 'enabled_date');
$end_date = filter_input(INPUT_POST, 'end_date');
$status = 1;
if($type == 1)
    $mode = 3;
else
    $mode = 5;
if(empty($option) || empty($type) || empty($point) || (empty($enabled_date) && $type == 1) || (empty($end_date) && $type == 1) || ($option == 1 && empty($Customer_IDS))) {
    $err_msg = "欄位";
    if(empty($option))
        $err_field[] = "個人/群組";
    if(empty($type))
        $err_field[] = "類型";
    if(empty($point))
        $err_field[] = "點數";
    if((empty($enabled_date) && $type == 1))
        $err_field[] = "啟用時間";
    if((empty($end_date) && $type == 1))
        $err_field[] = "到期時間";
    if($option == 1 && empty($Customer_IDS))
        $err_field[] = "UID";
    $err_msg .= "'".implode("','", $err_field)."'不可為空值";
    js_go_back_self($err_msg);
    exit;
}
if($type==1){
	$end_date = str_replace("/", "-", $end_date);
	if($end_date <=date("Y-m-d H:i:s")){
		js_go_back_self("到期時間不可小於當前時間");
		exit;
	}
}

$create_datetime = date("Y-m-d H:i:s");
$create_user     = $_SESSION[SESSION_VARIABLE."_user_id"];
$update_datetime = date("Y-m-d H:i:s");
$update_user     = $_SESSION[SESSION_VARIABLE."_user_id"];

$sql_where = "";
switch($option) {
    case "1";
        $sql_where = " and Customer_ID in (".$Customer_IDS.")";
    break;
    case "2";
        $sql_where = "";
    break;
    case "3";
        $sql_where = " and level = 0";
    break;
    case "4";
        $sql_where = " and level = 1";
    break;
    case "5";
        $sql_where = " and level = 2";
    break;
    case "6";
        $sql_where = " and level = 3";
    break;
}

$sql_cmd = "select * from customer where deleted_at is null ".$sql_where;
$rs = $db->query($sql_cmd);

while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
    $sql_cmd = "select * from point_summary where Customer_ID = '".$row['Customer_ID']."'";
    $rs_summary = $db->query($sql_cmd);
	
	if ($mode == 3) {
		$enabled_date = str_replace("/", "-", $enabled_date);
		$end_date = str_replace("/", "-", $end_date);
	}
	if ($mode == 5) {
		$enabled_date = date("Y-m-d H:i");
		$end_date = date("Y-m-d H:i");
	}
    

    if($enabled_date <= date("Y-m-d H:i") && $end_date >= date("Y-m-d H:i")) {
        if($rs_summary->numRows() > 0) {
            $row_point = $rs_summary->fetchRow(MDB2_FETCHMODE_ASSOC);
            if ($mode == 3) {
                $point_total = $row_point['total'] + $point;
                $point_in = $row_point['in'] + $point;
                $point_out = $row_point['out'];
            }
            if ($mode == 5) {
                $point_total = $row_point['total'] - $point;
                $point_in = $row_point['in'];
                $point_out = $row_point['out'] - $point;
                $point_total = $point_total < 0 ? 0 : $point_total;
                $point_in = $point_in < 0 ? 0 : $point_in;
            }
            $sql_cmd = "update point_summary set `in` = ".$point_in.", `out` = ".$point_out.", total = ".$point_total." where Customer_ID = '".$row['Customer_ID']."'";
            $rs2 = $db->query($sql_cmd);
        }
        else {
            $point_summary_id = get_id();
            $sql_array = array(
                "id"          => array("2", $point_summary_id),
                "Customer_ID" => array("2", $row['Customer_ID']),
                "in"          => array("2", $point),
                "out"         => array("2", 0),
                "total"       => array("2", $point),
            );
            $sql_cmd = insert("point_summary", $sql_array);
            $rs2 = $db->query($sql_cmd);
            $row_point['total'] = 0;
        }
    }
    else {
		$row_point = $rs_summary->fetchRow(MDB2_FETCHMODE_ASSOC);
		if($end_date < date("Y-m-d H:i")){
			$status = 1;
			$point_total = $row_point['total'];
		}
		else{
			$status = 0;	
			$point_total = $row_point['total'];
		}
    }
    $point_list_id = get_id();
    $sql_array = array(
        "id"           => array("2", checkinput_sql($point_list_id, 19)),
        "Customer_ID"  => array("2", checkinput_sql($row['Customer_ID'], 200)),
        "point"        => array("2", checkinput_sql($point, 200)),
        "mode"         => array("2", checkinput_sql($mode , 45)),
        "type"         => array("2", checkinput_sql($type,2)),
        "datetime"     => array("2", checkinput_sql($create_datetime, 50)),
        "Order_ID"     => array("2", checkinput_sql("", 50)),
        "left"         => array("2", $point_total),
        "status"       => array("2", checkinput_sql($status, 50)),
        "level"        => array("2", checkinput_sql($row['level'], 50)),
    );

    if(! empty($enabled_date)) {
        $sql_array['enabled_date'] = array("2", checkinput_sql($enabled_date, 50));
    }

    if(! empty($end_date)) {
        $sql_array['end_date'] = array("2", checkinput_sql($end_date, 50));
    }

    $sql_cmd = insert("point_list", $sql_array);
    $db->query($sql_cmd);

}

$pear = new PEAR();
if ($pear->isError($rs))
{
    js_go_back_global("DB_ADD_ERROR");
    exit;
}else{
    add_log('熊贈點加點/扣點','1');
    $db->disconnect();
    js_repl_global( "./trade.php", "ADD_SUCCESS");
    exit;
}

?>
