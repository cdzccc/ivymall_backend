<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("point_record");
$sql_where = "";

$title  = filter_input(INPUT_GET, 'title');
$mode  = filter_input(INPUT_GET, 'mode');
$status = filter_input(INPUT_GET, 'status');
$s_date  = filter_input(INPUT_GET, 's_date');
$e_date  = filter_input(INPUT_GET, 'e_date');

if(!empty($title))
    $sql_where .= " and (l.Order_ID like '%".checkinput_sql($title,255)."%' or l.Customer_ID like '%".checkinput_sql($title,255)."%')";
if(in_array($status, ["0","1"]))
    $sql_where .= " and l.status = '".checkinput_sql($status,3)."'";
if(!empty($s_date))
    $sql_where .= " and l.datetime >= '".checkinput_sql($s_date,19)."'";
if(!empty($e_date))
    $sql_where .= " and l.datetime <= '".checkinput_sql($e_date,19)."'";
if(in_array($mode, ["1","2","3","4","5"]))
    $sql_where .= " and l.mode = '".checkinput_sql($mode,3)."'";

//取出總筆數
$sql_cmd = "select count(*) from point_summary as s
        join point_list as l on s.Customer_ID = l.Customer_ID 
        where 1=1 ".$sql_where." ";
$rs = $db->query($sql_cmd);
$row = $rs->fetchRow();
$total = $row[0];

$mypage = new page;
$pages = $mypage->set_page($total, $PAGE_NUM);

//設定分頁的初始值
if (empty($_GET['num']))
{
    $num = 1;
}else{
    $num = intval($_GET['num']);
}

$sql_cmd = "select l.*,s.total from point_summary as s
        join point_list as l on s.Customer_ID = l.Customer_ID 
        where 1=1 ".$sql_where." order by l.datetime desc ".$pages[$num];
$rs_link  = $db->query($sql_cmd);
$tmp = $_GET;
unset($tmp['num']);
$val = http_build_query($tmp);
$page = $mypage->multi($total , $PAGE_NUM, $num, "./list.php", "", $val);
?><!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php"); ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                熊贈點紀錄管理
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box">
                <div class="box-header">
                    <form action="./list.php" method="GET" name="search">
                        <div style="float:left; display:inline-block; width:350px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">搜尋</button>
                                </div>
                                <input type="text" name="title" class="form-control input-sm"
                                       placeholder="會員ID、訂單編號" value="<?=(!empty($_GET['title']))?$_GET['title']:""?>">
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:200px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">類型</button>
                                </div>
                                <select class="form-control input-sm" name="mode">
                                    <option value="all">全部</option>
                                    <?php 
                                        foreach($ARRall['point_mode'] as $key=> $point_mode) {
                                    ?>
                                    <option value="<?=$key?>" <?=($mode == $key)?"selected":""?>><?=$point_mode?></option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:328px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">建立時間</button>
                                </div>
                                <input type="text" name="s_date" class="form-control input-sm datetimepicker"
                                       placeholder="起始日" value="<?=(!empty($_GET['s_date']))?$_GET['s_date']:""?>">
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:300px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">~</button>
                                </div>
                                <input type="text" name="e_date" class="form-control input-sm datetimepicker"
                                       placeholder="結束日" value="<?=(!empty($_GET['e_date']))?$_GET['e_date']:""?>">
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:300px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">狀態</button>
                                </div>
                                <select class="form-control input-sm" name="status">
                                    <option value="all">全部</option>
                                    <option value="1" <?=($status == "1")?"selected":""?>>有效</option>
                                    <option value="0" <?=($status == "0")?"selected":""?>>失效</option>
                                </select>
                            </div>
                        </div>
                        <button type="button" class="btn btn-success btn-sm pull-right" onclick="search.action = 'list.php';search.submit();" style="margin-left: 10px;">送出</button>
                        <a class="btn btn-danger btn-sm pull-right" href="./list.php" style="margin-left: 10px;">清除</a>
                        <button type="button" class="btn btn-success btn-sm pull-right" onclick="search.action = 'export.php';search.submit();">匯出csv</button>
                    </form>
                </div>
                <div class="box-body no-padding">
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                <th>ID</th>
                                <th>會員ID</th>
                                <th>類型</th>
                                <th>點數</th>
                                <th>狀態</th>
                                <th>建立時間</th>
                                <th>操作</th>
                            </tr>
                            <?php
                            if($rs_link->numRows() > 0) {
                            while($row = $rs_link->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                                $desc = "";
                                if(in_array($row['mode'], [1,2])) {
                                    $desc = "(".$ARRall['level'][$row['level']].")".$row['Order_ID'];
                                }
                             ?>
                            <tr onClick="iCheckThisRow(this);">
                                <th><?=$row['id']?></th>
                                <th><?=$row['Customer_ID']?></th>
                                <th><?=$ARRall['point_mode'][$row['mode']].$desc?></th>
                                <th><?=$row['point']?></th>
                                <th><?=($row['status']==0)?"失效":"有效"?></th>
                                <th><?=$row['datetime']?></th>
                                <th>
                                    <a class="btn btn-xs btn-primary" href="./detail.php?action=edit&id=<?=$row['id']?>">檢視</a>
                                </th>
                            </tr>
                            <?php }} ?>
                        </tbody>
                    </table>
                </div>
            </div>
		<?=$page?>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>