<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("delivery_price");

//取出總筆數
$sql_cmd = "select * from var where category = 'DELIVERY'";
$rs = $db->query($sql_cmd);
while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
    $row_var[$row['type']] = $row['value'];
}
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php");?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                運費設定管理
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box box-primary">
                <form class="form-horizontal" action="./edit.php" method="POST">
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-sm-12">

                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <th width="200px">名稱</th>
                                            <th>參數</th>
                                        </tr>
                                        <tr onClick="iCheckThisRow(this);">
                                            <th><a style="color:red">*</a>貨到付款</th>
                                            <th><input type="number" name="var[DELIVERY_PAY]" value="<?=$row_var['DELIVERY_PAY']?>"></th>
                                        </tr>
                                        <tr onClick="iCheckThisRow(this);">
                                            <th><a style="color:red">*</a>物流配送</th>
                                            <th><input type="number" name="var[DELIVERY]" value="<?=$row_var['DELIVERY']?>" placeholder=""></th>
                                        </tr>
                                        <tr onClick="iCheckThisRow(this);">
                                            <th><a style="color:red">*</a>7-11超商取貨</th>
                                            <th><input type="number" name="var[STORE_711]" value="<?=$row_var['STORE_711']?>" placeholder=""></th>
                                        </tr>
                                        <tr onClick="iCheckThisRow(this);">
                                            <th><a style="color:red">*</a>7-11超商取貨付款</th>
                                            <th><input type="number" name="var[STORE_711_PAY]" value="<?=$row_var['STORE_711_PAY']?>"></th>
                                        </tr>
                                        <tr onClick="iCheckThisRow(this);">
                                            <th><a style="color:red">*</a>便利達康超商取貨</th>
                                            <th><input type="number" name="var[STORE_FAMILY]" value="<?=$row_var['STORE_FAMILY']?>" placeholder=""></th>
                                        </tr>
                                        <tr onClick="iCheckThisRow(this);">
                                            <th><a style="color:red">*</a>便利達康超商取貨付款</th>
                                            <th><input type="number" name="var[STORE_FAMILY_PAY]" value="<?=$row_var['STORE_FAMILY_PAY']?>" placeholder=""></th>
                                        </tr>
                                         <!-- <tr onClick="iCheckThisRow(this);">
                                            <th>OK超商取貨</th>
                                            <th><input type="number" name="var[STORE_OK]" value="<?=$row_var['STORE_OK']?>"></th>
                                        </tr>
                                        <tr onClick="iCheckThisRow(this);">
                                            <th>OK超商取貨付款</th>
                                            <th><input type="number" name="var[STORE_OK_PAY]" value="<?=$row_var['STORE_OK_PAY']?>" placeholder=""></th>
                                        </tr>
                                        <tr onClick="iCheckThisRow(this);">
                                            <th>萊爾富超商取貨</th>
                                            <th><input type="number" name="var[STORE_HILIFE]" value="<?=$row_var['STORE_HILIFE']?>" placeholder=""></th>
                                        </tr>
                                        <tr onClick="iCheckThisRow(this);">
                                            <th>萊爾富超商取貨付款</th>
                                            <th><input type="number" name="var[STORE_HILIFE_PAY]" value="<?=$row_var['STORE_HILIFE_PAY']?>"></th>
                                        </tr> 
                                        <tr onClick="iCheckThisRow(this);">
                                            <th><a style="color:red">*</a>掛號費金額/期</th>
                                            <th><input type="number" name="var[REGISTERED_MAIL]" value="<?=$row_var['REGISTERED_MAIL']?>" placeholder=""></th>
                                        </tr>-->
                                        <tr onClick="iCheckThisRow(this);">
                                            <th>物流配送-滿額免運金額</th>
                                            <th><input type="number" name="var[DELIVERY_FREE_PRICE]" value="<?=$row_var['DELIVERY_FREE_PRICE']?>" placeholder=""></th>
                                        </tr>
                                        <tr onClick="iCheckThisRow(this);">
                                            <th>物流配送-滿額免運開始時間</th>
                                            <th><input type="text" name="var[DELIVERY_FREE_SDATE]" value="<?=$row_var['DELIVERY_FREE_SDATE']?>" class="datetimepicker"></th>
                                        </tr>
                                        <tr onClick="iCheckThisRow(this);">
                                            <th>物流配送-滿額免運到期時間</th>
                                            <th><input type="text" name="var[DELIVERY_FREE_EDATE]" value="<?=$row_var['DELIVERY_FREE_EDATE']?>" placeholder="" class="datetimepicker"></th>
                                        </tr>
                                        <tr onClick="iCheckThisRow(this);">
                                            <th>超商-滿額免運金額</th>
                                            <th><input type="number" name="var[STORE_FREE_PRICE]" value="<?=$row_var['STORE_FREE_PRICE']?>" placeholder=""></th>
                                        </tr>
                                        <tr onClick="iCheckThisRow(this);">
                                            <th>超商-滿額免運開始時間</th>
                                            <th><input type="text" name="var[STORE_FREE_SDATE]" value="<?=$row_var['STORE_FREE_SDATE']?>" class="datetimepicker"></th>
                                        </tr>
                                        <tr onClick="iCheckThisRow(this);">
                                            <th>超商-滿額免運到期時間</th>
                                            <th><input type="text" name="var[STORE_FREE_EDATE]" value="<?=$row_var['STORE_FREE_EDATE']?>" placeholder="" class="datetimepicker"></th>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer text-right">
                        <button type="submit" class="btn btn-primary">儲存</button>
                    </div>
                </form>

            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>