<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("delivery_price_edit");

$var = filter_input(INPUT_POST, 'var', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

if(empty($var[DELIVERY_PAY])
  || (empty($var[DELIVERY]))
  || (empty($var[STORE_711]))
  || (empty($var[STORE_711_PAY]))
  || (empty($var[STORE_FAMILY]))
  || (empty($var[STORE_FAMILY_PAY]))
	){
    $err_msg = "欄位";
    if(empty($var[DELIVERY_PAY]))
        $err_field[] = "貨到付款";
    if(empty($var[DELIVERY]))
        $err_field[] = "物流配送";
    if(empty($var[STORE_711]))
        $err_field[] = "7-11超商取貨";
    if(empty($var[STORE_711_PAY]))
        $err_field[] = "7-11超商取貨付款";
    if(empty($var[STORE_FAMILY]))
        $err_field[] = "便利達康超商取貨";
    if(empty($var[STORE_FAMILY_PAY]))
        $err_field[] = "便利達康超商取貨付款";
    $err_msg .= "'".implode("','", $err_field)."'不可為空值";
    js_go_back_self($err_msg);
    exit;
}

foreach ($var as $key => $value) {
    $sql_cmd = "select * from `var` where type = '".checkinput_sql($key,20)."'";
    $rs = $db->query($sql_cmd);
    if($rs->numRows() > 0) {
        $sql_cmd = "update var set value = '".checkinput_sql($value,20)."' where type = '".checkinput_sql($key,20)."'";
        $rs = $db->query($sql_cmd);        
    }
    else {
        $sql_cmd = "insert into var (`type`, `value`) value ('".checkinput_sql($key,20)."', '".checkinput_sql($value,20)."')";
        $rs = $db->query($sql_cmd);
    }
}
add_log('運費設定管理','2');
$db->disconnect();
js_repl_global( "./list.php", "EDIT_SUCCESS");
exit;
