<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("faq_category_add");
$id = get_id();
$name      = filter_input(INPUT_POST, 'name');
$desc      = filter_input(INPUT_POST, 'desc');
$alt       = filter_input(INPUT_POST, 'alt');
$status    = filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 1, 'default' => 0)));
$sort        = filter_input(INPUT_POST, 'sort', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 99999, 'default' => 0)));

if(empty($name) || empty($desc) || empty($_FILES['pic']['name'])) {
    $err_msg = "欄位";
    if(empty($name))
        $err_field[] = "標題";
    if(empty($desc))
        $err_field[] = "副標題";
    if(empty($_FILES['pic']['name']))
        $err_field[] = "圖片";
    $err_msg .= "'".implode("','", $err_field)."'不可為空值";
    js_go_back_self($err_msg);
    exit;
}

if ($_FILES['pic']['name'] != "none" && is_uploaded_file($_FILES['pic']['tmp_name']))
{
    //重組檔名
    $type = explode(".", $_FILES['pic']['name']);
    $img = $_FILES['pic']['name'];
    if(file_exists(DOCUMENT_ROOT."/upload/".$img)) {
        // js_go_back_self("檔名重複!!");
        // exit;
    }
    @copy($_FILES['pic']['tmp_name'], DOCUMENT_ROOT."/upload/".$img);
}
else {
    js_go_back_global("IMG_ERROR");
    exit;
}

$create_datetime = date("Y-m-d H:i:s");
$create_user     = $_SESSION[SESSION_VARIABLE."_user_id"];
$update_datetime = date("Y-m-d H:i:s");
$update_user     = $_SESSION[SESSION_VARIABLE."_user_id"];

$sql_array = array(
    "Category_Code"        => array("2", checkinput_sql($id, 19)),
    "Category_CodeGroup"   => array("2", "FAQ"),
    "Category_Name"        => array("2", checkinput_sql($name, 45)),
    "desc"                 => array("2", checkinput_sql($desc, 255)),
    "category_pic1"        => array("2", checkinput_sql($img, 200)),
    "pic_alt1"             => array("2", checkinput_sql($alt, 45)),
    "sort"                 => array("2", checkinput_sql($sort, 45)),
    "status"               => array("2", checkinput_sql($status,2)),
    "create_datetime"      => array("2", checkinput_sql($create_datetime, 50)),
    "create_user"          => array("2", checkinput_sql($create_user, 50)),
    "update_datetime"      => array("2", checkinput_sql($update_datetime, 50)),
    "update_user"          => array("2", checkinput_sql($update_user, 50)),
    "Parent_Category_Code" => array("2", ""),
);

$sql_cmd = insert("category", $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
   js_go_back_global("DB_ADD_ERROR");
   exit;
}else{
    add_log('客服中心-常見問題分類管理','1');
   $db->disconnect();
   js_repl_global( "./list.php", "ADD_SUCCESS");
   exit;
}
?>
