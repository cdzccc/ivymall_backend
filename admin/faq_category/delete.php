<?php
include("../config.php");

ClassJscript::islogin();
ClassJscript::isadmino("faq_category_del");
$id = filter_input(INPUT_GET, 'id');
if(empty($id))
  $id = implode("','", filter_input(INPUT_GET, 'id', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY));

$sql_cmd = "select * from category where Category_Code in ('".$id."')";
$rs_user = $db->query($sql_cmd);

if($rs_user->numRows() >0) {
    $sql_cmd = "update category set deleted_at = '".date("Y-m-d H:i:s")."' where Category_Code in ('".$id."')";
    $rs = $db->query($sql_cmd);
    $pear = new PEAR();
    if ($pear->isError($rs))
    {
       js_go_back_global("DB_DEL_ERROR");
       exit;
    }else{
        add_log('客服中心-常見問題分類管理','3');
       $db->disconnect();
       js_repl_global( "./list.php", "DEL_SUCCESS");
       exit;
    }
}
else {
    js_go_back_global("NOT_POST");
    exit;

}
?>