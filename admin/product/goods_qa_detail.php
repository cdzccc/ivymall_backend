<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("product");
if(isset($_GET['action']))
    $action = $_GET['action'];
else
    $action = "add";

$row["Goods_ID"]  = filter_input(INPUT_GET, 'Goods_ID');
$row["title"] = "";
$row["content"] = "";
$row["sort"] = "0";
$row["status"] = "1";

if($action == "edit") {
    $sql_cmd = "select * from goods_qa where id = '".checkinput_sql($_GET['id'],19)."' and deleted_at is null";
    $rs = $db->query($sql_cmd);
    $row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
}

?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php") ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                商品常見問題上稿管理
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" method="POST" action="./goods_qa_<?=$action?>.php?id=" enctype="multipart/form-data">
                    <div class="box-header">
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">標題名稱</label>
                            <div class="col-sm-10">
                                <input id="name" name="title" type="text" class="form-control"
                                       value="<?=$row["title"]?>" maxlength="30">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="desc" class="col-sm-2 control-label">內容</label>
                            <div class="col-sm-10">
                                <textarea name="editor" class="form-control"><?=$row["content"]?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="sort" class="col-sm-2 control-label">排序</label>
                            <div class="col-sm-10">
                                <input id="sort" name="sort" type="text" class="form-control"
                                       value="<?=$row["sort"]?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="status" class="col-sm-2 control-label">狀態</label>
                            <div class="col-sm-10 control-text">
                                <label>
                                    <input name="status" type="radio" <?=($row['status'] == 1)?"checked":""?>
                                       value="1">上架
                                </label>
                                <label>
                                    <input name="status" type="radio" <?=($row['status'] == 0)?"checked":""?>
                                       value="0">下架
                                </label>
                            </div>
                        </div>
                        <?php if($action == "edit") {?>
                        <div class="form-group">
                            <label for="sort" class="col-sm-2 control-label">修改時間</label>
                            <div class="col-sm-10  control-text">
                                <?=$row['update_datetime']?>
                            </div>
                        </div>
                        <?php }?>
                        <?php if($action == "edit") {?>
                        <div class="form-group">
                            <label for="sort" class="col-sm-2 control-label">建立時間</label>
                            <div class="col-sm-10  control-text">
                                <?=$row['create_datetime']?>
                            </div>
                        </div>
                        <?php }?>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-right">
                        <input type="hidden" name="Goods_ID" value="<?=$row["Goods_ID"]?>">
                        <input type="hidden" name="id" value="<?=$row["id"]?>">
                        <a href="./list.php" class="btn btn-danger" style="margin-right: 10px;">取消，並返回上一層</a>
                        <button type="submit" class="btn btn-primary">儲存</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>