<?php
include("../config.php");

ClassJscript::islogin();
ClassJscript::isadmino("product_add");

$Goods_ID = get_id();

$Dealer_ID     = filter_input(INPUT_POST, 'dealer_id');
$Category_ID   = filter_input(INPUT_POST, 'category', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$Name          = filter_input(INPUT_POST, 'name');
$Goods_sdate   = filter_input(INPUT_POST, 'daterange_s');
$Goods_edate   = filter_input(INPUT_POST, 'daterange_e');
$Desc          = filter_input(INPUT_POST, 'desc');
$status        = filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 1, 'default' => 0)));
// 規格
$variant       = filter_input(INPUT_POST, 'variant', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$option        = filter_input(INPUT_POST, 'option', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$item_category = filter_input(INPUT_POST, 'item_category', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$price         = filter_input(INPUT_POST, 'price', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$id_173        = filter_input(INPUT_POST, 'id_173', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$Rent_MinDay   = filter_input(INPUT_POST, 'Rent_MinDay');
$Rent_FreeDay1 = filter_input(INPUT_POST, 'Rent_FreeDay1');
$Rent_FreeDay2 = filter_input(INPUT_POST, 'Rent_FreeDay2');

$thumb = "";
$Goods_Pic = "";

// $sql_cmd = "select * from category where dealer_id = '".$Dealer_ID."' and Category_CodeGroup = 'Goods_Category'";
// $rs = $db->query($sql_cmd);
// while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
//     $category[] = $row['Category_Code'];
//     $Goods_Category_id[] = $row['Category_Code'];
// }

$Goods_Category = $Category_ID;
// variant
if(is_array($variant)) {
    $sql_cmd = "update category set status = 0 where Dealer_ID = '".$Dealer_ID."' and Category_CodeGroup = 'Goods_Item_Category' and desc = '".$Goods_ID."'";
    $rs = $db->query($sql_cmd);

    foreach ($variant as $key=>$val) {
        if(empty($val) && count($option[$key]) > 0) {
            echo "<script>alert('請填入完整規格資料');history.back();</script>";
            exit;
        }
        foreach($option[$val] as $item_key=>$item_val) {
            $sql_cmd = "select * from category where Category_CodeGroup = 'Goods_Item_Category' and Category_CodeGroup_Name = '".$val."' and Category_Name = '".$item_val."'";
            $rs = $db->query($sql_cmd);
            if($rs->numRows() == 0){
                $Category_Code = get_id();
                $sql_array = array(
                      "Category_Code"           => array("2", $Category_Code),
                      "Category_CodeGroup"      => array("2", "Goods_Item_Category"),
                      "Category_Name"           => array("2", checkinput_sql($item_val, 50)),
                      "Category_CodeGroup_Name" => array("2", checkinput_sql($val, 50)),
                      "desc"                    => array("2", checkinput_sql($Goods_ID, 50)),
                      "status"                  => array("2", 1),
                      "dealer_id"               => array("2", checkinput_sql($Dealer_ID, 50)),
                      "create_user"             => array("2", checkinput_sql($Dealer_ID, 50)),
                      "create_datetime"         => array("2", date("Y-m-d H:i:s")),
                      "update_user"             => array("2", checkinput_sql($Dealer_ID, 50)),
                      "update_datetime"         => array("2", date("Y-m-d H:i:s")),
                );
                $sql_cmd = insert("category", $sql_array);
                $rs = $db->query($sql_cmd);
            }
            else {
                $row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
                $Category_Code = $row["Category_Code"];
                $sql_cmd = "update category set status = 1 where Category_Code = '".$Category_Code."'";
                $rs = $db->query($sql_cmd);
            }
            $item_category_code[$val][$item_val] = $Category_Code;
        }
    }
}
if(is_array($item_category)) {
    foreach ($item_category as $key=>$val) {
        $sql_cmd = "select * from goods_item where Goods_ID = '".$Goods_ID."' and Name = '".$val."'";
        $rs = $db->query($sql_cmd);
        if($rs->numRows() == 0) {
            $item_category_arr = explode(" , ",$val);
            $id = [];
            foreach ($item_category_arr as $item_key => $item_value) {
                $id[] = $item_category_code[$variant[$item_key]][$item_value];
            }
            $Goods_Item_ID = get_id();
            $sql_array = array(
                "Goods_ID"        => array("2", $Goods_ID),
                "Goods_Item_ID"   => array("2", $Goods_Item_ID),
                "Category_Code"   => array("2", implode(",", $id)),
                "Price"           => array("2", checkinput_sql($price[$key],6)),
                "Name"            => array("2", checkinput_sql($val,50)),
                "Qty"             => array("2", checkinput_sql(0,6)),
                "id_173"          => array("2", checkinput_sql($id_173[$key],6)),
                "Status"          => array("2", 1),
                "create_user"     => array("2", checkinput_sql($Dealer_ID, 50)),
                "create_datetime" => array("2", date("Y-m-d H:i:s")),
                "update_user"     => array("2", checkinput_sql($Dealer_ID, 50)),
                "update_datetime" => array("2", date("Y-m-d H:i:s")),
            );
            $sql_cmd = insert("goods_item", $sql_array);
            $rs = $db->query($sql_cmd);
        }
        else {
            $row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
            $Goods_Item_ID = $row["Goods_Item_ID"];
            $sql_array = array(
                "Category_Code"   => array("2", implode(",", $id)),
                "Price"           => array("2", checkinput_sql($price[$key],6)),
                "Name"            => array("2", checkinput_sql($val,50)),
                "Qty"             => array("2", checkinput_sql(0,6)),
                "id_173"          => array("2", checkinput_sql($id_173[$key],6)),
                "Status"          => array("2", 1),
                "update_user"     => array("2", checkinput_sql($Dealer_ID, 50)),
                "update_datetime" => array("2", date("Y-m-d H:i:s")),
            );
            $sql_cmd = update("goods_item", array("Goods_Item_ID", $Goods_Item_ID), $sql_array);
            $rs = $db->query($sql_cmd);
        }
    }
}
else {
    $sql_cmd = "select * from goods_item where Goods_ID = '".$Goods_ID."' and Name = 'empty'";
    $rs = $db->query($sql_cmd);

    if($rs->numRows() == 0) {
        $Goods_Item_ID = get_id();
        $sql_array = array(
            "Goods_ID"        => array("2", $Goods_ID),
            "Goods_Item_ID"   => array("2", $Goods_Item_ID),
            "Category_Code"   => array("2", ""),
            "Price"           => array("2", checkinput_sql($price[0],6)),
            "Name"            => array("2", checkinput_sql("empty",50)),
            "Qty"             => array("2", checkinput_sql(0,6)),
            "id_173"          => array("2", checkinput_sql($id_173[$key],6)),
            "Status"          => array("2", 1),
            "create_user"     => array("2", checkinput_sql($Dealer_ID, 50)),
            "create_datetime" => array("2", date("Y-m-d H:i:s")),
            "update_user"     => array("2", checkinput_sql($Dealer_ID, 50)),
            "update_datetime" => array("2", date("Y-m-d H:i:s")),
        );
        $sql_cmd = insert("goods_item", $sql_array);
        $rs = $db->query($sql_cmd);
    }
    else {
        $row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
        $Goods_Item_ID = $row["Goods_Item_ID"];
        $sql_array = array(
            "Category_Code"   => array("2", ""),
            "Price"           => array("2", checkinput_sql($price[0],6)),
            "Name"            => array("2", checkinput_sql($val,50)),
            "Qty"             => array("2", checkinput_sql(0,6)),
            "id_173"          => array("2", checkinput_sql($id_173[$key],6)),
            "Status"          => array("2", 1),
            "update_user"     => array("2", checkinput_sql($Dealer_ID, 50)),
            "update_datetime" => array("2", date("Y-m-d H:i:s")),
        );
        $sql_cmd = update("goods_item", array("Goods_Item_ID", $Goods_Item_ID), $sql_array);
        $rs = $db->query($sql_cmd);
    }
}
$img_file = date("YmdHis");
for($i = 0; $i < count($_FILES["pic"]['name']); $i++) {
    if ($_FILES['pic']['name'][$i] != "none" && is_uploaded_file($_FILES['pic']['tmp_name'][$i]))
    {
        //重組檔名
        $type = explode(".", $_FILES['pic']['name'][$i]);
        $img[$i] = ($img_file+$i).".".$type[1];
        @copy($_FILES['pic']['tmp_name'][$i], DOCUMENT_ROOT."/upload/".$img[$i]);
    }
    else {
        $img[$i] = $_POST['old_pic'][$i];
    }

}
$Goods_Pic = implode(",", $img);

$sql_array = array(
    "Goods_ID"        => array("2", checkinput_sql($Goods_ID, 255)),
    "Dealer_ID"       => array("2", $Dealer_ID),
    "Category_ID"     => array("2", checkinput_sql(implode(",", $Goods_Category), 255)),
    "Goods_Name"      => array("2", $Name),
    "Goods_sdate"     => array("2", $Goods_sdate),
    "Goods_edate"     => array("2", $Goods_edate),
    "Goods_Desc"      => array("2", checkinput_sql($Desc, 255)),
    "Status"          => array("2", $status),
    "Goods_Pic"       => array("2", $Goods_Pic),
    "create_user"     => array("2", checkinput_sql($Dealer_ID, 50)),
    "create_datetime" => array("2", date("Y-m-d H:i:s")),
    "update_user"     => array("2", checkinput_sql($Dealer_ID, 50)),
    "type"            => array("2", 2),
    "Rent_MinDay"     => array("2", checkinput_sql($Rent_MinDay, 50)),
    "Rent_FreeDay"    => array("2", checkinput_sql($Rent_FreeDay1.",".$Rent_FreeDay2, 50)),

);

$sql_cmd = insert("goods", $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
    // exit;
   js_go_back_global("DB_EDIT_ERROR");
   exit;
}else{
   $db->disconnect();
   js_repl_global( "./rlist.php", "ADD_SUCCESS");
   exit;
}
?>
