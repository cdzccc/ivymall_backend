<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("product");

$select2_json = "";
if(isset($_GET['action']))
    $action = $_GET['action'];
else
    $action = "add";

$row_goods["Goods_ID"] = "";
$row_goods["Goods_Name"] = "";
$row_goods["Goods_Desc"] = "";
$row_goods["Status"] = "";
$row_goods["Goods_sdate"] = "";
$row_goods["Goods_edate"] = "";
$row_goods["Rent_MinDay"] = "";
$row_goods["Rent_FreeDay"][0] = "0";
$row_goods["Rent_FreeDay"][1] = "0";
$Category_Code = [];
if($action == "edit") {
    $sql_cmd = "select g.*,d.dealer_name from goods as g
            join dealer as d on g.Dealer_ID = d.Dealer_ID where g.delete_at is null and g.Goods_ID = ".checkinput($_GET['Goods_ID'],19);
    $rs_goods = $db->query($sql_cmd);
    $row_goods = $rs_goods->fetchRow(MDB2_FETCHMODE_ASSOC);
    $row_goods["Goods_sdate"] = ($row_goods["Goods_sdate"] == '0000-00-00 00:00:00')?"":date("Y-m-d H:i",strtotime($row_goods["Goods_sdate"]));
    $row_goods["Goods_edate"] = ($row_goods["Goods_edate"] == '0000-00-00 00:00:00')?"":date("Y-m-d H:i",strtotime($row_goods["Goods_edate"]));
    $row_goods['Rent_FreeDay'] = explode(",",$row_goods['Rent_FreeDay']);
    $daterange = date("Y-m-d H:i",strtotime($row_goods["Goods_sdate"]))." - ".date("Y-m-d H:i",strtotime($row_goods["Goods_edate"]));
    $select2_json = json_encode(explode(",", $row_goods['Category_ID']));

    $Category_Code = explode(",", $row_goods["Category_ID"]);

    $sql_cmd = "select * from category where 
        Category_CodeGroup = 'Goods_Item_Category' and `Status` = 1
        and dealer_id = '".$row_goods["Dealer_ID"]."' and `Desc` = '".$row_goods['Goods_ID']."' order by update_datetime asc";
    $rs_item_category = $db->query($sql_cmd);
    while($row_item_category = $rs_item_category->fetchRow(MDB2_FETCHMODE_ASSOC)) {
        $item_category[$row_item_category["Category_CodeGroup_Name"]][] = array(
            "id"   => $row_item_category["Category_Code"],
            "name" => $row_item_category["Category_Name"],
        );
    }
    $pic = explode(",", $row_goods['Goods_Pic']);
    $sql_cmd = "select * from goods_item where Goods_ID = '".checkinput($_GET['Goods_ID'],19)."' and Status = 1 order by update_datetime asc";
    $rs_goods_item = $db->query($sql_cmd);
}
$sql_cmd = "select Category_Code, Category_Name from category where 
    Category_CodeGroup = 'Goods_Category' and Status = 1
    order by update_datetime asc";
$rs_category = $db->query($sql_cmd);



$sql_cmd = "select * from dealer where status = 1 and delete_at is null order by name asc";
$rs_dealer = $db->query($sql_cmd);
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php"); ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                商品管理
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" method="POST" action="./r<?=$action?>.php" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">商家</label>
                            <div class="col-sm-10">
                                <?php
                                    if($action == "edit") {
                                        echo $row_goods['dealer_name'];
                                        echo "<input type='hidden' name='dealer_id' value='".$row_goods['Dealer_ID']."'>";
                                    }
                                    else {
                                ?>
                                <select class="select2_dealer form-control" name="dealer_id">
                                    <?php
                                    if($rs_dealer->numRows() > 0) {
                                        while($row = $rs_dealer->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                                            ?>
                                            <option value="<?=$row['Dealer_ID']?>"><?=$row['dealer_name']?></option>
                                        <?php }} ?>

                                </select>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">商品名稱</label>
                            <div class="col-sm-10">
                                <input id="name" name="name" type="text" class="form-control" placeholder=""
                                       value="<?=$row_goods["Goods_Name"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">介紹</label>
                            <div class="col-sm-10">
                                <textarea class="editor" name="desc"><?=$row_goods["Goods_Desc"]?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">分類</label>
                            <div class="col-sm-10">
                                <select name="category[]" class="form-control select2_ajax_category" multiple>
                                <?php
                                    while($row_category = $rs_category->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                                ?>
                                    <option value="<?=$row_category['Category_Code']?>" <?=in_array($row_category['Category_Code'],$Category_Code) ? 'selected' : '' ?>><?=$row_category['Category_Name']?></option>
                                <?php
                                    }

                                ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">圖片</label>
                            <div class="col-sm-10">
                                <?php
                                    if(!empty($pic[0])) {
                                        echo "<img src='/upload/".$pic[0]."' width='80px' class='img0'><input type='hidden' class='pic0' value='".$pic[0]."' name='old_pic[0]'>";
                                        echo "<br>".$pic[0];
                                        echo "<a href='javascript:void(0);' class='btn btn-danger' onclick='$(\".pic0\").val(\"\");$(\".img0\").hide();$(this).hide()'>刪除圖片</a>";
                                    }
                                ?>
                                <input type="file" name="pic[]">
                                <?php
                                    if(!empty($pic[1])) {
                                        echo "<img src='/upload/".$pic[1]."' width='80px' class='img1'><input type='hidden' class='pic1' value='".$pic[1]."' name='old_pic[1]'>";
                                        echo "<br>".$pic[1];
                                        echo "<a href='javascript:void(0);' class='btn btn-danger' onclick='$(\".pic1\").val(\"\");$(\".img1\").hide();$(this).hide()'>刪除圖片</a>";
                                    }
                                ?>
                                <input type="file" name="pic[]">
                                <?php
                                    if(!empty($pic[2])) {
                                        echo "<img src='/upload/".$pic[2]."' width='80px' class='img2'><input type='hidden' class='pic2' value='".$pic[2]."' name='old_pic[2]'>";
                                        echo "<br>".$pic[2];
                                        echo "<a href='javascript:void(0);' class='btn btn-danger' onclick='$(\".pic2\").val(\"\");$(\".img2\").hide();$(this).hide()'>刪除圖片</a>";
                                    }
                                ?>
                                <input type="file" name="pic[]">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">最短租借天數</label>
                            <div class="col-sm-10">
                                <input id="name" name="Rent_MinDay" type="text" class="form-control" placeholder=""
                                       value="<?=$row_goods["Rent_MinDay"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">租借免費天數</label>
                            <div class="col-sm-10">
                                <div class="col-sm-2">

                                    前<input id="Rent_FreeDay1" name="Rent_FreeDay1" type="text" class="form-control" placeholder=""
                                       value="<?=$row_goods["Rent_FreeDay"][0]?>">
                                </div>
                                <div class="col-sm-2">
                                    後<input id="Rent_FreeDay2" name="Rent_FreeDay2" type="text" class="form-control" placeholder=""
                                       value="<?=$row_goods["Rent_FreeDay"][1]?>">
                                </div>
                            </div>
                        </div>

                        <div>
                            <?php
                                if($action == "edit" && isset($item_category) && count($item_category) > 0) {
                                    foreach ($item_category as $key => $value) {
                            ?>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">規格</label>
                                <div class="col-sm-2">
                                    <input class="variant form-control" name="variant[]" type="text" class="form-control" placeholder=""
                                           value="<?=$key?>" onkeyup="change_variant()">
                                </div>
                                <label class="col-sm-1 control-label">選項</label>
                                <div class="col-sm-6">
                                    <select name="option[<?=$key?>][]" class="form-control select2_category_item" multiple onchange="setItem()">
                                        <?php
                                            foreach($value as $subkey=>$subval) {
                                                echo "<option value='".$subval["name"]."' selected>".$subval["name"]."</option>";
                                            }
                                        ?>
                                    </select>
                                    <div>
                                    </div>
                                </div>
                                <div class="col-sm-1">
                                    <a href="javascript:void(0);" class="remove_variant" onclick="remove_variant(this)">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </div>
                            </div>
                            <?php
                                    }
                                }
                                else {
                            ?>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">規格</label>
                                <div class="col-sm-2">
                                    <input class="variant form-control" name="variant[]" type="text" class="form-control" placeholder=""
                                           value="" onkeyup="change_variant()">
                                </div>
                                <label class="col-sm-1 control-label">選項</label>
                                <div class="col-sm-6">
                                    <select name="option[][]" class="form-control select2_category_item" multiple onchange="setItem()"></select>
                                    <div>

                                    </div>

                                </div>
                                <div class="col-sm-1">
                                    <a href="javascript:void(0);" class="remove_variant" onclick="remove_variant(this)" style="display:none;">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </div>
                            </div>

                            <?php
                                }
                            ?>
                            <div class="form-group add-variant_btn">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-10">
                                    <a href="javascript:void(0);" onclick="add_variant();">為商品新增更多規格</a>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-10">
                                    <table class="table table-bordered table-hover product_item" <?=($action == "add" || $rs_goods_item->numRows() == 0)?'style="display:none;"':""?>>
                                        <thead>
                                            <tr class="">
                                                <? if(isset($item_category) || $action == "add"):?>
                                                <td>規格</td>
                                                <? endif ?>
                                                <td>售價</td>
                                                <td>173商品id</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        if($action == "edit" && $rs_goods_item->numRows() > 0) {
                                            while($row_goods_item = $rs_goods_item->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                                        ?>
                                        <tr>
                                            <? if($row_goods_item['Name'] != 'empty'):?>
                                            <td><?=$row_goods_item["Name"]?></td>
                                            <? endif ?>
                                            <td><input type='text' name='price[]' value="<?=$row_goods_item["Price"]?>"><input class='category' name='item_category[]' type='hidden' value='<?=$row_goods_item["Name"]?>'></td>
                                            <td><input type='text' name='id_173[]' value="<?=$row_goods_item["id_173"]?>"></td>
                                        </tr>
                                        <?php
                                            }
                                        }
                                        ?>
                                        </tbody>
                                    </table>

                                    <table class="table table-bordered table-hover product"  <?=($action == "edit" && $rs_goods_item->numRows() > 0)?'style="display:none;"':""?>>
                                        <thead>
                                        <tr class="">
                                            <td>售價</td>
                                            <td>173商品id</td>
                                        </tr>
                                        <tr class="">
                                            <td><input type="text" name="price[]"></td>
                                            <td><input type="text" name="id_173[]"></td>
                                        </tr>

                                        </thead>
                                    </table>
                                </div>
                            </div>

                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">上架日期</label>
                            <div class="col-sm-10">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input class="datetimepicker" name="daterange_s" type="text" class="form-control pull-right rangepicker" value="<?=$row_goods['Goods_sdate']?>">
                                     - 
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input class="datetimepicker" name="daterange_e" type="text" class="form-control pull-right rangepicker" value="<?=$row_goods['Goods_edate']?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">狀態</label>
                            <div class="col-sm-10">
                                <label>
                                    <input type="radio" name="status" value="0" <?php if($row_goods["Status"] == 0) {echo "checked";}?>>下架
                                </label>
                                <label>
                                    <input type="radio" name="status" value="1" <?php if($row_goods["Status"] == 1) {echo "checked";}?>>上架
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="f_path" class="col-sm-2 control-label">建立時間</label>
                            <div class="col-sm-10" style="line-height: 32px;">
                                <i class="fa fa-clock-o" aria-hidden="true"></i> <?=date("Y-m-d H:i:s")?>
                            </div>
                        </div>


                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-right">
                        <input type="hidden" name="id" value="<?=$row_goods["Goods_ID"]?>">
                        <a href="./list.php" class="btn btn-danger" style="margin-right: 10px;">取消，並返回上一層</a>
                        <button type="button" class="btn btn-primary" onclick="check_file();">儲存</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
    <div class="add-variant hidden">
        <div class="form-group">
            <label class="col-sm-2 control-label">規格</label>
            <div class="col-sm-2">
                <input class="variant" name="variant[]" type="text" class="form-control" placeholder=""
                       value="" onkeyup="change_variant()">
            </div>
            <label for="name" class="col-sm-1 control-label">選項</label>
            <div class="col-sm-6">
                <select name="option[][]" class="form-control select2_category_item" multiple onchange="setItem()"></select>
                <div>

                </div>

            </div>
            <div class="col-sm-1">
                <a href="javascript:void(0);" class="remove_variant" onclick="remove_variant(this)" style="display:none;">
                    <i class="fa fa-trash"></i>
                </a>
            </div>
        </div>

    </div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>