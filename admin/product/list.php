﻿<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("product");

$title  = filter_input(INPUT_GET, 'title');
$status = filter_input(INPUT_GET, 'status');
$category = filter_input(INPUT_GET, 'category');
$sub_category = filter_input(INPUT_GET, 'sub_category');
$sell_price = filter_input(INPUT_GET, 'sell_price', FILTER_VALIDATE_INT);
$discount_price = filter_input(INPUT_GET, 'discount_price', FILTER_VALIDATE_INT);
$type = filter_input(INPUT_GET, 'type');
$gift = filter_input(INPUT_GET, 'gift');
$s_date  = filter_input(INPUT_GET, 's_date');
$e_date  = filter_input(INPUT_GET, 'e_date');
$sql_where = "";
if(!empty($title)) {
    $sql_where .= " and (g.Goods_Name like '%".checkinput_sql($title,1900)."%'
                    or g.isbn like '%".checkinput_sql($title,1900)."%'
					or g.item_number like '%".checkinput_sql($title,1900)."%'
                    )";
}
if(in_array($status, ["0","1"])) {
    $sql_where .= " and g.Status like '%".checkinput_sql($status,19)."%'";
}
if(!empty($category)) {
    $sql_where .= " and g.category = '".checkinput_sql($category,19)."'";
}
if(!empty($sub_category)) {
    $sql_where .= " and g.sub_category = '".checkinput_sql($sub_category,19)."'";
}
if(in_array($type, ["2","1"])) {
    $sql_where .= " and g.type = '".checkinput_sql($type,19)."'";
}
if(!empty($gift) && $gift != 'all') {
    if($gift == 1)
        $sql_where .= " and g.gift_number != ''";
    else
        $sql_where .= " and g.gift_number = ''";
}
if(!empty($sell_price)) {
    $sql_where .= " and g.sell_price = '".checkinput_sql($sell_price,19)."'";
}
if(!empty($discount_price) || $discount_price === 0) {
    $sql_where .= " and g.discount_price2 = '".checkinput_sql($discount_price,19)."'";
}

if(!empty($s_date))
    $sql_where .= " and g.create_datetime >= '".checkinput_sql($s_date." 00:00:00",19)."'";
if(!empty($e_date))
    $sql_where .= " and g.create_datetime <= '".checkinput_sql($e_date." 23:59:59",19)."'";
//取出總筆數
$sql_cmd = "select count(*) from goods as g
             where g.deleted_at is null and g.type in (1,2) ".$sql_where." ";
$rs = $db->query($sql_cmd);
$row = $rs->fetchRow();
$total = $row[0];

$mypage = new page;
$pages = $mypage->set_page($total, $PAGE_NUM);

//設定分頁的初始值
if (empty($_GET['num']))
{
    $num = 1;
}else{
    $num = intval($_GET['num']);
}


$sql_cmd = "select g.* from goods as g
            where g.deleted_at is null and g.type in (1,2) ".$sql_where." order by Goods_ID desc ".$pages[$num];
$rs_goods = $db->query($sql_cmd);
$tmp = $_GET;
unset($tmp['num']);
$val = http_build_query($tmp);
$page = $mypage->multi($total , $PAGE_NUM, $num, "./list.php", "", $val);

// 分類list
$sql_cmd = "select Category_Code, Category_Name from category where
    Category_CodeGroup = 'Goods_Category' and Status = 1 and deleted_at is null
    and Category_Name <> '雜誌館'
    order by sort asc";
	$rs = $db->query($sql_cmd);
while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
    $row_category[] = $row;
}
// sub分類list
$row_sub_category = [];
if(!empty($category)) {
    $sql_cmd = "select * from category where Category_CodeGroup = 'Goods_Sub_Category' and Status = 1 order by create_datetime desc ";
    $rs = $db->query($sql_cmd);
    while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
        $row_sub_category[] = $row;
    }
}

?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php"); ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                單本／套書商品上稿管理
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box">
                <div class="box-header">
                    <form action="./list.php" method="GET">
                        <div style="overflow: hidden; width:100%;">
                            <div style="float:left; display:inline-block; width:250px; padding-right:15px; padding-bottom:10px;">
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-primary btn-sm">搜尋</button>
                                    </div>
                                    <input type="text" name="title" class="form-control input-sm"
                                           placeholder="商品名稱、ISBN、商品貨號" value="<?=(!empty($_GET['title']))?$_GET['title']:""?>">
                                </div>
                            </div>
                            <div style="float:left; display:inline-block; width:250px; padding-right:15px; padding-bottom:10px;">
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-primary btn-sm">第一層分類</button>
                                    </div>
                                    <select class="form-control input-sm product_category" name="category">
                                        <option value="">全部</option>
                                        <? foreach($row_category as $item): ?>
                                            <option value="<?=$item['Category_Code']?>" <?=(!empty($category) && $category == $item['Category_Code'])?"selected":""?>><?=$item['Category_Name']?></option>
                                        <? endforeach ?>
                                    </select>
                                </div>
                            </div>
                            <div style="float:left; display:inline-block; width:250px; padding-right:15px; padding-bottom:10px;">
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-primary btn-sm">第二層分類</button>
                                    </div>
                                    <select class="form-control input-sm product_sub_category" name="sub_category">
                                        <option value="">全部</option>
                                        <? foreach($row_sub_category as $item): ?>
                                            <option value="<?=$item['Category_Code']?>" <?=(!empty($_GET['category']) && $_GET['category'] == $item['Category_Code'])?"selected":""?>><?=$item['Category_Name']?></option>
                                        <? endforeach ?>
                                    </select>
                                </div>
                            </div>
                            <div style="float:left; display:inline-block; width:250px; padding-right:15px; padding-bottom:10px;">
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-primary btn-sm">市售價</button>
                                    </div>
                                    <input type="text" name="sell_price" class="form-control input-sm"
                                           placeholder="市售價" value="<?=(!empty($_GET['sell_price']))?$_GET['sell_price']:""?>">
                                </div>
                            </div>
                            <div style="float:left; display:inline-block; width:250px; padding-right:15px; padding-bottom:10px;">
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-primary btn-sm">優惠價</button>
                                    </div>
                                    <input type="text" name="discount_price" class="form-control input-sm"
                                           placeholder="優惠價" value="<?=$discount_price?>">
                                </div>
                            </div>
                            <div style="float:left; display:inline-block; width:250px; padding-right:15px; padding-bottom:10px;">
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-primary btn-sm">商品種類</button>
                                    </div>
                                    <select class="form-control input-sm" name="type">
                                        <option value="">全部</option>
                                        <option value="1" <?=($type == "1")?"selected":""?>>單本</option>
                                        <option value="2" <?=($type == "2")?"selected":""?>>套書</option>
                                    </select>
                                </div>
                            </div>
                            <div style="float:left; display:inline-block; width:250px; padding-right:15px; padding-bottom:10px;">
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-primary btn-sm">贈品</button>
                                    </div>
                                    <select class="form-control input-sm" name="gift">
                                        <option value="">全部</option>
                                        <option value="1" <?=($gift == "1")?"selected":""?>>有</option>
                                        <option value="2" <?=($gift == "2")?"selected":""?>>無</option>
                                    </select>
                                </div>
                            </div>
                            <div style="float:left; display:inline-block; width:250px; padding-right:15px; padding-bottom:10px;">
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-primary btn-sm">狀態</button>
                                    </div>
                                    <select class="form-control input-sm" name="status">
                                        <option value="">全部</option>
                                        <option value="1" <?=($status == "1")?"selected":""?>>上架</option>
                                        <option value="0" <?=($status == "0")?"selected":""?>>下架</option>
                                    </select>
                                </div>
                            </div>
							<div style="float:left; display:inline-block; width:250px; padding-bottom:10px;">
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-primary btn-sm">建立時間</button>
                                    </div>
                                    <input type="text" name="s_date" class="form-control input-sm datetimepicker"
                                           placeholder="起始日" value="<?=(!empty($_GET['s_date']))?$_GET['s_date']:""?>">
                                </div>
                            </div>
                            <div style="float:left; display:inline-block; width:250px; padding-right:15px; padding-bottom:10px;">
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-primary btn-sm">~</button>
                                    </div>
                                    <input type="text" name="e_date" class="form-control input-sm datetimepicker"
                                           placeholder="結束日" value="<?=(!empty($_GET['e_date']))?$_GET['e_date']:""?>">
                                </div>
                            </div>
				<button type="submit" class="btn btn-success btn-sm pull-right">送出</button>
                        <a class="btn btn-danger btn-sm pull-right" href="./list.php" style="margin-right: 10px;">清除</a>
                        </div>
			
                        <div style="float:left; display:inline-block; width:100%;">
                            <a class="pull-right btn btn-success btn-sm" href="./detail.php?action=add">新增</a>
                            <input class="pull-right btn btn-success btn-sm" type="button" value="刪除" onclick="if(confirm('是否刪除資料？刪除後無法刪除後無法還原！')) {$('form').attr('action','./delete.php');$('form').submit();}"style="margin-right: 10px;">
                        </div>
                    </div>
                    <div class="box-body no-padding table-responsive">
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <th style="width:100px;"><a href="javascript:void(0)" onclick="select_all('')">(全選)</a></th>
                                    <th style="min-width:100px;">名稱</th>
                                    <th style="min-width:100px;">第一層分類</th>
                                    <th style="min-width:100px;">第二層分類</th>
                                    <th style="min-width:60px;">市售價</th>
                                    <th style="min-width:60px;">優惠價</th>
                                    <th style="min-width:80px;">商品貨號</th>
                                    <th style="min-width:80px;">商品種類</th>
                                    <th style="min-width:50px;">贈品</th>
                                    <th style="min-width:40px;">ISBN</th>
                                    <th style="min-width:120px;">商品常見問題</th>
                                    <th style="min-width:80px;">庫存數量</th>
                                    <th style="min-width:100px;">庫存安全量</th>
                                    <th style="min-width:140px;">支援使用熊贈點</th>
                                    <th style="min-width:160px;">支援會員等級折扣</th>
                                    <th style="min-width:140px;">支援使用折價券</th>
                                    <th style="min-width:200px;">支援滿額贈禮/折價券</th>
                                    <th style="min-width:120px;">支援折扣活動</th>
                                    <th style="min-width:120px;">支援折抵活動</th>
                                    <th style="min-width:80px;">物流配送</th>
                                    <th style="min-width:80px;">超商取貨</th>
                                    <th style="min-width:120px;">超商取貨付款</th>
                                    <th style="min-width:80px;">線上付款</th>
                                    <th style="min-width:80px;">貨到付款</th>
                                    <th style="min-width:80px;">上架時間</th>
                                    <th style="min-width:80px;">下架時間</th>
                                    <th style="min-width:50px;">排序</th>
                                    <th style="min-width:50px;">狀態</th>
                                    <th style="min-width:80px;">創建時間</th>
                                    <th style="min-width:40px;">操作</th>
                                </tr>
                                <?php
                                    while($row = $rs_goods->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                                        $sql_cmd = "select Category_Name from category where
                                            Category_CodeGroup = 'Goods_Category'
                                            and Category_Code = '".$row['category']."'";
                                        $rs = $db->query($sql_cmd);
                                        $row_category = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
                                        $Category_Name = $row_category["Category_Name"];
										$row['sub_category'] = str_replace(",","','",$row['sub_category']);
                                        $sql_cmd = "select Category_Name from category where
                                            Category_CodeGroup = 'Goods_Sub_Category'
                                            and Category_Code in ( '".$row['sub_category']."')";
                                        $rs = $db->query($sql_cmd);
                                        $SUB_row_category = [];
										while($row_category = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
											$SUB_row_category[] = $row_category['Category_Name'];
										}
                                        $SUB_Category_Name =  implode(",",$SUB_row_category);
                                ?>
                                <tr onClick="iCheckThisRow(this);">
                                    <th><input type="checkbox" name="id[]" value="<?=$row["Goods_ID"]?>"></th>
                                    <th><?=$row['Goods_Name']?></th>
                                    <th><?=$Category_Name?></th>
                                    <th><?=$SUB_Category_Name?></th>
                                    <th><?=$row['sell_price']?></th>
                                    <th><?=$row['discount_price2']?></th>
                                    <th><?=$row['item_number']?></th>
                                    <th><?=($row['type']==1?"單本":"套書")?></th>
                                    <th><?=(!empty($row['gift_number']))?"有":"無"?></th>
                                    <th><?=$row['isbn']?></th>
                                    <th><input type="button" value="常見問題" onclick="location.href='./goods_qa_list.php?Goods_ID=<?=$row['Goods_ID']?>'"></th>
                                    <th><?=$row['stock']?></th>
                                    <th><?=$row['safe_stock']?></th>
                                    <th><?=($row['point_use']==1)?"是":"否"?></th>
                                    <th><?=($row['level_use']==1)?"是":"否"?></th>
                                    <th><?=($row['ticket_use']==1)?"是":"否"?></th>
                                    <th><?=($row['price_ticket_use']==1)?"是":"否"?></th>
                                    <th><?=($row['item_discount_use']==1)?"是":"否"?></th>
                                    <th><?=($row['price_discount_use']==1)?"是":"否"?></th>
                                    <th><?=($row['delivery_use']==1)?"是":"否"?></th>
                                    <th><?=($row['store_use']==1)?"是":"否"?></th>
                                    <th><?=($row['store_pay_use']==1)?"是":"否"?></th>
                                    <th><?=($row['online_pay_use']==1)?"是":"否"?></th>
                                    <th><?=($row['delivery_pay_use']==1)?"是":"否"?></th>
                                    <th><?=$row['Goods_sdate']?></th>
                                    <th><?=$row['Goods_edate']?></th>
                                    <th><?=$row['sort']?></th>
                                    <th><?=($row['Status']==1)?"上架":"下架"?></th>
                                    <th><?=$row['create_datetime']?></th>
                                    <th>
                                        <a class="btn btn-xs btn-primary" href="./detail.php?action=edit&Goods_ID=<?=$row['Goods_ID']?>">編輯</a>
                                    </th>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
            <?=$page?>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>