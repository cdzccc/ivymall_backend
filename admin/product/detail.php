﻿<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("product");

$select2_json = "";
if(isset($_GET['action']))
    $action = $_GET['action'];
else
    $action = "add";

$row_goods["Goods_ID"]           = "";
$row_goods["Goods_Name"]         = filter_input(INPUT_POST, 'name');
$row_goods["category"]           = filter_input(INPUT_POST, 'category');
$row_goods["sub_category"]       = filter_input(INPUT_POST, 'sub_category', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
if(empty($row_goods["sub_category"]))
    $row_goods["sub_category"] = [];
$row_goods["company"]            = filter_input(INPUT_POST, 'company');
$row_goods["list_desc"]          = filter_input(INPUT_POST, 'list_desc');
$row_goods["cover_alt"]          = filter_input(INPUT_POST, 'cover_alt');
$row_goods["banner_pc_alt"]      = filter_input(INPUT_POST, 'banner_pc_alt');
$row_goods["banner_m_alt"]       = filter_input(INPUT_POST, 'banner_m_alt');
$row_goods["Goods_Desc"]         = filter_input(INPUT_POST, 'Goods_Desc');
$row_goods["sell_price"]         = filter_input(INPUT_POST, 'sell_price');
$row_goods["discount_price"]     = filter_input(INPUT_POST, 'discount_price');
$row_goods["discount_price2"]    = filter_input(INPUT_POST, 'discount_price2');
$row_goods["related_title1"]     = filter_input(INPUT_POST, 'related_title1');
$row_goods["related_link1"]      = filter_input(INPUT_POST, 'related_link1');
$row_goods["related_title2"]     = filter_input(INPUT_POST, 'related_title2');
$row_goods["related_link2"]      = filter_input(INPUT_POST, 'related_link2');
$row_goods["related_title3"]     = filter_input(INPUT_POST, 'related_title3');
$row_goods["related_link3"]      = filter_input(INPUT_POST, 'related_link3');
$row_goods["related_title4"]     = filter_input(INPUT_POST, 'related_title4');
$row_goods["related_link4"]      = filter_input(INPUT_POST, 'related_link4');
$row_goods["related_title5"]     = filter_input(INPUT_POST, 'related_title5');
$row_goods["related_link5"]      = filter_input(INPUT_POST, 'related_link5');
$row_goods["item_number"]        = filter_input(INPUT_POST, 'item_number');
$row_goods["type"]               = filter_input(INPUT_POST, 'type');
$row_goods["gift_number"]        = filter_input(INPUT_POST, 'gift_number');
$row_goods["gift_count"]         = filter_input(INPUT_POST, 'gift_count');
$row_goods["isbn"]               = filter_input(INPUT_POST, 'isbn');
$row_goods["author"]             = filter_input(INPUT_POST, 'author');
$row_goods["publisher"]          = filter_input(INPUT_POST, 'publisher');
$row_goods["publish_date"]       = filter_input(INPUT_POST, 'publish_date');
$row_goods["publish_place"]      = filter_input(INPUT_POST, 'publish_place');
$row_goods["brand"]              = filter_input(INPUT_POST, 'brand');
$row_goods["language"]           = filter_input(INPUT_POST, 'language');
$row_goods["binding"]            = filter_input(INPUT_POST, 'binding');
$row_goods["weight"]             = filter_input(INPUT_POST, 'weight');
$row_goods["spec"]               = filter_input(INPUT_POST, 'spec');
$row_goods["book_desc"]          = filter_input(INPUT_POST, 'book_desc');
$row_goods["preview_title1"]     = filter_input(INPUT_POST, 'preview_title1');
$row_goods["preview_alt1"]       = filter_input(INPUT_POST, 'preview_alt1');
$row_goods["preview_title2"]     = filter_input(INPUT_POST, 'preview_title2');
$row_goods["preview_alt2"]       = filter_input(INPUT_POST, 'preview_alt2');
$row_goods["preview_title3"]     = filter_input(INPUT_POST, 'preview_title3');
$row_goods["preview_alt3"]       = filter_input(INPUT_POST, 'preview_alt3');
$row_goods["preview_title4"]     = filter_input(INPUT_POST, 'preview_title4');
$row_goods["preview_alt4"]       = filter_input(INPUT_POST, 'preview_alt4');
$row_goods["preview_title5"]     = filter_input(INPUT_POST, 'preview_title5');
$row_goods["preview_alt5"]       = filter_input(INPUT_POST, 'preview_alt5');
$row_goods["preview_title6"]     = filter_input(INPUT_POST, 'preview_title6');
$row_goods["preview_alt6"]       = filter_input(INPUT_POST, 'preview_alt6');
$row_goods["preview_title7"]     = filter_input(INPUT_POST, 'preview_title7');
$row_goods["preview_alt7"]       = filter_input(INPUT_POST, 'preview_alt7');
$row_goods["preview_title8"]     = filter_input(INPUT_POST, 'preview_title8');
$row_goods["preview_alt8"]       = filter_input(INPUT_POST, 'preview_alt8');
$row_goods["preview_title9"]     = filter_input(INPUT_POST, 'preview_title9');
$row_goods["preview_alt9"]       = filter_input(INPUT_POST, 'preview_alt9');
$row_goods["preview_title10"]    = filter_input(INPUT_POST, 'preview_title10');
$row_goods["preview_alt10"]      = filter_input(INPUT_POST, 'preview_alt10');
$row_goods["preview_file"]       = filter_input(INPUT_POST, 'preview_file');
$row_goods["stock"]              = filter_input(INPUT_POST, 'stock');
$row_goods["safe_stock"]         = filter_input(INPUT_POST, 'safe_stock');
$row_goods["min_buy"]            = filter_input(INPUT_POST, 'min_buy', FILTER_VALIDATE_INT, ['options' => ['default'=> 1]]);
$row_goods["max_buy"]            = filter_input(INPUT_POST, 'max_buy');
$row_goods["point_use"]          = filter_input(INPUT_POST, 'point_use');
$row_goods["level_use"]          = filter_input(INPUT_POST, 'level_use');
$row_goods["ticket_use"]         = filter_input(INPUT_POST, 'ticket_use');
$row_goods["price_ticket_use"]   = filter_input(INPUT_POST, 'price_ticket_use');
$row_goods["item_discount_use"]  = filter_input(INPUT_POST, 'item_discount_use');
$row_goods["price_discount_use"] = filter_input(INPUT_POST, 'price_discount_use');
$row_goods["registered_use"]     = filter_input(INPUT_POST, 'registered_use');
$row_goods["registered_price"]   = filter_input(INPUT_POST, 'registered_price');
$row_goods["delivery_use"]       = filter_input(INPUT_POST, 'delivery_use');
$row_goods["store_use"]          = filter_input(INPUT_POST, 'store_use');
$row_goods["store_pay_use"]      = filter_input(INPUT_POST, 'store_pay_use');
$row_goods["online_pay_use"]     = filter_input(INPUT_POST, 'online_pay_use');
$row_goods["delivery_pay_use"]   = filter_input(INPUT_POST, 'delivery_pay_use');
$row_goods["meta_keyrowd"]       = filter_input(INPUT_POST, 'meta_keyrowd');
$row_goods["meta_desc"]          = filter_input(INPUT_POST, 'meta_desc');
$row_goods["Goods_sdate"]        = filter_input(INPUT_POST, 'Goods_sdate');
$row_goods["Goods_edate"]        = filter_input(INPUT_POST, 'Goods_edate');
$row_goods["sort"]               = filter_input(INPUT_POST, 'sort');
$row_goods["recommand_goods"]    = filter_input(INPUT_POST, 'recommand_goods');
$row_goods["Status"]             = filter_input(INPUT_POST, 'Status', FILTER_VALIDATE_INT, ['options' => ['default'=> 1]]);

if(empty($row_goods["book_desc"])) {
    $row_goods["book_desc"] = '<h4>標題用&lt;h4&gt;<br>
        賴世雄教授親自編審，史上最完整口袋單字書！<br>
        情境超強∼鋪天蓋地 42 種情境，<br>
        通用多益、托福、雅思、全民英檢、升學等各項測驗與日常生活實境！</h4>
        <br>
        <p><a href="javascript:;">連結</a></p>
        <br>

        <p>項目用&lt;ul&gt;</p>
        <ul>
        <li>內容豐富超實用：本書一網打盡日常生活與考試測驗中超實用高頻率單字，運用於生活中沒有漏網之魚！</li>
        <li>情境分類超全面：全書共 42 種情境，全面蒐集日常生活、社交、旅遊、職場、學術、自然、人文情境下您會遇到的單字！</li>
        <li>看圖記憶超有趣：看圖讓您身歷其境快速背片語，單字片語學習變得好容易！</li>
        <li>辨析講解超詳細：詳細講解單字片語的細節用法與常見混淆處，所有搞不懂的單字一次搞定！</li>
        <li>習題練習超給力：每章節皆有練習題，立即強化單字記憶，讓您學習效果加倍！</li>
        </ul>

        <p><img src="'.WEBSITE_URL.'/images/book_shop/book_cont.jpg" alt="從不會到很會 英語能力養成計畫 1+1絕對大於2"></p>
        <br>
        <h4>囊括所有情境！生活與考試必備的《情境 15000 英語單字袋著走》，翻轉您的人生！</h4>
        <p>本書主題涵蓋生活會遇到的各種情境，熟讀本書即可掌握 15,000 到 20,000 個字彙以及其用法，讓您面對生活情境與各種考試都游刃有餘！</p>
        <br>

        <p>影片直接複製YouTube分享&gt;嵌入，只有YouTue有用</p>
        <p><iframe width="720" height="450" src="https://www.youtube.com/embed/sr_VSS_gnGE" frameborder="0" allowfullscreen></iframe></p>

        <!-- 影片調整大小：更改max-width數值 -->
        <p><iframe style="max-width: 560px" width="560" height="315" src="https://www.youtube.com/embed/3e2-XFS3_OI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>
';
}

$Category_Code = [];
if($action == "edit") {
    $sql_cmd = "select g.* from goods as g where g.deleted_at is null and g.Goods_ID = '".checkinput_sql($_GET['Goods_ID'],19)."'";
    $rs_goods = $db->query($sql_cmd);
    $row_goods = $rs_goods->fetchRow(MDB2_FETCHMODE_ASSOC);
    $row_goods["Goods_sdate"] = ($row_goods["Goods_sdate"] == '')?"":date("Y-m-d H:i",strtotime($row_goods["Goods_sdate"]));
    $row_goods["Goods_edate"] = ($row_goods["Goods_edate"] == '')?"":date("Y-m-d H:i",strtotime($row_goods["Goods_edate"]));
    $daterange = date("Y-m-d H:i",strtotime($row_goods["Goods_sdate"]))." - ".date("Y-m-d H:i",strtotime($row_goods["Goods_edate"]));
    $row_goods["recommand_goods"] = explode(",", $row_goods["recommand_goods"]);
    $row_goods['sub_category'] = explode(",", $row_goods["sub_category"]);
}

$sql_cmd = "select * from goods where type in (1,2) and deleted_at is null";
$rs = $db->query($sql_cmd);
$arr_select_goods = [];
while($row_select_goods = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
    $arr_select_goods[] = $row_select_goods;
}
$sql_cmd = "select * from gift where deleted_at is null";
$rs = $db->query($sql_cmd);
$arr_gift_goods = [];
while($row_gift_goods = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
    if($row_gift_goods['type'] != 3) {
        $sql_cmd = "select * from goods where item_number = '".$row_gift_goods['item_number']."'";
        $rs_gift = $db->query($sql_cmd);
        $row_gift_goods['name'] = $rs_gift->fetchRow(MDB2_FETCHMODE_ASSOC)['Goods_Name'];
    }
    $arr_gift_goods[] = $row_gift_goods;
}

$arr_category = [];
$sql_cmd = "select Category_Code, Category_Name from category where
    Category_CodeGroup = 'Goods_Category' and Status = 1 and deleted_at is null
    and Category_Name <> '雜誌館'
    order by sort asc";
$rs_category = $db->query($sql_cmd);
while($row_category = $rs_category->fetchRow(MDB2_FETCHMODE_ASSOC)) {
    $arr_category[] = $row_category;
}
$arr_sub_category = [];
if(!empty($row_goods["category"])) {
    $sql_cmd = "select Category_Code, Category_Name from category where
        Category_CodeGroup = 'Goods_Sub_Category' and deleted_at is null and Status = 1 and Parent_Category_Code = ".$row_goods['category']."
        order by update_datetime asc";
    $rs_sub_category = $db->query($sql_cmd);

    while($row_sub_category = $rs_sub_category->fetchRow(MDB2_FETCHMODE_ASSOC)) {
        $arr_sub_category[] = $row_sub_category;
    }

}
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php"); ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                單本／套書商品上稿管理
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" method="POST" action="./<?=$action?>.php" id="form1" onsubmit="return isRemeber()" enctype="multipart/form-data" name="form1">
                    <div class="box-body">
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><a style="color:red">*</a>商品名稱</label>
                            <div class="col-sm-10">
                                <input id="name" name="name" type="text" class="form-control" placeholder="" value="<?=$row_goods["Goods_Name"]?>" maxlength="150">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><a style="color:red">*</a>第一層分類</label>
                            <div class="col-sm-10">
                                <select name="category" id="category" class="form-control product_category">
                                    <option value="">請選擇</option>
                                <?php
                                    foreach($arr_category as $category) {
                                ?>
                                    <option value="<?=$category['Category_Code']?>" <?=($category['Category_Code'] == $row_goods["category"]) ? 'selected' : '' ?>><?=$category['Category_Name']?></option>
                                <?php
                                    }

                                ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><a style="color:red">*</a>第二層分類</label>
                            <div class="col-sm-10">
                                <select name="sub_category[]" id="sub_category" class="form-control select2_ajax_category product_sub_category" multiple>
                                <?php
                                    foreach($arr_sub_category as $sub_category) {
										echo $row_goods['sub_category'];
                                ?>

                                    <option value="<?=$sub_category['Category_Code']?>" <?=(in_array($sub_category['Category_Code'], $row_goods['sub_category'])) ? 'selected' : '' ?>><?=$sub_category['Category_Name']?></option>
                                <?php
                                    }
                                ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><a style="color:red">*</a>所屬公司</label>
                            <div class="col-sm-10">
                                <select name="company" id="company" class="form-control">
                                    <option value="">請選擇</option>
                                    <option value="常春藤有聲出版有限公司" <?=($row_goods['company'] == "常春藤有聲出版有限公司") ? 'selected' : '' ?>>常春藤有聲出版有限公司</option>
                                    <option value="常春藤解析英語有限公司" <?=($row_goods['company'] == "常春藤解析英語有限公司") ? 'selected' : '' ?>>常春藤解析英語有限公司</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">列表簡述</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="list_desc" value="<?=$row_goods['list_desc']?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">封面圖片<br><font class="text-red">建議尺寸:180x250</font></label>
                            <div class="col-sm-10" style="padding-top: 7px;">
                                <?php
                                    if(!empty($row_goods['cover_pic'])) {
                                        echo "<img src='".WEBSITE_URL."upload/".$row_goods['cover_pic']."' width='80px' class='cover_pic'><input type='hidden' class='old_cover_pic' value='".$row_goods['cover_pic']."' name='old_cover_pic'>";
                                        echo "<br>".$row_goods['cover_pic'];
                                        echo "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<a href='javascript:void(0);' class='btn btn-danger' onclick='$(\".old_cover_pic\").val(\"\");$(\".cover_pic\").hide();$(this).hide()'>刪除圖片</a>";
                                    }
                                ?>
                                <div class="control-text">
                                    <input type="file" name="cover_pic" id="cover_pic" value="<?=(!empty($_FILES['cover_pic']))?$_FILES['cover_pic']:"";?>" onchange="upload(this.id)">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">alt</label>
                            <div class="col-sm-10" style="padding-top: 7px;">
                                <input type="text" class="form-control" name="cover_alt" value="<?=$row_goods['cover_alt']?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><a style="color:red">*</a>banner pc<br><font class="text-red">建議尺寸：1920x800</font></label>
                            <div class="col-sm-10">
                                <?php
                                    if(!empty($row_goods['banner_pc_pic'])) {
                                        echo "<img src='".WEBSITE_URL."upload/".$row_goods['banner_pc_pic']."' width='80px' class='banner_pc_pic'><input type='hidden' class='old_banner_pc_pic' value='".$row_goods['banner_pc_pic']."' name='old_banner_pc_pic' id='old_banner_pc_pic'>";
                                        echo "<br>".$row_goods['banner_pc_pic'];
                                        echo "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<a href='javascript:void(0);' class='btn btn-danger' onclick='$(\".old_banner_pc_pic\").val(\"\");$(\".banner_pc_pic\").hide();$(this).hide()'>刪除圖片</a>";
                                    }
                                ?>
                                <div class="control-text">
                                    <input type="file" name="banner_pc_pic" id="banner_pc_pic" onchange="upload(this.id)">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">alt</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="banner_pc_alt" value="<?=$row_goods['banner_pc_alt']?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><a style="color:red">*</a>banner mobile<br><font class="text-red">建議尺寸：1200x675</font></label>
                            <div class="col-sm-10" style="padding-top: 7px;">
                                <?php
                                    if(!empty($row_goods['banner_m_pic'])) {
                                        echo "<img src='".WEBSITE_URL."upload/".$row_goods['banner_m_pic']."' width='80px' class='banner_m_pic'><input type='hidden' class='old_banner_m_pic' value='".$row_goods['banner_m_pic']."' name='old_banner_m_pic' id='old_banner_m_pic'>";
                                        echo "<br>".$row_goods['banner_m_pic'];
                                        echo "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<a href='javascript:void(0);' class='btn btn-danger' onclick='$(\".old_banner_m_pic\").val(\"\");$(\".banner_m_pic\").hide();$(this).hide()'>刪除圖片</a>";
                                    }
                                ?>
                                <div class="control-text">
                                    <input type="file" name="banner_m_pic" id="banner_m_pic" onchange="upload(this.id)">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">alt</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="banner_m_alt" value="<?=$row_goods['banner_m_alt']?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">商品描述</label>
                            <div class="col-sm-10">
                                <textarea class="desc form-control" name="Goods_Desc"><?=$row_goods["Goods_Desc"]?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><a style="color:red">*</a>市售價</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control" name="sell_price" id="sell_price" value="<?=$row_goods["sell_price"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">優惠價</label>
                            <div class="col-sm-10  control-text">
                                優惠<input id="discount_price" onchange="count()" type="number" min="0.1" max="100" step="0.1" class="" name="discount_price" value="<?=$row_goods["discount_price"]?>">折
								<input readonly id="discount_price2" type="text" class="" name="discount_price2" value="<?=$row_goods["discount_price2"]?>">元
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">相關產品</label>
                            <div class="col-sm-10">
                                標題
                                <input type="text" class="form-control" name="related_title1" value="<?=$row_goods["related_title1"]?>">
                                連結
                                <input type="text" class="form-control" name="related_link1" value="<?=$row_goods["related_link1"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">相關產品2</label>
                            <div class="col-sm-10">
                                標題
                                <input type="text" class="form-control" name="related_title2" value="<?=$row_goods["related_title2"]?>">
                                連結
                                <input type="text" class="form-control" name="related_link2" value="<?=$row_goods["related_link2"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">相關產品3</label>
                            <div class="col-sm-10">
                                標題
                                <input type="text" class="form-control" name="related_title3" value="<?=$row_goods["related_title3"]?>">
                                連結
                                <input type="text" class="form-control" name="related_link3" value="<?=$row_goods["related_link3"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">相關產品4</label>
                            <div class="col-sm-10">
                                標題
                                <input type="text" class="form-control" name="related_title4" value="<?=$row_goods["related_title4"]?>">
                                連結
                                <input type="text" class="form-control" name="related_link4" value="<?=$row_goods["related_link4"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">相關產品5</label>
                            <div class="col-sm-10">
                                標題
                                <input type="text" class="form-control" name="related_title5" value="<?=$row_goods["related_title5"]?>">
                                連結
                                <input type="text" class="form-control" name="related_link5" value="<?=$row_goods["related_link5"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><a style="color:red">*</a>商品貨號</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="item_number" id="item_number" value="<?=$row_goods["item_number"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><a style="color:red">*</a>商品種類</label>
                            <div class="col-sm-10 control-text">
                                <input type="radio" name="type" value="1" <?=($row_goods['type'] == 1)?"checked":""?> onclick="$('.item_group').hide();">單本
                                <input type="radio" name="type" value="2" <?=($row_goods['type'] == 2)?"checked":""?> onclick="$('.item_group').show();">套書
                            </div>
                        </div>
                        <div class="form-group item_group" <?=($row_goods['type'] != 2)?"style='display:none;'":""?> >
                            <label class="col-sm-2 control-label"><a style="color:red">*</a>組成商品</label>
                            <div class="col-sm-10">
                                <select class="select2_category_item form-control" multiple name="item_group[]" id="item_group" style="width:100%;">
									<?php
										$row_goods['item_group']= explode(',',$row_goods['item_group']);
                                        foreach ($arr_select_goods as $key => $value) {
									?>
								<option value="<?=$value['Goods_ID']?>" <?=(in_array($value['Goods_ID'], $row_goods['item_group'])) ? 'selected' : '' ?>><?="[".$value['item_number']."]".$value['Goods_Name']?></option>
									<?php
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">贈品貨號</label>
                            <div class="col-sm-10">
                                <select class="select2_category_item form-control" name="gift_number" id="gift_number" style="width:100%;" onchange="if($(this).val() != ''){$('.gift_count').show();} else {$('.gift_count').hide();}">
                                    <option></option>
                                    <?php
                                        foreach ($arr_gift_goods as $key => $value) {
                                            $selected = "";
                                            if($value['item_number'] == $row_goods['gift_number'])
                                                $selected = "selected";
                                            echo "<option value=".$value['item_number']." ".$selected.">[".$value['item_number']."]".$value['name']."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group gift_count" <?php if(empty($row_goods['gift_number'])){ echo 'style="display:none;"';}?>>
                            <label class="col-sm-2 control-label"><a style="color:red">*</a>贈品數量</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control" name="gift_count" id="gift_count" value="<?=$row_goods["gift_count"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">ISBN</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="isbn" value="<?=$row_goods["isbn"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">書籍作者</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="author" value="<?=$row_goods["author"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">出版商</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="publisher" value="<?=$row_goods["publisher"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">出版日期</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="publish_date" value="<?=$row_goods["publish_date"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">出版地</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="publish_place" value="<?=$row_goods["publish_place"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">品牌</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="brand" value="<?=$row_goods["brand"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">語言</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="language" value="<?=$row_goods["language"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">裝訂</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="binding" value="<?=$row_goods["binding"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">重量</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="weight" value="<?=$row_goods["weight"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">規格</label>
                            <div class="col-sm-10">
                                <textarea class="spec form-control" name="spec"><?=$row_goods["spec"]?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">書籍介紹</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" name="editor"><?=$row_goods["book_desc"]?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">試閱內容</label>
                            <div class="col-sm-10">
                                <table class="table table-bordered table-hover">
                                    <tr>
                                        <td>試閱標題</td>
                                        <td colspan="3"><input type="text" class="form-control" name="preview_title1" value="<?=$row_goods["preview_title1"]?>"></td>
                                    </tr>
                                    <tr>
                                        <td>試閱圖片</td>
                                        <td>
                                            <?php
                                                if(!empty($row_goods['preview_pic1'])) {
                                                    echo "<img src='".WEBSITE_URL."upload/".$row_goods['preview_pic1']."' width='80px' class='preview_pic1'>";
                                                    echo "<input type='hidden' class='old_preview_img1' value='".$row_goods['preview_pic1']."' name='old_preview_pic1'>";
                                                    echo "<input type='hidden' class='old_preview_img1' value='".$row_goods['preview_pic1_wh']."' name='old_preview_pic1_wh'>";
                                                    echo "<br>".$row_goods['preview_pic1'];
                                                    echo "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<a href='javascript:void(0);' class='btn btn-danger' onclick='$(\".old_preview_img1\").val(\"\");$(\".preview_pic1\").hide();$(this).hide()'>刪除圖片</a>";
                                                }
                                            ?>
                                            <input type="file" name="preview_pic1" id="preview_pic1" onchange="upload(this.id)">
                                        </td>
                                        <td>ALT欄位</td>
                                        <td>
                                            <input type="text" class="form-control" name="preview_alt1" value="<?=$row_goods["preview_alt1"]?>">
                                        </td>
                                    </tr>
                                </table>
                                <table class="table table-bordered table-hover">
                                    <tr>
                                        <td>試閱標題</td>
                                        <td colspan="3"><input type="text" class="form-control" name="preview_title2" value="<?=$row_goods["preview_title2"]?>"></td>
                                    </tr>
                                    <tr>
                                        <td>試閱圖片</td>
                                        <td>
                                            <?php
                                                if(!empty($row_goods['preview_pic2'])) {
                                                    echo "<img src='".WEBSITE_URL."upload/".$row_goods['preview_pic2']."' width='80px' class='preview_pic2'>";
													echo "<input type='hidden' class='old_preview_img2' value='".$row_goods['preview_pic2']."' name='old_preview_pic2'>";
                                                    echo "<input type='hidden' class='old_preview_img2' value='".$row_goods['preview_pic2_wh']."' name='old_preview_pic2_wh'>";
													echo "<br>".$row_goods['preview_pic2'];
                                                    echo "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<a href='javascript:void(0);' class='btn btn-danger' onclick='$(\".old_preview_img2\").val(\"\");$(\".preview_pic2\").hide();$(this).hide()'>刪除圖片</a>";
                                                }
                                            ?>
                                            <input type="file" name="preview_pic2" id="preview_pic2" onchange="upload(this.id)">
                                        </td>
                                        <td>ALT欄位</td>
                                        <td>
                                            <input type="text" class="form-control" name="preview_alt2" value="<?=$row_goods["preview_alt2"]?>">
                                        </td>
                                    </tr>
                                </table>
                                <table class="table table-bordered table-hover">
                                    <tr>
                                        <td>試閱標題</td>
                                        <td colspan="3"><input type="text" class="form-control" name="preview_title3" value="<?=$row_goods["preview_title3"]?>"></td>
                                    </tr>
                                    <tr>
                                        <td>試閱圖片</td>
                                        <td>
                                            <?php
                                                if(!empty($row_goods['preview_pic3'])) {
                                                    echo "<img src='".WEBSITE_URL."upload/".$row_goods['preview_pic3']."' width='80px' class='preview_pic3'>";
													echo "<input type='hidden' class='old_preview_img3' value='".$row_goods['preview_pic3']."' name='old_preview_pic3'>";
													echo "<input type='hidden' class='old_preview_img3' value='".$row_goods['preview_pic3_wh']."' name='old_preview_pic3_wh'>";
                                                    echo "<br>".$row_goods['preview_pic3'];
                                                    echo "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<a href='javascript:void(0);' class='btn btn-danger' onclick='$(\".old_preview_img3\").val(\"\");$(\".preview_pic3\").hide();$(this).hide()'>刪除圖片</a>";
                                                }
                                            ?>
                                            <input type="file" name="preview_pic3" id="preview_pic3" onchange="upload(this.id)">
                                        </td>
                                        <td>ALT欄位</td>
                                        <td>
                                            <input type="text" class="form-control" name="preview_alt3" value="<?=$row_goods["preview_alt3"]?>">
                                        </td>
                                    </tr>
                                </table>
                                <table class="table table-bordered table-hover">
                                    <tr>
                                        <td>試閱標題</td>
                                        <td colspan="3"><input type="text" class="form-control" name="preview_title4" value="<?=$row_goods["preview_title4"]?>"></td>
                                    </tr>
                                    <tr>
                                        <td>試閱圖片</td>
                                        <td>
                                            <?php
                                                if(!empty($row_goods['preview_pic4'])) {
                                                    echo "<img src='".WEBSITE_URL."upload/".$row_goods['preview_pic4']."' width='80px' class='preview_pic4'>";
													echo "<input type='hidden' class='old_preview_img4' value='".$row_goods['preview_pic4']."' name='old_preview_pic4'>";
													echo "<input type='hidden' class='old_preview_img4' value='".$row_goods['preview_pic4_wh']."' name='old_preview_pic4_wh'>";
                                                    echo "<br>".$row_goods['preview_pic4'];
                                                    echo "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<a href='javascript:void(0);' class='btn btn-danger' onclick='$(\".old_preview_img4\").val(\"\");$(\".preview_pic4\").hide();$(this).hide()'>刪除圖片</a>";
                                                }
                                            ?>
                                            <input type="file" name="preview_pic4" id="preview_pic4" onchange="upload(this.id)">
                                        </td>
                                        <td>ALT欄位</td>
                                        <td>
                                            <input type="text" class="form-control" name="preview_alt4" value="<?=$row_goods["preview_alt4"]?>">
                                        </td>
                                    </tr>
                                </table>
                                <table class="table table-bordered table-hover">
                                    <tr>
                                        <td>試閱標題</td>
                                        <td colspan="3"><input type="text" class="form-control" name="preview_title5" value="<?=$row_goods["preview_title5"]?>"></td>
                                    </tr>
                                    <tr>
                                        <td>試閱圖片</td>
                                        <td>
                                            <?php
                                                if(!empty($row_goods['preview_pic5'])) {
                                                    echo "<img src='".WEBSITE_URL."upload/".$row_goods['preview_pic5']."' width='80px' class='preview_pic5'>";
													echo "<input type='hidden' class='old_preview_img5' value='".$row_goods['preview_pic5']."' name='old_preview_pic5'>";
													echo "<input type='hidden' class='old_preview_img5' value='".$row_goods['preview_pic5_wh']."' name='old_preview_pic5_wh'>";
                                                    echo "<br>".$row_goods['preview_pic5'];
                                                    echo "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<a href='javascript:void(0);' class='btn btn-danger' onclick='$(\".old_preview_img5\").val(\"\");$(\".preview_pic5\").hide();$(this).hide()'>刪除圖片</a>";
                                                }
                                            ?>
                                            <input type="file" name="preview_pic5" id="preview_pic5" onchange="upload(this.id)">
                                        </td>
                                        <td>ALT欄位</td>
                                        <td>
                                            <input type="text" class="form-control" name="preview_alt5" value="<?=$row_goods["preview_alt5"]?>">
                                        </td>
                                    </tr>
                                </table>
                                <table class="table table-bordered table-hover">
                                    <tr>
                                        <td>試閱標題</td>
                                        <td colspan="3"><input type="text" class="form-control" name="preview_title6" value="<?=$row_goods["preview_title6"]?>"></td>
                                    </tr>
                                    <tr>
                                        <td>試閱圖片</td>
                                        <td>
                                            <?php
                                                if(!empty($row_goods['preview_pic6'])) {
                                                    echo "<img src='".WEBSITE_URL."upload/".$row_goods['preview_pic6']."' width='80px' class='preview_pic6'>";
													echo "<input type='hidden' class='old_preview_img6' value='".$row_goods['preview_pic6']."' name='old_preview_pic6'>";
													echo "<input type='hidden' class='old_preview_img6' value='".$row_goods['preview_pic6_wh']."' name='old_preview_pic6_wh'>";
                                                    echo "<br>".$row_goods['preview_pic6'];
                                                    echo "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<a href='javascript:void(0);' class='btn btn-danger' onclick='$(\".old_preview_img6\").val(\"\");$(\".preview_pic6\").hide();$(this).hide()'>刪除圖片</a>";
                                                }
                                            ?>
                                            <input type="file" name="preview_pic6" id="preview_pic6" onchange="upload(this.id)">
                                        </td>
                                        <td>ALT欄位</td>
                                        <td>
                                            <input type="text" class="form-control" name="preview_alt6" value="<?=$row_goods["preview_alt6"]?>">
                                        </td>
                                    </tr>
                                </table>
                                <table class="table table-bordered table-hover">
                                    <tr>
                                        <td>試閱標題</td>
                                        <td colspan="3"><input type="text" class="form-control" name="preview_title7" value="<?=$row_goods["preview_title7"]?>"></td>
                                    </tr>
                                    <tr>
                                        <td>試閱圖片</td>
                                        <td>
                                            <?php
                                                if(!empty($row_goods['preview_pic7'])) {
                                                    echo "<img src='".WEBSITE_URL."upload/".$row_goods['preview_pic7']."' width='80px' class='preview_pic7'>";
													echo "<input type='hidden' class='old_preview_img7' value='".$row_goods['preview_pic7']."' name='old_preview_pic7'>";
													echo "<input type='hidden' class='old_preview_img7' value='".$row_goods['preview_pic7_wh']."' name='old_preview_pic7_wh'>";
                                                    echo "<br>".$row_goods['preview_pic7'];
                                                    echo "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<a href='javascript:void(0);' class='btn btn-danger' onclick='$(\".old_preview_img7\").val(\"\");$(\".preview_pic7\").hide();$(this).hide()'>刪除圖片</a>";
                                                }
                                            ?>
                                            <input type="file" name="preview_pic7" id="preview_pic7" onchange="upload(this.id)">
                                        </td>
                                        <td>ALT欄位</td>
                                        <td>
                                            <input type="text" class="form-control" name="preview_alt7" value="<?=$row_goods["preview_alt7"]?>">
                                        </td>
                                    </tr>
                                </table>
                                <table class="table table-bordered table-hover">
                                    <tr>
                                        <td>試閱標題</td>
                                        <td colspan="3"><input type="text" class="form-control" name="preview_title8" value="<?=$row_goods["preview_title8"]?>"></td>
                                    </tr>
                                    <tr>
                                        <td>試閱圖片</td>
                                        <td>
                                            <?php
                                                if(!empty($row_goods['preview_pic8'])) {
                                                    echo "<img src='".WEBSITE_URL."upload/".$row_goods['preview_pic8']."' width='80px' class='preview_pic8'>";
													echo "<input type='hidden' class='old_preview_img8' value='".$row_goods['preview_pic8']."' name='old_preview_pic8'>";
													echo "<input type='hidden' class='old_preview_img8' value='".$row_goods['preview_pic8_wh']."' name='old_preview_pic8_wh'>";
                                                    echo "<br>".$row_goods['preview_pic8'];
                                                    echo "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<a href='javascript:void(0);' class='btn btn-danger' onclick='$(\".old_preview_img8\").val(\"\");$(\".preview_pic8\").hide();$(this).hide()'>刪除圖片</a>";
                                                }
                                            ?>
                                            <input type="file" name="preview_pic8" id="preview_pic8" onchange="upload(this.id)">
                                        </td>
                                        <td>ALT欄位</td>
                                        <td>
                                            <input type="text" class="form-control" name="preview_alt8" value="<?=$row_goods["preview_alt8"]?>">
                                        </td>
                                    </tr>
                                </table>
                                <table class="table table-bordered table-hover">
                                    <tr>
                                        <td>試閱標題</td>
                                        <td colspan="3"><input type="text" class="form-control" name="preview_title9" value="<?=$row_goods["preview_title9"]?>"></td>
                                    </tr>
                                    <tr>
                                        <td>試閱圖片</td>
                                        <td>
                                            <?php
                                                if(!empty($row_goods['preview_pic9'])) {
                                                    echo "<img src='".WEBSITE_URL."upload/".$row_goods['preview_pic9']."' width='80px' class='preview_pic9'>";
													echo "<input type='hidden' class='old_preview_img9' value='".$row_goods['preview_pic9']."' name='old_preview_pic9'>";
													echo "<input type='hidden' class='old_preview_img9' value='".$row_goods['preview_pic9_wh']."' name='old_preview_pic9_wh'>";
                                                    echo "<br>".$row_goods['preview_pic9'];
                                                    echo "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<a href='javascript:void(0);' class='btn btn-danger' onclick='$(\".old_preview_img9\").val(\"\");$(\".preview_pic9\").hide();$(this).hide()'>刪除圖片</a>";
                                                }
                                            ?>
                                            <input type="file" name="preview_pic9" id="preview_pic9" onchange="upload(this.id)">
                                        </td>
                                        <td>ALT欄位</td>
                                        <td>
                                            <input type="text" class="form-control" name="preview_alt9" value="<?=$row_goods["preview_alt9"]?>">
                                        </td>
                                    </tr>
                                </table>
                                <table class="table table-bordered table-hover">
                                    <tr>
                                        <td>試閱標題</td>
                                        <td colspan="3"><input type="text" class="form-control" name="preview_title10" value="<?=$row_goods["preview_title10"]?>"></td>
                                    </tr>
                                    <tr>
                                        <td>試閱圖片</td>
                                        <td>
                                            <?php
                                                if(!empty($row_goods['preview_pic10'])) {
                                                    echo "<img src='".WEBSITE_URL."upload/".$row_goods['preview_pic10']."' width='80px' class='preview_pic10'>";
													echo "<input type='hidden' class='old_preview_img10' value='".$row_goods['preview_pic10']."' name='old_preview_pic10'>";
													echo "<input type='hidden' class='old_preview_img10' value='".$row_goods['preview_pic10_wh']."' name='old_preview_pic10_wh'>";
                                                    echo "<br>".$row_goods['preview_pic10'];
                                                    echo "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<a href='javascript:void(0);' class='btn btn-danger' onclick='$(\".old_preview_img10\").val(\"\");$(\".preview_pic10\").hide();$(this).hide()'>刪除圖片</a>";
                                                }
                                            ?>
                                            <input type="file" name="preview_pic10" id="preview_pic10" onchange="upload(this.id)">
                                        </td>
                                        <td>ALT欄位</td>
                                        <td>
                                            <input type="text" class="form-control" name="preview_alt10" value="<?=$row_goods["preview_alt10"]?>">
                                        </td>
                                    </tr>
                                </table>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">試聽檔案</label>
                            <div class="col-sm-10">
                                <?php
                                    if(!empty($row_goods['preview_file'])) {
                                        echo "<audio controls class='old_preview_file2'>
                                              <source src=\"".WEBSITE_URL."upload/".$row_goods['preview_file']."\" type=\"audio/mpeg\">
                                            Your browser does not support the audio element.
                                            </audio>";
                                        echo "<br>".$row_goods['preview_file'];
                                        echo "<input type='hidden' class='old_preview_file' value='".$row_goods['preview_file']."' name='old_preview_file'>";
                                        echo "<a href='javascript:void(0);' class='btn btn-danger' onclick='$(\".old_preview_file\").val(\"\");$(\".old_preview_file2\").hide();$(this).hide()'>刪除</a>";
                                    }
                                ?>
                                <input type="file" name="preview_file">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">其他推薦商品</label>
                            <div class="col-sm-10">
                                <select class="select2_ajax_category form-control" multiple name="recommand_goods[]" style="width:100%;">
                                    <?php
                                        foreach ($arr_select_goods as $key => $value) {
                                            $selected = "";
                                            if(in_array($value["Goods_ID"], $row_goods['recommand_goods']))
                                                $selected = "selected";
                                            if($value["Goods_ID"] == $row_goods['Goods_ID'])
                                                continue;
                                            echo "<option value=".$value['Goods_ID']." ".$selected.">[".$value['item_number']."]".$value['Goods_Name']."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">庫存數量</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control" name="stock" value="<?=$row_goods["stock"]?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">庫存安全量</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control" name="safe_stock" value="<?=$row_goods["safe_stock"]?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label"><a style="color:red">*</a>最低購買量</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control" name="min_buy" id="min_buy" value="<?=$row_goods["min_buy"]?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label"><a style="color:red">*</a>最大購買量</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control" name="max_buy" id="max_buy" value="<?=$row_goods["max_buy"]?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">支援使用熊贈點</label>
                            <div class="col-sm-10 control-text">
								 <?php if($action=="edit"){ ?>
                                <input type="radio" name="point_use" class="point_use" value="1" <?=($row_goods['point_use'] == 1)?"checked":""?>>是
                                <input type="radio" name="point_use" class="point_use" value="0" <?=($row_goods['point_use'] == 0)?"checked":""?>>否
								 <?php }else{ ?>
                                <input type="radio" name="point_use" class="point_use" value="1">是
                                <input type="radio" name="point_use" class="point_use" value="0">否
								 <?php } ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">支援會員等級折扣</label>
                            <div class="col-sm-10 control-text">
								<?php if($action=="edit"){ ?>
                                <input type="radio" name="level_use" value="1" <?=($row_goods['level_use'] == 1)?"checked":""?>>是
                                <input type="radio" name="level_use" value="0" <?=($row_goods['level_use'] == 0)?"checked":""?>>否
								 <?php }else{ ?>
                                <input type="radio" name="level_use" value="1">是
                                <input type="radio" name="level_use" value="0">否
								 <?php } ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">支援使用折價券</label>
                            <div class="col-sm-10 control-text">
								<?php if($action=="edit"){ ?>
                                <input type="radio" name="ticket_use" value="1" <?=($row_goods['ticket_use'] == 1)?"checked":""?>>是
                                <input type="radio" name="ticket_use" value="0" <?=($row_goods['ticket_use'] == 0)?"checked":""?>>否
								 <?php }else{ ?>
                                <input type="radio" name="ticket_use" value="1">是
                                <input type="radio" name="ticket_use" value="0">否
								 <?php } ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">支援滿額贈禮/折價券</label>
                            <div class="col-sm-10 control-text">
								<?php if($action=="edit"){ ?>
                                <input type="radio" name="price_ticket_use" value="1" <?=($row_goods['price_ticket_use'] == 1)?"checked":""?>>是
                                <input type="radio" name="price_ticket_use" value="0" <?=($row_goods['price_ticket_use'] == 0)?"checked":""?>>否
								 <?php }else{ ?>
                                <input type="radio" name="price_ticket_use" value="1">是
                                <input type="radio" name="price_ticket_use" value="0">否
								 <?php } ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">支援折扣活動</label>
                            <div class="col-sm-10 control-text">
								<?php if($action=="edit"){ ?>
                                <input type="radio" name="item_discount_use" value="1" <?=($row_goods['item_discount_use'] == 1)?"checked":""?>>是
                                <input type="radio" name="item_discount_use" value="0" <?=($row_goods['item_discount_use'] == 0)?"checked":""?>>否
								 <?php }else{ ?>
                                <input type="radio" name="item_discount_use" value="1">是
                                <input type="radio" name="item_discount_use" value="0">否
								 <?php } ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">支援折抵活動</label>
                            <div class="col-sm-10 control-text">
								<?php if($action=="edit"){ ?>
                                <input type="radio" name="price_discount_use" value="1" <?=($row_goods['price_discount_use'] == 1)?"checked":""?>>是
                                <input type="radio" name="price_discount_use" value="0" <?=($row_goods['price_discount_use'] == 0)?"checked":""?>>否
								 <?php }else{ ?>
                                <input type="radio" name="price_discount_use" value="1">是
                                <input type="radio" name="price_discount_use" value="0">否
								 <?php } ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">物流配送</label>
                            <div class="col-sm-10 control-text">
								<?php if($action=="edit"){ ?>
                                <input type="radio" name="delivery_use" value="1" <?=($row_goods['delivery_use'] == 1)?"checked":""?>>是
                                <input type="radio" name="delivery_use" value="0" <?=($row_goods['delivery_use'] == 0)?"checked":""?>>否
								 <?php }else{ ?>
                                <input type="radio" name="delivery_use" value="1">是
                                <input type="radio" name="delivery_use" value="0">否
								 <?php } ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">超商取貨</label>
                            <div class="col-sm-10 control-text">
								<?php if($action=="edit"){ ?>
                                <input type="radio" name="store_use" value="1" <?=($row_goods['store_use'] == 1)?"checked":""?>>是
                                <input type="radio" name="store_use" value="0" <?=($row_goods['store_use'] == 0)?"checked":""?>>否
								 <?php }else{ ?>
                                <input type="radio" name="store_use" value="1">是
                                <input type="radio" name="store_use" value="0">否
								 <?php } ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">超商取貨付款</label>
                            <div class="col-sm-10 control-text">
								<?php if($action=="edit"){ ?>
                                <input type="radio" name="store_pay_use" value="1" <?=($row_goods['store_pay_use'] == 1)?"checked":""?>>是
                                <input type="radio" name="store_pay_use" value="0" <?=($row_goods['store_pay_use'] == 0)?"checked":""?>>否
								 <?php }else{ ?>
                                <input type="radio" name="store_pay_use" value="1">是
                                <input type="radio" name="store_pay_use" value="0">否
								 <?php } ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">線上付款</label>
                            <div class="col-sm-10 control-text">
								<?php if($action=="edit"){ ?>
                                <input type="radio" name="online_pay_use" value="1" <?=($row_goods['online_pay_use'] == 1)?"checked":""?>>是
                                <input type="radio" name="online_pay_use" value="0" <?=($row_goods['online_pay_use'] == 0)?"checked":""?>>否
								 <?php }else{ ?>
                                <input type="radio" name="online_pay_use" value="1">是
                                <input type="radio" name="online_pay_use" value="0">否
								 <?php } ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">貨到付款</label>
                            <div class="col-sm-10 control-text">
								<?php if($action=="edit"){ ?>
                                <input type="radio" name="delivery_pay_use" value="1" <?=($row_goods['delivery_pay_use'] == 1)?"checked":""?>>是
                                <input type="radio" name="delivery_pay_use" value="0" <?=($row_goods['delivery_pay_use'] == 0)?"checked":""?>>否
								 <?php }else{ ?>
                                <input type="radio" name="delivery_pay_use" value="1">是
                                <input type="radio" name="delivery_pay_use" value="0">否
								 <?php } ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Meta Keywords</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="meta_keyrowd" value="<?=$row_goods['meta_keyrowd']?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Meta Description</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="meta_desc" value="<?=$row_goods['meta_desc']?>">
                            </div>
                        </div>
						
						<div class="form-group">
                            <label for="name" class="col-sm-2 control-label">上架日期</label>
                            <div class="col-sm-10">
                                <input id="stime" name="daterange_s" type="text" class="datetimepicker" value="<?=(!empty($row_goods["Goods_sdate"]))?date("Y/m/d H:i", strtotime($row_goods['Goods_sdate'])):""?>"> - 
                                <input id="etime" name="daterange_e" type="text" class="datetimepicker" value="<?=(!empty($row_goods["Goods_edate"]))?date("Y/m/d H:i", strtotime($row_goods['Goods_edate'])):""?>">
                            </div>
                        </div>
						
                        <div class="form-group">
                            <label class="col-sm-2 control-label">排序</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="sort" value="<?=$row_goods['sort']?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">狀態</label>
                            <div class="col-sm-10 control-text">
                                <label>
                                    <input type="radio" name="status" value="0" <?php if($row_goods["Status"] == 0) {echo "checked";}?>>下架
                                </label>
                                <label>
                                    <input type="radio" name="status" value="1" <?php if($row_goods["Status"] == 1) {echo "checked";}?>>上架
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">建立時間</label>
                            <div class="col-sm-10" style="line-height: 32px;">
                                <i class="fa fa-clock-o" aria-hidden="true"></i> <?=date("Y-m-d H:i:s")?>
                            </div>
                        </div>


                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-right">
                        <input type="hidden" name="id" value="<?=$row_goods["Goods_ID"]?>">
                        <a href="javascript:history.back()" class="btn btn-danger" style="margin-right: 10px;">取消，並返回上一層</a>
                        <button type="button" class="btn btn-primary" onclick="check_file();">儲存</button>
                        <input type="hidden" name="referer" value="<?=(!empty($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:"")?>">
                    </div>
                    <!-- /.box-footer -->
                </form>
                <script>
					//優惠折數
					function count(){
						if(document.getElementById("discount_price").value>100){
							alert("優惠折價數需小於100");
							document.getElementById("discount_price").value = "";
							document.getElementById("discount_price2").value = "";
							document.getElementById('discount_price').focus();
							return false;
						}else if(document.getElementById("discount_price").value > 0){ 
							if(document.getElementById("discount_price").value >=10){
								document.getElementById("discount_price2").value = Math.round(document.getElementById("sell_price").value * document.getElementById("discount_price").value / 100); 
							}
							else{
								document.getElementById("discount_price2").value = Math.round(document.getElementById("sell_price").value * document.getElementById("discount_price").value / 10); 	
							}
							//alert(1);
						}else{ 
							document.getElementById("discount_price2").value = "";
							//alert(9);
						}
						
					}		
		
					var filePath ="";
					var fileType ="";
					function upload(id){
						filePath = $('#'+id).val().toLowerCase().split(".");
						fileType =  filePath[filePath.length - 1]; //獲得文件結尾的類型如 zip rar 這種寫法確保是最後的
						console.log(filePath);
						console.log(fileType);
						if(!(fileType == "png" || fileType == "jpg" || fileType == "jpeg" )){
							alert('圖片檔案格式有誤，只支援.PNG、.JPG、.JPEG');
							$('#'+id).val('');
							return false;
						//}else if(fileSize.size>10485760){
							//alert('錯誤！請上傳不超過10M的文件');
							//$('#'+id).val('');
							//return false;
						}
					}
					
                    function isRemeber() {
                        if (document.getElementById("name").value == '') {
                            alert('商品名稱不可為空值');
                            return false;
                        }

                        var category = document.getElementById("category");
                        var index = category.selectedIndex;

                        if ( category.options[index].value == '') {
                            alert('第一層分類不可為空值');
                            return false;
                        }

						var sub_category = document.getElementById("sub_category");
                        console.log(sub_category);
                        var indexs = sub_category.selectedIndex;
                        
                        if (indexs == -1) {
                            alert('第二層分類不可為空值');
                            return false;
                        }

                        var company = document.getElementById("company");
                        var indexss = company.selectedIndex;

                        if ( company.options[indexss].value == '') {
                            alert('所屬公司不可為空值');
                            return false;
                        }
						
						if (document.getElementById("banner_pc_pic").value == '') {
							if(document.getElementById("old_banner_pc_pic")) {
								if (document.getElementById("old_banner_pc_pic").value == '') {
									alert('banner pc不可為空值');
									return false;
								}
							} else {
								alert('banner pc不可為空值');
								return false;
							}
                        }
						
						if (document.getElementById("banner_m_pic").value == '') {
							if(document.getElementById("old_banner_m_pic")) {
								if (document.getElementById("old_banner_m_pic").value == '') {
									alert('banner mobile不可為空值');
									return false;
								}
							} else {
								alert('banner mobile不可為空值');
								return false;
							}
                        }
						
						
                        if (document.getElementById("sell_price").value == '') {
                            alert('市售價不可為空值');
                            return false;
                        }

                        if (document.getElementById("item_number").value == '') {
                            alert('商品貨號不可為空值');
                            return false;
                        }

                        var item = null;
                        var obj = document.getElementsByName("type")
                        for (var i = 0; i < obj.length; i++) { //遍歷Radio 
                            if (obj[i].checked) {
                                item = obj[i].value;                   
                            }
                        }
                        if(item == null) {
                            alert('商品種類不可為空值');
                            return false;
                        } else if(item == 2) {
                            var item_group = document.getElementById("item_group");
							console.log(item_group);
                            var indexsss = item_group.selectedIndex;
                            if (indexsss == -1) {
                                alert('組成商品不可為空值');
                                return false;
                            }

                        }
						if (document.getElementById("gift_number").value != '') {
                            if (document.getElementById("gift_count").value == '') {
								alert('贈品數量不可為空值');
								return false;
							}
                        }
						

                        if (document.getElementById("min_buy").value == '') {
                            alert('最低購買量不可為空值');
                            return false;
                        }

                        if (document.getElementById("max_buy").value == '') {
                            alert('最大購買量不可為空值');
                            return false;
                        }
						if ($('input[name=point_use]:checked').val() == null) {
                            alert('支援使用熊贈點不可為空值');
                            return false;
                        }
						if ($('input[name=level_use]:checked').val() == null) {
                            alert('支援會員等級折扣不可為空值');
                            return false;
                        }
						if ($('input[name=ticket_use]:checked').val() == null) {
                            alert('支援使用折價券不可為空值');
                            return false;
                        }
						if ($('input[name=price_ticket_use]:checked').val() == null) {
                            alert('支援滿額贈禮/折價券不可為空值');
                            return false;
                        }
						if ($('input[name=item_discount_use]:checked').val() == null) {
                            alert('支援折扣活動不可為空值');
                            return false;
                        }
						if ($('input[name=price_discount_use]:checked').val() == null) {
                            alert('支援折抵活動不可為空值');
                            return false;
                        }
						if ($('input[name=delivery_use]:checked').val() == null) {
                            alert('物流配送不可為空值');
                            return false;
                        }
						if ($('input[name=store_use]:checked').val() == null) {
                            alert('超商取貨不可為空值');
                            return false;
                        }
						if ($('input[name=store_pay_use]:checked').val() == null) {
                            alert('超商取貨付款不可為空值');
                            return false;
                        }
						if ($('input[name=online_pay_use]:checked').val() == null) {
                            alert('線上付款');
                            return false;
                        }
						if ($('input[name=delivery_pay_use]:checked').val() == null) {
                            alert('貨到付款');
                            return false;
                        }
                    }
                </script>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
    <div class="add-variant hidden">
        <div class="form-group">
            <label class="col-sm-2 control-label">規格</label>
            <div class="col-sm-2">
                <input class="variant" name="variant[]" type="text" class="form-control" placeholder=""
                       value="" onkeyup="change_variant()">
            </div>
            <label class="col-sm-1 control-label">選項</label>
            <div class="col-sm-6">
                <select name="option[][]" class="form-control select2_category_item" multiple onchange="setItem()"></select>
                <div>

                </div>

            </div>
            <div class="col-sm-1">
                <a href="javascript:void(0);" class="remove_variant" onclick="remove_variant(this)" style="display:none;">
                    <i class="fa fa-trash"></i>
                </a>
            </div>
        </div>

    </div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>