<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("product");

$sql_where = "";
if(!empty($_GET['title'])) {
    $sql_where .= " and g.Goods_Name like '%".checkinput_sql($_GET['title'],19)."%'";
}

if(!empty($_GET['category'])) {
    $sql_where .= " and g.Category_ID like '%".checkinput_sql($_GET['category'],19)."%'";
}
if(!empty($_GET['dealer'])) {
    $sql_where .= " and g.Dealer_ID = '".checkinput_sql($_GET['dealer'],19)."'";
}

//取出總筆數
$sql_cmd = "select count(*) from goods as g
            join dealer as d on g.Dealer_ID = d.Dealer_ID where g.delete_at is null ".$sql_where." ";
$rs = $db->query($sql_cmd);
$row = $rs->fetchRow();
$total = $row[0];

$mypage = new page;
$pages = $mypage->set_page($total, $PAGE_NUM);

//設定分頁的初始值
if (empty($_GET['num']))
{
    $num = 1;
}else{
    $num = intval($_GET['num']);
}


$sql_cmd = "select g.*,d.dealer_name from goods as g
            join dealer as d on g.Dealer_ID = d.Dealer_ID
            where g.delete_at is null ".$sql_where." order by Goods_ID desc ".$pages[$num];
$rs_goods = $db->query($sql_cmd);
$tmp = $_GET;
unset($tmp['num']);
$val = http_build_query($tmp);
$page = $mypage->multi($total , $PAGE_NUM, $num, "./rlist.php", "", $val);


// 商家list
$sql_cmd = "select * from dealer where delete_at is null";
$rs = $db->query($sql_cmd);
while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
    $row_dealer[] = $row;
}

// 分類list
$sql_cmd = "select * from category where Category_CodeGroup = 'Goods_Category' and Status = 1 order by create_datetime desc ";
$rs = $db->query($sql_cmd);
while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
    $row_category[] = $row;
}

?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php"); ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                商品管理
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box">
                <div class="box-header">
                    <form action="./rlist.php" method="GET">
                        <div style="float:left; display:inline-block; width:180px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">搜尋</button>
                                </div>
                                <input type="text" name="title" class="form-control input-sm"
                                       placeholder="名稱" value="<?=(!empty($_GET['title']))?$_GET['title']:""?>">
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:180px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">分類</button>
                                </div>
                                <select class="form-control" name="category">
                                    <option value="">全部</option>
                                    <? foreach($row_category as $item): ?>
                                        <option value="<?=$item['Category_Code']?>" <?=(!empty($_GET['category']) && $_GET['category'] == $item['Category_Code'])?"selected":""?>><?=$item['Category_Name']?></option>
                                    <? endforeach ?>
                                </select>
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:180px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">商家</button>
                                </div>
                                <select class="form-control" name="dealer">
                                    <option value="">全部</option>
                                    <? foreach($row_dealer as $item): ?>
                                        <option value="<?=$item['Dealer_ID']?>" <?=(!empty($_GET['dealer']) && $_GET['dealer'] === $item['Dealer_ID'])?"selected":""?>><?=$item['dealer_name']?></option>
                                    <? endforeach ?>
                                </select>
                            </div>
                        </div>

                        <a class="btn btn-danger btn-sm" href="./rlist.php">清除</a>
                        <button type="submit" class="btn btn-success btn-sm">送出</button>
                        <a class="pull-right btn btn-success btn-sm" href="./rdetail.php?action=add">新增</a>
                    </form>
                </div>
                <div class="box-body no-padding">
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                <th>id</th>
                                <th>名稱</th>
                                <th>分類</th>
                                <th>商家</th>
                                <th>狀態</th>
                                <th>創建時間</th>
                                <th>操作</th>
                            </tr>
                            <?php 
                                while($row = $rs_goods->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                                    $Category_Name = array();
                                    $Category_Code = explode(",", $row["Category_ID"]);
                                    $sql_cmd = "select Category_Name from category where 
                                        Category_CodeGroup = 'Goods_Category' 
                                        and Category_Code in ('".implode("','", $Category_Code)."')";
                                    $rs = $db->query($sql_cmd);
                                while($row_category = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                                    $Category_Name[] = $row_category["Category_Name"];
                                }
                            ?>
                            <tr onClick="iCheckThisRow(this);">
                                <th><?=$row['Goods_ID']?></th>
                                <th><?=$row['Goods_Name']?></th>
                                <th><?=implode(",", $Category_Name);?></th>
                                <th><?=$row['dealer_name']?></th>
                                <th><?=($row['Status']==1)?"上架":"下架"?></th>
                                <th><?=$row['create_datetime']?></th>
                                <th>
                                    <a class="btn btn-xs btn-primary" href="./rdetail.php?action=edit&Goods_ID=<?=$row['Goods_ID']?>">編輯</a>
                                    <a class="btn btn-xs btn-danger" href="javascript:void(0);" onclick="if(confirm('是否確認刪除？')){location.href='./rdelete.php?Goods_ID=<?=$row['Goods_ID']?>';}">刪除</a>
                                </th>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <?=$page?>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>