<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("product");

$sql_where = "";

$Goods_ID  = filter_input(INPUT_GET, 'Goods_ID');
$title  = filter_input(INPUT_GET, 'title');
$status = filter_input(INPUT_GET, 'status');
$s_date  = filter_input(INPUT_GET, 's_date');
$e_date  = filter_input(INPUT_GET, 'e_date');


if(!empty($title))
    $sql_where .= " and title like '%".checkinput_sql($title,255)."%'";
if(in_array($status, ["0","1"]))
    $sql_where .= " and status = '".checkinput_sql($status,3)."'";
if(!empty($s_date))
    $sql_where .= " and update_datetime >= '".checkinput_sql($s_date." 00:00:00",19)."'";
if(!empty($e_date))
    $sql_where .= " and update_datetime <= '".checkinput_sql($e_date." 23:59:59",19)."'";

//取出總筆數
$sql_cmd = "select count(*) from goods_qa where Goods_ID = '".$Goods_ID."' and deleted_at is null ".$sql_where." ";
$rs = $db->query($sql_cmd);
$row = $rs->fetchRow();
$total = $row[0];

$mypage = new page;
$pages = $mypage->set_page($total, $PAGE_NUM);

//設定分頁的初始值
if (empty($_GET['num']))
{
    $num = 1;
}else{
    $num = intval($_GET['num']);
}


$sql_cmd = "select * from goods_qa where Goods_ID = '".$Goods_ID."' and deleted_at is null ".$sql_where." order by create_datetime desc ".$pages[$num];
$rs = $db->query($sql_cmd);
$row_category = [];
while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
    $row_category[] = $row;
}
$tmp = $_GET;
unset($tmp['num']);
$val = http_build_query($tmp);
$page = $mypage->multi($total , $PAGE_NUM, $num, "./list.php", "", $val);



?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php"); ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                商品常見問題上稿管理
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box">
                <form action="./goods_qa_list.php" method="GET">
                    <div class="box-header">
                        <div style="float:left; display:inline-block; width:180px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">搜尋</button>
                                </div>
                                <input type="text" name="title" class="form-control input-sm"
                                       placeholder="標題" value="<?=(!empty($_GET['title']))?$_GET['title']:""?>">
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:150px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">修改時間</button>
                                </div>
                                <input type="text" name="s_date" class="form-control input-sm datetimepicker"
                                       placeholder="起始日" value="<?=(!empty($_GET['s_date']))?$_GET['s_date']:""?>">
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:150px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">~</button>
                                </div>
                                <input type="text" name="e_date" class="form-control input-sm datetimepicker"
                                       placeholder="結束日" value="<?=(!empty($_GET['e_date']))?$_GET['e_date']:""?>">
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:180px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">狀態</button>
                                </div>
                                <select class="form-control" name="status">
                                    <option value="all">全部</option>
                                    <option value="1" <?=($status == "1")?"selected":""?>>啟用</option>
                                    <option value="0" <?=($status == "0")?"selected":""?>>停用</option>
                                </select>
                            </div>
                        </div>
                        <input type='hidden' value="<?=$Goods_ID?>" name="Goods_ID"> 
                        <button type="submit" class="btn btn-success btn-sm pull-right">送出</button>
                        <a class="btn btn-danger btn-sm pull-right" href="./goods_qa_list.php?Goods_ID=<?=$Goods_ID?>" style="margin-right: 10px;">清除</a>
                        <div style="float:left; display:inline-block; width:100%;margin-top: 10px;">
                            <a class="pull-right btn btn-success btn-sm" href="./goods_qa_detail.php?action=add&Goods_ID=<?=$Goods_ID?>">新增</a>
                            <input class="pull-right btn btn-success btn-sm" type="button" value="刪除" onclick="if(confirm('是否刪除資料？刪除後無法刪除後無法還原！')) {$('form').attr('action','./goods_qa_delete.php');$('form').submit();}"style="margin-right: 10px;">
                        </div>
                    </div>
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <th><a href="javascript:void(0)" onclick="select_all('')">(全選)</a></th>
                                    <th>標題名稱</th>
                                    <th>排序</th>
                                    <th>狀態</th>
                                    <th>修改時間</th>
                                    <th>操作</th>
                                </tr>
                                <?php 
                                    foreach ($row_category as $key => $value) :
                                ?>
                                <tr onClick="iCheckThisRow(this);">
                                    <th><input type="checkbox" name="id[]" value="<?=$value["id"]?>"></th>
                                    <th><?=$value['title']?></th>
                                    <th><?=$value['sort']?></th>
                                    <th><?=($value['status'] == 1)?"啟用":"停用"?></th>
                                    <th><?=$value['update_datetime']?></th>
                                    <th>
                                        <a class="btn btn-xs btn-primary" href="./goods_qa_detail.php?action=edit&id=<?=$value['id']?>&Goods_ID=<?=$Goods_ID?>">編輯</a>
                                        <a class="btn btn-xs btn-danger" href="javascript:void(0);" onclick="if(confirm('是否確認刪除？')){location.href='./goods_qa_delete.php?id=<?=$value['id']?>';}">刪除</a>
                                    </th>
                                </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
            <?=$page?>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>