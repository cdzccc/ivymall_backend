<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("rank_edit");
$id = filter_input(INPUT_POST, 'id');

$rank_id     = filter_input(INPUT_POST, 'rank_id');
$alt         = filter_input(INPUT_POST, 'alt');
$item_number = filter_input(INPUT_POST, 'item_number');
$sort        = filter_input(INPUT_POST, 'sort', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 99999, 'default' => 0)));

$old_pic     = filter_input(INPUT_POST, 'old_cover_pic');

if(empty($id)) {
    js_go_back_global("NOT_POST");
    exit;
}

if(empty($item_number)) {
    $err_msg = "欄位";
    if(empty($item_number))
        $err_field[] = "商品貨號";
    $err_msg .= "'".implode("','", $err_field)."'不可為空值";
    js_go_back_self($err_msg);
    exit;
}


if ($_FILES['pic']['name'] != "none" && is_uploaded_file($_FILES['pic']['tmp_name']))
{
    //重組檔名
    $type = explode(".", $_FILES['pic']['name']);
    $pic = $_FILES['pic']['name'];
    if(file_exists(DOCUMENT_ROOT."/upload/".$pic)) {
        // js_go_back_self("檔名重複!!");
        // exit;
    }
    @copy($_FILES['pic']['tmp_name'], DOCUMENT_ROOT."/upload/".$pic);
}
else {
    $pic = $old_pic;
}

$update_datetime = date("Y-m-d H:i:s");
$update_user     = $_SESSION[SESSION_VARIABLE."_user_id"];

$sql_array = array(
    "alt"             => array("2", checkinput_sql($alt, 200)),
    "sort"            => array("2", checkinput_sql($sort, 200)),
    "item_number"     => array("2", checkinput_sql($item_number , 45)),
    "pic"             => array("2", checkinput_sql($pic,200)),
    "update_datetime" => array("2", checkinput_sql($update_datetime, 50)),
    "update_user"     => array("2", checkinput_sql($update_user, 50)),
);
$sql_cmd = update("goods_rank_list", array("id", $id), $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
   js_go_back_global("DB_EDIT_ERROR");
   exit;
}else{
    add_log('排行榜-參與商品','2');
   $db->disconnect();
    $referer = filter_input(INPUT_POST, 'referer');
    $redirect = "./list.php?rank_id=".$rank_id;
    if(!empty($referer))
        $redirect = $referer;
    js_repl_global( $redirect, "EDIT_SUCCESS");

   exit;
}
?>
