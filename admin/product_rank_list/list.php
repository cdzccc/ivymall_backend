<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("rank");
$sql_where = "";

$title  = filter_input(INPUT_GET, 'title');

if(!empty($title))
    $sql_where .= " and (item_number like '%".checkinput_sql($title,255)."%')";

//取出總筆數
$sql_cmd = "select count(*) from goods_rank_list where deleted_at is null ".$sql_where." and rank_id = '".$_GET['rank_id']."'";
$rs = $db->query($sql_cmd);
$row = $rs->fetchRow();
$total = $row[0];

$mypage = new page;
$pages = $mypage->set_page($total, $PAGE_NUM);

//設定分頁的初始值
if (empty($_GET['num']))
{
    $num = 1;
}else{
    $num = intval($_GET['num']);
}

$sql_cmd = "select * from goods_rank_list where deleted_at is null ".$sql_where." and rank_id = '".$_GET['rank_id']."' order by create_datetime desc ".$pages[$num];
$rs_link  = $db->query($sql_cmd);
$tmp = $_GET;
unset($tmp['num']);
$val = http_build_query($tmp);
$page = $mypage->multi($total , $PAGE_NUM, $num, "./list.php", "", $val);
?><!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php"); ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                排行榜-參與商品
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box">
                <form action="./list.php" method="GET">
                    <div class="box-header">
                        <div style="float:left; display:inline-block; width:180px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">搜尋</button>
                                </div>
                                <input type="text" name="title" class="form-control input-sm"
                                       placeholder="貨號" value="<?=(!empty($_GET['title']))?$_GET['title']:""?>">
                            </div>
                        </div>
                        <input type="hidden" name="rank_id" value="<?=$_GET['rank_id']?>">
                        <button type="submit" class="btn btn-success btn-sm pull-right">送出</button>
                        <a class="btn btn-danger btn-sm pull-right" href="./list.php" style="margin-right: 10px;">清除</a>
                        <div style="float:left; display:inline-block; width:100%;margin-top: 10px;">
                            <a class="pull-right btn btn-success btn-sm" href="./detail.php?action=add&rank_id=<?=$_GET['rank_id']?>">新增</a>
                            <input class="pull-right btn btn-success btn-sm" type="button" value="刪除" onclick="if(confirm('是否刪除資料？刪除後無法刪除後無法還原！')) {$('form').attr('action','./delete.php&rank_id=<?=$_GET['rank_id']?>');$('form').submit();}"style="margin-right: 10px;">
                        </div>
                    </div>
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <th style="width:100px;"><a href="javascript:void(0)" onclick="select_all('')">(全選)</a></th>
                                    <th>商品貨號</th>
                                    <th>排序</th>
                                    <th>操作</th>
                                </tr>
                                <?php
                                if($rs_link->numRows() > 0) {
                                while($row = $rs_link->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                                ?>
                                <tr onClick="iCheckThisRow(this);">
                                    <th><input type="checkbox" name="id[]" value="<?=$row["id"]?>"></th>
                                    <th><?=$row['item_number']?></th>
                                    <th><?=$row['sort']?></th>
                                    <th>
                                        <a class="btn btn-xs btn-primary" href="./detail.php?action=edit&id=<?=$row['id']?>&rank_id=<?=$_GET['rank_id']?>">編輯</a>
                                        <a class="btn btn-xs btn-danger" href="javascript:void(0);" onclick="if(confirm('是否確認刪除？')){location.href='./delete.php?id=<?=$row["id"]?>&rank_id=<?=$_GET['rank_id']?>';}">刪除</a>
                                    </th>
                                </tr>
                                <?php }} ?>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
		<?=$page?>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>