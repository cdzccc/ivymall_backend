<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("store_711");
$sql_where = "";

$title       = filter_input(INPUT_GET, 'title');
$Delivery_No = filter_input(INPUT_GET, 'Delivery_No');
$s_date      = filter_input(INPUT_GET, 's_date');
$e_date      = filter_input(INPUT_GET, 'e_date');
$status      = filter_input(INPUT_GET, 'status');

if(!isset($_GET['s_date']))
    $s_date = date("Y-m-d 00:00:00", strtotime("- 1 days"));
if(!isset($_GET['e_date']))
    $e_date = date("Y-m-d 23:59:59");

if(!empty($title))
    $sql_where .= " and (o.Order_ID like '%".checkinput_sql($title,255)."%')";
if(!empty($Delivery_No))
    $sql_where .= " and (o.Delivery_No like '%".checkinput_sql($Delivery_No,255)."%')";
if(!empty($s_date))
    $sql_where .= " and s.crdate >= '".checkinput_sql($s_date." 00:00:00",19)."'";
if(!empty($e_date))
    $sql_where .= " and s.crdate <= '".checkinput_sql($e_date." 23:59:59",19)."'";
if(is_numeric($status))
    $sql_where .= " and o.store_update = '".checkinput_sql($status,19)."'";

//取出總筆數
$sql_cmd = "select count(*) from `order` as o
    left join store as s on o.Order_ID = s.order_id
    where o.`Status` not in (1,2,9,10) and o.delivery = 1 and o.deleted_at is null ".$sql_where." ";
$rs = $db->query($sql_cmd);
// print_r($rs);
$row = $rs->fetchRow();
$total = $row[0];

$mypage = new page;
$pages = $mypage->set_page($total, $PAGE_NUM);

//設定分頁的初始值
if (empty($_GET['num']))
{
    $num = 1;
}else{
    $num = intval($_GET['num']);
}

$sql_cmd = "select * from `order` as o
    left join store as s on o.Order_ID = s.order_id
    where o.`Status` not in (1,2,9,10) and delivery = 1 and o.deleted_at is null ".$sql_where." order by create_datetime desc ".$pages[$num];
$rs_link  = $db->query($sql_cmd);
$tmp = $_GET;
unset($tmp['num']);
$val = http_build_query($tmp);
$page = $mypage->multi($total , $PAGE_NUM, $num, "./list.php", "", $val);
?><!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php"); ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                7-11超商訂單查詢
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box">
                <div class="box-header">
                    <form name="search" action="./list.php" method="GET" enctype="multipart/form-data">
                        <div style="float:left; display:inline-block; width:180px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">搜尋</button>
                                </div>
                                <input type="text" name="title" class="form-control input-sm"
                                       placeholder="訂單編號" value="<?=(!empty($_GET['title']))?$_GET['title']:""?>">
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:180px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">配送編號</button>
                                </div>
                                <input type="text" name="Delivery_No" class="form-control input-sm"
                                       placeholder="" value="<?=(!empty($_GET['Delivery_No']))?$_GET['Delivery_No']:""?>">
                            </div>
                        </div>

                        <div style="float:left; display:inline-block; width:230px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">轉出成功時間</button>
                                </div>
                                <input type="text" name="s_date" class="form-control input-sm datetimepicker5"
                                       placeholder="起始日" value="<?=$s_date?>">
                            </div>
                        </div>

                        <div style="float:left; display:inline-block; width:180px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">~</button>
                                </div>
                                <input type="text" name="e_date" class="form-control input-sm datetimepicker5"
                                       placeholder="結束日" value="<?=$e_date?>">
                            </div>
                        </div>
                        
                        <div style="float:left; display:inline-block; width:200px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">轉出狀態</button>
                                </div>
                                <select class="form-control input-sm" name="status">
                                    <option value="">全部</option>
                                    <option value="0" <?=($status == "0")?"selected":""?>>尚未轉出</option>
                                    <option value="1" <?=($status == "1")?"selected":""?>>轉出準備中</option>
                                    <option value="2" <?=($status == "2")?"selected":""?>>已轉出</option>
                                </select>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-success btn-sm pull-right">送出</button>
                        <a class="btn btn-danger btn-sm pull-right" href="./list.php?s_date=&e_date=" style="margin-right: 10px;">清除</a>
                        <div style="float:left; display:inline-block; width:100%; padding-top:10px;">
                            <input class="pull-left" type="file" name="upload">
                            <input class="pull-left" type="button" value="上傳解析檔" onclick="$('form[name=search]').attr('action','./upload.php');$('form[name=search]').attr('method','post');$('form[name=search]').submit()">
							<input class="pull-right btn btn-success btn-sm" type="button" value="下載超商標籤" onclick="$('form[name=data]').attr('action','./label.php');$('form').submit()">
							<input class="pull-right btn btn-success btn-sm" type="button" value="下載出貨檔" onclick="$('form[name=data]').attr('action','./download.php');$('form').submit()" style="margin-right:10px">
							<input class="pull-right btn btn-success btn-sm" type="button" value="轉出" onclick="$('form[name=data]').attr('action','./order_delivery.php');$('.store_update').val($('.store_update_tmp').val());$('form').submit()" style="margin-right:10px">
                            <select name="store_update_tmp" class="pull-right form-control input-sm store_update_tmp" style="width: 80px;">
                                <option value="0">尚未轉出</option>
                                <option value="1">轉出準備中</option>
                            </select>
                        </div>
                    </form>
                </div>
                <div class="box-body no-padding table-responsive">
                    <form name="data" action="./list.php" method="GET">
                        <table class="table table-striped">
                            <tbody>
                                <tr>
									<th>
                                        <a href="javascript:void(0)" onclick="select_all('')">(全選)</a>
                                        <input type="hidden" name="store_update" class="store_update">
                                    </th>
                                    <th>訂單編號</th>
                                    <th style="min-width:100px;">購買人姓名</th>
                                    <th style="min-width:100px;">購買人電話</th>
                                    <th style="min-width:100px;">收件人</th>
                                    <th style="min-width:100px;">收件人手機</th>
                                    <th style="min-width:80px;">消費金額</th>
                                    <th style="min-width:100px;">配送編號</th>
                                    <th style="min-width:150px;">上傳狀態</th>
                                    <th style="min-width:180px;">轉出成功時間</th>
                                    <th style="min-width:180px;">貨到物流時間</th>
                                    <th style="min-width:100px;">最新貨態</th>
                                    <th>操作</th>
                                </tr>
                                <?php
                                if($rs_link->numRows() > 0) {
                                while($row = $rs_link->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                                ?>
                                <tr onClick="iCheckThisRow(this);">
								    <th><input class="id_label" type="checkbox" name="id[]" value="<?=$row["Order_ID"]?>"></th>
                                    <th><?=$row['Order_ID']?></th>
                                    <th><?=$row['name']?></th>
                                    <th><?=$row['phone']?></th>
                                    <th><?=$row['Delivery_Name']?></th>
                                    <th><?=$row['Delivery_Mobile']?></th>
                                    <th><?=$row['total_price']?></th>
                                    <th><?=$row['Delivery_No']?></th>
                                    <th>
                                        <?php
                                            switch ($row['store_update']) {
                                                case '0':
                                                    echo "尚未轉出";
                                                    break;
                                                case '1':
                                                    echo "轉出準備中";
                                                    break;
                                                case '2':
                                                    echo "已轉出";
                                                    break;
                                            }
                                        ?>
                                    </th>
                                    <th>
									<?php if($row['store_update'] == 2) { echo $row['store_update_time']; } ?>
									</th>
                                    <th><?=$row['store_time']?></th>
                                    <th><?=$row['statusstr']?></th>
                                    <th>
                                        <a class="btn btn-xs btn-primary" href="./detail.php?action=edit&id=<?=$row['Order_ID']?>">編輯</a>
                                    </th>
                                </tr>
                                <?php }} ?>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
		<?=$page?>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>