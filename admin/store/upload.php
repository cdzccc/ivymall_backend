<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("store_711");
include DOCUMENT_ROOT.'/schedule/store/PRESCO/storefun.php';

if ($_FILES['upload']['name'] != "none" && is_uploaded_file($_FILES['upload']['tmp_name']))
{
    //重組檔名
    $name = $_FILES["upload"]["name"];
    $ext = end((explode(".", $name)));
    if(!in_array($ext, ["SRP","FILEOK","SURP","eta","ein","PPS","esp","ert","edr","evr"])) {
        js_go_back_self("未上傳檔案");
        exit;
    }
    $SRPDATA=file($_FILES['upload']['tmp_name']);
    switch ($ext) {
        case 'SRP':
            store_SRP($SRPDATA);
            break;
        case 'FILEOK':
            store_FILEOK($SRPDATA);
            break;
        case 'SURP':
            store_SURP($SRPDATA);
            break;
        case 'eta':
            store_ETA($SRPDATA);
            break;
        case 'ein':
            store_EIN($SRPDATA);
            break;
        case 'PPS':
            store_PPS($SRPDATA);
            break;
        case 'esp':
            store_ESP($SRPDATA);
            break;
        case 'ert':
            store_ERT($SRPDATA);
            break;
        case 'edr':
            store_EDR($SRPDATA);
            break;
        case 'evr':
            store_EVR($SRPDATA);
            break;

        default:
            # code...
            break;
   }                   
}
else {
    js_go_back_self("未上傳檔案");
    exit;
}