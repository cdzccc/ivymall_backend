<?php
$root = preg_replace("/(.*?)\/admin.*/", "$1", $_SERVER['SCRIPT_FILENAME']);
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("store_711");
include DOCUMENT_ROOT.'/schedule/store/PRESCO/storefun.php';

$id = filter_input(INPUT_GET, 'id', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

$sql_cmd = "select s.*,o.name from `order` as o
    left join store as s on o.Order_ID = s.order_id
    where s.`order_id` in ('". implode("','", $id) ."')";
$rs = $db->query($sql_cmd);
$row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
$allrow=$row['name'];
if(empty($allrow)){
	js_go_back_self("請先轉出訂單，再進行下載");
    exit();
}
$ClassPDF = new ClassPDF();
$data = [];
$sql_cmd = "select s.*,o.name from `order` as o
    left join store as s on o.Order_ID = s.order_id
    where s.`order_id` in ('". implode("','", $id) ."')";
$rs = $db->query($sql_cmd);
while ($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
    switch ($row['servicetype']) {
        case '1':
            $note = "不用檢視證件";
            break;
        case '3':
            $note = "商品已付款，請檢視證件";
            break;
    }
    $data[] = [
        'note' => $note,
        'name' => $row['name'],
        'store_id' => $store_ParentId.substr(($row['id']+60450001),0,8),
        'store_name' => $row['storename']." ".$row['storeid'],
        'chars' => ($row['storeid']).($store_ParentId).($store_eshopid).substr(($row['id']+60450001),0,8),
        'chars_img' => WEBSITE_URL."lib/code128.php?chars=".($store_ParentId).($store_eshopid).substr(($row['id']+60450001),0,8),
        'ShipDate' => date("Y/m/d",strtotime("+1 day",strtotime($row['ShipDate']))),
        'ReturnDate' => str_replace("-", "/", $row['ReturnDate']),
        'order_id' => $row['order_id'],
    ];
	//echo substr(($row['id']+100000000),1,8);
	//print_r($data);
}
//print_r($data);

$ClassPDF->create_store_711_label($data);

