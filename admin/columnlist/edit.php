<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("columnlist_edit");
$id = filter_input(INPUT_POST, 'id');

$name      = filter_input(INPUT_POST, 'name');
$color      = filter_input(INPUT_POST, 'color');
$status    = filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 1, 'default' => 0)));
$sort        = filter_input(INPUT_POST, 'sort', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 99999, 'default' => 0)));


if(empty($id)) {
    js_go_back_global("NOT_POST");
    exit;
}

if(empty($name)) {
    $err_msg = "欄位";
    if(empty($name))
        $err_field[] = "分類名稱";
    $err_msg .= "'".implode("','", $err_field)."'不可為空值";
    js_go_back_self($err_msg);
    exit;
}

$update_datetime = date("Y-m-d H:i:s");
$update_user     = $_SESSION[SESSION_VARIABLE."_user_id"];

$sql_array = array(
    "Category_CodeGroup"   => array("2", "COLUMN"),
    "Category_Name"        => array("2", checkinput_sql($name, 60)),
    "color"                => array("2", checkinput_sql($color, 255)),
    "desc"                 => array("2", checkinput_sql($name, 255)),
    "sort"                 => array("2", checkinput_sql($sort, 45)),
    "status"               => array("2", checkinput_sql($status,2)),
    "update_datetime"      => array("2", checkinput_sql($update_datetime, 50)),
    "update_user"          => array("2", checkinput_sql($update_user, 50)),
);
$sql_cmd = update("category", array("Category_Code", $id), $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
   js_go_back_global("DB_EDIT_ERROR");
   exit;
}else{
    add_log('學習專欄分類管理','2');
   $db->disconnect();
    $referer = filter_input(INPUT_POST, 'referer');
    $redirect = "./list.php";
    if(!empty($referer))
        $redirect = $referer;
    js_repl_global( $redirect, "EDIT_SUCCESS");
   exit;
}
?>
