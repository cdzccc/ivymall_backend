<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("email_business_edit");
$id = filter_input(INPUT_POST, 'id');

$send_mail = false;

$reply      = filter_input(INPUT_POST, 'editor');
if(empty($reply)) {
    $reply      = filter_input(INPUT_POST, 'reply');
}
else {
  $send_mail = true;
}
$remark      = filter_input(INPUT_POST, 'remark');
$status    = filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 2, 'default' => 0)));

if(empty($id)) {
    js_go_back_global("NOT_POST");
    exit;
}

if(($status == 2 && empty($reply))) {
    $err_msg = "欄位";
    if(empty($reply))
        $err_field[] = "客服回覆";
    $err_msg .= "'".implode("','", $err_field)."'不可為空值";
    js_go_back_self($err_msg);
    exit;
}


$update_datetime = date("Y-m-d H:i:s");
$update_user     = $_SESSION[SESSION_VARIABLE."_user_id"];

$sql_array = array(
    "reply"           => array("2", checkinput_sql($reply, 9999999)),
    "remark"          => array("2", checkinput_sql($remark, 9999999)),
    "status"          => array("2", checkinput_sql($status,2)),
    "update_datetime" => array("2", checkinput_sql($update_datetime, 50)),
    "update_user"     => array("2", checkinput_sql($update_user, 50)),
);
$sql_cmd = update("mail_content", array("id", $id), $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
   js_go_back_global("DB_EDIT_ERROR");
   exit;
}else{
  if($send_mail) {
    $sql_cmd = "select * from mail_content where id = '".$id."'";
    $rs = $db->query($sql_cmd);
    $row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
    $email = $row['email'];
    $datas = [
        "title"   => "常春藤網路書城-企業服務詢問表單回覆",
        "type"    => "1",
        "content" => $reply,
        "mail"    => $email,
    ];
    ClassMail::send_mail($datas);
  }
    add_log('企業服務信箱管理','2');
   $db->disconnect();
    $referer = filter_input(INPUT_POST, 'referer');
    $redirect = "./list.php";
    if(!empty($referer))
        $redirect = $referer;
    js_repl_global( $redirect, "EDIT_SUCCESS");
   exit;
}
?>
