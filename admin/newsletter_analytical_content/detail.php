<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("newsletterlist_analytical");

$category = filter_input(INPUT_GET, 'category');

if(isset($_GET['action']))
    $action = $_GET['action'];
else
    $action = "add";

$row["id"] = "";
$row["name"] = "";
$row["sub_name"] = "";
$row["desc"] = "";
$row["post_time"] = "";
$row["author"] = "";
$row["status"] = "1";
$row['sort'] = 0;

$sql_cmd = "select * from category where Category_CodeGroup = 'FAQ' and status = 1 and deleted_at is null";
$rs_category = $db->query($sql_cmd);
if($action == "edit") {
    $sql_cmd = "select * from article where deleted_at is null and id = '".checkinput_sql($_GET['id'],19)."'";
    $rs = $db->query($sql_cmd);
    $row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
}

?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php") ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                解析英語電子報內容管理
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" method="POST" action="./<?=$action?>.php" enctype="multipart/form-data">
                    <div class="box-header">
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">標題名稱</label>
                            <div class="col-sm-10">
                                <input name="name" type="text" class="form-control" value="<?=$row["name"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">英文副標題</label>
                            <div class="col-sm-10">
                                <input name="sub_name" type="text" class="form-control" value="<?=$row["sub_name"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="post_time" class="col-sm-2 control-label">文章日期</label>
                            <div class="col-sm-10">
                                <input id="post_time" name="post_time" type="text" class="datepicker form-control" value="<?=$row["post_time"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">作者名稱</label>
                            <div class="col-sm-10">
                                <input name="author" type="text" class="form-control" value="<?=$row["author"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="editor" class="col-sm-2 control-label">內容</label>
                            <div class="col-sm-10">
                                <textarea class="editor" name="editor"><?=$row["desc"]?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="sort" class="col-sm-2 control-label">排序</label>
                            <div class="col-sm-10">
                                <input id="sort" name="sort" type="text" class="form-control" value="<?=$row["sort"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">狀態</label>
                            <div class="col-sm-10">
                                <span class="control-text">
                                    <label>
                                        <input name="status" type="radio" <?=($row['status'] == 1)?"checked":""?>
                                           value="1">上架
                                    </label>
                                    <label>
                                        <input name="status" type="radio" <?=($row['status'] == 0)?"checked":""?>
                                           value="0">下架
                                    </label>
                                </span>
                            </div>
                        </div>
                        <?php if($action == "edit") {?>
                        <div class="form-group">
                            <label for="sort" class="col-sm-2 control-label">建立時間</label>
                            <div class="col-sm-10">
                                <span class="control-text">
                                    <?=$row['create_datetime']?>
                                </span>
                            </div>
                        </div>
                        <?php }?>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-right">
                        <input type="hidden" name="id" value="<?=$row['id']?>">
                        <input type="hidden" name="category" value="<?=$category?>">
                        <a href="javascript:history.back()?category=<?=$category?>" class="btn btn-danger" style="margin-right: 10px;">取消，並返回上一層</a>
                        <button type="submit" class="btn btn-primary">儲存</button>
                        <input type="hidden" name="referer" value="<?=(!empty($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:"")?>">
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>