<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("newsletterlist_analytical_add");
$id = get_id();
$name      = filter_input(INPUT_POST, 'name');
$category  = filter_input(INPUT_POST, 'category');
$sub_name  = filter_input(INPUT_POST, 'sub_name');
$post_time = filter_input(INPUT_POST, 'post_time');
$author    = filter_input(INPUT_POST, 'author');
$content   = filter_input(INPUT_POST, 'editor');
$status    = filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 1, 'default' => 0)));
$sort        = filter_input(INPUT_POST, 'sort', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 99999, 'default' => 0)));

if(empty($name) || empty($content) || empty($post_time)) {
    $err_msg = "欄位";
    if(empty($name))
        $err_field[] = "標題名稱";
    if(empty($content))
        $err_field[] = "內容";
    if(empty($post_time))
        $err_field[] = "文章日期";
    $err_msg .= "'".implode("','", $err_field)."'不可為空值";
    js_go_back_self($err_msg);
    exit;
}

$create_datetime = date("Y-m-d H:i:s");
$create_user     = $_SESSION[SESSION_VARIABLE."_user_id"];
$update_datetime = date("Y-m-d H:i:s");
$update_user     = $_SESSION[SESSION_VARIABLE."_user_id"];

$sql_array = array(
    "id"              => array("2", checkinput_sql($id, 19)),
    "name"            => array("2", checkinput_sql($name, 255)),
    "sub_name"        => array("2", checkinput_sql($sub_name, 255)),
    "Category"        => array("2", $category),
    "desc"            => array("2", checkinput_sql($content, 9999999)),
    "post_time"       => array("2", checkinput_sql($post_time, 30)),
    "status"          => array("2", checkinput_sql($status, 5)),
    "author"          => array("2", checkinput_sql($author, 200)),
    "pic"             => array("2", checkinput_sql("", 30)),
    "create_datetime" => array("2", checkinput_sql($create_datetime, 50)),
    "create_user"     => array("2", checkinput_sql($create_user, 50)),
    "update_datetime" => array("2", checkinput_sql($update_datetime, 50)),
    "update_user"     => array("2", checkinput_sql($update_user, 50)),
    "sort"            => array("2", checkinput_sql($sort, 5)),
);
$sql_cmd = insert("article", $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
   js_go_back_global("DB_ADD_ERROR");
   exit;
}else{
    add_log('解析英語電子報內容管理','1');
   $db->disconnect();
   js_repl_global( "./list.php?category=".$category, "ADD_SUCCESS");
   exit;
}
?>
