<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("video");

if(isset($_GET['action']))
    $action = $_GET['action'];
else
    $action = "add";

$row["id"] = "";
$row["name"] = "";
$row["desc"] = "";
$row["desc2"] = "";
$row["url"] = "";
$row["stime"] = "";
$row["etime"] = "";
$row["status"] = "1";
$row['pic'] = "";
$row['pic_m'] = "";
$row['sort'] = 0;

if($action == "edit") {
    $sql_cmd = "select * from message where Category = 'Video' and deleted_at is null and id = '".checkinput_sql($_GET['id'],19)."'";
    $rs = $db->query($sql_cmd);
    $row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
}

?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php") ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                影音補給站管理
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" method="POST" action="./<?=$action?>.php" enctype="multipart/form-data">
                    <div class="box-header">
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">名稱</label>
                            <div class="col-sm-10">
                                <input name="name" type="text" class="form-control" value="<?=$row["name"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="url" class="col-sm-2 control-label">連結</label>
                            <div class="col-sm-10">
                                <input id="url" name="url" type="text" class="form-control" value="<?=$row["url"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="pic" class="col-sm-2 control-label">封面圖</label>
                            <div class="col-sm-10">
                                <? if(!empty($row['pic'])): ?>
                                    <img src="<?=WEBSITE_URL?>upload/<?=$row['pic']?>" width="100" class="pic">
                                    <br><?=$row['pic']?>
                                     &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<a href='javascript:void(0);' class='btn btn-danger' onclick='$(".old_pic").val("");$(".pic").hide();$(this).hide()'>刪除圖片</a>
                                <? endif ?>
                                <input id="pic" name="pic" type="file">
                                <input name="old_pic" type="hidden" value='<?=$row['pic']?>'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="alt" class="col-sm-2 control-label">Alt</label>
                            <div class="col-sm-10">
                                <input id="alt" name="alt" type="text" class="form-control" value="<?=$row["desc"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">上下架時間</label>
                            <div class="col-sm-10">
                                <input id="stime" name="stime" type="text" class="datetimepicker" value="<?=(!empty($row["stime"]))?date("Y/m/d H:i", strtotime($row['stime'])):""?>"> -
                                <input id="etime" name="etime" type="text" class="datetimepicker" value="<?=(!empty($row["etime"]))?date("Y/m/d H:i", strtotime($row['etime'])):""?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="sort" class="col-sm-2 control-label">排序</label>
                            <div class="col-sm-10">
                                <input id="sort" name="sort" type="text" class="form-control" value="<?=$row["sort"]?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">狀態</label>
                            <div class="col-sm-10">
                                <span class="control-text">
                                    <label>
                                        <input name="status" type="radio" <?=($row['status'] == 1)?"checked":""?>
                                           value="1">上架
                                    </label>
                                    <label>
                                        <input name="status" type="radio" <?=($row['status'] == 0)?"checked":""?>
                                           value="0">下架
                                    </label>
                                </span>
                            </div>
                        </div>
                        <?php if($action == "edit") {?>
                        <div class="form-group">
                            <label for="sort" class="col-sm-2 control-label">建立時間</label>
                            <div class="col-sm-10">
                                <span class="control-text">
                                    <?=$row['create_datetime']?>
                                </span>
                            </div>
                        </div>
                        <?php }?>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-right">
                        <input type="hidden" name="id" value="<?=$row['id']?>">
                        <a href="javascript:history.back()" class="btn btn-danger" style="margin-right: 10px;">取消，並返回上一層</a>
                        <button type="button" class="btn btn-primary" onclick="check_file();">儲存</button>
                        <input type="hidden" name="referer" value="<?=(!empty($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:"")?>">
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>