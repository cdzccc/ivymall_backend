<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("video_edit");
$id = filter_input(INPUT_POST, 'id');

$name        = filter_input(INPUT_POST, 'name');
$desc        = filter_input(INPUT_POST, 'alt');
$url         = filter_input(INPUT_POST, 'url');
$stime       = filter_input(INPUT_POST, 'stime');
$etime       = filter_input(INPUT_POST, 'etime');
$status      = filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 1, 'default' => 0)));
$sort        = filter_input(INPUT_POST, 'sort', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 99999, 'default' => 0)));
$old_pic       = filter_input(INPUT_POST, 'old_pic');

if(empty($id)) {
    js_go_back_global("NOT_POST");
    exit;
}

if(empty($name) || empty($url)) {
    $err_msg = "欄位";
    if(empty($name))
        $err_field[] = "名稱";
    if(empty($url))
        $err_field[] = "連結";
    $err_msg .= "'".implode("','", $err_field)."'不可為空值";
    js_go_back_self($err_msg);
    exit;
}

if ($_FILES['pic']['name'] != "none" && is_uploaded_file($_FILES['pic']['tmp_name']))
{
    //重組檔名
    $type = explode(".", $_FILES['pic']['name']);
    $img = $_FILES['pic']['name'];
    if(file_exists(DOCUMENT_ROOT."/upload/".$img)) {
        // js_go_back_self("檔名重複!!");
        // exit;
    }
    @copy($_FILES['pic']['tmp_name'], DOCUMENT_ROOT."/upload/".$img);
}
else {
    $img = $old_pic;
}

$update_datetime = date("Y-m-d H:i:s");
$update_user     = $_SESSION[SESSION_VARIABLE."_user_id"];


$sql_array = array(
    "name"            => array("2", checkinput_sql($name, 255)),
    "Category"        => array("2", "Video"),
    "desc"            => array("2", checkinput_sql($desc, 1000)),
    "url"             => array("2", checkinput_sql($url, 100)),
    "stime"           => array("2", checkinput_sql((empty($stime))?"":date("Y-m-d H:i:s", strtotime($stime)), 30)),
    "etime"           => array("2", checkinput_sql((empty($etime))?"":date("Y-m-d H:i:s", strtotime($etime)), 30)),
    "status"          => array("2", checkinput_sql($status, 5)),
    "pic"             => array("2", checkinput_sql($img, 200)),
    "pic_m"           => array("2", ""),
    "desc2"           => array("2", ""),
    "update_datetime" => array("2", checkinput_sql($update_datetime, 50)),
    "update_user"     => array("2", checkinput_sql($update_user, 50)),
    "sort"            => array("2", checkinput_sql($sort, 5)),

);$sql_cmd = update("message", array("id", $id), $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
   js_go_back_global("DB_EDIT_ERROR");
   exit;
}else{
    add_log('影音補給站管理','2');
   $db->disconnect();
    $referer = filter_input(INPUT_POST, 'referer');
    $redirect = "./list.php";
    if(!empty($referer))
        $redirect = $referer;
    js_repl_global( $redirect, "EDIT_SUCCESS");
   exit;
}
?>
