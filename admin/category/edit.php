<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("category_edit");
$Category_Code = filter_input(INPUT_POST, 'Category_Code');

$Category_Name  = filter_input(INPUT_POST, 'Category_Name');
$pic_alt1  = filter_input(INPUT_POST, 'alt');
$sort        = filter_input(INPUT_POST, 'sort', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 99999, 'default' => 0)));
$status        = filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 99999, 'default' => 0)));
$old_pic       = filter_input(INPUT_POST, 'old_pic');
if(empty($Category_Code)) {
    js_go_back_global("NOT_POST");
    exit;
}
if(empty($Category_Name)) {
    $err_msg = "欄位";
    if(empty($Category_Name))
        $err_field[] = "名稱";
    $err_msg .= "'".implode("','", $err_field)."'不可為空值";
    js_go_back_self($err_msg);
    exit;
}

if ($_FILES['pic']['name'] != "none" && is_uploaded_file($_FILES['pic']['tmp_name']))
{
    //重組檔名
    $type = explode(".", $_FILES['pic']['name']);
    $img = $_FILES['pic']['name'];
    if(file_exists(DOCUMENT_ROOT."/upload/".$img)) {
        // js_go_back_self("檔名重複!!");
        // exit;
    }
    @copy($_FILES['pic']['tmp_name'], DOCUMENT_ROOT."/upload/".$img);
}
else {
    $img = $old_pic;
}

$update_datetime = date("Y-m-d H:i:s");
$update_user     = $_SESSION[SESSION_VARIABLE."_user_id"];


$sql_array = array(
    "Category_Name"   => array("2", checkinput_sql($Category_Name, 45)),
    "update_datetime" => array("2", checkinput_sql($update_datetime, 50)),
    "update_user"     => array("2", checkinput_sql($update_user, 50)),
    "category_pic1"   => array("2", checkinput_sql($img, 200)),
    "pic_alt1"        => array("2", checkinput_sql($pic_alt1, 200)),
);
$sql_cmd = update("category",array("Category_Code", $Category_Code), $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
   js_go_back_global("DB_EDIT_ERROR");
   exit;
}else{
   $db->disconnect();
    $referer = filter_input(INPUT_POST, 'referer');
    $redirect = "./list.php";
    if(!empty($referer))
        $redirect = $referer;
    js_repl_global( $redirect, "EDIT_SUCCESS");
   exit;
}
?>
