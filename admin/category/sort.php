<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("category");

$sql_where = "";

$sql_cmd = "select * from category where Category_CodeGroup = 'Goods_Category' and Status = 1 ".$sql_where." order by Sort desc,create_datetime desc ";
$rs = $db->query($sql_cmd);
$row_category = [];
$sort_category = [];
while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
    $row_category[] = $row;
    if($row['Sort'] > 0)
        $sort_category[] = $row['Category_Code'];
}
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php"); ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                分類排序管理
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box">
                <form class="form-horizontal" method="POST" action="./sort_change.php">
                    <div class="box-header">
                    </div>
                    <div class="box-body no-padding">
                            <div class="col-sm-10">
                                <select name="category[]" id="category" class="select2 form-control" multiple="multiple" style="width:100%">
                                    <? foreach ($row_category as $key => $value) : ?>
                                        <option value="<?=$value['Category_Code']?>" <?=(in_array($value["Category_Code"],$sort_category) ? 'selected' : '');?>><?=$value['Category_Name']?></option>
                                    <? endforeach ?>
                                </select>
                                <span>設定說明：先選取想要排序的分類，可拖拉項目調整順序</span>
                            </div>
                            
                    </div>
                    <div class="box-footer text-right">
                        <button type="submit" class="btn btn-primary">儲存</button>
                    </div>
                </form>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>
<style>
    .select2-selection__choice {
        margin-left: 5px;
    }
</style>
</body>
</html>