<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("magazine_edit");
$id  = filter_input(INPUT_POST, 'id');

$title  = filter_input(INPUT_POST, 'title');
$content  = filter_input(INPUT_POST, 'editor');
$Goods_ID  = filter_input(INPUT_POST, 'Goods_ID');
$sort        = filter_input(INPUT_POST, 'sort', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 99999, 'default' => 0)));
$status        = filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 99999, 'default' => 0)));

if(empty($title) || empty($content)) {
    $err_msg = "欄位";
    if(empty($title))
        $err_field[] = "標題名稱";
    if(empty($content))
        $err_field[] = "內容";
    $err_msg .= "'".implode("','", $err_field)."'不可為空值";
    js_go_back_self($err_msg);
    exit;
}

$update_datetime = date("Y-m-d H:i:s");
$update_user     = $_SESSION[SESSION_VARIABLE."_user_id"];


$sql_array = array(
    "title"           => array("2", checkinput_sql($title, 200)),
    "content"         => array("2", checkinput_sql($content, 9999999)),
    "status"          => array("2", checkinput_sql($status, 50)),
    "update_datetime" => array("2", checkinput_sql($update_datetime, 50)),
    "update_user"     => array("2", checkinput_sql($update_user, 50)),
    "sort"            => array("2", checkinput_sql($sort, 50)),
);
$sql_cmd = update("goods_qa",array("id", $id), $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
   js_go_back_global("DB_EDIT_ERROR");
   exit;
}else{
    add_log('商品常見問題上稿管理','2');
   $db->disconnect();
   js_repl_global( "./goods_qa_list.php?Goods_ID=".$Goods_ID, "EDIT_SUCCESS");
   exit;
}
?>
