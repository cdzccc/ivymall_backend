<?php
include("../config.php");

ClassJscript::islogin();
ClassJscript::isadmino("magazine_add");

$Goods_ID = get_id();

$Goods_Name         = filter_input(INPUT_POST, 'name');
$category           = "2018110614233319522";
$sub_category       = filter_input(INPUT_POST, 'sub_category', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$company            = filter_input(INPUT_POST, 'company');
$list_desc          = filter_input(INPUT_POST, 'list_desc');
$cover_alt          = filter_input(INPUT_POST, 'cover_alt');
$banner_pc_alt      = filter_input(INPUT_POST, 'banner_pc_alt');
$banner_m_alt       = filter_input(INPUT_POST, 'banner_m_alt');
$Goods_Desc         = filter_input(INPUT_POST, 'Goods_Desc');
$sell_price         = filter_input(INPUT_POST, 'sell_price');
$discount_price     = filter_input(INPUT_POST, 'discount_price');
$discount_price2    = filter_input(INPUT_POST, 'discount_price2');
$related_title1     = filter_input(INPUT_POST, 'related_title1');
$related_link1      = filter_input(INPUT_POST, 'related_link1');
$related_title2     = filter_input(INPUT_POST, 'related_title2');
$related_link2      = filter_input(INPUT_POST, 'related_link2');
$related_title3     = filter_input(INPUT_POST, 'related_title3');
$related_link3      = filter_input(INPUT_POST, 'related_link3');
$related_title4     = filter_input(INPUT_POST, 'related_title4');
$related_link4      = filter_input(INPUT_POST, 'related_link4');
$related_title5     = filter_input(INPUT_POST, 'related_title5');
$related_link5      = filter_input(INPUT_POST, 'related_link5');
$item_number        = filter_input(INPUT_POST, 'item_number');
$goods_type         = 3;
$magazine_type_back        = filter_input(INPUT_POST, 'magazine_type_back');
$gift_number        = filter_input(INPUT_POST, 'gift_number');
$gift_count         = filter_input(INPUT_POST, 'gift_count');
$isbn               = filter_input(INPUT_POST, 'isbn');
$author             = filter_input(INPUT_POST, 'author');
$publisher          = filter_input(INPUT_POST, 'publisher');
$publish_date       = filter_input(INPUT_POST, 'publish_date');
$publish_place      = filter_input(INPUT_POST, 'publish_place');
$brand              = filter_input(INPUT_POST, 'brand');
$language           = filter_input(INPUT_POST, 'language');
$binding            = filter_input(INPUT_POST, 'binding');
$weight             = filter_input(INPUT_POST, 'weight');
$spec               = filter_input(INPUT_POST, 'spec');
$book_desc          = filter_input(INPUT_POST, 'editor');
$preview_file       = filter_input(INPUT_POST, 'preview_file');
$stock              = filter_input(INPUT_POST, 'stock');
$safe_stock         = filter_input(INPUT_POST, 'safe_stock');
$min_buy            = filter_input(INPUT_POST, 'min_buy');
$max_buy            = filter_input(INPUT_POST, 'max_buy');
$point_use          = filter_input(INPUT_POST, 'point_use');
$level_use          = filter_input(INPUT_POST, 'level_use');
$ticket_use         = filter_input(INPUT_POST, 'ticket_use');
$price_ticket_use   = filter_input(INPUT_POST, 'price_ticket_use');
$item_discount_use  = filter_input(INPUT_POST, 'item_discount_use');
$price_discount_use = filter_input(INPUT_POST, 'price_discount_use');
$delivery_use       = filter_input(INPUT_POST, 'delivery_use');
$store_use          = filter_input(INPUT_POST, 'store_use');
$store_pay_use      = filter_input(INPUT_POST, 'store_pay_use');
$online_pay_use     = filter_input(INPUT_POST, 'online_pay_use');
$delivery_pay_use   = filter_input(INPUT_POST, 'delivery_pay_use');
$meta_keyrowd       = filter_input(INPUT_POST, 'meta_keyrowd');
$meta_desc          = filter_input(INPUT_POST, 'meta_desc');
$Goods_sdate        = filter_input(INPUT_POST, 'daterange_s');
$Goods_edate        = filter_input(INPUT_POST, 'daterange_e');
$sort               = filter_input(INPUT_POST, 'sort');
$status             = filter_input(INPUT_POST, 'status');
$item_group         = filter_input(INPUT_POST, 'item_group', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$recommand_goods    = filter_input(INPUT_POST, 'recommand_goods', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$goods_item_name           = filter_input(INPUT_POST, 'goods_item_name', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$goods_item_no             = filter_input(INPUT_POST, 'goods_item_no', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$goods_item_gift           = filter_input(INPUT_POST, 'goods_item_gift', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$goods_item_gift_num       = filter_input(INPUT_POST, 'goods_item_gift_num', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$goods_item_sell_price     = filter_input(INPUT_POST, 'goods_item_sell_price', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$goods_item_discount_price = filter_input(INPUT_POST, 'goods_item_discount_price', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$goods_item_discount_price2 = filter_input(INPUT_POST, 'goods_item_discount_price2', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$preview_title1     = filter_input(INPUT_POST, 'preview_title1');
$preview_alt1       = filter_input(INPUT_POST, 'preview_alt1');
$preview_title2     = filter_input(INPUT_POST, 'preview_title2');
$preview_alt2       = filter_input(INPUT_POST, 'preview_alt2');
$preview_title3     = filter_input(INPUT_POST, 'preview_title3');
$preview_alt3       = filter_input(INPUT_POST, 'preview_alt3');
$preview_title4     = filter_input(INPUT_POST, 'preview_title4');
$preview_alt4       = filter_input(INPUT_POST, 'preview_alt4');
$preview_title5     = filter_input(INPUT_POST, 'preview_title5');
$preview_alt5       = filter_input(INPUT_POST, 'preview_alt5');
$preview_title6     = filter_input(INPUT_POST, 'preview_title6');
$preview_alt6       = filter_input(INPUT_POST, 'preview_alt6');
$preview_title7     = filter_input(INPUT_POST, 'preview_title7');
$preview_alt7       = filter_input(INPUT_POST, 'preview_alt7');
$preview_title8     = filter_input(INPUT_POST, 'preview_title8');
$preview_alt8       = filter_input(INPUT_POST, 'preview_alt8');
$preview_title9     = filter_input(INPUT_POST, 'preview_title9');
$preview_alt9       = filter_input(INPUT_POST, 'preview_alt9');
$preview_title10    = filter_input(INPUT_POST, 'preview_title10');
$preview_alt10      = filter_input(INPUT_POST, 'preview_alt10');
$start_periods      = filter_input(INPUT_POST, 'start_periods');
$start_month        = filter_input(INPUT_POST, 'start_month');
$registered_use     = filter_input(INPUT_POST, 'registered_use');
$registered_price   = filter_input(INPUT_POST, 'registered_price');

$is_error = 0;
for ($i=0; $i < 5; $i++) {
    foreach ($goods_item_name['spec'.$i] as $key => $value) {
        if(!empty($goods_item_gift['spec'.$i][$key]) && $goods_item_gift_num['spec'.$i][$key] == 0) {
            $err_field[] = "贈品數量";
            $is_error = 1;
            break;
        }
    }
}

if(empty($Goods_Name) 
    || empty($sub_category) 
    || empty($company)
    || empty($item_number)
    || empty($magazine_type_back)
    || empty($min_buy)
    || empty($max_buy)
    || ($registered_use == 1 && empty($registered_price))
    || empty($_FILES['banner_pc_pic']['name'])
    || empty($_FILES['banner_m_pic']['name'])
    || $is_error == 1
) {
    $err_msg = "欄位";
    if(empty($Goods_Name))
        $err_field[] = "商品名稱";
    if(empty($sub_category))
        $err_field[] = "第二層分類";
    if(empty($company))
        $err_field[] = "所屬公司";
    if(empty($item_number))
        $err_field[] = "主貨號";
	if(empty($magazine_type_back))
        $err_field[] = "商品種類";
    if(empty($min_buy))
        $err_field[] = "最低購買量";
    if(empty($max_buy))
        $err_field[] = "最大購買量";
    if(($registered_use == 1 && empty($registered_price)))
        $err_field[] = "支援掛號費";
    if(empty($_FILES['banner_pc_pic']['name']))
        $err_field[] = "banner pc";
    if(empty($_FILES['banner_m_pic']['name']))
        $err_field[] = "banner mobile";
    $err_msg .= "'".implode("','", $err_field)."'不可為空值";
    js_go_back_self($err_msg);
    exit;
}


if ($_FILES['cover_pic']['name'] != "none" && is_uploaded_file($_FILES['cover_pic']['tmp_name']))
{
    //重組檔名
    $type = explode(".", $_FILES['cover_pic']['name']);
    $cover_pic = $_FILES['cover_pic']['name'];
    if(file_exists(DOCUMENT_ROOT."/upload/".$cover_pic)) {
        // js_go_back_self("檔名重複!!");
        // exit;
    }
    @copy($_FILES['cover_pic']['tmp_name'], DOCUMENT_ROOT."/upload/".$cover_pic);
}
else {
    $cover_pic = "";
}

if ($_FILES['banner_pc_pic']['name'] != "none" && is_uploaded_file($_FILES['banner_pc_pic']['tmp_name']))
{
    //重組檔名
    $type = explode(".", $_FILES['banner_pc_pic']['name']);
    $banner_pc_pic = $_FILES['banner_pc_pic']['name'];
    if(file_exists(DOCUMENT_ROOT."/upload/".$banner_pc_pic)) {
        // js_go_back_self("檔名重複!!");
        // exit;
    }
    @copy($_FILES['banner_pc_pic']['tmp_name'], DOCUMENT_ROOT."/upload/".$banner_pc_pic);
}
else {
    js_go_back_global("IMG_ERROR");
    exit;
}

if ($_FILES['banner_m_pic']['name'] != "none" && is_uploaded_file($_FILES['banner_m_pic']['tmp_name']))
{
    //重組檔名
    $type = explode(".", $_FILES['banner_m_pic']['name']);
    $banner_m_pic = $_FILES['banner_m_pic']['name'];
    if(file_exists(DOCUMENT_ROOT."/upload/".$banner_m_pic)) {
        // js_go_back_self("檔名重複!!");
        // exit;
    }
    @copy($_FILES['banner_m_pic']['tmp_name'], DOCUMENT_ROOT."/upload/".$banner_m_pic);
}
else {
    js_go_back_global("IMG_ERROR");
    exit;
}

for ($i=1; $i <= 10; $i++) { 
    if ($_FILES['preview_pic'.$i]['name'] != "none" && is_uploaded_file($_FILES['preview_pic'.$i]['tmp_name']))
    {
		//存圖片大小
		$array = getimagesize($_FILES['preview_pic'.$i]['tmp_name']);
		//print_r($array);
		$preview_pic_wh[$i] =  $array[0]."x".$array[1];
		
        //重組檔名
        $type = explode(".", $_FILES['preview_pic'.$i]['name']);
        $preview_pic[$i] = $_FILES['preview_pic'.$i]['name'];
        if(file_exists(DOCUMENT_ROOT."/upload/".$preview_pic[$i])) {
            //js_go_back_self("檔名重複!!");
            //exit;
        }
        @copy($_FILES['preview_pic'.$i]['tmp_name'], DOCUMENT_ROOT."/upload/".$preview_pic[$i]);
    }
    else {
        $preview_pic[$i] = "";
		$preview_pic_wh[$i] = "";
    }
}

if ($_FILES['preview_file']['name'] != "none" && is_uploaded_file($_FILES['preview_file']['tmp_name']))
{
    //重組檔名
    $type = explode(".", $_FILES['cover_pic']['name']);
    $preview_file = $_FILES['preview_file']['name'];
    if(file_exists(DOCUMENT_ROOT."/upload/".$preview_file)) {
        // js_go_back_self("檔名重複!!");
        // exit;
    }
    @copy($_FILES['preview_file']['tmp_name'], DOCUMENT_ROOT."/upload/".$preview_file);
}
else {
    $preview_file = "";
}

$create_datetime = date("Y-m-d H:i:s");
$create_user     = $_SESSION[SESSION_VARIABLE."_user_id"];
$update_datetime = date("Y-m-d H:i:s");
$update_user     = $_SESSION[SESSION_VARIABLE."_user_id"];

for ($i=1; $i <=5 ; $i++) { 
    $spec[$i]['spec']                      = filter_input(INPUT_POST, 'spec'.$i);
    $spec[$i]['goods_item_name']           = $goods_item_name['spec'.$i];
    $spec[$i]['goods_item_no']             = $goods_item_no['spec'.$i];
    $spec[$i]['goods_item_gift']           = $goods_item_gift['spec'.$i];
    $spec[$i]['goods_item_gift_num']       = $goods_item_gift_num['spec'.$i];
    $spec[$i]['goods_item_sell_price']     = $goods_item_sell_price['spec'.$i];
    $spec[$i]['goods_item_discount_price'] = $goods_item_discount_price['spec'.$i];
    $spec[$i]['goods_item_discount_price2'] = $goods_item_discount_price2['spec'.$i];
    foreach($spec[$i]['goods_item_name'] as $key => $item) {
        if(!empty($spec[$i]['goods_item_name'][$key])) {
            $Goods_Item_ID = get_id();
            $sql_array = array(
                "Goods_ID"        => array("2", $Goods_ID),
                "Goods_Item_ID"   => array("2", $Goods_Item_ID),
                "spec"            => array("2", checkinput_sql($spec[$i]['spec'],50)),
                "Name"            => array("2", checkinput_sql($spec[$i]['goods_item_name'][$key],50)),
                "item_number"     => array("2", checkinput_sql($spec[$i]['goods_item_no'][$key],50)),
                "sell_price"      => array("2", checkinput_sql($spec[$i]['goods_item_sell_price'][$key],6)),
                "discount_price"  => array("2", checkinput_sql(intval($spec[$i]['goods_item_discount_price'][$key]),6)),
                "discount_price2" => array("2", checkinput_sql(intval($spec[$i]['goods_item_discount_price2'][$key]),6)),
                "gift"            => array("2", checkinput_sql($spec[$i]['goods_item_gift'][$key],20)),
                "gift_num"        => array("2", checkinput_sql($spec[$i]['goods_item_gift_num'][$key],20)),
                "Status"          => array("2", 1),
                "create_user"     => array("2", $create_user),
                "create_datetime" => array("2", $create_datetime),
                "update_user"     => array("2", $update_user),
                "update_datetime" => array("2", $update_datetime),
                "Qty"             => array("2", 0),
            );
            $sql_cmd = insert("goods_item", $sql_array);
            $rs = $db->query($sql_cmd);
        }
    }
}

$sql_array = array(
    "Goods_ID"           => array("2", checkinput_sql($Goods_ID, 19)),
    "Goods_Name"         => array("2", checkinput_sql($Goods_Name, 100)),
    "category"           => array("2", checkinput_sql($category, 19)),
    "sub_category"       => array("2", checkinput_sql(implode(",", $sub_category), 255)),
    "company"            => array("2", checkinput_sql($company, 50)),
    "list_desc"          => array("2", checkinput_sql($list_desc, 1000)),
    "cover_alt"          => array("2", checkinput_sql($cover_alt, 100)),
    "cover_pic"          => array("2", checkinput_sql($cover_pic, 200)),
    "banner_pc_alt"      => array("2", checkinput_sql($banner_pc_alt, 100)),
    "banner_pc_pic"      => array("2", checkinput_sql($banner_pc_pic, 200)),
    "banner_m_alt"       => array("2", checkinput_sql($banner_m_alt, 100)),
    "banner_m_pic"       => array("2", checkinput_sql($banner_m_pic, 200)),
    "Goods_Desc"         => array("2", checkinput_sql($Goods_Desc, 1000)),
    "sell_price"         => array("2", checkinput_sql(intval($sell_price), 5)),
    "discount_price"     => array("2", checkinput_sql(intval($discount_price), 5)),
    "discount_price2"    => array("2", checkinput_sql(intval($discount_price2), 5)),
    "related_title1"     => array("2", checkinput_sql($related_title1, 200)),
    "related_link1"      => array("2", checkinput_sql($related_link1, 200)),
    "related_title2"     => array("2", checkinput_sql($related_title2, 200)),
    "related_link2"      => array("2", checkinput_sql($related_link2, 200)),
    "related_title3"     => array("2", checkinput_sql($related_title3, 200)),
    "related_link3"      => array("2", checkinput_sql($related_link3, 200)),
    "related_title4"     => array("2", checkinput_sql($related_title4, 200)),
    "related_link4"      => array("2", checkinput_sql($related_link4, 200)),
    "related_title5"     => array("2", checkinput_sql($related_title5, 200)),
    "related_link5"      => array("2", checkinput_sql($related_link5, 200)),
    "item_number"        => array("2", checkinput_sql($item_number, 50)),
    "magazine_type_back"        => array("2", checkinput_sql($magazine_type_back, 50)),
    "type"               => array("2", checkinput_sql($goods_type, 1)),
    "gift_number"        => array("2", checkinput_sql($gift_number, 50)),
    "gift_count"         => array("2", checkinput_sql(intval($gift_count), 5)),
    "isbn"               => array("2", checkinput_sql($isbn, 50)),
    "author"             => array("2", checkinput_sql($author, 100)),
    "publisher"          => array("2", checkinput_sql($publisher, 100)),
    "publish_date"       => array("2", checkinput_sql($publish_date, 15)),
    "publish_place"      => array("2", checkinput_sql($publish_place, 50)),
    "brand"              => array("2", checkinput_sql($brand, 100)),
    "language"           => array("2", checkinput_sql($language, 50)),
    "binding"            => array("2", checkinput_sql($binding, 50)),
    "weight"             => array("2", checkinput_sql($weight, 50)),
    "spec"               => array("2", checkinput_sql($spec, 1000)),
    "book_desc"          => array("2", checkinput_sql($book_desc, 9999999)),
    "preview_pic1"       => array("2", checkinput_sql($preview_pic[1], 200)),
	"preview_pic1_wh"       => array("2", checkinput_sql($preview_pic_wh[1], 200)),
    "preview_alt1"       => array("2", checkinput_sql($preview_alt1, 100)),
    "preview_title2"     => array("2", checkinput_sql($preview_title2, 100)),
    "preview_pic2"       => array("2", checkinput_sql($preview_pic[2], 200)),
	"preview_pic2_wh"       => array("2", checkinput_sql($preview_pic_wh[2], 200)),
    "preview_alt2"       => array("2", checkinput_sql($preview_alt2, 100)),
    "preview_title3"     => array("2", checkinput_sql($preview_title3, 100)),
    "preview_pic3"       => array("2", checkinput_sql($preview_pic[3], 200)),
	"preview_pic3_wh"       => array("2", checkinput_sql($preview_pic_wh[3], 200)),
    "preview_alt3"       => array("2", checkinput_sql($preview_alt3, 100)),
    "preview_title4"     => array("2", checkinput_sql($preview_title4, 100)),
    "preview_pic4"       => array("2", checkinput_sql($preview_pic[4], 200)),
	"preview_pic4_wh"       => array("2", checkinput_sql($preview_pic_wh[4], 200)),
    "preview_alt4"       => array("2", checkinput_sql($preview_alt4, 100)),
    "preview_title5"     => array("2", checkinput_sql($preview_title5, 100)),
    "preview_pic5"       => array("2", checkinput_sql($preview_pic[5], 200)),
	"preview_pic5_wh"       => array("2", checkinput_sql($preview_pic_wh[5], 200)),
    "preview_alt5"       => array("2", checkinput_sql($preview_alt5, 100)),
    "preview_title6"     => array("2", checkinput_sql($preview_title6, 100)),
    "preview_pic6"       => array("2", checkinput_sql($preview_pic[6], 200)),
	"preview_pic6_wh"       => array("2", checkinput_sql($preview_pic_wh[6], 200)),
    "preview_alt6"       => array("2", checkinput_sql($preview_alt6, 100)),
    "preview_title7"     => array("2", checkinput_sql($preview_title7, 100)),
    "preview_pic7"       => array("2", checkinput_sql($preview_pic[7], 200)),
	"preview_pic7_wh"       => array("2", checkinput_sql($preview_pic_wh[7], 200)),
    "preview_alt7"       => array("2", checkinput_sql($preview_alt7, 100)),
    "preview_title8"     => array("2", checkinput_sql($preview_title8, 100)),
    "preview_pic8"       => array("2", checkinput_sql($preview_pic[8], 200)),
	"preview_pic8_wh"       => array("2", checkinput_sql($preview_pic_wh[8], 200)),
    "preview_alt8"       => array("2", checkinput_sql($preview_alt8, 100)),
    "preview_title9"     => array("2", checkinput_sql($preview_title9, 100)),
    "preview_pic9"       => array("2", checkinput_sql($preview_pic[9], 200)),
	"preview_pic9_wh"       => array("2", checkinput_sql($preview_pic_wh[9], 200)),
    "preview_alt9"       => array("2", checkinput_sql($preview_alt9, 100)),
    "preview_title10"    => array("2", checkinput_sql($preview_title10, 100)),
    "preview_pic10"      => array("2", checkinput_sql($preview_pic[10], 200)),
	"preview_pic10_wh"       => array("2", checkinput_sql($preview_pic_wh[10], 200)),
    "preview_alt10"      => array("2", checkinput_sql($preview_alt10, 100)),
    "preview_file"       => array("2", checkinput_sql($preview_file, 200)),
    "min_buy"            => array("2", checkinput_sql(intval($min_buy), 5)),
    "max_buy"            => array("2", checkinput_sql(intval($max_buy), 5)),
    "point_use"          => array("2", checkinput_sql($point_use, 1)),
    "level_use"          => array("2", checkinput_sql($level_use, 1)),
    "ticket_use"         => array("2", checkinput_sql($ticket_use, 1)),
    "price_ticket_use"   => array("2", checkinput_sql($price_ticket_use, 1)),
    "item_discount_use"  => array("2", checkinput_sql($item_discount_use, 1)),
    "price_discount_use" => array("2", checkinput_sql($price_discount_use, 1)),
    "registered_use"     => array("2", checkinput_sql($registered_use, 1)),
    "registered_price"   => array("2", checkinput_sql(intval($registered_price), 5)),
    "delivery_use"       => array("2", checkinput_sql($delivery_use, 1)),
    "store_use"          => array("2", checkinput_sql($store_use, 1)),
    "store_pay_use"      => array("2", checkinput_sql($store_pay_use, 1)),
    "online_pay_use"     => array("2", checkinput_sql($online_pay_use, 1)),
    "delivery_pay_use"   => array("2", checkinput_sql($delivery_pay_use, 1)),
    "meta_keyrowd"       => array("2", checkinput_sql($meta_keyrowd, 50)),
    "meta_desc"          => array("2", checkinput_sql($meta_desc, 1000)),
    "Goods_sdate"        => array("2", checkinput_sql($Goods_sdate, 50)),
    "Goods_edate"        => array("2", checkinput_sql($Goods_edate, 50)),
    "sort"               => array("2", checkinput_sql(intval($sort), 5)),
    "Status"             => array("2", checkinput_sql($status, 2)),
    "create_datetime"    => array("2", checkinput_sql($create_datetime, 50)),
    "create_user"        => array("2", checkinput_sql($create_user, 50)),
    "update_datetime"    => array("2", checkinput_sql($update_datetime, 50)),
    "update_user"        => array("2", checkinput_sql($update_user, 50)),
    "sell_count"         => array("2", "0"),
    "view_count"         => array("2", "0"),
    "item_group"         => array("2", implode(",",$item_group)),
    "recommand_goods"    => array("2", implode(",",$recommand_goods)),
    "start_periods"      => array("2", checkinput_sql($start_periods, 50)),
    "start_month"        => array("2", checkinput_sql($start_month, 50)),
);
	if(! empty($stock)) {
		$sql_array["stock"] = array("2", checkinput_sql($stock, 5));
	}
	if(! empty($safe_stock)) {
		$sql_array["safe_stock"] = array("2", checkinput_sql($safe_stock, 5));
	}

$sql_cmd = insert("goods", $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
    // exit;
   js_go_back_global("DB_ADD_ERROR");
   exit;
}else{
    add_log('雜誌商品上稿管理','1');

   $db->disconnect();
   js_repl_global( "./list.php", "ADD_SUCCESS");
   exit;
}
?>
