<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("variable");

//取出總筆數
$sql_cmd = "select * from var";
$rs = $db->query($sql_cmd);
while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
    $row_var[$row['type']] = $row['value'];
}
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php");?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                系統參數管理
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box">
                <form action="./edit.php" method="POST">
                    <div class="box-header">
                    </div>
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <th>名稱</th>
                                    <th>參數</th>
                                    <th>說明</th>
                                </tr>
                                <tr onClick="iCheckThisRow(this);">
                                    <th>點數發放率</th>
                                    <th><input type="number" name="var[BONUS]" value="<?=$row_var['BONUS']?>"></th>
                                    <th>
                                        消費金額(不包含運費) Ｘ 此參數 / 100<br>
                                        此數值為新增商家時預設帶入點數抵扣率的數值
                                    </th>
                                </tr>
                                <tr onClick="iCheckThisRow(this);">
                                    <th>紅利折抵上限</th>
                                    <th><input type="number" name="var[BONUS_TOP]" value="<?=$row_var['BONUS_TOP']?>" placeholder="10%"></th>
                                    <th>
                                        可折抵的總金額上限百分比
                                    </th>
                                </tr>
                                <tr onClick="iCheckThisRow(this);">
                                    <th>府方商家</th>
                                    <th><input type="number" name="var[GOV_STORE]" value="<?=$row_var['GOV_STORE']?>" placeholder="商家ID"></th>
                                    <th>
                                        府方商家
                                    </th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="box-footer">
                        <button class="pull-right btn btn-success btn-sm">修改</button>
                    </div>
                </form>

            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>