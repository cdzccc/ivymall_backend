<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("delivery_arrived_edit");
$ids = filter_input(INPUT_POST, 'id', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

if(empty($ids) || !is_array($ids)) {
    js_go_back_global("NOT_POST");
    exit;
}

$update_datetime = date("Y-m-d H:i:s");
$update_user     = $_SESSION[SESSION_VARIABLE."_user_id"];

foreach (array_filter($ids) as $id) {
    # code...
    $sql_array = array(
        "status"               => array("2", checkinput_sql(1,2)),
        "update_datetime"      => array("2", checkinput_sql($update_datetime, 50)),
        "update_user"          => array("2", checkinput_sql($update_user, 50)),
    );
    $sql_cmd = update("goods_arrived", array("id", $id), $sql_array);
    $rs = $db->query($sql_cmd);
    //send mail
    $sql_cmd = "select ga.*,g.Goods_Name, c.Contact_Email, g.Goods_ID, g.item_number, g.type, g.sub_category from goods_arrived as ga 
        join goods as g
            on g.Goods_ID = ga.goods_id
        join customer as c
            on c.Customer_ID = ga.customer_id
        where ga.deleted_at is null and ga.id = '".checkinput_sql($id,19)."'";
    $rs = $db->query($sql_cmd);
    $row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
    $data['mail'] = $row['Contact_Email'];
    $data['title'] = "常春藤網路書城 - 貨到通知";
    $data['type'] = "2";
    $data['goods_name'] = $row['Goods_Name'];
    if($data['type'] == 3)
        $folder = "product_magazine";
    else
        $folder = "product_book";
    $data['link_goods'] = "http://ivy2.begonia-design.com.tw/product/".$folder."/".$row['sub_category']."/".$row['item_number'];
    classMail::send_mail($data);
}
    add_log('貨到通知管理','2');
    $db->disconnect();
    $referer = filter_input(INPUT_POST, 'referer');
    $redirect = "./list.php";
    if(!empty($referer))
        $redirect = $referer;
    js_repl_global( $redirect, "EDIT_SUCCESS");
    exit;
?>
