<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("delivery_arrived");
$sql_where = "";

$title  = filter_input(INPUT_GET, 'title');
$status = filter_input(INPUT_GET, 'status');

if(!empty($title))
    $sql_where .= " and (g.Goods_Name like '%".checkinput_sql($title,255)."%'
    or ga.goods_id like '%".checkinput_sql($title,255)."%'
    or c.Customer_Mail like '%".checkinput_sql($title,255)."%' 
    or ga.customer_id like '%".checkinput_sql($title,255)."%')";
if(in_array($status, ["0","1"]))
    $sql_where .= " and ga.status = '".checkinput_sql($status,3)."'";
if(!empty($s_date))
    $sql_where .= " and ga.create_datetime >= '".checkinput_sql($s_date." 00:00:00",19)."'";
if(!empty($e_date))
    $sql_where .= " and ga.create_datetime <= '".checkinput_sql($e_date." 23:59:59",19)."'";

//取出總筆數
$sql_cmd = "select count(*) from goods_arrived as ga
    join goods as g
        on g.Goods_ID = ga.goods_id
    join customer as c
        on c.Customer_ID = ga.customer_id
    where ga.deleted_at is null ".$sql_where." ";
$rs = $db->query($sql_cmd);
$row = $rs->fetchRow();
$total = $row[0];

$mypage = new page;
$pages = $mypage->set_page($total, $PAGE_NUM);

//設定分頁的初始值
if (empty($_GET['num']))
{
    $num = 1;
}else{
    $num = intval($_GET['num']);
}

$sql_cmd = "select ga.*,g.Goods_Name, c.Customer_Mail,g.item_number, c.Contact_Email from goods_arrived as ga 
    join goods as g
        on g.Goods_ID = ga.goods_id
    join customer as c
        on c.Customer_ID = ga.customer_id
    where ga.deleted_at is null ".$sql_where." order by ga.create_datetime desc ".$pages[$num];
$rs_link  = $db->query($sql_cmd);
$tmp = $_GET;
unset($tmp['num']);
$val = http_build_query($tmp);
$page = $mypage->multi($total , $PAGE_NUM, $num, "./list.php", "", $val);
?><!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php"); ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                貨到通知管理
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box">
                <div class="box-header">
                    <form action="./list.php" method="GET">
                        <div style="float:left; display:inline-block; width:300px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">搜尋</button>
                                </div>
                                <input type="text" name="title" class="form-control input-sm"
                                       placeholder="商品貨號,商品名稱,UID,聯絡Email" value="<?=(!empty($_GET['title']))?$_GET['title']:""?>">
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:150px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">建立時間</button>
                                </div>
                                <input type="text" name="s_date" class="form-control input-sm datetimepicker"
                                       placeholder="起始日" value="<?=(!empty($_GET['s_date']))?$_GET['s_date']:""?>">
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:150px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">~</button>
                                </div>
                                <input type="text" name="e_date" class="form-control input-sm datetimepicker"
                                       placeholder="結束日" value="<?=(!empty($_GET['e_date']))?$_GET['e_date']:""?>">
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:180px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">狀態</button>
                                </div>
                                <select class="form-control input-sm" name="status">
                                    <option value="all">全部</option>
                                    <option value="1" <?=($status == "1")?"selected":""?>>已通知</option>
                                    <option value="0" <?=($status == "0")?"selected":""?>>未通知</option>
                                </select>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success btn-sm pull-right">送出</button>
                        <a class="btn btn-danger btn-sm pull-right" href="./list.php" style="margin-right: 10px;">清除</a>
                        <div style="float:left; display:inline-block; width:100%;margin-top: 10px;">
                            <input class="pull-right btn btn-success btn-sm" type="button" value="刪除" onclick="if(confirm('是否刪除資料？刪除後無法刪除後無法還原！')) {$('form.form_edit').attr('action','./delete.php');$('form.form_edit').submit();}">
					</form>
					<form action="./edit.php" method="post" class="form_edit">
						<button type="submit" onclick="return confirm('變更後無法修改，是否送出？')" class="pull-right btn btn-success btn-sm" style="margin-right: 10px;">發送通知</button>
					</div>
				</div>
                <div class="box-body no-padding">
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <th style="width:100px;"><a href="javascript:void(0)" onclick="select_all('')">(全選)</a></th>
                                    <th>商品貨號</th>
                                    <th>商品名稱</th>
                                    <th>UID</th>
                                    <th>聯絡Email</th>
                                    <th>建立時間</th>
                                    <th>狀態</th>
                                    <th>通知時間</th>
                                    <!-- <th>修改(<a onclick="select_all('id');">全選</a>)</th> -->
                                    <th>操作</th>
                                </tr>
                                <?php
                                if($rs_link->numRows() > 0) {
                                while($row = $rs_link->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                                ?>
                                <tr onClick="iCheckThisRow(this);">
                                    <th><input type="checkbox" name="id[]" value="<?=$row["id"]?>"></th>
                                    <th><?=$row['item_number']?></th>
                                    <th width="200"><?=$row['Goods_Name']?></th>
                                    <th><?=$row['customer_id']?></th>
                                    <th><?=$row['Contact_Email']?></th>
                                    <th><?=$row['create_datetime']?></th>
                                    <th>
                                        <?php
                                            if($row['status'] == 0) {
                                                // echo "<select name='id[]'>";
                                                // echo "<option value=''>未通知</option>";
                                                // echo "<option value='".$row['id']."'>已通知</option>";
                                                // echo "</select>";
                                                echo "未通知";
                                            }
                                            else {
                                                echo "已通知";
                                            }
                                        ?>
                                    </th>
                                    <th><?=($row['status']==1)?$row['update_datetime']:""?></th>
<!--                                     <th>
                                        <?php
                                            if($row['status'] == 0) {
                                        ?>
                                        <input class="id" type="checkbox" name="id[]" value="<?=$row['id']?>">
                                        <?php } ?>
                                    </th> -->
                                    <td>
                                        <a class="btn btn-xs btn-danger" href="javascript:void(0);" onclick="if(confirm('是否確認刪除？')){location.href='./delete.php?id=<?=$row["id"]?>';}">刪除</a>
                                    </th>
                                </tr>
                                <?php }} ?>
                                <!-- <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th>
                                        <button type="submit" onclick="return confirm('變更後無法修改，是否送出？')" class="btn btn-success btn-sm">送出</button>
                                    </th>
                                    <th></th>
                                </tr>-->
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
		<?=$page?>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>

</html>