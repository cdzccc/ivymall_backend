$(document).ready(function () {
    $(".sidebar-menu li a").each(function () {
        $this = $(this).parents('.sidebar-menu > li');
        $thisa = $(this).parents('li');
        if (this.href == window.location.href) {
            $this.addClass("menu-open");
            $this.children(".treeview-menu").show();
            $thisa.addClass("active");
        }
    });
})