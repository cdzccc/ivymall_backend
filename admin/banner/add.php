<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("banner_add");
$id = get_id();
$name        = filter_input(INPUT_POST, 'name');
$desc        = filter_input(INPUT_POST, 'alt');
$url         = filter_input(INPUT_POST, 'url');
$stime       = filter_input(INPUT_POST, 'stime');
$etime       = filter_input(INPUT_POST, 'etime');
$status      = filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 1, 'default' => 0)));
$desc2       = filter_input(INPUT_POST, 'alt_m');
$sort        = filter_input(INPUT_POST, 'sort', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 99999, 'default' => 0)));

if(empty($name) || empty($_FILES['pic']['name'])) {
    $err_msg = "欄位";
    if(empty($name))
        $err_field[] = "名稱";
    if(empty($_FILES['pic']['name']))
        $err_field[] = "圖片(電腦版)";
    $err_msg .= "'".implode("','", $err_field)."'不可為空值";
    js_go_back_self($err_msg);
    exit;
}


if ($_FILES['pic']['name'] != "none" && is_uploaded_file($_FILES['pic']['tmp_name']))
{
    //重組檔名
    $type = explode(".", $_FILES['pic']['name']);
    $img = $_FILES['pic']['name'];
    if(file_exists(DOCUMENT_ROOT."/upload/".$img)) {
        // js_go_back_self("檔名重複!!");
        // exit;
    }
    @copy($_FILES['pic']['tmp_name'], DOCUMENT_ROOT."/upload/".$img);
}
else {
    js_go_back_global("IMG_ERROR");
    exit;
}

if ($_FILES['pic_m']['name'] != "none" && is_uploaded_file($_FILES['pic_m']['tmp_name']))
{
    //重組檔名
    $type = explode(".", $_FILES['pic_m']['name']);
    $img_m = $_FILES['pic_m']['name'];
    if(file_exists(DOCUMENT_ROOT."/upload/".$img_m)) {
        // js_go_back_self("檔名重複!!");
        // exit;
    }
    @copy($_FILES['pic_m']['tmp_name'], DOCUMENT_ROOT."/upload/".$img_m);
}
else {
    $img_m = "";
}

$create_datetime = date("Y-m-d H:i:s");
$create_user     = $_SESSION[SESSION_VARIABLE."_user_id"];
$update_datetime = date("Y-m-d H:i:s");
$update_user     = $_SESSION[SESSION_VARIABLE."_user_id"];


$sql_array = array(
    "id"              => array("2", checkinput_sql($id, 19)),
    "name"            => array("2", checkinput_sql($name, 255)),
    "Category"        => array("2", "Banner"),
    "desc"            => array("2", checkinput_sql($desc, 100)),
    "url"             => array("2", checkinput_sql($url, 100)),
    "stime"           => array("2", checkinput_sql((empty($stime))?"":date("Y-m-d H:i:s", strtotime($stime)), 30)),
    "etime"           => array("2", checkinput_sql((empty($etime))?"":date("Y-m-d H:i:s", strtotime($etime)), 30)),
    "status"          => array("2", checkinput_sql($status, 5)),
    "pic"             => array("2", checkinput_sql($img, 200)),
    "pic_m"           => array("2", checkinput_sql($img_m, 200)),
    "desc2"           => array("2", checkinput_sql($desc2, 2000)),
    "create_datetime" => array("2", checkinput_sql($create_datetime, 50)),
    "create_user"     => array("2", checkinput_sql($create_user, 50)),
    "update_datetime" => array("2", checkinput_sql($update_datetime, 50)),
    "update_user"     => array("2", checkinput_sql($update_user, 50)),
    "sort"            => array("2", checkinput_sql($sort, 5)),
);
$sql_cmd = insert("message", $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
   js_go_back_global("DB_ADD_ERROR");
   exit;
}else{
    add_log('banner管理','1');
   $db->disconnect();
   js_repl_global( "./list.php", "ADD_SUCCESS");
   exit;
}
?>
