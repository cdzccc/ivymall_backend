<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("email_info");

if(isset($_GET['action']))
    $action = $_GET['action'];
else
    $action = "add";

$row["id"] = "";
$row["name"] = "";
$row["status"] = "1";
$row['email'] = "";
$row['sort'] = 0;
$row['pic_alt1'] = "";
$option = [];
if($action == "edit") {
    $sql_cmd = "select * from mail_info where deleted_at is null and id = '".checkinput_sql($_GET['id'],19)."'";
    $rs = $db->query($sql_cmd);
    $row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
    $option = explode(',', $row['option']);
}

?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php") ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                聯絡我們收信人管理
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" method="POST" action="./<?=$action?>.php" enctype="multipart/form-data">
                    <div class="box-header">
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">收件人名稱</label>
                            <div class="col-sm-10">
                                <input name="name" type="text" class="form-control" value="<?=$row["name"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label">收件人信箱</label>
                            <div class="col-sm-10">
                                <input name="email" type="email" class="form-control" value="<?=$row["email"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="alt" class="col-sm-2 control-label">收信單元</label>
                            <div class="col-sm-10 control-text">
                                <label><input type="checkbox" name="option[]" value="1" <?=(in_array(1,$option))?"checked":""?>>聯絡我們</label>
                                <label><input type="checkbox" name="option[]" value="2" <?=(in_array(2,$option))?"checked":""?>>企業服務</label>
                                <label><input type="checkbox" name="option[]" value="3" <?=(in_array(3,$option))?"checked":""?>>廣告刊登</label>
                                <label><input type="checkbox" name="option[]" value="4" <?=(in_array(4,$option))?"checked":""?>>企業團購</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">狀態</label>
                            <div class="col-sm-10 control-text">
                                <label>
                                    <input name="status" type="radio" <?=($row['status'] == 1)?"checked":""?>
                                       value="1">上架
                                </label>
                                <label>
                                    <input name="status" type="radio" <?=($row['status'] == 0)?"checked":""?>
                                       value="0">下架
                                </label>
                            </div>
                        </div>
                        <?php if($action == "edit") {?>
                        <div class="form-group">
                            <label for="sort" class="col-sm-2 control-label">建立時間</label>
                            <div class="col-sm-10 control-text">
                                <?=$row['create_datetime']?>
                            </div>
                        </div>
                        <?php }?>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-right">
                        <input type="hidden" name="id" value="<?=$row['id']?>">
                        <a href="javascript:history.back()" class="btn btn-danger" style="margin-right: 10px;">取消，並返回上一層</a>
                        <button type="submit" class="btn btn-primary">儲存</button>
                        <input type="hidden" name="referer" value="<?=(!empty($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:"")?>">
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>