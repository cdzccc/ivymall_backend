<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("email_info_add");
$id = get_id();
$name   = filter_input(INPUT_POST, 'name');
$email  = filter_input(INPUT_POST, 'email');
$option = filter_input(INPUT_POST, 'option', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$status = filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 1, 'default' => 0)));

if(empty($name) || empty($option)) {
    $err_msg = "欄位";
    if(empty($name))
        $err_field[] = "收件人名稱";
    if(empty($option))
        $err_field[] = "收信單元";
    $err_msg .= "'".implode("','", $err_field)."'不可為空值";
    js_go_back_self($err_msg);
    exit;
}

if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    js_go_back_global("EMAIL_ERROR");
    exit;
}
$create_datetime = date("Y-m-d H:i:s");
$create_user     = $_SESSION[SESSION_VARIABLE."_user_id"];
$update_datetime = date("Y-m-d H:i:s");
$update_user     = $_SESSION[SESSION_VARIABLE."_user_id"];

$sql_array = array(
    "id"              => array("2", checkinput_sql($id, 19)),
    "name"            => array("2", checkinput_sql($name, 200)),
    "email"           => array("2", checkinput_sql($email, 200)),
    "option"          => array("2", checkinput_sql(implode(",", $option) , 45)),
    "status"          => array("2", checkinput_sql($status,2)),
    "create_datetime" => array("2", checkinput_sql($create_datetime, 50)),
    "create_user"     => array("2", checkinput_sql($create_user, 50)),
    "update_datetime" => array("2", checkinput_sql($update_datetime, 50)),
    "update_user"     => array("2", checkinput_sql($update_user, 50)),
);

$sql_cmd = insert("mail_info", $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
   js_go_back_global("DB_ADD_ERROR");
   exit;
}else{
    add_log('聯絡我們收信人管理','1');
   $db->disconnect();
   js_repl_global( "./list.php", "ADD_SUCCESS");
   exit;
}
?>
