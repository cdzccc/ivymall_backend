<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("member");
$select2_json = "";
$Customer_ID = filter_input(INPUT_GET, 'Customer_ID');
$sql_cmd = "select * from member where Customer_ID = '".checkinput_sql($Customer_ID,19)."'";
$rs = $db->query($sql_cmd);

$row_summary = [];
$row_customer = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
$sql_cmd = "select * from point_summary where Customer_ID = '".checkinput_sql($Customer_ID,19)."'";
$rs = $db->query($sql_cmd);
$row_summary = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);

$row_list = [];
$sql_cmd = "select * from point_list where Customer_ID = '".checkinput_sql($Customer_ID,19)."'";
$rs = $db->query($sql_cmd);
while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
    $row_list[] = $row;
}

?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php") ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                會員管理
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" method="POST" action="/article/{{ $action }}">
                    <div class="box-body">

                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">姓名</label>
                            <div class="col-sm-10">
                                <input id="title" name="title" type="text" class="form-control" placeholder=""
                                       value="<?=$row_customer["Customer_Name"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">email</label>
                            <div class="col-sm-10">
                                <input id="title" name="title" type="text" class="form-control" placeholder=""
                                       value="<?=$row_customer["Customer_Mail"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">電話</label>
                            <div class="col-sm-10">
                                <input id="title" name="title" type="text" class="form-control" placeholder=""
                                       value="<?=$row_customer["Customer_Phone"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">地址</label>
                             <div class="col-sm-10">
                                <div id="twzipcode">
                                    <div data-role="zipcode"
                                         data-name="zipcode"
                                         data-value=""
                                         data-style="zipcode-style">
                                    </div>
                                    <div data-role="county"
                                         data-name="Dealer_Address_City"
                                         data-value="<?=$row_customer['Customer_City']?>"
                                         data-style="city-style">
                                    </div>
                                    <div data-role="district"
                                         data-name="Dealer_Address_Township"
                                         data-value="<?=$row_customer['Customer_Area']?>"
                                         data-style="district-style">
                                    </div>
                                    <input name="Dealer_Address" type="text"  value="<?=$row_customer['Customer_Addr']?>"/>
                                </div>

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="f_path" class="col-sm-2 control-label">建立時間</label>
                            <div class="col-sm-10" style="line-height: 32px;">
                                <i class="fa fa-clock-o" aria-hidden="true"></i> <?=$row_customer['create_datetime']?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">點數紀錄</label>
                            <div class="col-sm-10">
                                <table class="table table-bordered">
                                    <tr>
                                        <td>點數兌換日期</td>
                                        <td>訂單號</td>
                                        <td>存取點數</td>
                                        <td>剩餘點數</td>
                                    </tr>
                                    <?php 
                                        $sum = 0;
                                        foreach ($row_list as $key => $value) :
                                    ?>
                                    <tr>
                                        <td><?=date("Y-m-d",strtotime($value['datetime']))?></td>
                                        <td><?=$value['Order_ID']?></td>
                                        <td><?=(($value['type']==2)?"-":"").$value['point']?></td>
                                        <td><?=$value['left']?></td>
                                    </tr>
                                    <? endforeach ?>
                                </table>
                            </div>

                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-right">
                        <input type="hidden" name="id" value="{{ $article['id'] }}">
                        <a href="/admin/member/list.php" class="btn btn-danger" style="margin-right: 10px;">取消，並返回上一層</a>
                        <!-- <button type="submit" class="btn btn-primary">儲存</button> -->
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>