<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("member");

$sql_where = "";
$order_by = " order by create_datetime desc ";
if(!empty($_GET['name'])) {
    $sql_where .= " and Customer_Name like '%".$_GET["name"]."%'";
}
if(!empty($_GET['email'])) {
    $sql_where .= " and Customer_Mail like '%".$_GET["email"]."%'";
}

//取出總筆數
$sql_cmd = "select count(*) from member where 1=1 ".$sql_where." ";
$rs = $db->query($sql_cmd);
$row = $rs->fetchRow();
$total = $row[0];

$mypage = new page;
$pages = $mypage->set_page($total, $PAGE_NUM);

//設定分頁的初始值
if (empty($_GET['num']))
{
    $num = 1;
}else{
    $num = intval($_GET['num']);
}


$sql_cmd = "select * from member where 1=1 ".$sql_where.$order_by.$pages[$num];
$rs_member = $db->query($sql_cmd);
$tmp = $_GET;
unset($tmp['num']);
$val = http_build_query($tmp);
$page = $mypage->multi($total , $PAGE_NUM, $num, "./list.php", "", $val);
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php") ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                會員管理
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box">
                <div class="box-header">
                    <form action="./list.php" method="GET">
                        <div style="float:left; display:inline-block; width:320px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">姓名</button>
                                </div>
                                <input type="text" name="name" class="form-control input-sm"
                                       placeholder="" value="<?=(!empty($_GET['name']))?$_GET['name']:""?>">
                            </div>
                        </div>
                        <div style="float:left; padding-right:15px; width:200px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">email</button>
                                </div>
                                <input type="text" name="email" class="form-control input-sm"
                                       placeholder="" value="<?=(!empty($_GET['email']))?$_GET['email']:""?>">
                            </div>
                        </div>
                        <a class="btn btn-danger btn-sm" href="./list.php">清除</a>
                        <button type="submit" class="btn btn-success btn-sm">送出</button>
                    </form>
                </div>
                <div class="box-body no-padding">
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                <th>id</th>
                                <th>姓名</th>
                                <th>email</th>
                                <th>創建時間</th>
                                <th>操作</th>
                            </tr>
                            <?php while($row = $rs_member->fetchRow(MDB2_FETCHMODE_ASSOC)) {?>
                            <tr onClick="iCheckThisRow(this);">
                                <th><?=$row["Customer_ID"]?></th>
                                <th>
								<?php                                 
									if(preg_match('/^.+@weixin.com$/',$row['Customer_Mail'])){
										
									    if(empty($row['Customer_Name'])){
									        echo $row["nickname_weixin"];
										}else {										   
										    echo $row["Customer_Name"];
										}
									}else {										
										echo $row["Customer_Name"];                                }
									?>
								</th>
                                <th><?=$row["Customer_Mail"]?></th>
                                <th><?=$row["create_datetime"]?></th>
                                <th>
                                    <a class="btn btn-xs btn-primary" href="./detail.php?Customer_ID=<?=$row["Customer_ID"]?>">檢視</a>
                                    <!-- <a class="btn btn-xs btn-danger" href="#">刪除</a> -->
                                </th>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
<?=$page?>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>