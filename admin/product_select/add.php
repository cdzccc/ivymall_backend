<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("product_add");
$id = get_id();
$category     = filter_input(INPUT_POST, 'category');
$sub_category = filter_input(INPUT_POST, 'sub_category');
$Goods_ID     = filter_input(INPUT_POST, 'Goods_ID');
$stime       = filter_input(INPUT_POST, 'stime');
$etime       = filter_input(INPUT_POST, 'etime');
$status      = filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 1, 'default' => 0)));
$sort        = filter_input(INPUT_POST, 'sort', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 99999, 'default' => 0)));

if(empty($Goods_ID)) {
    $err_msg = "欄位";
    if(empty($Goods_ID))
        $err_field[] = "商品名稱";
    $err_msg .= "'".implode("','", $err_field)."'不可為空值";
    js_go_back_self($err_msg);
    exit;
}

$create_datetime = date("Y-m-d H:i:s");
$create_user     = $_SESSION[SESSION_VARIABLE."_user_id"];
$update_datetime = date("Y-m-d H:i:s");
$update_user     = $_SESSION[SESSION_VARIABLE."_user_id"];


$sql_array = array(
    "id"              => array("2", checkinput_sql($id, 19)),
    "Goods_ID"        => array("2", checkinput_sql($Goods_ID, 255)),
    "s_date"           => array("2", checkinput_sql(date("Y-m-d H:i:s", strtotime($stime)), 30)),
    "e_date"           => array("2", checkinput_sql(date("Y-m-d H:i:s", strtotime($etime)), 30)),
    "status"          => array("2", checkinput_sql($status, 5)),
    "create_datetime" => array("2", checkinput_sql($create_datetime, 50)),
    "create_user"     => array("2", checkinput_sql($create_user, 50)),
    "update_datetime" => array("2", checkinput_sql($update_datetime, 50)),
    "update_user"     => array("2", checkinput_sql($update_user, 50)),
    "sort"            => array("2", checkinput_sql($sort, 5)),
);
$sql_cmd = insert("goods_select", $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
   js_go_back_global("DB_ADD_ERROR");
   exit;
}else{
    add_log('總編嚴選管理','1');
   $db->disconnect();
   js_repl_global( "./list.php", "ADD_SUCCESS");
   exit;
}
?>
