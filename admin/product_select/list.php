<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("product");
$sql_where = "";

$title  = filter_input(INPUT_GET, 'title');
$status = filter_input(INPUT_GET, 'status');
$category = filter_input(INPUT_GET, 'category');

if(!empty($title))
    $sql_where .= " and g.Goods_Name like '%".checkinput_sql($title,255)."%'";
if(in_array($status, ["0","1"]))
    $sql_where .= " and gs.status = '".checkinput_sql($status,3)."'";
if(!empty($category))
    $sql_where .= " and cate.Category_Code = '".checkinput_sql($category,19)."'";

//取出總筆數
$sql_cmd = "select count(*) from goods_select as gs
    join goods as g on g.Goods_ID = gs.Goods_ID
    join category as sub_cate on g.sub_category  = sub_cate.Category_Code and sub_cate.Category_CodeGroup = 'Goods_Sub_Category'
    join category as cate on sub_cate.Parent_Category_Code = cate.Category_Code and cate.Category_CodeGroup = 'Goods_Category'
    where gs.deleted_at is null ".$sql_where." ";
$rs = $db->query($sql_cmd);
// print_r($rs);
$row = $rs->fetchRow();
$total = $row[0];

$mypage = new page;
$pages = $mypage->set_page($total, $PAGE_NUM);

//設定分頁的初始值
if (empty($_GET['num']))
{
    $num = 1;
}else{
    $num = intval($_GET['num']);
}

$sql_cmd = "select gs.*,g.Goods_Name,cate.Category_Name from goods_select as gs
    join goods as g on g.Goods_ID = gs.Goods_ID
    join category as sub_cate on g.sub_category  = sub_cate.Category_Code and sub_cate.Category_CodeGroup = 'Goods_Sub_Category'
    join category as cate on sub_cate.Parent_Category_Code  = cate.Category_Code and cate.Category_CodeGroup = 'Goods_Category'
    where gs.deleted_at is null ".$sql_where." order by id desc ".$pages[$num];
$rs_banner  = $db->query($sql_cmd);
$tmp = $_GET;
unset($tmp['num']);
$val = http_build_query($tmp);
$page = $mypage->multi($total , $PAGE_NUM, $num, "./list.php", "", $val);

// 分類list
$sql_cmd = "select * from category where Category_CodeGroup = 'Goods_Category' and Status = 1 order by create_datetime desc ";
$rs = $db->query($sql_cmd);
while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
    $row_category[] = $row;
}

?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php"); ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                總編嚴選管理
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box">
                <form action="./list.php" method="GET">
                    <div class="box-header">
                        <div style="float:left; display:inline-block; width:180px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">搜尋</button>
                                </div>
                                <input type="text" name="title" class="form-control input-sm"
                                       placeholder="名稱" value="<?=(!empty($_GET['title']))?$_GET['title']:""?>">
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:180px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">狀態</button>
                                </div>
                                <select class="form-control input-sm" name="status">
                                    <option value="">全部</option>
                                    <option value="1" <?=($status == "1")?"selected":""?>>上架</option>
                                    <option value="0" <?=($status == "0")?"selected":""?>>下架</option>
                                </select>
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:180px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">分類</button>
                                </div>
                                <select class="form-control input-sm" name="category">
                                    <option value="">全部</option>
                                    <?php
                                        //foreach($row_category as $arr_category) {
                                    ?>
                                        <!--<option value="<?=$arr_category['Category_Code']?>" <?=($arr_category['Category_Code'] == $category)?"selected":""?>><?=$arr_category['Category_Name']?></option>-->
                                    <?php
                                        //}
                                    ?>
									    <option value="2018110614234929355" <?=("2018110614234929355" == $category)?"selected":""?>>升學館</option>
									    <option value="2018110614240697420" <?=("2018110614240697420" == $category)?"selected":""?>>多益館</option>
									    <option value="2018110614244266622" <?=("2018110614244266622" == $category)?"selected":""?>>英檢館</option>
									    <option value="2018110614245784103" <?=("2018110614245784103" == $category)?"selected":""?>>進修館</option>
									    <option value="2018110614252450380" <?=("2018110614252450380" == $category)?"selected":""?>>兒童館</option>
                                </select>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success btn-sm pull-right">送出</button>
                        <a class="btn btn-danger btn-sm pull-right" href="./list.php" style="margin-right: 10px;">清除</a>
                        <div style="float:left; display:inline-block; width:100%;margin-top: 10px;">
                            <a class="pull-right btn btn-success btn-sm" href="./detail.php?action=add">新增</a>
                            <input class="pull-right btn btn-success btn-sm" type="button" value="刪除" onclick="if(confirm('是否刪除資料？刪除後無法刪除後無法還原！')) {$('form').attr('action','./delete.php');$('form').submit();}"style="margin-right: 10px;">
                        </div>
                    </div>
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <th style="min-width:100px;"><a href="javascript:void(0)" onclick="select_all('')">(全選)</a></th>
                                    <th>主分類</th>
                                    <th>商品名稱</th>
                                    <th>上架時間</th>
                                    <th>下架時間</th>
                                    <th>排序</th>
                                    <th>狀態</th>
                                    <th>操作</th>
                                </tr>
                                <?php
                                if($rs_banner->numRows() > 0) {
                                while($row = $rs_banner->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                                ?>
                                <tr onClick="iCheckThisRow(this);">
                                    <th><input type="checkbox" name="id[]" value="<?=$row["id"]?>"></th>
                                    <th><?=$row['Category_Name']?></th>
                                    <th><?=$row['Goods_Name']?></th>
                                    <th><?=$row['s_date']?></th>
                                    <th><?=$row['e_date']?></th>
                                    <th><?=$row['sort']?></th>
                                    <th><?=($row['status']==0)?"下架":"上架"?></th>
                                    <th>
                                        <a class="btn btn-xs btn-primary" href="./detail.php?action=edit&id=<?=$row['id']?>">編輯</a>
                                    </th>
                                </tr>
                                <?php }} ?>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
		<?=$page?>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>