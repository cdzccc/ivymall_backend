<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("product");

if(isset($_GET['action']))
    $action = $_GET['action'];
else
    $action = "add";
$id     = filter_input(INPUT_GET, 'id');
$category     = filter_input(INPUT_POST, 'category');
$sub_category = filter_input(INPUT_POST, 'sub_category');
$Goods_ID     = filter_input(INPUT_POST, 'Goods_ID');

$row["id"] = "";
$row["name"] = "";
$row["stime"] = "";
$row["etime"] = "";
$row["status"] = "1";
$row['sort'] = 0;

if($action == "edit") {
    $sql_cmd = "select * from goods_select where deleted_at is null and id = '".checkinput_sql($_GET['id'],19)."'";
    $rs = $db->query($sql_cmd);
    $row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
    $sql_cmd = "select gs.*,sub_cate.Category_Code,sub_cate.Parent_Category_Code from goods_select as gs
        join goods as g on g.Goods_ID = gs.Goods_ID
        join category as sub_cate on g.sub_category  = sub_cate.Category_Code and sub_cate.Category_CodeGroup = 'Goods_Sub_Category'
        where gs.deleted_at is null and gs.id = '".checkinput_sql($_GET['id'],19)."'";
    $rs = $db->query($sql_cmd);
    $row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
    $Goods_ID =     $row['Goods_ID'];
    if(empty($category))
        $category     = $row['Parent_Category_Code'];
    if(empty($sub_category))
        $sub_category = $row['Category_Code'];

}

$row_category = [];
$sql_cmd = "select Category_Code, Category_Name from category where
    Category_CodeGroup = 'Goods_Category' and Status = 1  and deleted_at is null
    order by update_datetime asc";
$rs_category = $db->query($sql_cmd);
while($row_category_tmp = $rs_category->fetchRow(MDB2_FETCHMODE_ASSOC)) {
    $row_category[] = $row_category_tmp;
}

$row_sub_category = [];
if(!empty($category)) {
    $sql_cmd = "select Category_Code, Category_Name from category where
    Category_CodeGroup = 'Goods_Sub_Category' and Status = 1 and Parent_Category_Code = '".checkinput_sql($category, 19)."'and deleted_at is null
    order by update_datetime asc";
    $rs_sub_category = $db->query($sql_cmd);
    while($row_sub_category_tmp = $rs_sub_category->fetchRow(MDB2_FETCHMODE_ASSOC)) {
        $row_sub_category[] = $row_sub_category_tmp;
    }
}

$row_goods = [];
if(!empty($sub_category)) {
    $sql_cmd = "select * from goods where
    status = 1 and sub_category = '".checkinput_sql($sub_category, 19)."'and deleted_at is null
    order by update_datetime asc";
    $rs_goods = $db->query($sql_cmd);
    while($row_goods_tmp = $rs_goods->fetchRow(MDB2_FETCHMODE_ASSOC)) {
        $row_goods[] = $row_goods_tmp;
    }
}

?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php") ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                總編嚴選管理
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" method="POST" action="./<?=$action?>.php" enctype="multipart/form-data">
                    <div class="box-header">
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">商品主分類</label>
                            <div class="col-sm-10">
                                <select name="category" class="form-control select2_ajax_category" onchange="reload();">
                                    <option value="">請選擇</option>
                                <?php
                                    //foreach($row_category as $category_arr) {
                                ?>
                                    <!--<option value="<?=$category_arr['Category_Code']?>" <?=($category_arr['Category_Code'] == $category) ? 'selected' : '' ?>><?=$category_arr['Category_Name']?></option>-->
                                <?php
                                    //}
                                ?>
								<option value="2018110614234929355" <?=("2018110614234929355" == $category)?"selected":""?>>升學館</option>
								<option value="2018110614240697420" <?=("2018110614240697420" == $category)?"selected":""?>>多益館</option>
								<option value="2018110614244266622" <?=("2018110614244266622" == $category)?"selected":""?>>英檢館</option>
								<option value="2018110614245784103" <?=("2018110614245784103" == $category)?"selected":""?>>進修館</option>
								<option value="2018110614252450380" <?=("2018110614252450380" == $category)?"selected":""?>>兒童館</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="url" class="col-sm-2 control-label">商品次分類</label>
                            <div class="col-sm-10">
                                <select name="sub_category" class="form-control select2_ajax_category" onchange="reload();">
                                    <option value="">請選擇</option>
                                <?php
                                    foreach($row_sub_category as $sub_category_arr) {
                                ?>
                                    <option value="<?=$sub_category_arr['Category_Code']?>" <?=($sub_category_arr['Category_Code'] == $sub_category) ? 'selected' : '' ?>><?=$sub_category_arr['Category_Name']?></option>
                                <?php
                                    }
                                ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="header" class="col-sm-2 control-label">商品名稱</label>
                            <div class="col-sm-10">
                                <select name="Goods_ID" class="form-control select2_ajax_category"">
                                    <option value="">請選擇</option>
                                <?php
                                    foreach($row_goods as $goods_arr) {
                                ?>
                                    <option value="<?=$goods_arr['Goods_ID']?>" <?=($goods_arr['Goods_ID'] == $Goods_ID) ? 'selected' : '' ?>><?=$goods_arr['Goods_Name']?></option>
                                <?php
                                    }
                                ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">上架時間</label>
                            <div class="col-sm-10">
                                <input id="stime" name="stime" type="text" class="datetimepicker" value="<?=$row["s_date"]?>"> -
                                <input id="etime" name="etime" type="text" class="datetimepicker" value="<?=$row["e_date"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="sort" class="col-sm-2 control-label">排序</label>
                            <div class="col-sm-10">
                                <input id="sort" name="sort" type="text" class="form-control" value="<?=$row["sort"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">狀態</label>
                            <div class="col-sm-10 control-text">
                                <label>
                                    <input name="status" type="radio" <?=($row['status'] == 1)?"checked":""?>
                                       value="1">上架
                                </label>
                                <label>
                                    <input name="status" type="radio" <?=($row['status'] == 0)?"checked":""?>
                                       value="0">下架
                                </label>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-right">
                        <input type="hidden" name="id" value="<?=$row['id']?>">
                        <a href="javascript:history.back()" class="btn btn-danger" style="margin-right: 10px;">取消，並返回上一層</a>
                        <button type="submit" class="btn btn-primary">儲存</button>
                        <input type="hidden" name="referer" value="<?=(!empty($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:"")?>">
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>
<script>
    function reload() {
        $("form.form-horizontal").attr("action","detail.php?id=<?=$id?>&action=<?=$action?>");
        $("form.form-horizontal").submit();
    }
</script>
</body>
</html>