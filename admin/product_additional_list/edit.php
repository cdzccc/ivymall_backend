<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("additional_edit");
$id = filter_input(INPUT_POST, 'id');

$additional_id     = filter_input(INPUT_POST, 'additional_id');
$name          = filter_input(INPUT_POST, 'name');
$item_type     = filter_input(INPUT_POST, 'type');
$price         = filter_input(INPUT_POST, 'price');
$item_number   = filter_input(INPUT_POST, 'item_number', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$sort          = filter_input(INPUT_POST, 'sort', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 99999, 'default' => 0)));
$status        = filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 99999, 'default' => 0)));
$stock  = filter_input(INPUT_POST, 'stock');

$old_pic     = filter_input(INPUT_POST, 'old_pic');

if(empty($id)) {
    js_go_back_global("NOT_POST");
    exit;
}

if(empty($item_number)) {
    $err_msg = "欄位";
    if(empty($item_number[$item_type]))
        $err_field[] = "商品貨號";
    $err_msg .= "'".implode("','", $err_field)."'不可為空值";
    js_go_back_self($err_msg);
    exit;
}


if ($_FILES['pic']['name'] != "none" && is_uploaded_file($_FILES['pic']['tmp_name']))
{
    //重組檔名
    $type = explode(".", $_FILES['pic']['name']);
    $pic = $_FILES['pic']['name'];
    if(file_exists(DOCUMENT_ROOT."/upload/".$pic)) {
        // js_go_back_self("檔名重複!!");
        // exit;
    }
    @copy($_FILES['pic']['tmp_name'], DOCUMENT_ROOT."/upload/".$pic);
}
else {
    if($item_type == 2 && empty($old_pic)) {
        js_go_back_global("IMG_ERROR");
        exit;
    }
    $pic = $old_pic;
}

$update_datetime = date("Y-m-d H:i:s");
$update_user     = $_SESSION[SESSION_VARIABLE."_user_id"];

$sql_array = array(
    "type"            => array("2", checkinput_sql($item_type, 200)),
    "item_number"     => array("2", checkinput_sql($item_number[$item_type] , 45)),
    "sort"            => array("2", checkinput_sql($sort, 2)),
    "status"          => array("2", checkinput_sql($status, 2)),
    "name"            => array("2", checkinput_sql($name,100)),
    "pic"             => array("2", checkinput_sql($pic,200)),
    "price"           => array("2", checkinput_sql($price,30)),
    "update_datetime" => array("2", checkinput_sql($update_datetime, 50)),
    "update_user"     => array("2", checkinput_sql($update_user, 50)),
    "stock"           => array("2", checkinput_sql($stock,10)),
);
$sql_cmd = update("goods_additional_list", array("id", $id), $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
   js_go_back_global("DB_EDIT_ERROR");
   exit;
}else{
    add_log('加購品-參與商品','2');
    $db->disconnect();
    $referer = filter_input(INPUT_POST, 'referer');
    $redirect = "./list.php";
    if(!empty($referer))
        $redirect = $referer;
    js_repl_global( $redirect, "EDIT_SUCCESS");
   exit;
}
?>
