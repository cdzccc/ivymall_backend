<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("additional");

if(isset($_GET['action']))
    $action = $_GET['action'];
else
    $action = "add";

$row["id"] = "";
$row["additional_id"] = $_GET['additional_id'];
$row["item_number"] = "";
$row["alt"] = "";
$row["sort"] = "0";
$row["status"] = "1";
$row['item_number'] = "";
$row['type'] = "1";
$row['name'] = "";
$row['stock'] = "0";
$row['price'] = "";
$option = [];
if($action == "edit") {
    $sql_cmd = "select * from goods_additional_list where deleted_at is null and additional_id = '".$_GET['additional_id']."' and id = '".checkinput_sql($_GET['id'],19)."'";
    $rs = $db->query($sql_cmd);
    $row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
}

$goods = [];
$sql_cmd = "select * from goods where type in (1,2,3) and deleted_at is null";
$rs = $db->query($sql_cmd);
$arr_select_goods = [];
while($row_select_goods = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
    $goods[] = $row_select_goods;
}
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php") ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                加購品-參與商品
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" method="POST" action="./<?=$action?>.php" enctype="multipart/form-data">
                    <div class="box-header">
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="type" class="col-sm-2 control-label"><a style="color:red">*</a>加購品類型</label>
                            <div class="col-sm-10" style="padding-top:7px">
                                <label><input type="radio" name="type" value="1" <?=($row['type'] == 1)?"checked":""?> onclick="$('.item_number').hide();$('.item_number1').show();">上架商品</label>
                                <label><input type="radio" name="type" value="2" <?=($row['type'] == 2)?"checked":""?> onclick="$('.item_number').hide();$('.item_number2').show();">自訂</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="item_number" class="col-sm-2 control-label"><a style="color:red">*</a>商品貨號</label>
                            <div class="col-sm-10 item_number2 item_number" style="<?=($row['type'] == 2)?"":"display: none;"?>">
                                <input name="item_number[2]" type="text" class="form-control" value="<?=$row["item_number"]?>">
                            </div>
                            <div class="col-sm-10 item_number1 item_number" style="<?=($row['type']== 1)?"":"display: none;"?>">
                                <select name="item_number[1]" class="form-control select2_ajax_category" style="width: 100%;">
                                        <option value="">請選擇</option>
                                    <?php
                                        foreach($goods as $value) {
                                            $checked = "";
                                            if($value['item_number'] == $row['item_number'])
                                                $checked = "selected";
                                            echo "<option value=".$value['item_number']." ".$checked.">[".$value['item_number']."]".$value['Goods_Name']."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group item_number2 item_number" style="<?=($row['type'] == 2)?"":"display: none;"?>">
                            <label for="stock" class="col-sm-2 control-label">庫存數量</label>
                            <div class="col-sm-10">
                                <input name="stock" type="stock" class="form-control" value="<?=$row["stock"]?>">
                            </div>
                        </div>
                        <div class="form-group item_number2 item_number" style="<?=($row['type']== 2)?"":"display: none;"?>">
                            <label for="name" class="col-sm-2 control-label">商品標題</label>
                            <div class="col-sm-10">
                                <input name="name" type="name" class="form-control" value="<?=$row["name"]?>">
                            </div>
                        </div>
                        <div class="form-group item_number2 item_number" style="<?=($row['type']== 2)?"":"display: none;"?>">
                            <label for="alt" class="col-sm-2 control-label">商品圖片</label>
                            <div class="col-sm-10">
                                <?php
                                    if(!empty($row['pic'])) {
                                        echo "<img src='".WEBSITE_URL."upload/".$row['pic']."' width='80px' class='pic'><input type='hidden' class='old_pic' value='".$row['pic']."' name='old_pic'>";
                                        echo "<br>".$row['pic'];
                                        echo "<a href='javascript:void(0);' class='btn btn-danger' onclick='$(\".old_pic\").val(\"\");$(\".pic\").hide();$(this).hide()'>刪除圖片</a>";
                                    }
                                ?>
                                <input type="file" name="pic">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="price" class="col-sm-2 control-label"><a style="color:red">*</a>加購價</label>
                            <div class="col-sm-10">
                                <input name="price" type="name" class="form-control" value="<?=$row["price"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="sort" class="col-sm-2 control-label">排序</label>
                            <div class="col-sm-10">
                                <input name="sort" type="name" class="form-control" value="<?=$row["sort"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">狀態</label>
                            <div class="col-sm-10 control-text">
                                <label>
                                    <input name="status" type="radio" <?=($row['status'] == 1)?"checked":""?>
                                       value="1">上架
                                </label>
                                <label>
                                    <input name="status" type="radio" <?=($row['status'] == 0)?"checked":""?>
                                       value="0">下架
                                </label>
                            </div>
                        </div>
                        <?php if($action == "edit") {?>
                        <div class="form-group">
                            <label for="sort" class="col-sm-2 control-label">建立時間</label>
                            <div class="col-sm-10 control-text">
                                <?=$row['create_datetime']?>
                            </div>
                        </div>
                        <?php }?>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-right">
                        <input type="hidden" name="id" value="<?=$row['id']?>">
                        <input type="hidden" name="additional_id" value="<?=$row['additional_id']?>">
                        <a href="javascript:history.back()" class="btn btn-danger" style="margin-right: 10px;">取消，並返回上一層</a>
                        <button type="button" class="btn btn-primary" onclick="check_file();">儲存</button>
                        <input type="hidden" name="referer" value="<?=(!empty($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:"")?>">
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>