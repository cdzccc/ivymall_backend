<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("newslettercontent_edit");
$id = filter_input(INPUT_POST, 'id');

$category  = filter_input(INPUT_POST, 'category');
$name      = filter_input(INPUT_POST, 'name');
$sub_name  = filter_input(INPUT_POST, 'sub_name');
$post_time = filter_input(INPUT_POST, 'post_time');
$author    = filter_input(INPUT_POST, 'author');
$content   = filter_input(INPUT_POST, 'editor');
$status    = filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 1, 'default' => 0)));
$sort        = filter_input(INPUT_POST, 'sort', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 99999, 'default' => 0)));

if(empty($id)) {
    js_go_back_global("NOT_POST");
    exit;
}

if(empty($name) || empty($content) || empty($post_time)) {
    js_go_back_global("DATA_EMPTY");
    exit;
}

$update_datetime = date("Y-m-d H:i:s");
$update_user     = $_SESSION[SESSION_VARIABLE."_user_id"];

$sql_array = array(
    "id"              => array("2", checkinput_sql($id, 19)),
    "name"            => array("2", checkinput_sql($name, 255)),
    "sub_name"        => array("2", checkinput_sql($sub_name, 255)),
    "desc"            => array("2", checkinput_sql($content, 9999999)),
    "post_time"       => array("2", checkinput_sql($post_time, 30)),
    "status"          => array("2", checkinput_sql($status, 5)),
    "author"          => array("2", checkinput_sql($author, 200)),
    "update_datetime" => array("2", checkinput_sql($update_datetime, 50)),
    "update_user"     => array("2", checkinput_sql($update_user, 50)),
    "sort"            => array("2", checkinput_sql($sort, 5)),
);
$sql_cmd = update("article", array("id", $id), $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
   js_go_back_global("DB_EDIT_ERROR");
   exit;
}else{
    add_log('學習電子報內容管理','2');
   $db->disconnect();
    $referer = filter_input(INPUT_POST, 'referer');
    $redirect = "./list.php?category=".$category;
    if(!empty($referer))
        $redirect = $referer;
    js_repl_global( $redirect, "EDIT_SUCCESS");

   exit;
}
?>
