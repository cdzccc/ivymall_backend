<footer class="main-footer" style="min-height:5%">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; <?php echo date("Y"); ?> <a href="#">Company</a>.</strong> All rights reserved.
</footer>
