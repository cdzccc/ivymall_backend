<!-- ./wrapper -->
<link href="<?=WEBSITE_URL?>bower_components/select2/dist/css/select2.min.css" rel="stylesheet"
type="text/css"/>

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="<?=WEBSITE_URL?>bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?=WEBSITE_URL?>bower_components/jquery-ui/jquery-ui.min.js"></script>
<script src="<?=WEBSITE_URL?>bower_components/moment/moment.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?=WEBSITE_URL?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?=WEBSITE_URL?>bower_components/ckeditor/ckeditor.js"></script>
<script src="<?=WEBSITE_URL?>bower_components/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?=WEBSITE_URL?>bower_components/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script src="<?=WEBSITE_URL?>bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0-beta.3/js/select2.min.js"></script>

<!-- AdminLTE App -->
<script src="<?=WEBSITE_URL?>admin/dist/js/adminlte.min.js"></script>
<script type="text/javascript" src="<?=WEBSITE_URL?>admin/dist/js/jquery.twzipcode.js"></script>
<script type="text/javascript" src="<?=WEBSITE_URL?>admin/dist/js/cuactive.js"></script>

<script type="text/javascript" src="<?=WEBSITE_URL?>assets/bootstraptable/bootstrap-table.js"></script>
<script type="text/javascript" src="<?=WEBSITE_URL?>assets/bootstraptable/locale/bootstrap-table-zh-TW.js"></script>
<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
<style>
    .control-text {
        display: block;
        padding-top: 6px;
        padding-bottom: 6px;
    }
    .form-group input[type=checkbox],.form-group input[type=radio] {
        margin: 0 5px 0 15px;
    }
    .box-body.table-responsive th {
        white-space: nowrap;
    }
    th:has(input[value="刪除"]) {
        min-width: 100px;
    }
    tr:not(:first-child) th {
        font-weight: normal;
    }
</style>
<script>
    function select_all(class_name) {
        var checked = 1;
        if(class_name !== '') {
            $("."+class_name).each(function() {
                if($(this).prop("checked") == false) {
                    checked = 0;
                }
            });
            if(checked == 1) {
                $("."+class_name).prop("checked", false);
            }
            else {
                $("."+class_name).prop("checked", true);
            }
        }
        else {
            $("input[type=checkbox]").each(function() {
                if($(this).prop("checked") == false) {
                    checked = 0;
                }
            });
            if(checked == 1) {
                $("input[type=checkbox]").prop("checked", false);
            }
            else {
                $("input[type=checkbox]").prop("checked", true);
            }

        }
    }
    function add_item(classname, clone_name, spec_input) {
        var add_variant = $("." + clone_name).clone();
        add_variant.find('input').each(function () {
            $(this).val("");
        });
        add_variant.removeClass(clone_name);
        add_variant.find("span.select2").remove();
        $("."+classname).before(add_variant);
        $('.box-body .select2_category_item').select2({
            theme: "classic",
            tags: true,
            allowClear: true
        });

        if($('.box-body .' + spec_input).length > 1) {
            $(".box-body .remove_variant").show();
        }
        else {
            $(".box-body .remove_variant").hide();
        }
    }
    function change_variant() {
        $(".variant").each(function() {
            $(this).closest( ".form-group" ).find("select.select2_category_item").attr("name","option["+$(this).val()+"][]");
        });
    }
    function remove_variant(e) {
        e.closest("tr").remove();
        // setItem();
        if($('.box-body .select2_category_item').length > 1) {
            $(".box-body .remove_variant").show();
        }
        else {
            $(".box-body .remove_variant").hide();
        }
    }
    function setItem() {
        var item = [];
        $(".box-body .select2_category_item").each(function() {
           var array = $(this).val();
            if(item.length == 0) {
                item = array;
            }
            else {
                var tmp = [];
                var k = 0;
                for(var i = 0;i< item.length; i++) {
                    for(var j = 0;j < array.length; j++) {
                        tmp[k] = item[i] + " , " + array[j];
                        k++;
                    }
                }
                item = tmp;
            }
        });
        $(".category").each(function() {
            var check = false;
            for(var i = 0;i< item.length; i++) {
                if ($(this).val() == item[i]) {
                    check = true;
                }
            }
            if(!check) {
                $(this).parents( "tr" ).remove();
            }
        });

        if(item.length > 0) {
            $("table.product").hide();
            $("table.product_item").show();
        }
        else {
            $("table.product").show();
            $("table.product_item").hide();
        }
        var html;
        for(var i = 0;i< item.length; i++) {
            var check = false;
            $(".category").each(function() {
                if($(this).val() == item[i]) {
                    check = true;
                }
            });
            if(!check) {
                html += "<tr>" +
                    "<td>" + item[i] + "</td>" +
                    "<td><input type='text' name='price[]'><input class='category' name='item_category[]' type='hidden' value='"+ item[i] +"'></td>" +
                    "<td><input type='text' name='qty[]'></td>" +
                    "</tr>";
            }
        }
        $(".table tbody").append(html);
    }
    function check_file() {
        var check = false;
        var count = 0;
        var flag = 0;
        if($('input[type=file]').length > 0) {
            $('input[type=file]').each(function () {
                if($(this).val() != "") {
                    var tmp_file_name = $(this).val().split('\\');
                    var file_name = tmp_file_name[tmp_file_name.length-1];
                    // var file_name = 
                    $.ajax({
                        url: '<?=WEBSITE_URL?>admin/api/file_check.php?file=' + $(this).val()
                        // dataType: 'json'
                    }).then(function (data) {
                        console.log("gg");
                        if(data == "1") {
                            var msg = "檔案["+ file_name +"]重複，是否覆蓋";
                            if(confirm(msg)) {
                                check =  true;
                            }
                        }
                        else {
                            count++;
                        }
                    });
                }
                else {
                    count++;
                }
            });
        }
        else {
            check = true;
        }
        setTimeout(function() {
            if(check == true || $('input[type=file]').length == count) {
                $("form").submit();
            }
        },300);
    }

    $(document).ready(function() {
        if($('.select2').length > 0) {
            $('.select2').select2({
                theme: "classic",
                tags: true,
                allowClear: true
            });
        }
        if($("ul.select2-selection__rendered").length > 0) {
            $("ul.select2-selection__rendered").sortable({
                containment: 'parent',
                stop: function() {
                    setTimeout(function() {
                        var title = [];
                        $(".select2-selection__choice").each(function() {
                            title.push($(this).clone().children().remove().end().text());
                        });

                        for(var i = title.length - 1; i >=0; i--) {
                            $(".select2 option").filter(function() {
                                return $(this).text() === title[i];
                            }).prependTo("select.select2");
                        }
                    },500);
                }
            });
        }
        if($("#twzipcode").length > 0) {
            $('#twzipcode').twzipcode({
                'countyName': 'Dealer_Address_City',     // 預設為 county
                'districtName': 'Dealer_Address_Township', // 預設為 district
                'zipcodeName': 'zipcode',   // 預設為 zipcode
                'readonly': true,
            });
        }
        if($("#twzipcode2").length > 0) {
            $('#twzipcode2').twzipcode({
                'countyName': 'Dealer_Address_City',     // 預設為 county
                'districtName': 'Dealer_Address_Township', // 預設為 district
                'zipcodeName': 'zipcode',   // 預設為 zipcode
                'readonly': true,
            });
        }
        $('.datepicker').datepicker({
            // locale: {
                format: 'yyyy/m/d'
            // }            
        });

        $('.rangepicker').daterangepicker({
            timePicker: true,
            timePickerIncrement: 5,
            timePicker24Hour: true,
            locale: {
                format: 'YYYY/MM/DD HH:mm',
                applyLabel: "套用",
                cancelLabel: "取消",
                daysOfWeek: ["日", "一", "二", "三", "四", "五", "六"],
                monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
            }
        });
        $('.datetimepicker').daterangepicker({
            // format: 'YYYY-MM-DD HH:mm:ss'
            timePicker: true,
            timePickerIncrement: 10,
            singleDatePicker: true,
            autoUpdateInput: false,
            locale: {
                format: 'YYYY/MM/DD HH:mm',
                applyLabel: "套用",
                cancelLabel: "取消",
                daysOfWeek: ["日", "一", "二", "三", "四", "五", "六"],
                monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
            }
        });
        $('.datetimepicker5').daterangepicker({
            // format: 'YYYY-MM-DD HH:mm:ss'
            timePicker: true,
            timePicker24Hour: true,
            timePickerIncrement: 10,
            singleDatePicker: true,
            autoUpdateInput: false,
            timePickerSeconds:true,
            locale: {
                format: 'YYYY/MM/DD HH:mm:ss',
                applyLabel: "套用",
                cancelLabel: "取消",
                daysOfWeek: ["日", "一", "二", "三", "四", "五", "六"],
                monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
            }
        });
        $('.datetimepicker3').daterangepicker({
            // format: 'YYYY-MM-DD HH:mm:ss'
            timePicker: true,
            timePickerIncrement: 60,
            singleDatePicker: true,
            autoUpdateInput: false,
            locale: {
                format: 'YYYY/MM/DD HH:mm',
                applyLabel: "套用",
                cancelLabel: "取消",
                daysOfWeek: ["日", "一", "二", "三", "四", "五", "六"],
                monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
            }
        });
        $('.datetimepicker4').daterangepicker({
            // format: 'YYYY-MM-DD HH:mm:ss'
            timePicker: false,
            timePickerIncrement: 60,
            singleDatePicker: true,
            autoUpdateInput: false,
            locale: {
                format: 'YYYY/MM/DD HH:mm',
                applyLabel: "套用",
                cancelLabel: "取消",
                daysOfWeek: ["日", "一", "二", "三", "四", "五", "六"],
                monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
            }
        });
        $('.datetimepicker').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('YYYY/MM/DD HH:mm'));
        });

        $('.datetimepicker').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
        $('.datetimepicker2').daterangepicker({
            // format: 'YYYY-MM-DD HH:mm:ss'
            timePicker: true,
            timePickerIncrement: 10,
            singleDatePicker: true,
            autoUpdateInput: false,
            locale: {
                format: 'YYYY/MM/DD HH:mm:ss',
                applyLabel: "套用",
                cancelLabel: "取消",
                daysOfWeek: ["日", "一", "二", "三", "四", "五", "六"],
                monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
            }
        });
        $('.datetimepicker2').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('YYYY/MM/DD HH:mm:ss'));
        });

        $('.datetimepicker2').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
        $('.datetimepicker3').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('YYYY/MM/DD HH:mm:ss'));
        });

        $('.datetimepicker3').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
        $('.datetimepicker4').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('YYYY/MM/DD 23:59:59'));
        });

        $('.datetimepicker4').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
        $('.datetimepicker5').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('YYYY/MM/DD HH:mm:ss'));
        });

        $('.datetimepicker5').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

        $('.daterange').daterangepicker({
            autoUpdateInput: false,
            locale: {
                format: 'YYYY/MM/DD',
                applyLabel: "套用",
                cancelLabel: "取消",
                daysOfWeek: ["日", "一", "二", "三", "四", "五", "六"],
                monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
            }
        });
        $('.datetimepicker_push').daterangepicker({
            // format: 'YYYY-MM-DD HH:mm:ss'
            timePicker: true,
            timePickerIncrement: 1,
            singleDatePicker: true,
            minDate:new Date(),
            timePicker24Hour:true,
            startDate:new Date(),
            locale: {
                format: 'YYYY/MM/DD HH:mm:ss',
                applyLabel: "套用",
                cancelLabel: "取消",
                daysOfWeek: ["日", "一", "二", "三", "四", "五", "六"],
                monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
            }
        });
        $('.daterange').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('YYYY/MM/DD') + ' - ' + picker.endDate.format('YYYY/MM/DD'));
        });
        $('.daterange').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

        $('.ym_datepicker').datepicker( {
           format: "yyyy/mm",
            viewMode: "months", 
            minViewMode: "months"
        });
        $('.rangepicker').on('cancel.daterangepicker', function(ev, picker) {
            //do something, like clearing an input
            $('.rangepicker').val('');
        });
        $('.rangepicker').val('');
        // CKEDITOR.replace('desc');
        if($("textarea[name=editor]").length > 0) {
            CKEDITOR.replace('editor');
        }
        var $select2 = $(".select2_dealer").select2({
            theme: "classic",
            tags: false
        });

        $select2.val('<?=(!empty($select2_json))?$select2_json:""?>').trigger("change");
        $('.select2_ajax_category').select2({
            theme: "classic",
        });
        $('.box-body .select2_category_item').select2({
            theme: "classic",
            tags: true,
            allowClear: true
        });
        // $('.select2_ajax_sub_category').select2({
        //     ajax: {
        //         url: '<?=WEBSITE_URL?>admin/api/category.php',
        //         dataType: 'json'
        //         // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
        //     }
        // });
        if($('.product_category').val() != "" && $('.product_sub_category').val() == "") {
            $('.product_sub_category').prop('disabled', false);
            var product_sub_category = $('.product_sub_category');
            $.ajax({
                type: 'GET',
                url: '<?=WEBSITE_URL?>admin/api/category.php?category=' + $('.product_category').val()
            }).then(function (data) {
                var obj = JSON.parse(data);
                var output = "<option value=''>全部</option>";
                $.each(obj, function(key, val) {
                    output += '<option value="' + val.id + '" data-foo="">' + decodeURI(val.text) + '</option>';
                });
                product_sub_category.html(output);
            });
        }
        $('.product_category').change(function () {
            $('.product_sub_category').prop('disabled', false);
            var product_sub_category = $('.product_sub_category');
            $.ajax({
                type: 'GET',
                url: '<?=WEBSITE_URL?>admin/api/category.php?category=' + $('.product_category').val()
            }).then(function (data) {
                var obj = JSON.parse(data);
                var output = "<option value=''>全部</option>";
                $.each(obj, function(key, val) {
                    output += '<option value="' + val.id + '" data-foo="">' + decodeURI(val.text) + '</option>';
                });
                product_sub_category.html(output);
            });
        });
        <?php
            if(!empty($_GET['ppc']))
                echo "$('#ppc').val('".$_GET['ppc']."');";
        ?>
    });
</script>
<style>
    #twzipcode div,#twzipcode input {
        display: inline-block;
    }
</style>
<?php
    //結束db連線
    if(!empty($db->connection))
        $db->disconnect();
?>
