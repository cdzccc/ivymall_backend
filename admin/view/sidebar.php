<section class="sidebar">
    <!-- Sidebar Menu -->
    <ul class="sidebar-menu" data-widget="tree">
        <li class="header">管理單元</li>
        <!-- Optionally, you can add icons to the links -->
        <li class="treeview">
            <a href="#">
                <i class="fa fa-list-alt"></i>
                <span>系統管理</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="<?=WEBSITE_URL?>admin/admin/list.php"><i class="fa fa-user"></i> <span>管理員管理</span></a></li>
                <li><a href="<?=WEBSITE_URL?>admin/action_log/list.php"><i class="fa fa-user"></i> <span>log管理</span></a></li>
                <li><a href="<?=WEBSITE_URL?>admin/notice_mail/list.php"><i class="fa fa-user"></i> <span>網站通知信管理</span></a></li>
            </ul>
        </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-list-alt"></i>
                <span>網站上稿管理</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="<?=WEBSITE_URL?>admin/banner/list.php"><i class="fa fa-money"></i> <span>首頁 - 主視覺Banner管理</span></a></li>
                <li><a href="<?=WEBSITE_URL?>admin/product_select/list.php"><i class="fa fa-link"></i> <span>首頁 - 總編嚴選管理</span></a></li>
                <li><a href="<?=WEBSITE_URL?>admin/video/list.php"><i class="fa fa-money"></i> <span>首頁 - 影音補給站管理</span></a></li>
                <li><a href="<?=WEBSITE_URL?>admin/news/list.php"><i class="fa fa-money"></i> <span>首頁 - 即時消息管理</span></a></li>
                <li><a href="<?=WEBSITE_URL?>admin/link/list.php"><i class="fa fa-money"></i> <span>首頁 - 好站連結管理</span></a></li>
                <li><a href="<?=WEBSITE_URL?>admin/columnlist/list.php"><i class="fa fa-list-alt"></i>學習專欄分類管理</a></li>
                <li><a href="<?=WEBSITE_URL?>admin/columncontent/list.php"><i class="fa fa-list-alt"></i>學習專欄內容管理</a></li>
                <li><a href="<?=WEBSITE_URL?>admin/newsletter_analytical_list/list.php"><i class="fa fa-list-alt"></i>解析英語電子報管理</a></li>
                <li><a href="<?=WEBSITE_URL?>admin/newsletter_life_list/list.php"><i class="fa fa-list-alt"></i>生活英語電子報管理</a></li>
                <li><a href="<?=WEBSITE_URL?>admin/about_history/list.php"><i class="fa fa-list-alt"></i>關於我們 - 企業沿革管理</a></li>
                <li><a href="<?=WEBSITE_URL?>admin/about_media/list.php"><i class="fa fa-list-alt"></i>關於我們 - 媒體報導管理</a></li>
                <li><a href="<?=WEBSITE_URL?>admin/faq_category/list.php"><i class="fa fa-list-alt"></i>客服中心 - 常見問題分類管理</a></li>
                <li><a href="<?=WEBSITE_URL?>admin/faq_content/list.php"><i class="fa fa-list-alt"></i>客服中心 - 常見問題內容管理</a></li>
                <li><a href="<?=WEBSITE_URL?>admin/keyword/list.php"><i class="fa fa-money"></i> <span>搜尋關鍵字管理</span></a></li>
                <li><a href="<?=WEBSITE_URL?>admin/other_link/list.php"><i class="fa fa-link"></i> <span>其他設定管理</span></a></li>
                <li><a href="<?=WEBSITE_URL?>admin/customer_event/list.php"><i class="fa fa-list-alt"></i>會員活動管理</a></li>
            </ul>
        </li>

        <li class="treeview">
            <a href="#">
                <i class="fa fa-list-alt"></i>
                <span>信件管理</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="<?=WEBSITE_URL?>admin/email_info/list.php"><i class="fa fa-list-alt"></i>收信人管理</a></li>
                <li><a href="<?=WEBSITE_URL?>admin/email_contact_us/list.php"><i class="fa fa-list-alt"></i>聯絡我們收信管理</a></li>
                <li><a href="<?=WEBSITE_URL?>admin/email_business/list.php"><i class="fa fa-list-alt"></i>企業服務信箱管理</a></li>
                <li><a href="<?=WEBSITE_URL?>admin/email_ad/list.php"><i class="fa fa-list-alt"></i>廣告刊登信箱管理</a></li>
                <li><a href="<?=WEBSITE_URL?>admin/email_group_buy/list.php"><i class="fa fa-list-alt"></i>企業團購信箱管理</a></li>
            </ul>
        </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-list-alt"></i>
                <span>會員相關管理</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="<?=WEBSITE_URL?>admin/customer/list.php"><i class="fa fa-users"></i> <span>會員管理</span></a></li>
                <li><a href="<?=WEBSITE_URL?>admin/point/trade.php"><i class="fa fa-list-alt"></i>熊贈點加點/扣點</a></li>
                <li><a href="<?=WEBSITE_URL?>admin/point/list.php"><i class="fa fa-list-alt"></i>熊贈點紀錄管理</a></li>
                <li><a href="<?=WEBSITE_URL?>admin/ticket/list.php"><i class="fa fa-list-alt"></i>折價券上稿管理</a></li>
                <li><a href="<?=WEBSITE_URL?>admin/sms_send/list.php"><i class="fa fa-link"></i> <span>簡訊發送管理</span></a></li>
            </ul>
        </li>

        <li class="treeview">
            <a href="#">
                <i class="fa fa-list-alt"></i>
                <span>商品管理</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="<?=WEBSITE_URL?>admin/category/list.php"><i class="fa fa-link"></i> <span>第一層分類Banner管理</span></a></li>
                <li><a href="<?=WEBSITE_URL?>admin/sub_category/list.php"><i class="fa fa-link"></i> <span>第二層分類管理</span></a></li>
                <li><a href="<?=WEBSITE_URL?>admin/product/list.php"><i class="fa fa-link"></i> <span>單本／套書商品上稿管理</span></a></li>
                <li><a href="<?=WEBSITE_URL?>admin/magazine/list.php"><i class="fa fa-link"></i> <span>雜誌商品上稿管理</span></a></li>
                <li><a href="<?=WEBSITE_URL?>admin/gift/list.php"><i class="fa fa-link"></i> <span>贈品上稿管理</span></a></li>
                <li><a href="<?=WEBSITE_URL?>admin/product_rank/list.php"><i class="fa fa-link"></i> <span>排行榜分類及上稿管理</span></a></li>
                <li><a href="<?=WEBSITE_URL?>admin/product_additional/list.php"><i class="fa fa-link"></i> <span>加購活動上稿管理</span></a></li>
                <li><a href="<?=WEBSITE_URL?>admin/product_event/list.php"><i class="fa fa-link"></i> <span>行銷活動設定管理</span></a></li>
                <li><a href="<?=WEBSITE_URL?>admin/delivery_price/list.php"><i class="fa fa-link"></i> <span>運費設定管理</span></a></li>
            </ul>
        </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-list-alt"></i>
                <span>訂單相關管理</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="<?=WEBSITE_URL?>admin/orders/list.php"><i class="fa fa-link"></i> <span>訂單管理</span></a></li>
                <li><a href="<?=WEBSITE_URL?>admin/store/list.php"><i class="fa fa-link"></i> <span>7-11超商訂單查詢</span></a></li>
                <li><a href="<?=WEBSITE_URL?>admin/store_cvs/list.php"><i class="fa fa-link"></i> <span>便利達康超商訂單查詢</span></a></li>
                <li><a href="<?=WEBSITE_URL?>admin/product_arrived/list.php"><i class="fa fa-link"></i> <span>貨到通知管理</span></a></li>
            </ul>
        </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-list-alt"></i>
                <span>網站其他管理</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li><a href="<?=WEBSITE_URL?>admin/sales_data/list.php"><i class="fa fa-link"></i> <span>站內銷售數據統計</span></a></li>
                <li><a href="<?=WEBSITE_URL?>admin/report/list.php"><i class="fa fa-link"></i> <span>財務報表</span></a></li>
            </ul>
        </li>
    </ul>
    <!-- /.sidebar-menu -->
</section>
