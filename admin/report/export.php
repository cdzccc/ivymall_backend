<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("sales_data");
require DOCUMENT_ROOT.'/vendor/autoload.php';
// use PhpOffice\phpexcel\PHPExcel;
// use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
require_once DOCUMENT_ROOT.'/class/Classes/PHPExcel.php';

$filename = "【常春藤英語購物網站】財務報表 - ".date("Y-m-d");
$table    = curl(WEBSITE_URL."admin/report/report.php","get", http_build_query($_GET))["result"];
// save $table inside temporary file that will be deleted later
// $tmpfile = tempnam("./", 'html');
// file_put_contents($tmpfile, $table);

// // insert $table into $objPHPExcel's Active Sheet through $excelHTMLReader
// $objPHPExcel     = new PHPExcel();
// $excelHTMLReader = PHPExcel_IOFactory::createReader('HTML');
// $excelHTMLReader->loadIntoExisting($tmpfile, $objPHPExcel);
// $objPHPExcel->getActiveSheet()->setTitle(date('Y-m-d')); // Change sheet's title if you want

// unlink($tmpfile); // delete temporary file because it isn't needed anymore

// $writer = new PHPExcel_Writer_Excel5($objPHPExcel);
// // $writer->save('php://output');

// $writer->save("./【常春藤英語購物網站】財務報表.xls");

// header("location:./【常春藤英語購物網站】財務報表.html");
header('Content-type:application/force-download'); 
header('Content-Transfer-Encoding: Binary'); 
header('Content-Disposition:attachment;filename='.$filename.'.html'); //檔名
echo $table;
?>