<?php
include("../config.php");
// ClassJscript::islogin();
// ClassJscript::isadmino("sales_data");

$sql_where = "";
$delivery = "";
$payment = "";
$order_status = [];
$s_date = "";
if(!empty($_GET['payment']) && $_GET['payment'] != 'all') {
    $sql_where .= " and o.Payment = '".$_GET["payment"]."'";
    $payment = $ARRall['payment'][$_GET['payment']];
}
else {
	$payment = "不限";
}
if(!empty($_GET['delivery']) && $_GET['delivery'] != 'all') {
    $sql_where .= " and o.delivery = '".$_GET["delivery"]."'";
    $delivery = $ARRall['delivery'][$_GET['delivery']];
}
else {
	$delivery = "不限";
}

if(!empty($_GET['order_status']) && $_GET['order_status'] != 'all') {
    $sql_where .= " and o.Status in ('".implode("','", $_GET["order_status"])."')";
    foreach ($_GET['order_status'] as $value) {
        $order_status[] = $ARRall['order_status'][$value];
    }
}
if(!empty($_GET['s_date']) && !empty($_GET['e_date'])) {
    $sql_where .= " and o.create_datetime between '".$_GET['s_date']." 00:00:00' and '".$_GET['e_date']." 23:59:59'";
    $s_date = $_GET['s_date']." - ".$_GET['e_date'];
}

$total_price = 0;
$total_order = 0;
$Delivery_Price = 0;
$data_price_1 = 0;
$data_price_2 = 0;
$data_price_3 = 0;
$data = [];
$discount = [
	"discount_ticket"   => 0,
	"discount_customer" => 0,
	"discount_purchase" => 0,
	"discount_off"      => 0,
];
$all_order = [];
$sql_cmd = "SELECT 
        sum(oi.Goods_Qty) as Goods_Qty,
        sum(oi.Total_Price) as total_price,
        g.type,
        g.Goods_Name,
        oi.Goods_Price,
        o.Order_ID,
        o.discount_ticket,
        o.discount_customer,
        o.discount_purchase,
        o.discount_off,
        g.item_number,
        g.Goods_ID,
        o.Delivery_Price,
        g.item_group,
        oi.Goods_Discount_Price,
        c.Category_Name
    from `order` as o 
    join `order_item` as oi
        on o.Order_ID = oi.Order_ID
    join goods as g 
        on g.Goods_ID = oi.goodsId
    join category as c
    	on c.Category_Code = g.sub_category
    where o.deleted_at is null ".$sql_where." group by g.Goods_ID, oi.Goods_Price order by g.item_number asc";
$rs_member = $db->query($sql_cmd);

while($row = $rs_member->fetchRow(MDB2_FETCHMODE_ASSOC)) {
	$total_price += $row["total_price"];
	$Delivery_Price += $row["Delivery_Price"];
	$total_order ++;
	if(!empty($row['discount_ticket'])) {
		$discount["discount_ticket"] += $row['discount_ticket'];
	}
	if(!empty($row['discount_customer'])) {
		$discount["discount_customer"] += $row['discount_customer'];
	}
	if(!empty($row['discount_purchase'])) {
		$discount["discount_purchase"] += $row['discount_purchase'];
	}
	if(!empty($row['discount_off'])) {
		$discount["discount_off"] += $row['discount_off'];
	}
	$Order_ID = [];
	$sql_cmd = "SELECT 
	        o.Order_ID
	    from `order` as o 
	    join `order_item` as oi
	        on o.Order_ID = oi.Order_ID
	    join goods as g 
	        on g.Goods_ID = oi.goodsId
	    where o.deleted_at is null and g.Goods_ID = '".$row['Goods_ID']."' and oi.Goods_Price = '".$row['Goods_Price']."' order by o.create_datetime asc";
	$rs_order = $db->query($sql_cmd);
	while($row_order = $rs_order->fetchRow(MDB2_FETCHMODE_ASSOC)) {
		// 取得商品訂單
		if(empty($Order_ID[$row_order['Order_ID']]))
			$Order_ID[$row_order['Order_ID']] = $row_order['Order_ID'];
		// 取得所以訂單
		if(empty($all_order[$row_order['Order_ID']]))
			$all_order[$row_order['Order_ID']] = $row_order['Order_ID'];
	}
	if($row['type'] == 1) {
		// 一般書籍
		$data[1][] = [
			"Order_ID"    => implode(", ", $Order_ID) ,
			"item_number" => $row["item_number"],
			"item_group"  => ($row['type'] == 2)?".":"",
			"name"        => $row["Goods_Name"],
			"num"         => $row["Goods_Qty"],
			"price"       => $row["Goods_Discount_Price"],
			"total_price" => $row["total_price"],
		];
		$data_price_1 += ($row["Goods_Price"]*$row["Goods_Qty"]);
	}
	else if($row['type'] == 2) {
		// 套書
		$data[1][] = [
			"Order_ID"    => implode(", ", $Order_ID) ,
			"item_number" => $row["item_number"],
			"item_group"  => ($row['type'] == 2)?"●":"",
			"name"        => $row["Goods_Name"],
			"num"         => $row["Goods_Qty"],
			"price"       => $row["Goods_Discount_Price"],
			"total_price" => $row["total_price"],
		];
		$item_group = explode(",", $row['item_group']);
		$sql_cmd = "select * from goods where Goods_ID in ('".implode("','", $item_group)."')";
		$rs_group = $db->query($sql_cmd);
		while ($row_group = $rs_group->fetchRow(MDB2_FETCHMODE_ASSOC)) {
			$data[1][] = [
				"Order_ID"    => "",
				"item_number" => "",
				"item_group"  => $row_group["item_number"],
				"name"        => $row_group["Goods_Name"],
				"num"         => $row["Goods_Qty"],
				"price"       => 0,
				"total_price" => "",
			];
		}
	}
	else {
		// 雜誌
		if($row['Category_Name'] == "解析英語") {
			$data[2][] = [
				"Order_ID"    => implode(", ", $Order_ID) ,
				"item_number" => $row["item_number"],
				"item_group"  => ($row['type'] == 2)?".":"",
				"name"        => $row["Goods_Name"],
				"num"         => $row["Goods_Qty"],
				"price"       => $row["Goods_Discount_Price"],
				"total_price" => $row["total_price"],
			];
			$sql_cmd = "SELECT 
	                        sum(oi.Goods_Qty) as Goods_Qty,
	                        oi.Goods_Name,
	                        oi.Goods_ID
	                    from `order` as o 
	                    join `order_item` as oi
	                        on o.Order_ID = oi.Order_ID
					    where o.deleted_at is null and oi.goodsId = '".$row['Goods_ID']."' and oi.Goods_Price = '".$row['Goods_Price']."'
					    group by oi.Goods_Item_ID";
			$rs_group = $db->query($sql_cmd);
			while ($row_group = $rs_group->fetchRow(MDB2_FETCHMODE_ASSOC)) {
				$data[2][] = [
					"Order_ID"    => "",
					"item_number" => "",
					"item_group"  => $row_group["Goods_ID"],
					"name"        => $row_group["Goods_Name"],
					"num"         => $row_group["Goods_Qty"],
					"price"       => 0,
					"total_price" => "",
				];
			}
			$data_price_2 += ($row["Goods_Price"]*$row["Goods_Qty"]);
		}
		else {
			if(empty($data[3])) {
				$data[3][] = [
					"Order_ID"    => "生活英語雜誌",
					"item_number" => "",
					"item_group"  => "",
					"name"        => "",
					"num"         => "",
					"price"       => "",
					"total_price" => "",
				];
			}
			$data[3][] = [
				"Order_ID"    => implode(", ", $Order_ID) ,
				"item_number" => $row["item_number"],
				"item_group"  => ($row['type'] == 2)?".":"",
				"name"        => $row["Goods_Name"],
				"num"         => $row["Goods_Qty"],
				"price"       => $row["Goods_Discount_Price"],
				"total_price" => $row["total_price"],
			];
			$sql_cmd = "SELECT 
	                        sum(oi.Goods_Qty) as Goods_Qty,
	                        oi.Goods_Name,
	                        oi.Goods_ID
	                    from `order` as o 
	                    join `order_item` as oi
	                        on o.Order_ID = oi.Order_ID
					    where o.deleted_at is null and oi.goodsId = '".$row['Goods_ID']."' and oi.Goods_Price = '".$row['Goods_Price']."'
					    group by oi.Goods_Item_ID";
			$rs_group = $db->query($sql_cmd);
			while ($row_group = $rs_group->fetchRow(MDB2_FETCHMODE_ASSOC)) {
				$data[3][] = [
					"Order_ID"    => "",
					"item_number" => "",
					"item_group"  => $row_group["Goods_ID"],
					"name"        => $row_group["Goods_Name"],
					"num"         => $row_group["Goods_Qty"],
					"price"       => 0,
					"total_price" => "",
				];
			}
			$data_price_3 += ($row["Goods_Price"]*$row["Goods_Qty"]);
		}
	}
}
$data[1][] = [
				"Order_ID"    => "小計",
				"item_number" => "",
				"item_group"  => "",
				"name"        => "",
				"num"         => "",
				"price"       => "",
				"total_price" => $data_price_1,
			];
$data[2][] = [
				"Order_ID"    => "小計",
				"item_number" => "",
				"item_group"  => "",
				"name"        => "",
				"num"         => "",
				"price"       => "",
				"total_price" => $data_price_2,
			];
$data[3][] = [
				"Order_ID"    => "小計",
				"item_number" => "",
				"item_group"  => "",
				"name"        => "",
				"num"         => "",
				"price"       => "",
				"total_price" => $data_price_3,
			];
$data[2] = array_merge($data[2], $data[3]);
$sql_cmd = "SELECT 
        sum(oi.Goods_Qty) as Goods_Qty,
        sum(o.total_price) as total_price,
        count(o.Order_ID) as total_order,
        g.name,
        o.Order_ID,
        g.item_number
    from `order` as o 
    join `order_item` as oi
        on o.Order_ID = oi.Order_ID
    join gift as g 
        on g.item_number = oi.gift_item_number
    where o.deleted_at is null ".$sql_where." group by g.item_number order by g.item_number asc";
$rs_member = $db->query($sql_cmd);
if($rs_member->numRows() > 0) {
	$data[2][] = [
					"Order_ID"    => "非書類／贈品",
					"item_number" => "",
					"item_group"  => "",
					"name"        => "",
					"num"         => "",
					"price"       => "",
					"total_price" => "",
				];
}
while($row = $rs_member->fetchRow(MDB2_FETCHMODE_ASSOC)) {
	$total_price += $row["total_price"];
	$total_order ++;
	$data[2][] = [
			"Order_ID"    => $row["Order_ID"],
			"item_number" => $row["item_number"],
			"item_group"  => "",
			"name"        => $row["name"],
			"num"         => $row["Goods_Qty"],
			"price"       => 0,
			"total_price" => 0,
		];
}
$data[2][] = [
				"Order_ID"    => "小計",
				"item_number" => "",
				"item_group"  => "",
				"name"        => "",
				"num"         => "",
				"price"       => "",
				"total_price" => 0,
			];
$count_1 = count($data[1]);
$count_2 = count($data[2]);
$count = $count_1;
if($count_1 < $count_2)
	$count = $count_2;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<!-- saved from url=(0038)https://www.ivy.com.tw/admin/index.php -->
<html lang="zh-tw" class="gr__ivy_com_tw">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body data-gr-c-s-loaded="true">

	<table border="1" cellspacing="0" cellpadding="0" bordercolor="#939393" whdth="1000">
		<tbody>
			<tr>
				<td>&nbsp;</td>
				<td colspan="7" class="tc" align="center">客服組出貨單</td>
				<td colspan="4" class="bl tc" align="center">【 ◎ 】網路書店</td>
				<td colspan="3" class="tc">製表日期：<?=date('Y/m/d')?></td>
			</tr>
			<tr>
				<td colspan="8">&nbsp;</td>
				<td class="bl">金額：</td>
				<td colspan="2" class="nl">NT $ <?=number_format($total_price)?></td>
				<td class="nl tc">訂單份數：</td>
				<td colspan="3" class="nl tr"><?=count($all_order)?> 份</td>
			</tr>

			<tr>
				<td rowspan="2">&nbsp;</td>
				<td colspan="7" class="tc" align="center"><?=$payment?> - <?=$delivery?></td>
				<td colspan="7" class="bl">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="7" class="tc" align="center"><?=$s_date?>訂單</td>
				<td colspan="7" class="bl">&nbsp;</td>
			</tr>

			<tr>
				<td colspan="8" class="bb">&nbsp;</td>
				<td colspan="7" class="bl bb">&nbsp;</td>
			</tr>

			<tr>
				<td colspan="8" class="bb tc" align="center">有聲+智藤</td>
				<td colspan="7" class="bl bb tc" align="center">解析</td>
			</tr>
			<tr>
				<td width="7%" class="bb tc">訂單編號</td>
				<td width="7%" class="bb tc">書碼</td>
				<td width="5%" class="bb tc">套書</td>
				<td colspan="2" width="26%" class="bb tc">書名</td>
				<td width="6%" class="bb tc">份數</td>
				<td width="5%" class="bb tc">價格</td>
				<td width="6%" class="bb tc">合計</td>
				<td width="7%" class="bb tc">訂單編號</td>
				<td width="9%" class="bb tc">書碼</td>
				<td width="5%" class="bb tc">套書</td>
				<td width="13%" class="bb tc">書名</td>
				<td width="7%" class="bb tc">份數</td>
				<td width="5%" class="bb tc">價格</td>
				<td width="6%" class="bb tc">合計</td>
			</tr>
			<?php
				for($i = 0;$i<= $count; $i++) {
			?>
			<tr>
				<td class=""><?=(!empty($data[1][$i]['Order_ID']))?$data[1][$i]['Order_ID']:""?></td>
				<td class=""><?=(!empty($data[1][$i]['item_number']))?$data[1][$i]['item_number']:""?></td>
				<td class="tc"><?=(!empty($data[1][$i]['item_group']))?$data[1][$i]['item_group']:""?></td>
				<td colspan="2" class=""><?=(!empty($data[1][$i]['name']))?$data[1][$i]['name']:""?></td>
				<td class="tr"><?=(!empty($data[1][$i]['num']))?$data[1][$i]['num']:""?></td>
				<td class="tr"><?=(!empty($data[1][$i]['price']))?"$".number_format($data[1][$i]['price']):""?></td>
				<td class="tr"><?=(!empty($data[1][$i]['total_price']))?"$".number_format($data[1][$i]['total_price']):""?></td>

				<td class="bl"><?=(!empty($data[2][$i]['Order_ID']))?$data[2][$i]['Order_ID']:""?></td>
				<td class="bl"><?=(!empty($data[2][$i]['item_number']))?$data[2][$i]['item_number']:""?></td>
				<td class="tc"><?=(!empty($data[2][$i]['item_group']))?$data[2][$i]['item_group']:""?></td>
				<td class=""><?=(!empty($data[2][$i]['name']))?$data[2][$i]['name']:""?></td>
				<td class="tr"><?=(!empty($data[2][$i]['num']))?$data[2][$i]['num']:""?></td>
				<td class="tr"><?=(!empty($data[2][$i]['price']))?number_format($data[2][$i]['price']):""?></td>
				<td class="tr"><?=(!empty($data[2][$i]['total_price']))?"$".number_format($data[2][$i]['total_price']):""?></td>
			</tr>
			<?php
				}
			?>

			<tr>
				<td colspan="13" class="bb tc" align="center">費用</td>
			</tr>

			<tr>
					<td>運費</td>
					<td class="tr">$<?=number_format($Delivery_Price)?></td>
					<td>&nbsp;</td>
					<td colspan="2">&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td colspan="2">折讓-折價券折抵</td>
					<td class="tr">-$<?=number_format($discount['discount_ticket'])?></td>
				</tr>
			<tr>
					<td>&nbsp;</td>
					<td class="tr">&nbsp;</td>
					<td>&nbsp;</td>
					<td colspan="2">&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td colspan="2">折讓-活動折扣</td>
					<td class="tr">-$<?=number_format($discount['discount_customer'])?></td>
				</tr>
			<tr>
					<td>&nbsp;</td>
					<td class="tr">&nbsp;</td>
					<td>&nbsp;</td>
					<td colspan="2">&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td colspan="2">折讓-滿額折扣</td>
					<td class="tr">-$<?=number_format($discount['discount_purchase'])?></td>
			</tr>
			<tr>
					<td>&nbsp;</td>
					<td class="tr">&nbsp;</td>
					<td>&nbsp;</td>
					<td colspan="2">&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td colspan="2">折讓-滿額折抵</td>
					<td class="tr">-$<?=number_format($discount['discount_off'])?></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>

				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td colspan="2" class="tc">實收金額</td>
				<td class="tr">$<?=number_format($total_price)?></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td colspan="2">&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>

				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td colspan="2" class="bb tc">小計</td>
				<td class="bb tr">$<?=number_format($total_price)?></td>
			</tr>

			<tr>
				<td colspan="3" class="bb tc" align="center">客服組簽章</td>
				<td colspan="3" class="bb bl tc" align="center">客服組主管簽章</td>
				<td colspan="9" class="bb bl tc">訂單編號</td>
			</tr>

			<tr>
				<td colspan="3" class="bb"><div>&nbsp;</div><div>&nbsp;</div></td>
				<td colspan="3" class="bb bl"><div>&nbsp;</div><div>&nbsp;</div></td>
				<td colspan="9" rowspan="3" valign="top" class="bb bl"><?=implode(", ", $all_order)?></td>
			</tr>

			<tr>
				<td colspan="6" class="bb tc" align="center">備註欄</td>
			</tr>

			<tr>
				<td colspan="6" class="bb"><div>&nbsp;</div><div>&nbsp;</div></td>
			</tr>

		</tbody>
	</table>
</body>
</html>