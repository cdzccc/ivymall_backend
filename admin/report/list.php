<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("sales_data");

$sql_cmd = "select * from goods where 
    Status = 1 and deleted_at is null
    order by Goods_ID asc";
$rs = $db->query($sql_cmd);
while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
    $arr_goods[] = $row;
}

?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php") ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                財務報表
                
            </h1>
        </section>
        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box">
                <div class="box-header">
                    <form action="./list.php" method="GET" name="search">
                        <div style="float:left; display:inline-block; width:250px; padding-right:15px; padding-bottom:10px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">日期區間</button>
                                </div>
                                <input type="text" name="s_date" class="form-control input-sm datetimepicker2"
                                       placeholder="起始日" value="<?=(!empty($_GET['s_date']))?$_GET['s_date']:""?>">
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:250px; padding-right:15px; padding-bottom:10px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">~</button>
                                </div>
                                <input type="text" name="e_date" class="form-control input-sm datetimepicker2"
                                       placeholder="結束日" value="<?=(!empty($_GET['e_date']))?$_GET['e_date']:""?>">
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:250px; padding-right:15px; padding-bottom:10px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">配送方式</button>
                                </div>
                                <select class="form-control input-sm" name="delivery">
                                    <option value="all">全部</option>
                                    <?php
                                        foreach($ARRall['delivery'] as $key => $value) {
                                            echo "<option value='".$key."'>".$value."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:250px; padding-right:15px; padding-bottom:10px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">付款方式</button>
                                </div>
                                <select class="form-control input-sm" name="payment">
                                    <option value="all">全部</option>
                                    <?php
                                        foreach($ARRall['payment'] as $key => $value) {
                                            echo "<option value='".$key."'>".$value."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div style="float:left; padding-right:15px; padding-right:15px; padding-bottom:10px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">訂單狀態</button>
                                </div>
                                <div style="padding-left: 35px;">
                                <label class='checkbox' style='display: inline-block;width: 200px;'><input type='checkbox' name='' value=''>不限</label>
                                <?php
                                    foreach($ARRall['order_status'] as $key => $value) {
                                        echo "<label class='checkbox' style='display: inline-block;width: 200px;'><input type='checkbox' name='order_status[]' value='".$key."'>".$value."</label>";
                                    }
                                ?>
                                </div>
                            </div>
                        </div>
                        <a class="btn btn-danger btn-sm" href="./list.php">清除</a>
                        <button type="button" class="btn btn-success btn-sm" onclick="search.action = 'export.php';search.submit();">匯出csv</button>
                    </form>
                </div>
                <div class="box-body no-padding">
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>