<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("action_log_edit");
$id = filter_input(INPUT_POST, 'id');
$send_mail = false;

$reply      = filter_input(INPUT_POST, 'editor');
if(empty($reply)) {
    $reply      = filter_input(INPUT_POST, 'reply');
}
else {
  $send_mail = true;
}
$remark      = filter_input(INPUT_POST, 'remark');
$status    = filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 2, 'default' => 0)));
if($status == 0)
  $status = 1;

if(empty($id)) {
    js_go_back_global("NOT_POST");
    exit;
}

if(($status == 2 && empty($reply))) {
    js_go_back_global("DATA_EMPTY");
    exit;
}


$update_datetime = date("Y-m-d H:i:s");
$update_user     = $_SESSION[SESSION_VARIABLE."_user_id"];

$sql_array = array(
    "reply"           => array("2", checkinput_sql($reply, 9999999)),
    "remark"          => array("2", checkinput_sql($remark, 500)),
    "status"          => array("2", checkinput_sql($status,2)),
    "update_datetime" => array("2", checkinput_sql($update_datetime, 50)),
    "update_user"     => array("2", checkinput_sql($update_user, 50)),
);
$sql_cmd = update("mail_content", array("id", $id), $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
   js_go_back_global("DB_EDIT_ERROR");
   exit;
}else{
  if($send_mail) {
    $datas = [
        "title"   => "廣告刊登回覆",
        "type"    => "1",
        "content" => $reply,
        "mail"    => $email,
    ];
    ClassMail::send_mail($datas);
  }
    add_log('log管理','2');
   $db->disconnect();
    $referer = filter_input(INPUT_POST, 'referer');
    $redirect = "./list.php";
    if(!empty($referer))
        $redirect = $referer;
    js_repl_global( $redirect, "EDIT_SUCCESS");
   exit;
}
?>
