<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("action_log");

if(isset($_GET['action']))
    $action = $_GET['action'];
else
    $action = "add";

if($action == "edit") {
    $sql_cmd = "select * from mail_content where deleted_at is null and id = '".checkinput_sql($_GET['id'],19)."'";
    $rs = $db->query($sql_cmd);
    $row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
    $user_name = "";
    if($row['status'] > 0) {
        $sql_cmd = "select name from user where id = '".$row['update_user']."'";
        $rs = $db->query($sql_cmd);
        $row_user = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
        $user_name = $row_user['name'];
    }
}

?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php") ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                廣告刊登信箱管理
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" method="POST" action="./<?=$action?>.php" enctype="multipart/form-data">
                    <div class="box-header">
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">聯絡人</label>
                            <div class="col-sm-10">
                                <?=$row["name"]?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">公司名稱</label>
                            <div class="col-sm-10">
                                <?=$row["company"]?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">職稱</label>
                            <div class="col-sm-10">
                                <?=$row["job_title"]?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label">email</label>
                            <div class="col-sm-10">
                                <?=$row["email"]?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label">會員ID</label>
                            <div class="col-sm-10">
                                <?=$row["Customer_ID"]?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label">聯絡電話</label>
                            <div class="col-sm-10">
                                <?=$row["phone"]?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="alt" class="col-sm-2 control-label">詢問事項</label>
                            <div class="col-sm-10">
                                <?=$ARRall['mail_question'][$row["question"]]?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="alt" class="col-sm-2 control-label">意見內容</label>
                            <div class="col-sm-10">
                                <?=$row["content"]?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="alt" class="col-sm-2 control-label">客服回覆</label>
                            <div class="col-sm-10">
                                <?php
                                    if(!empty($row["reply"])) {
                                        echo $row["reply"];
                                        echo "<input name='reply' type='hidden' value='".$row["reply"]."'>";
                                    }
                                    else {
                                ?>
                                <textarea class="editor" name="editor"><?=$row["reply"]?></textarea>
                                <?php
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">處理狀況</label>
                            <div class="col-sm-10">
                                <?php
                                    if($row['status'] == 2) {
                                        echo "已處理";
                                        echo "<input name='status' type='hidden' value='".$row["status"]."'>";
                                    }
                                    else {
                                ?>
                                    <label>
                                        <input name="status" type="radio" <?=($row['status'] == 0)?"checked":""?>
                                           value="0">未處理
                                    </label>
                                    <label>
                                        <input name="status" type="radio" <?=($row['status'] == 1)?"checked":""?>
                                           value="1">追蹤中
                                    </label>
                                    <label>
                                        <input name="status" type="radio" <?=($row['status'] == 2)?"checked":""?>
                                           value="2">已處理
                                    </label>
                                <?php
                                    }
                                ?>

                                </div>
                        </div>
                        <div class="form-group">
                            <label for="alt" class="col-sm-2 control-label">處理人員</label>
                            <div class="col-sm-10">
                                <?=$user_name?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="alt" class="col-sm-2 control-label">備註</label>
                            <div class="col-sm-10">
                                <textarea name="remark" class="form-control"><?=$row['remark']?></textarea>
                            </div>
                        </div>
                        <?php if($action == "edit") {
                        ?>
                        <div class="form-group">
                            <label for="alt" class="col-sm-2 control-label">建立時間</label>
                            <div class="col-sm-10">
                                <?=$row["create_datetime"]?>
                            </div>
                        </div>
                        <?php
                        }
                        ?>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-right">
                        <input type="hidden" name="id" value="<?=$row['id']?>">
                        <a href="javascript:history.back()" class="btn btn-danger" style="margin-right: 10px;">取消，並返回上一層</a>
                        <button type="submit" class="btn btn-primary">儲存</button>
                        <input type="hidden" name="referer" value="<?=(!empty($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:"")?>">
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>
<script>
    $(document).ready(function() {
        $(document).on('submit','form',function(){
            if($('.editor').val() != "" && $('.editor').val() != undefined && !confirm("客服回覆送出後無法修改，是否確定送出？")) {
                return false;
            }
        });
    })
</script>
</body>
</html>