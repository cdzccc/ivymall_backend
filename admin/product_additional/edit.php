<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("additional_edit");
$id = filter_input(INPUT_POST, 'id');

$sdate   = filter_input(INPUT_POST, 'sdate');
$edate  = filter_input(INPUT_POST, 'edate');
$option  = filter_input(INPUT_POST, 'option');
$status = filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 1, 'default' => 0)));

if(empty($id)) {
    js_go_back_global("NOT_POST");
    exit;
}

$update_datetime = date("Y-m-d H:i:s");
$update_user     = $_SESSION[SESSION_VARIABLE."_user_id"];

$sql_array = array(
    "option"          => array("2", checkinput_sql($option, 2)),
    "sdate"           => array("2", checkinput_sql($sdate, 20)),
    "edate"           => array("2", checkinput_sql($edate , 20)),
    "status"          => array("2", checkinput_sql($status,2)),
    "update_datetime" => array("2", checkinput_sql($update_datetime, 50)),
    "update_user"     => array("2", checkinput_sql($update_user, 50)),
);
$sql_cmd = update("goods_additional", array("id", $id), $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
   js_go_back_global("DB_EDIT_ERROR");
   exit;
}else{
    add_log('加購活動上稿管理','2');
   $db->disconnect();
    $referer = filter_input(INPUT_POST, 'referer');
    $redirect = "./list.php";
    if(!empty($referer))
        $redirect = $referer;
    js_repl_global( $redirect, "EDIT_SUCCESS");
   exit;
}
?>
