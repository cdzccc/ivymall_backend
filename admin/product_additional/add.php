<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("additional_add");
$id = get_id();
$sdate   = filter_input(INPUT_POST, 'sdate');
$edate  = filter_input(INPUT_POST, 'edate');
$option  = filter_input(INPUT_POST, 'option');
$status = filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 1, 'default' => 0)));

$create_datetime = date("Y-m-d H:i:s");
$create_user     = $_SESSION[SESSION_VARIABLE."_user_id"];
$update_datetime = date("Y-m-d H:i:s");
$update_user     = $_SESSION[SESSION_VARIABLE."_user_id"];

$sql_array = array(
    "id"              => array("2", checkinput_sql($id, 19)),
    "option"          => array("2", checkinput_sql($option, 2)),
    "sdate"           => array("2", checkinput_sql($sdate, 20)),
    "edate"           => array("2", checkinput_sql($edate , 20)),
    "status"          => array("2", checkinput_sql($status,2)),
    "create_datetime" => array("2", checkinput_sql($create_datetime, 50)),
    "create_user"     => array("2", checkinput_sql($create_user, 50)),
    "update_datetime" => array("2", checkinput_sql($update_datetime, 50)),
    "update_user"     => array("2", checkinput_sql($update_user, 50)),
);

$sql_cmd = insert("goods_additional", $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
   js_go_back_global("DB_ADD_ERROR");
   exit;
}else{
    add_log('加購活動上稿管理','1');
   $db->disconnect();
   js_repl_global( "./list.php", "ADD_SUCCESS");
   exit;
}
?>
