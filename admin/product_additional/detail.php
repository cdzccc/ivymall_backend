<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("additional");

if(isset($_GET['action']))
    $action = $_GET['action'];
else
    $action = "add";

$row["id"] = "";
$row["sdate"] = "";
$row["edate"] = "";
$row["status"] = "1";
$row['item_number'] = "";
$option = [];
if($action == "edit") {
    $sql_cmd = "select * from goods_additional where deleted_at is null and id = '".checkinput_sql($_GET['id'],19)."'";
    $rs = $db->query($sql_cmd);
    $row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
}
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php") ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                加購活動上稿管理
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" method="POST" action="./<?=$action?>.php" enctype="multipart/form-data">
                    <div class="box-header">
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="type" class="col-sm-2 control-label"><a style="color:red">*</a>活動館別</label>
                            <div class="col-sm-10">
                                <select name="option" class="form-control" style="width:200px">
                                    <option value="0">全部</option>
                                    <?php
                                        foreach($ARRall['goods_category'] as $key => $goods_category) {
                                            $selected = "";
                                            if($key == $row['option'])
                                                $selected = "selected";
                                            echo "<option value='".$key."' ".$selected.">".$goods_category."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">上架日期</label>
                            <div class="col-sm-10">
                                <div class="input-group">
                                    <input class="datetimepicker" name="sdate" type="text" class="form-control pull-right rangepicker" value="<?=$row['sdate']?>">
                                     - 
                                    <input class="datetimepicker" name="edate" type="text" class="form-control pull-right rangepicker" value="<?=$row['edate']?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">狀態</label>
                            <div class="col-sm-10 control-text">
                                <label>
                                    <input name="status" type="radio" <?=($row['status'] == 1)?"checked":""?>
                                       value="1">上架
                                </label>
                                <label>
                                    <input name="status" type="radio" <?=($row['status'] == 0)?"checked":""?>
                                       value="0">下架
                                </label>
                            </div>
                        </div>
                        <?php if($action == "edit") {?>
                        <div class="form-group">
                            <label for="sort" class="col-sm-2 control-label">建立時間</label>
                            <div class="col-sm-10 control-text">
                                <?=$row['create_datetime']?>
                            </div>
                        </div>
                        <?php }?>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-right">
                        <input type="hidden" name="id" value="<?=$row['id']?>">
                        <a href="javascript:history.back()" class="btn btn-danger" style="margin-right: 10px;">取消，並返回上一層</a>
                        <button type="submit" class="btn btn-primary">儲存</button>
                        <input type="hidden" name="referer" value="<?=(!empty($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:"")?>">
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>