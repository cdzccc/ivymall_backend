<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("email_contact_us");
$sql_where = "";

$title  = filter_input(INPUT_GET, 'title');
$status = filter_input(INPUT_GET, 'status');
$s_date  = filter_input(INPUT_GET, 's_date');
$e_date  = filter_input(INPUT_GET, 'e_date');
$question  = filter_input(INPUT_GET, 'question');

if(!empty($title))
    $sql_where .= " and (name like '%".checkinput_sql($title,255)."%' or email like '%".checkinput_sql($title,255)."%' or Customer_ID like '%".checkinput_sql($title,255)."%')";
if(in_array($status, ["0","1", "2"]))
    $sql_where .= " and status = '".checkinput_sql($status,3)."'";
if(in_array($question, ["11","12","13","14","15","99"]))
    $sql_where .= " and question = '".checkinput_sql($question,3)."'";
if(!empty($s_date))
    $sql_where .= " and create_datetime >= '".checkinput_sql($s_date." 00:00:00",19)."'";
if(!empty($e_date))
    $sql_where .= " and create_datetime <= '".checkinput_sql($e_date." 23:59:59",19)."'";

//取出總筆數
$sql_cmd = "select count(*) from mail_content where `option` = 1 and deleted_at is null ".$sql_where." ";
$rs = $db->query($sql_cmd);
$row = $rs->fetchRow();
$total = $row[0];

$mypage = new page;
$pages = $mypage->set_page($total, $PAGE_NUM);

//設定分頁的初始值
if (empty($_GET['num']))
{
    $num = 1;
}else{
    $num = intval($_GET['num']);
}

$sql_cmd = "select * from mail_content where `option` = 1 and deleted_at is null ".$sql_where." order by create_datetime desc ".$pages[$num];
$rs_link  = $db->query($sql_cmd);
$tmp = $_GET;
unset($tmp['num']);
$val = http_build_query($tmp);
$page = $mypage->multi($total , $PAGE_NUM, $num, "./list.php", "", $val);
?><!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php"); ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                聯絡我們信箱管理
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box">
                <form action="./list.php" method="GET">
                    <div class="box-header">
                        <div style="float:left; display:inline-block; width:250px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">搜尋</button>
                                </div>
                                <input type="text" name="title" class="form-control input-sm"
                                       placeholder="聯絡人、信箱、會員ID" value="<?=(!empty($_GET['title']))?$_GET['title']:""?>">
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:350px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">詢問事項</button>
                                </div>
                                <select class="form-control input-sm" name="question">
                                    <option value="all">全部</option>
                                    <option value="11" <?=($question == "11")?"selected":""?>>網路書城相關</option>
                                    <option value="12" <?=($question == "12")?"selected":""?>>解析雜誌相關</option>
                                    <option value="13" <?=($question == "13")?"selected":""?>>生活雜誌相關</option>
                                    <option value="14" <?=($question == "14")?"selected":""?>>常春藤叢書相關</option>
                                    <option value="15" <?=($question == "15")?"selected":""?>>網站錯誤回報</option>
                                    <option value="99" <?=($question == "99")?"selected":""?>>其他</option>
                                </select>
                            </div>
                        </div>

                        <div style="float:left; display:inline-block; width:180px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">狀態</button>
                                </div>
                                <select class="form-control input-sm" name="status">
                                    <option value="all">全部</option>
                                    <option value="0" <?=($status == "0")?"selected":""?>>未處理</option>
                                    <option value="1" <?=($status == "1")?"selected":""?>>追蹤中</option>
                                    <option value="2" <?=($status == "2")?"selected":""?>>已處理</option>
                                </select>
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:328px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">建立時間</button>
                                </div>
                                <input type="text" name="s_date" class="form-control input-sm datetimepicker"
                                       placeholder="起始日" value="<?=(!empty($_GET['s_date']))?$_GET['s_date']:""?>">
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:300px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">~</button>
                                </div>
                                <input type="text" name="e_date" class="form-control input-sm datetimepicker"
                                       placeholder="結束日" value="<?=(!empty($_GET['e_date']))?$_GET['e_date']:""?>">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success btn-sm pull-right">送出</button>
                        <a class="btn btn-danger btn-sm pull-right" href="./list.php" style="margin-right: 10px;">清除</a>
                        <div style="float:left; display:inline-block; width:100%;margin-top: 10px;">
                            <!--<a class="pull-right btn btn-success btn-sm" href="./detail.php?action=add">新增</a>-->
                            <input class="pull-right btn btn-success btn-sm" type="button" value="刪除" onclick="if(confirm('是否刪除資料？刪除後無法刪除後無法還原！')) {$('form').attr('action','./delete.php');$('form').submit();}">
                        </div>
                    </div>
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <th style="min-width:100px;"><a href="javascript:void(0)" onclick="select_all('')">(全選)</a></th>
                                    <th>聯絡人</th>
                                    <th>Email</th>
                                    <th>會員ID</th>
                                    <th>詢問事項</th>
                                    <th>狀態</th>
                                    <th>建立時間</th>
                                    <th>操作</th>
                                </tr>
                                <?php
                                if($rs_link->numRows() > 0) {
                                while($row = $rs_link->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                                ?>
                                <tr onClick="iCheckThisRow(this);">
                                    <th><input type="checkbox" name="id[]" value="<?=$row["id"]?>"></th>
                                    <th><?=$row['name']?></th>
                                    <th><?=$row['email']?></th>
                                    <th><?=$row['Customer_ID']?></th>
                                    <th><?=$ARRall['mail_question'][$row["question"]]?></th>
                                    <th>
                                        <?php
                                            switch ($row['status']) {
                                                case '1':
                                                    echo "追蹤中";
                                                    break;
                                                case '2':
                                                    echo "已處理";
                                                    break;
                                                case '0':
                                                    echo "未處理";
                                                    break;
                                            }
                                        ?>
                                    </th>
                                    <th><?=$row['create_datetime']?></th>
                                    <th>
                                        <a class="btn btn-xs btn-primary" href="./detail.php?action=edit&id=<?=$row['id']?>">編輯</a>
                                    </th>
                                </tr>
                                <?php }} ?>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
		<?=$page?>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>