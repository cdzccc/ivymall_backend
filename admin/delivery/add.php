<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("delivery_add");
$delivery_id = get_id();

$name  = filter_input(INPUT_POST, 'name');
$price = filter_input(INPUT_POST, 'price', FILTER_VALIDATE_INT, ['options' => ['min_range' => 0, 'max_range' => 9999, 'default' => 0]]);
$type  = filter_input(INPUT_POST, 'type', FILTER_VALIDATE_INT, ['options' => ['min_range' => 0, 'max_range' => 9999, 'default' => 0]]);
if(empty($name) || empty($type)) {
    $err_msg = "欄位";
    if(empty($name))
        $err_field[] = "名稱";
    if(empty($type))
        $err_field[] = "類型";
    $err_msg .= "'".implode("','", $err_field)."'不可為空值";
    js_go_back_self($err_msg);
    exit;
}
$create_datetime = date("Y-m-d H:i:s");
$create_user     = $_SESSION[SESSION_VARIABLE."_user_id"];
$update_datetime = date("Y-m-d H:i:s");
$update_user     = $_SESSION[SESSION_VARIABLE."_user_id"];


$sql_array = array(
    "delivery_id"     => array("2", checkinput_sql($delivery_id, 19)),
    "name"            => array("2", checkinput_sql($name, 45)),
    "price"           => array("2", checkinput_sql($price, 5)),
    "create_datetime" => array("2", checkinput_sql($create_datetime, 50)),
    "create_user"     => array("2", checkinput_sql($create_user, 50)),
    "update_datetime" => array("2", checkinput_sql($update_datetime, 50)),
    "update_user"     => array("2", checkinput_sql($update_user, 50)),
    "type"            => array("2", checkinput_sql($type, 50)),
);
$sql_cmd = insert("delivery", $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
   js_go_back_global("DB_EDIT_ERROR");
   exit;
}else{
   $db->disconnect();
   js_repl_global( "./list.php", "ADD_SUCCESS");
   exit;
}
?>
