<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("delivery");
$sql_where = "";

//取出總筆數
$sql_cmd = "select count(*) from delivery where 1=1 ".$sql_where." ";
$rs = $db->query($sql_cmd);
$row = $rs->fetchRow();
$total = $row[0];

$mypage = new page;
$pages = $mypage->set_page($total, $PAGE_NUM);

//設定分頁的初始值
if (empty($_GET['num']))
{
    $num = 1;
}else{
    $num = intval($_GET['num']);
}


$sql_cmd = "select * from delivery where 1=1 ".$sql_where." order by delivery_id desc ".$pages[$num];
$rs_delivery  = $db->query($sql_cmd);
$tmp = $_GET;
unset($tmp['num']);
$val = http_build_query($tmp);
$page = $mypage->multi($total , $PAGE_NUM, $num, "./list.php", "", $val);
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php") ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                運費管理
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box">
                <div class="box-header">
                    <form action="./list.php" method="GET">
                        <a class="pull-right btn btn-success btn-sm" href="./detail.php?action=add">新增</a>

                    </form>
                </div>
                <div class="box-body no-padding">
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                <th>名稱</th>
                                <th>價格</th>
                                <th>類別</th>
                                <th>操作</th>
                            </tr>
                            <?php
                            if($rs_delivery->numRows() > 0) {
                            while($row = $rs_delivery->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                                switch ($row["type"]) {
                                    case '1':
                                        $type = "取貨";
                                        break;
                                    case '2':
                                        $type = "歸還";
                                        break;
                                    default:
                                        $type = "取貨";
                                        break;
                                }
                            ?>
                            <tr onClick="iCheckThisRow(this);">
                                <th><?=$row['name']?></th>
                                <th><?=number_format($row['price'])?></th>
                                <th><?=$type?></th>
                                <th>
                                    <a class="btn btn-xs btn-primary" href="./detail.php?action=edit&delivery_id=<?=$row['delivery_id']?>">編輯</a>
                                </th>
                            </tr>
                            <?php
                            } }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
		<?=$page?>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>