<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("delivery");
if(isset($_GET['action']))
    $action = $_GET['action'];
else
    $action = "add";

$row_delivery["delivery_id"] = "";
$row_delivery["name"] = "";
$row_delivery["price"] = "";
$row_delivery["type"] = 1;

if($action == "edit") {
    $sql_cmd = "select * from delivery where delivery_id = '".checkinput_sql($_GET['delivery_id'],19)."'";
    $rs_delivery = $db->query($sql_cmd);
    $row_delivery = $rs_delivery->fetchRow(MDB2_FETCHMODE_ASSOC);
}

?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php") ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                運費管理
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" method="POST" action="./<?=$action?>.php">
                    <div class="box-header">
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">類型</label>
                            <div class="col-sm-10">
                                <label>
                                    <input name="type" type="radio" value="1" <?=($row_delivery['type'] == 1)?"checked":""; ?>>
                                    取貨
                                </label>
                                <label>
                                    <input name="type" type="radio" value="2" <?=($row_delivery['type'] == 2)?"checked":""; ?>>
                                    還機
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">名稱</label>
                            <div class="col-sm-10">
                                <input id="name" name="name" type="text" class="form-control"
                                       value="<?=$row_delivery["name"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="price" class="col-sm-2 control-label">價格</label>
                            <div class="col-sm-10">
                                <input id="price" name="price" type="text" class="form-control"
                                       value="<?=$row_delivery["price"]?>">
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-right">
                        <input type="hidden" name="delivery_id" value="<?=$row_delivery["delivery_id"]?>">
                        <a href="javascript:history.back()" class="btn btn-danger" style="margin-right: 10px;">取消，並返回上一層</a>
                        <button type="submit" class="btn btn-primary">儲存</button>
                        <input type="hidden" name="referer" value="<?=(!empty($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:"")?>">
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>