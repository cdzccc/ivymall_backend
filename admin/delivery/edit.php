<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("delivery_edit");
$delivery_id = filter_input(INPUT_POST, 'delivery_id');

$name  = filter_input(INPUT_POST, 'name');
$price = filter_input(INPUT_POST, 'price', FILTER_VALIDATE_INT, ['options' => ['min_range' => 0, 'max_range' => 9999, 'default' => 0]]);
$type  = filter_input(INPUT_POST, 'type', FILTER_VALIDATE_INT, ['options' => ['min_range' => 0, 'max_range' => 9999, 'default' => 0]]);
if(empty($delivery_id)) {
    js_go_back_global("NOT_POST");
    exit;
}
if(empty($name) || empty($type)) {
    $err_msg = "欄位";
    if(empty($name))
        $err_field[] = "名稱";
    if(empty($type))
        $err_field[] = "類型";
    $err_msg .= "'".implode("','", $err_field)."'不可為空值";
    js_go_back_self($err_msg);
    exit;
}
$update_datetime = date("Y-m-d H:i:s");
$update_user     = $_SESSION[SESSION_VARIABLE."_user_id"];


$sql_array = array(
    "name"            => array("2", checkinput_sql($name, 45)),
    "price"           => array("2", checkinput_sql($price, 5)),
    "update_datetime" => array("2", checkinput_sql($update_datetime, 50)),
    "update_user"     => array("2", checkinput_sql($update_user, 50)),
    "type"            => array("2", checkinput_sql($type, 50)),
);
$sql_cmd = update("delivery",array("delivery_id", $delivery_id), $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
   js_go_back_global("DB_EDIT_ERROR");
   exit;
}else{
   $db->disconnect();
    $referer = filter_input(INPUT_POST, 'referer');
    $redirect = "./list.php";
    if(!empty($referer))
        $redirect = $referer;
    js_repl_global( $redirect, "EDIT_SUCCESS");
   exit;
}
?>
