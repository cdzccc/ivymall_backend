<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("newsletterlist_analytical_edit");
$id = filter_input(INPUT_POST, 'id');

$name      = filter_input(INPUT_POST, 'name');
$alt       = filter_input(INPUT_POST, 'alt');
$status    = filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 1, 'default' => 0)));
$sort        = filter_input(INPUT_POST, 'sort', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 99999, 'default' => 0)));
$old_pic   = filter_input(INPUT_POST, 'old_pic');


if(empty($id)) {
    js_go_back_global("NOT_POST");
    exit;
}

if(empty($name)) {
    $err_msg = "欄位";
    if(empty($name))
        $err_field[] = "月份";
    $err_msg .= "'".implode("','", $err_field)."'不可為空值";
    js_go_back_self($err_msg);
    exit;
}

if ($_FILES['pic']['name'] != "none" && is_uploaded_file($_FILES['pic']['tmp_name']))
{
    //重組檔名
    $type = explode(".", $_FILES['pic']['name']);
    $img = $_FILES['pic']['name'];
    if(file_exists(DOCUMENT_ROOT."/upload/".$img)) {
        // js_go_back_self("檔名重複!!");
        // exit;
    }
    @copy($_FILES['pic']['tmp_name'], DOCUMENT_ROOT."/upload/".$img);
}
else {
    $img = $old_pic;
}

$update_datetime = date("Y-m-d H:i:s");
$update_user     = $_SESSION[SESSION_VARIABLE."_user_id"];

$sql_array = array(
    "Category_CodeGroup"   => array("2", "NEWSLETTER_ANALYTICAL"),
    "Category_Name"        => array("2", checkinput_sql($name, 45)),
    "desc"                 => array("2", checkinput_sql($desc, 255)),
    "category_pic1"        => array("2", checkinput_sql($img, 200)),
    "pic_alt1"             => array("2", checkinput_sql($alt, 45)),
    "sort"                 => array("2", checkinput_sql($sort, 45)),
    "status"               => array("2", checkinput_sql($status,2)),
    "update_datetime"      => array("2", checkinput_sql($update_datetime, 50)),
    "update_user"          => array("2", checkinput_sql($update_user, 50)),
);
$sql_cmd = update("category", array("Category_Code", $id), $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
   js_go_back_global("DB_EDIT_ERROR");
   exit;
}else{
    add_log('解析英語電子報管理','2');
   $db->disconnect();
    $referer = filter_input(INPUT_POST, 'referer');
    $redirect = "./list.php";
    if(!empty($referer))
        $redirect = $referer;
    js_repl_global( $redirect, "EDIT_SUCCESS");
   exit;
}
?>
