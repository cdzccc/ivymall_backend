<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("columncontent_edit");
$id = get_ids();

$login_type              = filter_input(INPUT_POST, 'login_type');
$hash_key                = filter_input(INPUT_POST, 'hash_key');
$Customer_Mail           = filter_input(INPUT_POST, 'Customer_Mail');
$Customer_Contact_Person = filter_input(INPUT_POST, 'Customer_Contact_Person');

$status                  = filter_input(INPUT_POST, 'status');
$enabled                 = filter_input(INPUT_POST, 'enabled');
$password                = filter_input(INPUT_POST, 'password');
$birthday                = filter_input(INPUT_POST, 'birthday');
$epaper1                 = filter_input(INPUT_POST, 'epaper1');
$epaper2                 = filter_input(INPUT_POST, 'epaper2');
$Contact_Email           = filter_input(INPUT_POST, 'Contact_Email');
$email_verify            = filter_input(INPUT_POST, 'email_verify');
$level                   = filter_input(INPUT_POST, 'level');
$level_sdate             = filter_input(INPUT_POST, 'level_sdate');
$level_edate             = filter_input(INPUT_POST, 'level_edate');
$Customer_Name           = filter_input(INPUT_POST, 'Customer_Name');
$gender                  = filter_input(INPUT_POST, 'gender');
$Customer_Phone          = filter_input(INPUT_POST, 'Customer_Phone');
$Dealer_Address_City     = filter_input(INPUT_POST, 'Dealer_Address_City');
$Dealer_Address_Township = filter_input(INPUT_POST, 'Dealer_Address_Township');
$Dealer_Address          = filter_input(INPUT_POST, 'Dealer_Address');
$remarks                 = filter_input(INPUT_POST, 'remarks');

if(
    (($login_type == "normal") &&
        empty($password)
    )
    || ((in_array($login_type, ["fb","google"])) &&
        empty($hash_key)
    )
    || empty($Customer_Mail)
) {
    $err_msg = "欄位";
    if(($login_type == "normal") &&
        empty($password))
        $err_field[] = "密碼";
    if((in_array($login_type, ["fb","google"])) &&
        empty($hash_key))
        $err_field[] = "hash key";
    if(empty($Customer_Mail))
        $err_field[] = "會員帳號";
    $err_msg .= "'".implode("','", $err_field)."'不可為空值";
    js_go_back_self($err_msg);
    exit;
}

$sql_cmd = "select * from customer where Customer_Mail = '".$Customer_Mail."'";
$rs = $db->query($sql_cmd);
if($rs->numRows() > 0) {
    js_go_back_self("帳號重複");
    exit;
}

if (empty($epaper1)) {
    $epaper1 = 0;
}

if (empty($epaper2)) {
    $epaper2 = 0;
}
if ($gender=="M") {
    $sex = 0;
}
if ($gender=="F") {
    $sex = 1;
}


$fbLogin = 0;
$googleLogin = 0;
if($login_type == "fb") {
    $fbLogin = 1;
}
if($login_type == "google")
    $googleLogin = 1;
// if(empty($name) || empty($category) || empty($content)) {
//     js_go_back_global("DATA_EMPTY");
//     exit;
// }

$sql_cmd = "select * from customer where Customer_ID = '".checkinput_sql($id,19)."'";
$rs = $db->query($sql_cmd);
$row_customer = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);


$update_datetime = date("Y-m-d H:i:s");
$update_user     = $_SESSION[SESSION_VARIABLE."_user_id"];

$sql_array = array(
    "Customer_ID"             => array("2", checkinput_sql($id, 255)),
    "hash_key"                => array("2", checkinput_sql($hash_key, 255)),
    "Customer_Mail"           => array("2", checkinput_sql($Customer_Mail, 255)),
    "fbLogin"                 => array("2", checkinput_sql($fbLogin, 255)),
    "googleLogin"             => array("2", checkinput_sql($googleLogin, 255)),
    "status"                  => array("2", checkinput_sql(intval($status), 255)),
    "enabled"                 => array("2", checkinput_sql($enabled, 255)),
    "epaper1"                 => array("2", checkinput_sql($epaper1, 19)),
    "epaper2"                 => array("2", checkinput_sql($epaper2, 255)),
    "Contact_Email"           => array("2", checkinput_sql($Contact_Email, 200)),
    "email_verify"            => array("2", checkinput_sql($email_verify, 30)),
    "level"                   => array("2", checkinput_sql($level, 255)),
    "level_sdate"             => array("2", checkinput_sql($level_sdate, 200)),
    "level_edate"       => array("2", checkinput_sql($level_edate.'-12-31 23:59:59', 200)),
    "Customer_Name"           => array("2", checkinput_sql($Customer_Name, 200)),
    "gender"                  => array("2", checkinput_sql($gender, 200)),
    "Customer_Phone"          => array("2", checkinput_sql($Customer_Phone, 19)),
    "Customer_City"           => array("2", checkinput_sql($Dealer_Address_City, 30)),
    "Customer_Area"           => array("2", checkinput_sql($Dealer_Address_Township, 30)),
    "Customer_Addr"           => array("2", checkinput_sql($Dealer_Address, 9999999)),
    "remarks"           	  => array("2", checkinput_sql($remarks, 9999999)),
    "Customer_Contact_Person" => array("2", checkinput_sql($Customer_Contact_Person, 30)),
);
if (!empty($gender)) {
    $sql_array['sex'] = array("2", checkinput_sql($sex, 200));
}

if (!empty($birthday)) {
    $sql_array['birthday'] = array("2", checkinput_sql($birthday, 200));
}

if(!empty($password)) {
    if(!preg_match('/^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{8,}$/', $password)) {
        js_go_back_global("PWD_FORMAT_ERROR");
        exit;
    }
    $sql_array['Customer_Pwd'] = array("2" ,checkinput_sql(("cqt".md5($password)),255));
}

if($email_verify == 1 && $row_customer['Contact_Email_tmp'] != $row_customer['Contact_Email'] && $row_customer['Contact_Email'] != $Contact_Email) {
    $Contact_Email_tmp = $row_customer['Contact_Email_tmp'];
    $sql_array['Contact_Email'] = $Contact_Email_tmp;
    $sql_array['Contact_Email_tmp'] = "";
}
$sql_cmd = insert("customer", $sql_array);

$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
   js_go_back_global("DB_ADD_ERROR");
   exit;
}else{
    add_log('會員管理','1');
   $db->disconnect();
   js_repl_global( "./list.php", "ADD_SUCCESS");
   exit;
}
?>
