﻿<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("member");
$row_customer["Customer_ID"] = "";
$row_customer["fbLogin"] = "";
$row_customer["googleLogin"] = "";
$row_customer["hash_key"] = "";
$row_customer["Customer_Mail"] = "";
$row_customer["status"] = 1;
$row_customer["password"] = "";
$row_customer["birthday"] = "";
$row_customer["epaper1"] = "";
$row_customer["epaper2"] = "";
$row_customer["Contact_Email"] = "";
$row_customer["email_verify"] = "";
$row_customer["level"] = "";
$row_customer["level_sdate"] = "";
$row_customer["level_edate"] = "";
$row_customer["Customer_Name"] = "";
$row_customer["gender"] = "";
$row_customer["Customer_Phone"] = "";
$row_customer["Customer_City"] = "";
$row_customer["Customer_Area"] = "";
$row_customer["Customer_Addr"] = "";
$row_customer["year_consumption"] = 0;
$row_summary["total"] = 0;
$row_customer["order_count"] = 0;
$row_customer["year_consumption"] = 0;
$row_customer["total_consumption"] = 0;
$row_customer["last_order_time"] = "";
$row_customer["remarks"] = "";
$row_customer["create_datetime"] = "";
$row_customer["last_login_time"] = "";
$row_customer["enabled"] = 1;
$row_customer["cycle_buy"] = "";

$age = 0;
if(isset($_GET['action']))
    $action = $_GET['action'];
else
    $action = "add";

$select2_json = "";


if($action == "edit") {

    $Customer_ID = filter_input(INPUT_GET, 'Customer_ID');
    $sql_cmd = "select * from customer where Customer_ID = '".checkinput_sql($Customer_ID,19)."'";
    $rs = $db->query($sql_cmd);

    $row_summary = [];
    $row_customer = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);

    $birthDate = $row_customer['birthday'];
    $today = date("Y-m-d");
    $diff = date_diff(date_create($birthDate), date_create($today));
    $age = $diff->format('%y');
      $sql_cmd = "select * from point_summary where Customer_ID = '".checkinput_sql($Customer_ID,19)."'";
    $rs = $db->query($sql_cmd);
    $row_summary = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);

    $row_list = [];
    $sql_cmd = "select * from point_list where Customer_ID = '".checkinput_sql($Customer_ID,19)."'";
    $rs = $db->query($sql_cmd);
    while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
        $row_list[] = $row;
    }
    $sql_cmd = "select * from `order` where Customer_ID = '".$row_customer['Customer_ID']."' and Status in (8) order by `create_datetime` asc";
    $rs = $db->query($sql_cmd);
    $order_day = [];
    $create_datetime = "";
    while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
        if(empty($create_datetime)) {
			
            $create_datetime = date("Y-m-d",strtotime($row['create_datetime']));
			//echo "<br>";
        }
        else {
            $order_day[] = datediff($create_datetime, date("Y-m-d",strtotime($row['create_datetime'])));
            $create_datetime = date("Y-m-d",strtotime($row['create_datetime']));
			//echo "<br>";

        }
    }
    if(count($order_day) == 0) {
        $row_customer["cycle_buy"] = "-";
    }
    else {
        $cycle_buy = "0";
        foreach ($order_day as $key => $value) {
			if($value==0)
				unset($order_day[$key]);
			else
				$cycle_buy += $value;
        }
		
        $row_customer["cycle_buy"] = round($cycle_buy/(count($order_day)-1), 2);
    }

	$ticket = "select count(*) from ticket_list as tl join ticket as t on tl.Ticket_ID = t.ticket_id where t.delete_at is null and tl.Customer_ID = '".checkinput_sql($Customer_ID,19)."' and tl.status = 1 and tl.used = 0 and tl.deleted_at is null and tl.date_end >= '".date("Y-m-d")."'";
    $res = $db->query($ticket);
    $ticket_res = $res->fetchRow();
	$ticket1 = "select count(*) from ticket_list where Customer_ID = '".checkinput_sql($Customer_ID,19)."' and mode = 2 and status = 1 and used = 0 and deleted_at is null and date_end >= '".date("Y-m-d")."'";
    $res1 = $db->query($ticket1);
    $ticket_res1 = $res1->fetchRow();
	
    //$row_customer["ticket_count"] = empty($ticket_res[0]) ? 0 : $ticket_res[0]+$ticket_res1[0];
    $row_customer["ticket_count"] = $ticket_res[0]+$ticket_res1[0];
} else {
    $row_customer["ticket_count"] = 0;
}
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php") ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                會員管理
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" method="POST" action="./<?=$action?>.php">
                    <div class="box-body">
                        <?php if($action=="edit") {?>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">UID</label>
                            <div class="col-sm-10 control-text">
                                <?=$row_customer["Customer_ID"]?>
                            </div>
                        </div>
                        <?php } ?>
                        <?php if($action=="edit") {?>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">會員類型</label>
                            <div class="col-sm-10 control-text">
                                <?php
                                    if($row_customer['fbLogin']) {
                                        echo "FB登入會員";
                                    } else if($row_customer['googleLogin']) {
                                        echo "Google登入會員";
                                    } else {
                                        echo "一般會員";
                                    }
                                ?>
                            </div>
                        </div>
                        <?php if($row_customer['fbLogin'] || $row_customer['googleLogin']) {?>
                            <input name="status" class="status" type="hidden" value="1" >
                        <?php } ?>
                        <?php } else { ?>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">會員類型</label>
                            <div class="col-sm-10">
                                <select class="form-control login_type" name="login_type">
                                    <option value="fb">FB登入會員</option>
                                    <option value="google">Google登入會員</option>
                                    <option value="normal">一般會員</option>
                                </select>
                            </div>
                        </div>

                        <?php } ?>
						<?php if($row_customer['fbLogin'] || $row_customer['googleLogin'] || $action == "add") {
                        ?>
                        <div class="form-group hash_key">
                            <label for="name" class="col-sm-2 control-label">金鑰</label>
                            <?php if($action=="edit") {?>
                            <div class="col-sm-10 control-text">
                                <?=$row_customer["hash_key"]?>
                            </div>
                            <?php } else { ?>
                            <div class="col-sm-10 ">
                                <input id="hash_key" name="hash_key" type="text" class="form-control">
                            </div>
                            <?php } ?>

                        </div>
                        <?php
                        }
                        ?>
                        
                        <?php if((!$row_customer['fbLogin'] && !$row_customer['googleLogin']) || $action == "add") {
                        ?>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">會員帳號</label>
                            <?php if($action=="edit") {?>
                            <div class="col-sm-10 control-text">
                                <?=$row_customer["Customer_Mail"]?>
                            </div>
                            <?php } else { ?>
                            <div class="col-sm-10">
                                <input id="Customer_Mail" name="Customer_Mail" type="text" class="form-control">
                            </div>
                            <?php } ?>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">完成帳號驗證</label>
                            <div class="col-sm-10 control-text">
                                <label>
                                    <input name="status" class="status" type="radio" value="1" <?=($row_customer["status"] == 1)?"checked=checked":""?> >是
                                </label>
                                <label>
                                    <input name="status" class="status" type="radio"  value="0" <?=($row_customer["status"] == 0)?"checked=checked":""?> >否
                                </label>
                            </div>
                        </div>
						<div class="form-group password" <?php var_dump($row_customer["status"]); if($action == "add"){echo "style='display:none;'"; }?>>
                            <label for="password" class="col-sm-2 control-label">修改密碼</label>
                            <div class="col-sm-10">
                                <input id="password" name="password" type="text" class="form-control" placeholder=""
                                       value="">
                            </div>
                        </div>
                        <?php
                        }
                        ?>
                        
                        <div class="form-group">
                            <label for="birthday" class="col-sm-2 control-label">生日</label>
                            <div class="col-sm-10">
                                <input id="birthday" name="birthday" type="text" class="form-control datepicker" placeholder=""
                                       value="<?=$row_customer["birthday"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">年齡</label>
                            <div class="col-sm-10 control-text">
                                <?=$age?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">訂閱電子報</label>
                            <div class="col-sm-10 control-text">
                                <label>
                                    <input id="epaper1" name="epaper1" type="checkbox" class="" placeholder=""
                                           value="1" <?=($row_customer["epaper1"] == 1)?"checked":""?>>解析英語學習電子報
                                </label>
                                <label>
                                    <input id="epaper2" name="epaper2" type="checkbox" class="" placeholder=""
                                           value="1" <?=($row_customer["epaper2"] == 1)?"checked":""?>>生活英語學習電子報
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">聯絡email</label>
                            <div class="col-sm-10">
                                <input id="Contact_Email" name="Contact_Email" type="text" class="form-control" placeholder=""
                                       value="<?=$row_customer["Contact_Email"]?>">
                            </div>
                        </div>
						<?php if($action == "edit") {?>
						<div class="form-group">
                            <label for="name" class="col-sm-2 control-label">驗證中聯絡email</label>
                            <div class="col-sm-10">
                                <input id="Contact_Email" name="Contact_Email" type="text" disabled ="disabled" class="form-control" placeholder=""
                                       value="<?=$row_customer["Contact_Email_tmp"]?>">
                            </div>
                        </div>
						<?PHP } ?>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">完成mail驗證</label>
                            <div class="col-sm-10 control-text">
                                <label>
                                    <input id="email_verify" name="email_verify" type="radio" class="" placeholder=""
                                           value="1" <?=($row_customer["email_verify"]==1)?"checked":""?>>是
                                </label>
                                <label>
                                    <input id="email_verify" name="email_verify" type="radio" class="" placeholder=""
                                           value="0" <?=($row_customer["email_verify"] == 0)?"checked":""?>>否
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">會員等級</label>
                            <div class="col-sm-10">
                                <select class="form-control level" name="level"  onchange="levelchange();">
                                    <option value="0" <?=($row_customer["level"] == "0")?"selected":""?>>種子會員</option>
                                    <option value="1" <?=($row_customer["level"] == "1")?"selected":""?>>幼苗會員</option>
                                    <option value="2" <?=($row_customer["level"] == "2")?"selected":""?>>大樹會員</option>
                                    <option value="3" <?=($row_customer["level"] == "3")?"selected":""?>>神木會員</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">會員等級啟用時間</label>
                            <div class="col-sm-10">
                                <input id="level_sdate" name="level_sdate" type="text" class="form-control datetimepicker col-sm-2" style="width:150px;"
                                       value="<?=$row_customer["level_sdate"]?>">
							    <div class="form-group type_item type_item_1" <?=($row_customer['level']>=2)?"":"style='display:none;'"?>>
									<div class="col-sm-2"  style="width:20px;">~</div>

									<input id="level_edate" name="level_edate" type="number" class="form-control col-sm-2" style="width:80px;"
										   value="<?=(!empty($row_customer["level_edate"])?date("Y",strtotime($row_customer["level_edate"])):date("Y"))?>"><label for="name" class="control-label">-12-31 23:59:59</label>
								</div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="Customer_Name" class="col-sm-2 control-label">購書人姓名</label>
                            <div class="col-sm-10">
                                <input id="Customer_Name" name="Customer_Name" type="text" class="form-control" placeholder=""
                                       value="<?=$row_customer["Customer_Name"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">購書人性別</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="gender">
                                    <option value="0" <?=($row_customer["gender"] == "3")?"selected":""?>>請選擇</option>
                                    <option value="M" <?=($row_customer["gender"] == "M")?"selected":""?>>男</option>
                                    <option value="F" <?=($row_customer["gender"] == "F")?"selected":""?>>女</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">購書人聯絡電話</label>
                            <div class="col-sm-10">
                                <input id="Customer_Phone" name="Customer_Phone" type="text" class="form-control" placeholder=""
                                       value="<?=$row_customer["Customer_Phone"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">購書人聯絡地址</label>
                             <div class="col-sm-10 control-text">
                                <div id="twzipcode">
                                    <div data-role="zipcode"
                                         data-name="zipcode"
                                         data-value=""
                                         data-style="zipcode-style">
                                    </div>
                                    <div data-role="county"
                                         data-name="Dealer_Address_City"
                                         data-value="<?=$row_customer['Customer_City']?>"
                                         data-style="city-style">
                                    </div>
                                    <div data-role="district"
                                         data-name="Dealer_Address_Township"
                                         data-value="<?=$row_customer['Customer_Area']?>"
                                         data-style="district-style">
                                    </div>
                                    <input name="Dealer_Address" type="text" style="width:500px;"  value="<?=$row_customer['Customer_Addr']?>"/>
                                </div>

                            </div>
                        </div>
                        <?php if($action == "edit") {?>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">年度累積消費金額</label>
                            <div class="col-sm-10 control-text">
                                <?=$row_customer["year_consumption"]?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">可使用熊贈點</label>
                            <div class="col-sm-10 control-text">
                                <?=intval($row_summary["total"])?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">累積下單次數</label>
                            <div class="col-sm-10 control-text">
                                <?=$row_customer["order_count"]?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">總累積消費金額</label>
                            <div class="col-sm-10 control-text">
                                <?=intval($row_customer["total_consumption"])?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">平均消費金額</label>
                            <div class="col-sm-10 control-text">
                                <?=($row_customer["order_count"] == 0)?0:floor($row_customer["total_consumption"]/$row_customer["order_count"])?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">最後一次下單時間</label>
                            <div class="col-sm-10 control-text">
                                <?=$row_customer["last_order_time"]?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">最後登入時間</label>
                            <div class="col-sm-10 control-text">
                                <?=$row_customer["last_login_time"]?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">可使用的折價券</label>
                            <div class="col-sm-10 control-text">
                                <?=$row_customer["ticket_count"]?>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">狀態</label>
                            <div class="col-sm-10 control-text">
                                <label>
                                    <input name="enabled" type="radio" class="" placeholder=""
                                           value="1" <?=($row_customer["enabled"] == 1)?"checked":""?>>啟用
                                </label>
                                <label>
                                    <input name="enabled" type="radio" class="" placeholder=""
                                           value="0" <?=($row_customer["enabled"] == 0)?"checked":""?>>停用
                                </label>
                            </div>
                        </div>
                        <?php if($action == "edit") {?>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">會員平均回購週期</label>
                            <div class="col-sm-10 control-text">
                                <?=intval($row_customer["cycle_buy"])?> 天
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">會員最近一次消費</label>
                            <div class="col-sm-10 control-text">
                                <?=intval((strtotime("now") - strtotime($row_customer["last_order_time"]))/ (60*60*24));?> 天
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">備註</label>
                            <div class="col-sm-10">
                                <textarea id="remarks" name="remarks" class="form-control" ><?=$row_customer["remarks"]?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="f_path" class="col-sm-2 control-label">建立時間</label>
                            <div class="col-sm-10 control-text">
                                <i class="fa fa-clock-o" aria-hidden="true"></i> <?=$row_customer['create_datetime']?>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-right">
                        <input type="hidden" name="id" value="<?=$row_customer['Customer_ID']?>">
                        <a href="javascript:history.back()" class="btn btn-danger" style="margin-right: 10px;">取消，並返回上一層</a>
                        <button type="submit" class="btn btn-primary">儲存</button>
                        <input type="hidden" name="referer" value="<?=(!empty($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:"")?>">
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>
<script type="text/javascript">
    $(document).ready(function() {
        $(".login_type").change(function () {
            var login_type = $(this).val();
            switch (login_type) {
                case "fb":
                    $('.password').hide();
                    $('.hash_key').show();
                break;
                case "google":
                    $('.password').hide();
                    $('.hash_key').show();
                break;
                case "normal":
                    $('.password').show();
                    $('.hash_key').hide();
                break;
            }
        });
		
		
		
		
    });
	
	function levelchange(){
		if($(".level").val()>=2){
			$('.type_item_1').show();
		}else{
			$('.type_item_1').hide();
		}
		
	}
</script>
</body>
</html>