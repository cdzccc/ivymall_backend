<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("member");

$sql_where = "";
$order_by = " order by c.create_datetime desc ";
if(!empty($_GET['name'])) {
    $sql_where .= " and c.Customer_Name like '%".$_GET["name"]."%'";
}
if(!empty($_GET['email'])) {
    $sql_where .= " and c.Customer_Mail like '%".$_GET["email"]."%'";
}

$sql_cmd = "select c.*,s.total from customer as c left join point_summary as s on c.Customer_ID = s.Customer_ID where 1=1 ".$sql_where.$order_by;
$rs_member = $db->query($sql_cmd);
$rs[] = [
        "UID",
        "會員類型",
        "金鑰",
        "會員帳號",
        "完成帳號驗證",
        "生日",
        "年齡",
        "訂閱電子報",
        "聯絡Email",
        "完成Email驗證",
        "會員等級",
        "會員等級啟用時間",
        "會員等級到期時間",
        "購書人姓名",
        "購書人性別",
        "購書人聯絡電話",
        "購書人聯絡地址",
        "年度累積消費金額",
        "可使用熊贈點",
        "累積下單次數",
        "總累積消費金額",
        "平均消費金額",
        "最後一次下單時間",
        "最後登入時間",
        "可使用的折價券",
        "備註",
        "會員平均回購週期",
        "會員最近一次消費",
        "狀態",
        "建立時間",
    ];
while($row = $rs_member->fetchRow(MDB2_FETCHMODE_ASSOC)) {
    $sql_cmd = "select * from `order` where Customer_ID = '".$row['Customer_ID']."' and Status in (2,3,4,5,7,8,12)";
    $rs_order = $db->query($sql_cmd);
    $order_day = [];
    $tmp_create_datetime = "";
    // while($row_order = $rs_order->fetchRow(MDB2_FETCHMODE_ASSOC)) {
    //     if(!empty($tmp_create_datetime)) {
    //         $tmp_create_datetime = $row_order['$tmp_create_datetime'];
    //     }
    //     else {
    //         $order_day[] = dateDiff($tmp_create_datetime, $row_order['$tmp_create_datetime']);
    //         $tmp_create_datetime = $row_order['$tmp_create_datetime'];
    //     }
    // }
    if(count($order_day) == 0) {
        $row["cycle_buy"] = "-";
    }
    else {
        $cycle_buy = "0";
        foreach ($order_day as $key => $value) {
            $cycle_buy += $value;
        }
        $row["cycle_buy"] = round($cycle_buy/count($order_day), 2);
    }
    if($row['fbLogin']) {
        $type = "FB登入會員";
    } else if($row['googleLogin']) {
        $type = "Google登入會員";
    } else {
        $type = "一般會員";
    }

    $birthDate = $row['birthday'];
    $today = date("Y-m-d");
    $diff = date_diff(date_create($birthDate), date_create($today));
    $age = $diff->format('%y');

    $epaper = [];
    if($row['epaper1'] == 1)
        $epaper[] = "解析英語學習電子報";
    if($row['epaper2'] == 1)
        $epaper[] = "生活英語學習電子報";
    $rs[] = [
        " ".$row['Customer_ID']."\t",
        $type,
        $row['token'],
        $row['Customer_Mail'],
        ($row["status"] == 1)?"是":"否",
        $row['birthday'],
        $age,
        addslashes(implode(",", $epaper)),
        $row['Contact_Email'],
        ($row["email_verify"] == 1)?"是":"否",
        $ARRall['level'][$row['level']],
        $row['level_sdate'],
        $row['level_edate'],
        $row['Customer_Name'],
        $row['gender'],
        $row['Customer_Phone']."\t",
        $row['Customer_City'].$row['Customer_Area'].$row['Customer_Addr'],
        $row['year_consumption'],
        intval($row['total']),
        $row['order_count'],
        $row['total_consumption'],
        ($row["order_count"] == 0)?0:floor($row["total_consumption"]/$row["order_count"]),
        $row['last_order_time'],
        $row['last_login_time'],
        0,
        $row['remarks'],
        $row["cycle_buy"],
        $row['last_order_time'],
        ($row["enabled"] == 1)?"啟用":"停用",
        $row['create_datetime'],
    ];
}
header('Content-Encoding: UTF-8');
header("Content-type: application/x-download");
header("Content-disposition: attachment; filename=customer.csv");
echo arr_to_csv($rs);
?>



