﻿<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("member");

$sql_where = "";
$status  = filter_input(INPUT_GET, 'status');
$level   = filter_input(INPUT_GET, 'level');
$type    = filter_input(INPUT_GET, 'type');
$s_date  = filter_input(INPUT_GET, 's_date');
$e_date  = filter_input(INPUT_GET, 'e_date');
$s_birth = filter_input(INPUT_GET, 's_birth');
$e_birth = filter_input(INPUT_GET, 'e_birth');
$s_point = filter_input(INPUT_GET, 's_point');
$e_point = filter_input(INPUT_GET, 'e_point');
$order_by = " order by c.create_datetime desc ";
if(!empty($_GET['uid'])) {
    $sql_where .= " and c.Customer_ID like '%".$_GET["uid"]."%'";
}
if(!empty($_GET['name'])) {
    $sql_where .= " and c.Customer_Name like '%".$_GET["name"]."%'";
}
if(!empty($_GET['email'])) {
    $sql_where .= " and c.Customer_Mail like '%".$_GET["email"]."%'";
}
if(in_array($status, ["0","1"]))
    $sql_where .= " and c.status = '".checkinput_sql($status,3)."'";
if(in_array($level, ["0","1","2","3"]))
    $sql_where .= " and c.level = '".checkinput_sql($level,3)."'";

if(!empty($s_date))
    $sql_where .= " and c.create_datetime >= '".checkinput_sql($s_date." 00:00:00",19)."'";
if(!empty($e_date))
    $sql_where .= " and c.create_datetime <= '".checkinput_sql($e_date." 23:59:59",19)."'";

if(!empty($s_birth))
    $sql_where .= " and c.birthday >= '".checkinput_sql($s_birth ,19)."'";
if(!empty($e_birth))
    $sql_where .= " and c.birthday <= '".checkinput_sql($e_birth ,19)."'";

if(!empty($s_point))
    $sql_where .= " and s.total >= '".checkinput_sql($s_point ,19)."'";
if(!empty($e_point))
    $sql_where .= " and s.total <= '".checkinput_sql($e_point ,19)."'";

switch ($type) {
    case 'fb':
        $sql_where .= " and fbLogin = '1'";
        break;
    case 'google':
        $sql_where .= " and googleLogin = '1'";
        break;
    case 'normal':
        $sql_where .= " and (fbLogin = '0' && googleLogin = '0')";
        break;
}

//取出總筆數
$sql_cmd = "select count(*) from customer as c left join point_summary as s
            on c.Customer_ID = s.Customer_ID where deleted_at is null ".$sql_where." ";
$rs = $db->query($sql_cmd);
// print_r($rs);
$row = $rs->fetchRow();
$total = $row[0];

$mypage = new page;
$pages = $mypage->set_page($total, $PAGE_NUM);

//設定分頁的初始值
if (empty($_GET['num']))
{
    $num = 1;
}else{
    $num = intval($_GET['num']);
}


$sql_cmd = "select c.*, s.total from customer as c left join point_summary as s
            on c.Customer_ID = s.Customer_ID where deleted_at is null ".$sql_where.$order_by.$pages[$num];
$rs_member = $db->query($sql_cmd);
$tmp = $_GET;
unset($tmp['num']);
$val = http_build_query($tmp);
$page = $mypage->multi($total , $PAGE_NUM, $num, "./list.php", "", $val);
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php") ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                會員管理
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box">
                <form action="./list.php" method="GET" name="search">
                    <div class="box-header">
                        <div style="float:left; display:inline-block; width:160px; padding-right:15px; padding-bottom:10px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">UID</button>
                                </div>
                                <input type="text" name="uid" class="form-control input-sm"
                                       placeholder="" value="<?=(!empty($_GET['uid']))?$_GET['uid']:""?>">
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:160px; padding-right:15px; padding-bottom:10px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">姓名</button>
                                </div>
                                <input type="text" name="name" class="form-control input-sm"
                                       placeholder="" value="<?=(!empty($_GET['name']))?$_GET['name']:""?>">
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:350px; padding-right:15px; padding-bottom:10px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">會員帳號</button>
                                </div>
                                <input type="text" name="email" class="form-control input-sm"
                                       placeholder="" value="<?=(!empty($_GET['email']))?$_GET['email']:""?>">
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:350px; padding-right:15px; padding-bottom:10px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">會員類型</button>
                                </div>
                                <select class="form-control input-sm" name="type">
                                    <option value="all">全部</option>
                                    <option value="fb" <?=($type == "fb")?"selected":""?>>FB登入會員</option>
                                    <option value="google" <?=($type == "google")?"selected":""?>>Google登入會員</option>
                                    <option value="normal" <?=($type == "normal")?"selected":""?>>一般會員</option>
                                </select>
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:250px; padding-right:15px; padding-bottom:10px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">生日</button>
                                </div>
                                <input type="text" name="s_birth" class="form-control input-sm datepicker"
                                       placeholder="起始日" value="<?=(!empty($_GET['s_birth']))?$_GET['s_birth']:""?>">
								<div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">~</button>
                                </div>
                                <input type="text" name="e_birth" class="form-control input-sm datepicker"
                                       placeholder="結束日" value="<?=(!empty($_GET['e_birth']))?$_GET['e_birth']:""?>">
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:180px; padding-right:15px; padding-bottom:10px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">會員等級</button>
                                </div>
                                <select class="form-control input-sm" name="level">
                                    <option value="all">全部</option>
                                    <option value="0" <?=($level == "0")?"selected":""?>>種子會員</option>
                                    <option value="1" <?=($level == "1")?"selected":""?>>幼苗會員</option>
                                    <option value="2" <?=($level == "2")?"selected":""?>>大樹會員</option>
                                    <option value="3" <?=($level == "3")?"selected":""?>>神木會員</option>
                                </select>
                            </div>
                        </div>
						
                        <div style="float:left; display:inline-block; width:250px; padding-right:15px; padding-bottom:10px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">可使用熊贈點</button>
                                </div>
                                <input type="text" name="s_point" class="form-control input-sm"
                                       placeholder="0" value="<?=(!empty($_GET['s_point']))?$_GET['s_point']:""?>">
									   <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">~</button>
                                </div>
                                <input type="text" name="e_point" class="form-control input-sm"
                                       placeholder="99999" value="<?=(!empty($_GET['e_point']))?$_GET['e_point']:""?>">
                            </div>
                        </div>
						
                        <div style="float:left; display:inline-block; width:600px; padding-right:15px; padding-bottom:10px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">建立時間</button>
                                </div>
                                <input type="text" name="s_date" class="form-control input-sm datetimepicker"
                                       placeholder="起始日" value="<?=(!empty($_GET['s_date']))?$_GET['s_date']:""?>">
									   <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">~</button>
                                </div>
                                <input type="text" name="e_date" class="form-control input-sm datetimepicker"
                                       placeholder="結束日" value="<?=(!empty($_GET['e_date']))?$_GET['e_date']:""?>">
                            </div>
                        </div>
                       
						
                        <div style="float:left; display:inline-block; width:180px; padding-right:15px; padding-bottom:10px;">
							<div class="input-group">
								<div class="input-group-btn">
									<button type="button" class="btn btn-primary btn-sm">狀態</button>
								</div>
								<select class="form-control input-sm" name="status">
									<option value="all">全部</option>
									<option value="1" <?=($status == "1")?"selected":""?>>啟用</option>
									<option value="0" <?=($status == "0")?"selected":""?>>停用</option>
								</select>
							</div>
						</div>
                        <div style="float:left; display:inline-block; width:110px;" class="pull-right">
							<button type="button" class="btn btn-success btn-sm pull-right" onclick="search.action = 'list.php';search.submit();" >送出</button>
							<a class="btn btn-danger btn-sm pull-right" href="./list.php" style="margin-right: 10px;">清除</a>
						</div>
                        <div style="float:left; display:inline-block; width:100%;">
                            <a class="pull-right btn btn-success btn-sm" href="./detail.php?action=add">新增</a>
                            <input class="pull-right btn btn-success btn-sm" type="button" value="刪除" onclick="if(confirm('是否刪除資料？刪除後無法刪除後無法還原！')) {$('form').attr('action','./delete.php');$('form').submit();}"style="margin-right: 10px;">
							<button type="button" class="pull-right btn btn-success" style="margin-right:10px;" onclick="search.action = 'export.php';search.submit();" >匯出csv</button>
                        </div>
                    </div>
                    <div class="box-body no-padding table-responsive">
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <th style="width:100px;"><a href="javascript:void(0)" onclick="select_all('')">(全選)</a></th>
                                    <th style="min-width:120px;">UID</th>
                                    <th style="min-width:120px;">會員類型</th>
                                    <th style="min-width:150px;">會員帳號</th>
                                    <th style="min-width:100px;">生日</th>
                                    <th style="min-width:100px;">會員等級</th>
                                    <th style="min-width:140px;">購書人姓名</th>
                                    <th style="min-width:140px;">可使用熊贈點</th>
                                    <th style="min-width:140px;">最後登入時間</th>
                                    <th style="min-width:60px;">狀態</th>
                                    <th>操作</th>
                                </tr>
                                <?php
                                    while($row = $rs_member->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                                        $sql_cmd = "select * from point_summary where Customer_ID = '".$row['Customer_ID']."'";
                                        $rs_point = $db->query($sql_cmd);
                                        if($rs_point->numRows() == 0)
                                            $row_point['total'] = 0;
                                        else
                                            $row_point = $rs_point->fetchRow(MDB2_FETCHMODE_ASSOC)
                                ?>
                                <tr onClick="iCheckThisRow(this);">
                                    <th><input type="checkbox" name="id[]" value="<?=$row["Customer_ID"]?>"></th>
                                    <th><?=$row["Customer_ID"]?></th>
                                    <th>
                                        <?php
                                        if($row['fbLogin']) {
                                            echo "FB登入會員";
                                        } else if($row['googleLogin']) {
                                            echo "Google登入會員";
                                        } else {
                                            echo "一般會員";
                                        }
                                        ?>
                                    </th>
                                    <th><?=$row["Customer_Mail"]?></th>
                                    <th><?=$row["birthday"]?></th>
                                    <th><?=$ARRall['level'][$row["level"]]?></th>
                                    <th><?=$row["Customer_Name"]?></th>
                                    <th><?=$row_point["total"]?></th>
                                    <th><?=$row["last_login_time"]?></th>
                                    <th><?=($row['enabled']==0)?"停用":"啟用"?></th>
                                    <th>
                                        <a class="btn btn-xs btn-primary" href="./detail.php?action=edit&Customer_ID=<?=$row["Customer_ID"]?>">編輯</a>
                                    </th>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
<?=$page?>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>