<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("columncontent_edit");
$id = filter_input(INPUT_POST, 'id');
$status                  = filter_input(INPUT_POST, 'status');
$enabled                 = filter_input(INPUT_POST, 'enabled');
$password                = filter_input(INPUT_POST, 'password');
$birthday                = filter_input(INPUT_POST, 'birthday');
$epaper1                 = filter_input(INPUT_POST, 'epaper1');
$epaper2                 = filter_input(INPUT_POST, 'epaper2');
$Contact_Email           = filter_input(INPUT_POST, 'Contact_Email');
$email_verify            = filter_input(INPUT_POST, 'email_verify');
$level                   = filter_input(INPUT_POST, 'level');
$level_sdate             = filter_input(INPUT_POST, 'level_sdate');
$level_edate             = filter_input(INPUT_POST, 'level_edate');
$Customer_Name           = filter_input(INPUT_POST, 'Customer_Name');
$gender                  = filter_input(INPUT_POST, 'gender');
$Customer_Phone          = filter_input(INPUT_POST, 'Customer_Phone');
$Dealer_Address_City     = filter_input(INPUT_POST, 'Dealer_Address_City');
$Dealer_Address_Township = filter_input(INPUT_POST, 'Dealer_Address_Township');
$Dealer_Address          = filter_input(INPUT_POST, 'Dealer_Address');
$remarks                 = filter_input(INPUT_POST, 'remarks');

if(empty($id)) {
    js_go_back_global("NOT_POST");
    exit;
}

if (empty($epaper1)) {
    $epaper1 = 0;
}

if (empty($epaper2)) {
    $epaper2 = 0;
}
if ($gender=="M") {
    $sex = 0;
}
if ($gender=="F") {
    $sex = 1;
}


// if(empty($name) || empty($category) || empty($content)) {
//     js_go_back_global("DATA_EMPTY");
//     exit;
// }

$sql_cmd = "select * from customer where Customer_ID = '".checkinput_sql($id,19)."'";
$rs = $db->query($sql_cmd);
$row_customer = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);


$update_datetime = date("Y-m-d H:i:s");
$update_user     = $_SESSION[SESSION_VARIABLE."_user_id"];

$sql_array = array(
    "status"            => array("2", checkinput_sql(intval($status), 255)),
    "enabled"           => array("2", checkinput_sql($enabled, 255)),
    "epaper1"           => array("2", checkinput_sql($epaper1, 19)),
    "epaper2"           => array("2", checkinput_sql($epaper2, 255)),
    "Contact_Email"     => array("2", checkinput_sql($Contact_Email, 200)),
    "email_verify"      => array("2", checkinput_sql($email_verify, 30)),
    "level"             => array("2", checkinput_sql($level, 255)),
    "level_sdate"       => array("2", checkinput_sql($level_sdate, 200)),
    "Customer_Name"     => array("2", checkinput_sql($Customer_Name, 200)),
    "gender"            => array("2", checkinput_sql($gender, 200)),
    "Customer_Phone"    => array("2", checkinput_sql($Customer_Phone, 19)),
    "Customer_City"     => array("2", checkinput_sql($Dealer_Address_City, 30)),
    "Customer_Area"     => array("2", checkinput_sql($Dealer_Address_Township, 30)),
    "Customer_Addr"     => array("2", checkinput_sql($Dealer_Address, 9999999)),
    "remarks"           => array("2", checkinput_sql($remarks, 9999999)),
);

if (!empty($birthday)) {
    $sql_array['birthday'] = array("2", checkinput_sql($birthday, 255));
}
if (!empty($gender)) {
    $sql_array['sex'] = array("2", checkinput_sql($sex, 200));
}

if(!empty($password)) {
    if(!preg_match('/^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{8,}$/', $password)) {
        js_go_back_global("PWD_FORMAT_ERROR");
        exit;
    }
    $sql_array['Customer_Pwd'] = array("2" ,checkinput_sql(("cqt".md5($password)),255));
}

if($level >= 2){
	$sql_array['level_edate'] = array("2", checkinput_sql($level_edate.'-12-31 23:59:59', 200));
}else{
	$sql_array['level_edate'] = array("2", checkinput_sql(null, 200));
}


/*if($email_verify == 1 && $row_customer['Contact_Email_tmp'] != $row_customer['Contact_Email'] && $row_customer['Contact_Email'] != $Contact_Email) {
    $Contact_Email_tmp = $row_customer['Contact_Email_tmp'];
    $sql_array['Contact_Email'] = $Contact_Email_tmp;
    $sql_array['Contact_Email_tmp'] = "";
}*/

$sql_cmd = update("customer", array("Customer_ID", $id), $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
   js_go_back_global("DB_EDIT_ERROR");
   exit;
}else{
    add_log('會員管理','2');
    $db->disconnect();
    $referer = filter_input(INPUT_POST, 'referer');
    $redirect = "./list.php";
    if(!empty($referer))
        $redirect = $referer;
    js_repl_global( $redirect, "EDIT_SUCCESS");
    exit;
}
?>
