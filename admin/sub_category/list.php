<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("sub_category");

$sql_where = "";

$title  = filter_input(INPUT_GET, 'title');
$status = filter_input(INPUT_GET, 'status');
$category = filter_input(INPUT_GET, 'category');
$s_date  = filter_input(INPUT_GET, 's_date');
$e_date  = filter_input(INPUT_GET, 'e_date');

if(!empty($title))
    $sql_where .= " and Category_Name like '%".checkinput_sql($title,255)."%'";
if(in_array($status, ["0","1"]))
    $sql_where .= " and status = '".checkinput_sql($status,3)."'";
if(!empty($category))
    $sql_where .= " and Parent_Category_Code = '".checkinput_sql($category,19)."'";
if(!empty($s_date))
    $sql_where .= " and create_datetime >= '".checkinput_sql($s_date." 00:00:00",19)."'";
if(!empty($e_date))
    $sql_where .= " and create_datetime <= '".checkinput_sql($e_date." 23:59:59",19)."'";

$sql_cmd = "select * from category where Category_CodeGroup = 'Goods_Category' and status = 1 and deleted_at is null";
$rs_category = $db->query($sql_cmd);
while($row = $rs_category->fetchRow(MDB2_FETCHMODE_ASSOC)) {
    $row_P_category[] = $row;
}
//取出總筆數
$sql_cmd = "select count(*) from category where Category_CodeGroup = 'Goods_Sub_Category' and deleted_at is null ".$sql_where." ";
$rs = $db->query($sql_cmd);
$row = $rs->fetchRow();
$total = $row[0];

$mypage = new page;
$pages = $mypage->set_page($total, $PAGE_NUM);

//設定分頁的初始值
if (empty($_GET['num']))
{
    $num = 1;
}else{
    $num = intval($_GET['num']);
}


$sql_cmd = "select * from category where Category_CodeGroup = 'Goods_Sub_Category' and deleted_at is null ".$sql_where." order by create_datetime desc ".$pages[$num];
$rs = $db->query($sql_cmd);
$row_category = [];
while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
    foreach($row_P_category as $P_category) {
        if($P_category['Category_Code'] == $row['Parent_Category_Code'])
            $row['P_category'] = $P_category['Category_Name'];
    }
    $row_category[] = $row;
}
$tmp = $_GET;
unset($tmp['num']);
$val = http_build_query($tmp);
$page = $mypage->multi($total , $PAGE_NUM, $num, "./list.php", "", $val);



?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php"); ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                第二層分類管理
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box">
                <form action="./list.php" method="GET">
                    <div class="box-header">
                        <div style="float:left; display:inline-block; width:180px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">搜尋</button>
                                </div>
                                <input type="text" name="title" class="form-control input-sm"
                                       placeholder="標題" value="<?=(!empty($_GET['title']))?$_GET['title']:""?>">
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:180px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">分類</button>
                                </div>
                                <select class="form-control input-sm" name="category">
                                    <option value="">全部</option>
                                    <?php
                                        foreach($row_P_category as $p_category) {
                                    ?>
                                    <option value="<?=$p_category['Category_Code']?>" <?=($category == $p_category['Category_Code'])?"selected":""?>><?=$p_category['Category_Name']?></option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:250px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">建立時間</button>
                                </div>
                                <input type="text" name="s_date" class="form-control input-sm datetimepicker"
                                       placeholder="起始日" value="<?=(!empty($_GET['s_date']))?$_GET['s_date']:""?>">
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:250px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">~</button>
                                </div>
                                <input type="text" name="e_date" class="form-control input-sm datetimepicker"
                                       placeholder="結束日" value="<?=(!empty($_GET['e_date']))?$_GET['e_date']:""?>">
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:180px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">狀態</button>
                                </div>
                                <select class="form-control input-sm" name="status">
                                    <option value="all">全部</option>
                                    <option value="1" <?=($status == "1")?"selected":""?>>啟用</option>
                                    <option value="0" <?=($status == "0")?"selected":""?>>停用</option>
                                </select>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success btn-sm pull-right">送出</button>
                        <a class="btn btn-danger btn-sm pull-right" href="./list.php" style="margin-right: 10px;">清除</a>
                        <div style="float:left; display:inline-block; width:100%;margin-top: 10px;">
                            <a class="pull-right btn btn-success btn-sm" href="./detail.php?action=add">新增</a>
                            <input class="pull-right btn btn-success btn-sm" type="button" value="刪除" onclick="if(confirm('是否刪除資料？刪除後無法刪除後無法還原！')) {$('form').attr('action','./delete.php');$('form').submit();}"style="margin-right: 10px;">
                        </div>
                    </div>
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <th style="min-width:100px;"><a href="javascript:void(0)" onclick="select_all('')">(全選)</a></th>
                                    <th>標題</th>
                                    <th>大分類列表圖片</th>
                                    <th>分類</th>
                                    <th>狀態</th>
                                    <th>創建時間</th>
                                    <th>操作</th>
                                </tr>
                                <?php 
                                    foreach ($row_category as $key => $value) :
                                ?>
                                <tr onClick="iCheckThisRow(this);">
                                    <th><input type="checkbox" name="id[]" value="<?=$value["Category_Code"]?>"></th>
                                    <th><?=$value['Category_Name']?></th>
                                    <th><img src="<?=WEBSITE_URL."upload/".$value['category_pic1']?>" height="50"></th>
                                    <th><?=$value['P_category']?></th>
                                    <th><?=($value['status'] == 1)?"啟用":"停用"?></th>
                                    <th><?=$value['create_datetime']?></th>
                                    <th>
                                        <a class="btn btn-xs btn-primary" href="./detail.php?action=edit&Category_Code=<?=$value['Category_Code']?>">編輯</a>
                                    </th>
                                </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
            <?=$page?>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>