<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("sub_category_add");
$Category_Code = get_id();

$Category_Name  = filter_input(INPUT_POST, 'Category_Name');
$Category_Sub_Name  = filter_input(INPUT_POST, 'Category_Sub_Name');
$desc  = filter_input(INPUT_POST, 'desc');
$pic_alt1  = filter_input(INPUT_POST, 'alt');
$pic_alt2  = filter_input(INPUT_POST, 'alt2');
$category  = filter_input(INPUT_POST, 'category');
$sort        = filter_input(INPUT_POST, 'sort', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 99999, 'default' => 0)));
$status        = filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 99999, 'default' => 0)));

if(empty($Category_Name) || empty($_FILES['pic2']['name']) || empty($category)) {
    $err_msg = "欄位";
    if(empty($Category_Name))
        $err_field[] = "標題名稱";
    if(empty($_FILES['pic2']['name']))
        $err_field[] = "Banner圖片";
	if(empty($category))
        $err_field[] = "分類";
    $err_msg .= "'".implode("','", $err_field)."'不可為空值";
    js_go_back_self($err_msg);
    exit;
}
$img = '';
if ($_FILES['pic']['name'] != "none" && is_uploaded_file($_FILES['pic']['tmp_name']))
{
    //重組檔名
    $type = explode(".", $_FILES['pic']['name']);
    $img = $_FILES['pic']['name'];
    if(file_exists(DOCUMENT_ROOT."/upload/".$img)) {
        // js_go_back_self("檔名重複!!");
        // exit;
    }
    @copy($_FILES['pic']['tmp_name'], DOCUMENT_ROOT."/upload/".$img);
}
// else {
//     js_go_back_global("IMG_ERROR");
//     exit;
// }
if ($_FILES['pic2']['name'] != "none" && is_uploaded_file($_FILES['pic2']['tmp_name']))
{
    //重組檔名
    $type = explode(".", $_FILES['pic2']['name']);
    $img2 = $_FILES['pic2']['name'];
    if(file_exists(DOCUMENT_ROOT."/upload/".$img2)) {
        // js_go_back_self("檔名重複!!");
        // exit;
    }
    @copy($_FILES['pic2']['tmp_name'], DOCUMENT_ROOT."/upload/".$img2);
}
else {
    js_go_back_global("IMG_ERROR");
    exit;
}


$create_datetime = date("Y-m-d H:i:s");
$create_user     = $_SESSION[SESSION_VARIABLE."_user_id"];
$update_datetime = date("Y-m-d H:i:s");
$update_user     = $_SESSION[SESSION_VARIABLE."_user_id"];


$sql_array = array(
    "Category_Code"        => array("2", checkinput_sql($Category_Code, 19)),
    "Category_CodeGroup"   => array("2", "Goods_Sub_Category"),
    "Category_Name"        => array("2", checkinput_sql($Category_Name, 60)),
    "Category_Sub_Name"    => array("2", checkinput_sql($Category_Sub_Name, 60)),
    "Desc"                 => array("2", checkinput_sql($desc, 1000)),
    "status"               => array("2", checkinput_sql($status, 50)),
    "create_datetime"      => array("2", checkinput_sql($create_datetime, 50)),
    "create_user"          => array("2", checkinput_sql($create_user, 50)),
    "update_datetime"      => array("2", checkinput_sql($update_datetime, 50)),
    "update_user"          => array("2", checkinput_sql($update_user, 50)),
    "sort"                 => array("2", checkinput_sql($sort, 50)),
    "Parent_Category_Code" => array("2", checkinput_sql($category, 50)),
    "category_pic1"        => array("2", checkinput_sql($img, 200)),
    "pic_alt1"             => array("2", checkinput_sql($pic_alt1, 200)),
    "category_pic2"        => array("2", checkinput_sql($img2, 200)),
    "pic_alt2"             => array("2", checkinput_sql($pic_alt2, 200)),
);
$sql_cmd = insert("category", $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
   js_go_back_global("DB_ADD_ERROR");
   exit;
}else{
    add_log('第二層分類管理','1');
   $db->disconnect();
   js_repl_global( "./list.php", "ADD_SUCCESS");
   exit;
}
?>
