<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("sub_category");
if(isset($_GET['action']))
    $action = $_GET['action'];
else
    $action = "add";

$row_delivery["delivery_id"] = "";
$row_delivery["name"] = "";
$row["Category_Name"] = "";
$row["Category_Sub_Name"] = "";
$row["desc"] = "";
$row["category_pic1"] = "";
$row["category_pic2"] = "";
$row["pic_alt1"] = "";
$row["pic_alt2"] = "";
$row["sort"] = "";
$row["status"] = "1";
$row["Parent_Category_Code"] = "";

$sql_cmd = "select * from category where Category_CodeGroup = 'Goods_Category' and status = 1 and deleted_at is null";
$rs_category = $db->query($sql_cmd);

if($action == "edit") {
    $sql_cmd = "select * from category where Category_Code = '".checkinput_sql($_GET['Category_Code'],19)."' and deleted_at is null";
    $rs_sub_category = $db->query($sql_cmd);
    $row = $rs_sub_category->fetchRow(MDB2_FETCHMODE_ASSOC);
}

?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php") ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                第二層分類管理
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" method="POST" action="./<?=$action?>.php" enctype="multipart/form-data">
                    <div class="box-header">
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label"><a style="color:red">*</a>標題名稱</label>
                            <div class="col-sm-10">
                                <input id="name" name="Category_Name" type="text" class="form-control"
                                       value="<?=$row["Category_Name"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">英文副標</label>
                            <div class="col-sm-10">
                                <input id="name" name="Category_Sub_Name" type="text" class="form-control"
                                       value="<?=$row["Category_Sub_Name"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="desc" class="col-sm-2 control-label">簡述</label>
                            <div class="col-sm-10">
                                <textarea name="desc" class="form-control"><?=$row["desc"]?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="pic" class="col-sm-2 control-label">大分類列表圖片<br><span class="text-red">建議尺寸: 530 x 282</span></label>
                            <div class="col-sm-10">
                                <? if(!empty($row['category_pic1'])): ?>
                                    <img src="<?=WEBSITE_URL?>upload/<?=$row['category_pic1']?>" width="100" class="pic">
                                    <br><?=$row['category_pic1']?>
                                     &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<a href='javascript:void(0);' class='btn btn-danger' onclick='$(".old_pic").val("");$(".pic").hide();$(this).hide()'>刪除圖片</a>
                                <? endif ?>
                                <input id="pic" name="pic" type="file">
                                <input name="old_pic" type="hidden" value='<?=$row['category_pic1']?>' class="old_pic">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="alt" class="col-sm-2 control-label">Alt</label>
                            <div class="col-sm-10">
                                <input id="alt" name="alt" type="text" class="form-control" value="<?=$row["pic_alt1"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="pic2" class="col-sm-2 control-label"><a style="color:red">*</a>Banner圖片<br><span class="text-red">建議尺寸: 1900 x 400</span></label>
                            <div class="col-sm-10">
                                <? if(!empty($row['category_pic2'])): ?>
                                    <img src="<?=WEBSITE_URL?>upload/<?=$row['category_pic2']?>" width="100" class="pic2">
                                    <br><?=$row['category_pic2']?>
                                     &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<a href='javascript:void(0);' class='btn btn-danger' onclick='$(".old_pic2").val("");$(".pic2").hide();$(this).hide()'>刪除圖片</a>
                                <? endif ?>
                                <input id="pic2" name="pic2" type="file">
                                <input name="old_pic2" class="old_pic2" type="hidden" value='<?=$row['category_pic2']?>'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="alt2" class="col-sm-2 control-label">Alt</label>
                            <div class="col-sm-10">
                                <input id="alt2" name="alt2" type="text" class="form-control" value="<?=$row["pic_alt2"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="sort" class="col-sm-2 control-label"><a style="color:red">*</a>分類</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="category">
                                    <option value="">請選擇</option>
                                    <?php while($row_category = $rs_category->fetchRow(MDB2_FETCHMODE_ASSOC)) { ?>
                                        <option value="<?=$row_category['Category_Code']?>" <?=($row_category['Category_Code'] == $row['Parent_Category_Code']?"selected":"")?>><?=$row_category['Category_Name']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">狀態</label>
                            <div class="col-sm-10 control-text">
                                <label>
                                    <input name="status" type="radio" <?=($row['status'] == 1)?"checked":""?>
                                       value="1">上架
                                </label>
                                <label>
                                    <input name="status" type="radio" <?=($row['status'] == 0)?"checked":""?>
                                       value="0">下架
                                </label>
                            </div>
                        </div>
						<?php if($action == "edit") {?>
                        <div class="form-group">
                            <label for="sort" class="col-sm-2 control-label">建立時間</label>
                            <div class="col-sm-10 control-text">
                                <?=$row['create_datetime']?>
                            </div>
                        </div>
                        <?php }?>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-right">
                        <input type="hidden" name="Category_Code" value="<?=$row["Category_Code"]?>">
                        <a href="javascript:history.back()" class="btn btn-danger" style="margin-right: 10px;">取消，並返回上一層</a>
                        <button type="button" class="btn btn-primary" onclick="check_file();">儲存</button>
                        <input type="hidden" name="referer" value="<?=(!empty($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:"")?>">
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>