<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("ticket");

$title  = filter_input(INPUT_GET, 'title');
$used  = filter_input(INPUT_GET, 'used');
$status = filter_input(INPUT_GET, 'status');
$order_id = filter_input(INPUT_GET, 'order_id');
$s_date  = filter_input(INPUT_GET, 's_date');
$e_date  = filter_input(INPUT_GET, 'e_date');

$sql_where = "";
if(!empty($title))
    $sql_where .= " and (t.TicketList_ID = '".checkinput_sql($title,19)."' or t.Customer_ID = '".checkinput_sql($title,19)."')";
if(!empty($order_id))
    $sql_where .= " and (t.order_id = '".checkinput_sql($order_id,19)."')";
if(!empty($s_date))
    $sql_where .= " and t.create_datetime >= '".checkinput_sql($s_date,19)."'";
if(!empty($e_date))
    $sql_where .= " and t.create_datetime <= '".checkinput_sql($e_date,19)."'";
if(in_array($status, ["0","1"]))
    $sql_where .= " and t.status = '".checkinput_sql($status,3)."'";
if(in_array($used, ["1","2","3"])) {
    switch ($used) {
        case '1':
            $sql_where .= " and t.used = '1'";
            break;
        case '2':
            $sql_where .= " and (t.used = '0' and date_end > '".date("Y-m-d")."')";
            break;
        case '3':
            $sql_where .= " and (t.used = '0' and date_end <= '".date("Y-m-d")."')";
            break;
    }
}

$sql_cmd = "select t.* from ticket_list as t where t.Ticket_ID = '".$_GET['id']."' ".$sql_where." order by t.ticket_id desc ";
$rs_ticket  = $db->query($sql_cmd);
$rs[] = [
        "序號",
        "會員ID",
        "建立時間",
        "到期時間",
        "使用訂單",
        "使用狀態",
        "狀態",
    ];
while($row = $rs_ticket->fetchRow(MDB2_FETCHMODE_ASSOC)) {
    $rs[] = [
        $row['TicketList_ID'],
        $row['Customer_ID'],
        $row['create_datetime'],
        $row['date_end'],
        $row['order_id'],
        ($row['used']==1)?"已使用":"未使用",
        ($row['status']==1)?"啟用":"停用",
    ];
}
header('Content-Encoding: UTF-8');
header("Content-type: application/x-download");
header("Content-disposition: attachment; filename=ticket_list.csv");
echo arr_to_csv($rs);
?>



