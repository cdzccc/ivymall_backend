<?php
include("../config.php");

ClassJscript::islogin();
ClassJscript::isadmino("ticket_edit");
$now = date('Y-m-d H:i:s');
$ticket_id = filter_input(INPUT_POST, 'id');

$desc       = filter_input(INPUT_POST, 'editor');
$status    = filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 1, 'default' => 1)));
$alt        = filter_input(INPUT_POST, 'alt');
$send_to        = filter_input(INPUT_POST, 'option');
$create_datetime = $now;
$update_datetime = $now;
$update_user     = $_SESSION[SESSION_VARIABLE."_user_id"];
$Customer_IDS       = filter_input(INPUT_POST, 'Customer_IDS');

if($send_to == 1 && empty($Customer_IDS)) {
    $err_msg = "欄位";
    if($send_to == 1 && empty($Customer_IDS))
        $err_field[] = "會員ID";
    $err_msg .= "'".implode("','", $err_field)."'不可為空值";
    js_go_back_self($err_msg);
    exit;
}
$sql_cmd = "select * from ticket where ticket_id = '".$ticket_id."'";
$rs = $db->query($sql_cmd);
$row_ticket = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
$price = $row_ticket['price'];
$date_start = $row_ticket['date_start'];
$date_end = $row_ticket['date_end'];


if ($_FILES['pic']['name'] != "none" && is_uploaded_file($_FILES['pic']['tmp_name']))
{
    //重組檔名
    $type = explode(".", $_FILES['pic']['name']);
    $img = $_FILES['pic']['name'];
    if(file_exists(DOCUMENT_ROOT."/upload/".$img)) {
        // js_go_back_self("檔名重複!!");
        // exit;
    }
    @copy($_FILES['pic']['tmp_name'], DOCUMENT_ROOT."/upload/".$img);
}
else {
    $img = $row_ticket['pic'];
}

$sql_array = array(
    "pic"             => array("2", checkinput_sql($img, 200)),
    "alt"             => array("2", checkinput_sql($alt, 30)),
    "desc"            => array("2", checkinput_sql($desc, 9999999)),
    "update_datetime" => array("2", checkinput_sql($update_datetime, 50)),
    "update_user"     => array("2", checkinput_sql($update_user, 50)),
);
if(!empty($send_to)){
	$sql_array["send_to"] = array("2", checkinput_sql($send_to, 1));
}

$sql_cmd = update("ticket", array("ticket_id", $ticket_id), $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
   js_go_back_global("DB_EDIT_ERROR");
   exit;
}else{
    switch ($send_to) {
        case '1':
            foreach(explode(",", $Customer_IDS)  as $Customer_ID) {
                //$sql_cmd = "select * from ticket_list where Customer_ID = '".$Customer_ID."' and Ticket_ID = '".$ticket_id."'";
                //$rs = $db->query($sql_cmd);
                //if($rs->numRows() == 0) {
                    $sql_array = array(
                        "Ticket_ID"       => array("2", checkinput_sql($ticket_id, 19)),
                        "CODE"            => array("2", ""),
                        "Customer_ID"     => array("2", checkinput_sql($Customer_ID, 50)),
						"price"           => array("2", checkinput_sql($price, 5)),
						"date_start"      => array("2", checkinput_sql($date_start, 50)),
                        "date_end"        => array("2", checkinput_sql($date_end, 50)),
                        "status"          => array("2", 1),
                        "create_datetime" => array("2", checkinput_sql($create_datetime, 50)),
                    );
                    $sql_cmd = insert("ticket_list", $sql_array);
                    $db->query($sql_cmd);
                //}
            }
            break;
        
        case '2':
            $sql_cmd = "select * from customer where deleted_at is null";
            $rs = $db->query($sql_cmd);
            while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                $sql_array = array(
                    "Ticket_ID"       => array("2", checkinput_sql($ticket_id, 19)),
                    "CODE"            => array("2", ""),
                    "Customer_ID"     => array("2", checkinput_sql($row['Customer_ID'], 50)),
					"price"           => array("2", checkinput_sql($price, 5)),
					"date_start"      => array("2", checkinput_sql($date_start, 50)),
                    "date_end"        => array("2", checkinput_sql($date_end, 50)),
                    "status"          => array("2", 1),
                    "create_datetime" => array("2", checkinput_sql($create_datetime, 50)),
                );
                $sql_cmd = insert("ticket_list", $sql_array);
                $db->query($sql_cmd);
            }
            break;
        case '3':
            $sql_cmd = "select * from customer where deleted_at is null and level = 0";
            $rs = $db->query($sql_cmd);
            while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                $sql_array = array(
                    "Ticket_ID"       => array("2", checkinput_sql($ticket_id, 19)),
                    "CODE"            => array("2", ""),
                    "Customer_ID"     => array("2", checkinput_sql($row['Customer_ID'], 50)),
					"price"           => array("2", checkinput_sql($price, 5)),
					"date_start"      => array("2", checkinput_sql($date_start, 50)),
                    "date_end"        => array("2", checkinput_sql($date_end, 50)),
                    "status"          => array("2", 1),
                    "create_datetime" => array("2", checkinput_sql($create_datetime, 50)),
                );
                $sql_cmd = insert("ticket_list", $sql_array);
                $db->query($sql_cmd);
            }
            break;
        case '4':
            $sql_cmd = "select * from customer where deleted_at is null and level = 1";
            $rs = $db->query($sql_cmd);
            while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                $sql_array = array(
                    "Ticket_ID"       => array("2", checkinput_sql($ticket_id, 19)),
                    "CODE"            => array("2", ""),
                    "Customer_ID"     => array("2", checkinput_sql($row['Customer_ID'], 50)),
					"price"           => array("2", checkinput_sql($price, 5)),
					"date_start"      => array("2", checkinput_sql($date_start, 50)),
                    "date_end"        => array("2", checkinput_sql($date_end, 50)),
                    "status"          => array("2", 1),
                    "create_datetime" => array("2", checkinput_sql($create_datetime, 50)),
                );
                $sql_cmd = insert("ticket_list", $sql_array);
                $db->query($sql_cmd);
            }
            break;
        case '5':
            $sql_cmd = "select * from customer where deleted_at is null and level = 2";
            $rs = $db->query($sql_cmd);
            while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                $sql_array = array(
                    "Ticket_ID"       => array("2", checkinput_sql($ticket_id, 19)),
                    "CODE"            => array("2", ""),
                    "Customer_ID"     => array("2", checkinput_sql($row['Customer_ID'], 50)),
					"price"           => array("2", checkinput_sql($price, 5)),
					"date_start"      => array("2", checkinput_sql($date_start, 50)),
                    "date_end"        => array("2", checkinput_sql($date_end, 50)),
                    "status"          => array("2", 1),
                    "create_datetime" => array("2", checkinput_sql($create_datetime, 50)),
                );
                $sql_cmd = insert("ticket_list", $sql_array);
                $db->query($sql_cmd);
            }
            break;
        case '6':
            $sql_cmd = "select * from customer where deleted_at is null and level = 3";
            $rs = $db->query($sql_cmd);
            while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                $sql_array = array(
                    "Ticket_ID"       => array("2", checkinput_sql($ticket_id, 19)),
                    "CODE"            => array("2", ""),
                    "Customer_ID"     => array("2", checkinput_sql($row['Customer_ID'], 50)),
					"price"           => array("2", checkinput_sql($price, 5)),
					"date_start"      => array("2", checkinput_sql($date_start, 50)),
                    "date_end"        => array("2", checkinput_sql($date_end, 50)),
                    "status"          => array("2", 1),
                    "create_datetime" => array("2", checkinput_sql($create_datetime, 50)),
                );
                $sql_cmd = insert("ticket_list", $sql_array);
                $db->query($sql_cmd);
            }
            break;
    }

    // exit;
    $db->disconnect();
    $referer = filter_input(INPUT_POST, 'referer');
    $redirect = "./list.php";
    if(!empty($referer))
        $redirect = $referer;
    js_repl_global( $redirect, "EDIT_SUCCESS");
	exit;
}
?>
