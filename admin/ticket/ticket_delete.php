<?php
include("../config.php");

ClassJscript::islogin();
ClassJscript::isadmino("ticket_del");
$list_id = filter_input(INPUT_GET, 'list_id');
if(empty($list_id))
  $list_id = implode("','", filter_input(INPUT_GET, 'list_id', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY));

$sql_cmd = "select * from `ticket_list` where TicketList_ID in ('".$list_id."')";
$rs_user = $db->query($sql_cmd);

if($rs_user->numRows() >0) {
    $sql_cmd = "update `ticket_list` set deleted_at = '".date("Y-m-d H:i:s")."' where TicketList_ID in ('".$list_id."')";
    $rs = $db->query($sql_cmd);
    $pear = new PEAR();
    if ($pear->isError($rs))
    {
       js_go_back_global("DB_DEL_ERROR");
       exit;
    }else{
       $db->disconnect();
       js_repl_global( "./ticket_list.php?id=".$_GET['id'], "DEL_SUCCESS");
       exit;
    }
}
else {
    js_go_back_global("NOT_POST");
    exit;

}
?>