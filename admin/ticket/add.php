<?php
include("../config.php");

ClassJscript::islogin();
ClassJscript::isadmino("ticket_add");

$now = date('Y-m-d H:i:s');
$ticket_id = get_id();
$name         = filter_input(INPUT_POST, 'name');
$Customer_IDS = filter_input(INPUT_POST, 'Customer_IDS');
$date_start   = filter_input(INPUT_POST, 'date_start');
$date_end     = filter_input(INPUT_POST, 'date_end');
$price        = filter_input(INPUT_POST, 'price');
$consume      = filter_input(INPUT_POST, 'consume');
$desc         = filter_input(INPUT_POST, 'editor');
$status       = filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 1, 'default' => 1)));
$alt          = filter_input(INPUT_POST, 'alt');
$send_to        = filter_input(INPUT_POST, 'option');
$create_datetime = $now;
$update_datetime = $now;
$create_user     = $_SESSION[SESSION_VARIABLE."_user_id"];
$update_user     = $_SESSION[SESSION_VARIABLE."_user_id"];

if(empty($name) || empty($date_start) || empty($price)) {
    $err_msg = "欄位";
    if(empty($name))
        $err_field[] = "名稱";
    if(empty($date_start))
        $err_field[] = "啟用時間";
    if(empty($price))
        $err_field[] = "折抵金額";
    $err_msg .= "'".implode("','", $err_field)."'不可為空值";
    js_go_back_self($err_msg);
    exit;
}

if(! empty($date_end)) {
    if(strtotime($date_end) < strtotime($date_start) ) {
        js_go_back_self('到期日期時間需在啟用日期以後，才可送出');
        exit;
    }
}

if ($_FILES['pic']['name'] != "none" && is_uploaded_file($_FILES['pic']['tmp_name']))
{
    //重組檔名
    $type = explode(".", $_FILES['pic']['name']);
    $img = $_FILES['pic']['name'];
    if(file_exists(DOCUMENT_ROOT."/upload/".$img)) {
        // js_go_back_self("檔名重複!!");
        // exit;
    }
    @copy($_FILES['pic']['tmp_name'], DOCUMENT_ROOT."/upload/".$img);
}
else {
    $img = '';
}

$sql_array = array(
    "ticket_id"       => array("2", checkinput_sql($ticket_id, 19)),
    "Dealer_ID"       => array("2", ""),
    "name"            => array("2", checkinput_sql($name, 9999999)),
    "date_start"      => array("2", checkinput_sql($date_start, 50)),
    "date_end"        => array("2", checkinput_sql($date_end, 50)),
    "point"           => array("2", 0),
    "rule"            => array("2", ""),
    "desc"            => array("2", checkinput_sql($desc, 9999999)),
    "status"          => array("2", checkinput_sql($status, 1)),
    "alt"             => array("2", checkinput_sql($alt, 30)),
    "pic"             => array("2", checkinput_sql($img, 200)),
    "product"         => array("2", ""),
    "qty"             => array("2", 0),
    "create_datetime" => array("2", checkinput_sql($create_datetime, 50)),
    "create_user"     => array("2", checkinput_sql($create_user, 50)),
    "update_datetime" => array("2", checkinput_sql($update_datetime, 50)),
    "update_user"     => array("2", checkinput_sql($update_user, 50)),
);

if(! empty($price)) {
    $sql_array['price'] = array("2", checkinput_sql($price, 5));
}

if(! empty($send_to)) {
    $sql_array['send_to'] = array("2", checkinput_sql($send_to, 1));
}
if(! empty($consume)) {
    $sql_array['consume'] = array("2", checkinput_sql($consume, 10));
}

$sql_cmd = insert("ticket", $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
// echo $pear->isError($rs);die;
if ($pear->isError($rs))
{
   js_go_back_global("DB_ADD_ERROR");
   exit;
}else{
    switch ($send_to) {
        case '1':
            foreach(explode(",", $Customer_IDS)  as $Customer_ID) {
                $sql_cmd = "select * from ticket_list where Customer_ID = '".$Customer_ID."' and Ticket_ID = '".$ticket_id."'";
                $rs = $db->query($sql_cmd);
                if($rs->numRows() == 0) {
                    $sql_array = array(
                        "Ticket_ID"       => array("2", checkinput_sql($ticket_id, 19)),
                        "CODE"            => array("2", ""),
                        "Customer_ID"     => array("2", checkinput_sql($Customer_ID, 50)),
						"price"           => array("2", checkinput_sql($price, 5)),
						"date_start"      => array("2", checkinput_sql($date_start, 50)),
                        "status"          => array("2", 1),
                        "create_datetime" => array("2", checkinput_sql($create_datetime, 50)),
                    );
					if(!empty($date_end)){
						$sql_array["date_end"]= array("2", checkinput_sql($date_end, 50));
					}
                    $sql_cmd = insert("ticket_list", $sql_array);

                    //echo $sql_cmd;
					$db->query($sql_cmd);
                }
            }
            break;
        
        case '2':
            $sql_cmd = "select * from customer where deleted_at is null";
            $rs = $db->query($sql_cmd);
            while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                $sql_array = array(
					"Ticket_ID"       => array("2", checkinput_sql($ticket_id, 19)),
					"CODE"            => array("2", ""),
					"Customer_ID"     => array("2", checkinput_sql($Customer_ID, 50)),
					"price"           => array("2", checkinput_sql($price, 5)),
					"date_start"      => array("2", checkinput_sql($date_start, 50)),
					"status"          => array("2", 1),
					"create_datetime" => array("2", checkinput_sql($create_datetime, 50)),
				);
				if(!empty($date_end)){
					$sql_array["date_end"]= array("2", checkinput_sql($date_end, 50));
				}
                $sql_cmd = insert("ticket_list", $sql_array);
                $db->query($sql_cmd);
            }
            break;
        case '3':
            $sql_cmd = "select * from customer where deleted_at is null and level = 0";
            $rs = $db->query($sql_cmd);
            while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                $sql_array = array(
					"Ticket_ID"       => array("2", checkinput_sql($ticket_id, 19)),
					"CODE"            => array("2", ""),
					"Customer_ID"     => array("2", checkinput_sql($Customer_ID, 50)),
					"price"           => array("2", checkinput_sql($price, 5)),
					"date_start"      => array("2", checkinput_sql($date_start, 50)),
					"status"          => array("2", 1),
					"create_datetime" => array("2", checkinput_sql($create_datetime, 50)),
				);
				if(!empty($date_end)){
					$sql_array["date_end"]= array("2", checkinput_sql($date_end, 50));
				}
                $sql_cmd = insert("ticket_list", $sql_array);
                $db->query($sql_cmd);
            }
            break;
        case '4':
            $sql_cmd = "select * from customer where deleted_at is null and level = 1";
            $rs = $db->query($sql_cmd);
            while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                $sql_array = array(
					"Ticket_ID"       => array("2", checkinput_sql($ticket_id, 19)),
					"CODE"            => array("2", ""),
					"Customer_ID"     => array("2", checkinput_sql($Customer_ID, 50)),
					"price"           => array("2", checkinput_sql($price, 5)),
					"date_start"      => array("2", checkinput_sql($date_start, 50)),
					"status"          => array("2", 1),
					"create_datetime" => array("2", checkinput_sql($create_datetime, 50)),
				);
				if(!empty($date_end)){
					$sql_array["date_end"]= array("2", checkinput_sql($date_end, 50));
				}
                $sql_cmd = insert("ticket_list", $sql_array);
                $db->query($sql_cmd);
            }
            break;
        case '5':
            $sql_cmd = "select * from customer where deleted_at is null and level = 2";
            $rs = $db->query($sql_cmd);
            while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                $sql_array = array(
					"Ticket_ID"       => array("2", checkinput_sql($ticket_id, 19)),
					"CODE"            => array("2", ""),
					"Customer_ID"     => array("2", checkinput_sql($Customer_ID, 50)),
					"price"           => array("2", checkinput_sql($price, 5)),
					"date_start"      => array("2", checkinput_sql($date_start, 50)),
					"status"          => array("2", 1),
					"create_datetime" => array("2", checkinput_sql($create_datetime, 50)),
				);
				if(!empty($date_end)){
					$sql_array["date_end"]= array("2", checkinput_sql($date_end, 50));
				}
                $sql_cmd = insert("ticket_list", $sql_array);
                $db->query($sql_cmd);
            }
            break;
        case '6':
            $sql_cmd = "select * from customer where deleted_at is null and level = 3";
            $rs = $db->query($sql_cmd);
            while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                $sql_array = array(
					"Ticket_ID"       => array("2", checkinput_sql($ticket_id, 19)),
					"CODE"            => array("2", ""),
					"Customer_ID"     => array("2", checkinput_sql($Customer_ID, 50)),
					"price"           => array("2", checkinput_sql($price, 5)),
					"date_start"      => array("2", checkinput_sql($date_start, 50)),
					"status"          => array("2", 1),
					"create_datetime" => array("2", checkinput_sql($create_datetime, 50)),
				);
				if(!empty($date_end)){
					$sql_array["date_end"]= array("2", checkinput_sql($date_end, 50));
				}
                $sql_cmd = insert("ticket_list", $sql_array);
                $db->query($sql_cmd);
            }
            break;
    }

	// exit;
    $db->disconnect();
    js_repl_global( "./list.php", "ADD_SUCCESS");
    exit;
}
?>
