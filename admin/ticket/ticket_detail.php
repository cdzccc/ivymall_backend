<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("ticket");

if(isset($_GET['action']))
    $action = $_GET['action'];
else
    $action = "add";

$row_ticket["ticket_id"]       = "";
$row_ticket["name"]            = "";
$row_ticket["date_range"]      = "";
$row_ticket["desc"]            = "";
$row_ticket["pic"]             = "";
$row_ticket["alt"]             = "";
$row_ticket["status"]          = "";
$row_ticket["price"]           = "";
$row_ticket["qty"]             = "";
$row_ticket["order_id"]             = "";
$row_ticket["create_datetime"] = date('Y-m-d H:i:s');
$date_range = "";
if($action == "edit") {
    $sql_cmd = "select t.* from ticket_list as t where t.TicketList_ID = '".$_GET['list_id']."'";
    $rs = $db->query($sql_cmd);
    $row_ticket = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
    if($row_ticket['used'] == 1) {
        $used = "已使用";
    }
    else if($row_ticket['used'] == 0 && $row_ticket['date_end'] <= date("Y-m-d")) {
        $used = "已過期";
    }
    else {
        $used = "未使用";
    }

}
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php"); ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                折價券上稿管理
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" method="POST" action="./ticket_<?=$action?>.php" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">序號</label>
                            <div class="col-sm-10">
                                <?=$row_ticket["TicketList_ID"]?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">會員ID</label>
                            <div class="col-sm-10">
                                <?=$row_ticket["Customer_ID"]?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="price" class="col-sm-2 control-label">建立時間</label>
                            <div class="col-sm-10">
                                <?=$row_ticket["create_datetime"]?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="consume" class="col-sm-2 control-label">使用訂單</label>
                            <div class="col-sm-10">
                                <?=$row_ticket["order_id"]?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="consume" class="col-sm-2 control-label">使用狀態</label>
                            <div class="col-sm-10">
                                <?=$used?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">狀態</label>
                            <div class="col-sm-10">
                                <label>
                                    <input name="status" type="radio" <?=($row_ticket['status'] == 1)?"checked":""?>
                                       value="1">上架
                                </label>
                                <label>
                                    <input name="status" type="radio" <?=($row_ticket['status'] == 0)?"checked":""?>
                                       value="0">下架
                                </label>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-right">
                        <input type="hidden" name="id" value="<?=$row_ticket['TicketList_ID']?>">
                        <input type="hidden" name="ticket_id" value="<?=$row_ticket['Ticket_ID']?>">
                        <a href="./ticket_list.php?id=<?=$row_ticket['Ticket_ID']?>" class="btn btn-danger" style="margin-right: 10px;">取消，並返回上一層</a>
                        <button type="submit" class="btn btn-primary">儲存</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>