<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("ticket");

$title  = filter_input(INPUT_GET, 'title');
$used  = filter_input(INPUT_GET, 'used');
$status = filter_input(INPUT_GET, 'status');
$order_id = filter_input(INPUT_GET, 'order_id');
$s_date  = filter_input(INPUT_GET, 's_date');
$e_date  = filter_input(INPUT_GET, 'e_date');

$sql_where = "";
if(!empty($title))
    $sql_where .= " and (t.TicketList_ID = '".checkinput_sql($title,19)."' or t.Customer_ID = '".checkinput_sql($title,19)."')";
if(!empty($order_id))
    $sql_where .= " and (t.order_id = '".checkinput_sql($order_id,19)."')";
if(!empty($s_date))
    $sql_where .= " and t.create_datetime >= '".checkinput_sql($s_date,19)."'";
if(!empty($e_date))
    $sql_where .= " and t.create_datetime <= '".checkinput_sql($e_date,19)."'";
if(in_array($status, ["0","1"]))
    $sql_where .= " and t.status = '".checkinput_sql($status,3)."'";
if(in_array($used, ["1","2","3"])) {
    switch ($used) {
        case '1':
            $sql_where .= " and t.used = '1'";
            break;
        case '2':
            $sql_where .= " and (t.used = '0' and date_end > '".date("Y-m-d")."')";
            break;
        case '3':
            $sql_where .= " and (t.used = '0' and date_end <= '".date("Y-m-d")."')";
            break;
    }
}

//取出總筆數
$sql_cmd = "select count(*) from ticket_list as t where t.deleted_at is null and t.Ticket_ID = '".$_GET['id']."' ".$sql_where;
$rs = $db->query($sql_cmd);
$row = $rs->fetchRow();
$total = $row[0];

$mypage = new page;
$pages = $mypage->set_page($total, $PAGE_NUM);

//設定分頁的初始值
if (empty($_GET['num']))
{
    $num = 1;
}else{
    $num = intval($_GET['num']);
}


$sql_cmd = "select t.* from ticket_list as t where t. deleted_at is null and t.Ticket_ID = '".$_GET['id']."' ".$sql_where." order by t.create_datetime DESC ".$pages[$num];
$rs_dealer  = $db->query($sql_cmd);
// echo $sql_cmd;
// var_dump(mysqli_fetch_array($rs_dealer));die;
$tmp = $_GET;
unset($tmp['num']);
$val = http_build_query($tmp);
$page = $mypage->multi($total , $PAGE_NUM, $num, "./ticket_list.php", "", $val);
?>
<!DOCTYPE html>
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <?php include("../view/sidebar.php"); ?>
        <!-- /.sidebar -->
    </aside>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                折價券序號管理
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box">
                <form action="./ticket_list.php" method="GET" name="search">
                    <div class="box-header">
                        <div style="float:left; display:inline-block; width:200px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">搜尋</button>
                                </div>
                                <input type="text" name="title" class="form-control input-sm"
                                       placeholder="序號、會員ID" value="<?=(!empty($_GET['title']))?$_GET['title']:""?>">
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:200px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">訂單</button>
                                </div>
                                <input type="text" name="order_id" class="form-control input-sm"
                                       placeholder="訂單" value="<?=(!empty($_GET['order_id']))?$_GET['order_id']:""?>">
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:228px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">建立時間</button>
                                </div>
                                <input type="text" name="s_date" class="form-control input-sm datepicker"
                                       placeholder="起始日" value="<?=(!empty($_GET['s_date']))?$_GET['s_date']:""?>">
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:200px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">~</button>
                                </div>
                                <input type="text" name="e_date" class="form-control input-sm datepicker"
                                       placeholder="結束日" value="<?=(!empty($_GET['e_date']))?$_GET['e_date']:""?>">
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:180px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">使用狀態</button>
                                </div>
                                <select class="form-control input-sm" name="used">
                                    <option value="all">全部</option>
                                    <option value="1" <?=($used == "1")?"selected":""?>>已使用</option>
                                    <option value="2" <?=($used == "2")?"selected":""?>>已過期</option>
                                    <option value="3" <?=($used == "3")?"selected":""?>>未使用</option>
                                </select>
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:180px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">狀態</button>
                                </div>
                                <select class="form-control input-sm" name="status">
                                    <option value="all">全部</option>
                                    <option value="1" <?=($status == "1")?"selected":""?>>啟用</option>
                                    <option value="0" <?=($status == "0")?"selected":""?>>停用</option>
                                </select>
                            </div>
                        </div>
                        <input type="hidden" name="id" value="<?=$_GET['id']?>">

                        <button type="submit" class="btn btn-success btn-sm pull-right">送出</button>
                        <a class="btn btn-danger btn-sm pull-right" href="./list.php" style="margin-right: 10px;">清除</a>
                        <div style="float:left; display:inline-block; width:100%;margin-top: 10px;">
                            <!--<a class="pull-right btn btn-success btn-sm" href="./detail.php?action=add">新增</a>-->
							<button type="button" class="btn btn-success btn-sm pull-right" onclick="search.action = 'ticket_export.php';search.submit();">匯出csv</button>
                            <input class="pull-right btn btn-success btn-sm" type="button" value="刪除" onclick="if(confirm('是否刪除資料？刪除後無法刪除後無法還原！')) {$('form').attr('action','./ticket_delete.php');$('form').submit();}"style="margin-right: 10px;">
                        </div>

                    </div>
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <th><a href="javascript:void(0)" onclick="select_all('')">(全選)</a></th>
                                    <th>序號</th>
                                    <th>會員ID</th>
                                    <th>建立時間</th>
                                    <th>使用訂單</th>
                                    <th>使用狀態</th>
                                    <th>狀態</th>
                                    <th>操作</th>
                                </tr>
                                <?php
                                if($rs_dealer->numRows() > 0) {
                                while($row = $rs_dealer->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                                    if($row['used'] == 1) {
                                        $used = "已使用";
                                    }
                                    else if($row['used'] == 0 && !empty($row['date_end']) && $row['date_end'] <= date("Y-m-d")) {
                                        $used = "已過期";
                                    }
                                    else {
                                        $used = "未使用";
                                    }
                                ?>
                                <tr onClick="iCheckThisRow(this);">
                                    <th><input type="checkbox" name="list_id[]" value="<?=$row["TicketList_ID"]?>"><input type="hidden" name="ticket_id" value="<?=$row["Ticket_ID"]?>"></th>
                                    <th><?=$row['TicketList_ID']?></th>
                                    <th><?=$row['Customer_ID']?></th>
                                    <th><?=$row['create_datetime']?></th>
                                    <th><?=$row['order_id']?></th>
                                    <th><?=$used?></th>
                                    <th><?=($row['status']==0)?"停用":"啟用"?></th>
                                    <th>
                                        <a class="btn btn-xs btn-primary" href="./ticket_detail.php?action=edit&list_id=<?=$row['TicketList_ID']?>">編輯</a>
                                        <!--<a class="btn btn-xs btn-danger" href="javascript:void(0);" onclick="if(confirm('是否確認刪除？')){location.href='./ticket_delete.php?list_id=<?=$row["TicketList_ID"]?>&id=<?=$_GET["id"]?>';}">刪除</a>-->
                                    </th>
                                </tr>
                                <?php
                                } }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
		<?=$page?>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>
    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>