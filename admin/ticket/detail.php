<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("ticket");

if(isset($_GET['action']))
    $action = $_GET['action'];
else
    $action = "add";

$row_ticket["ticket_id"]       = "";
$row_ticket["name"]            = "";
$row_ticket["date_start"]      = "";
$row_ticket["date_end"]      = "";
$row_ticket["desc"]            = "";
$row_ticket["pic"]             = "";
$row_ticket["alt"]             = "";
$row_ticket["status"]          = "";
$row_ticket["price"]           = "";
$row_ticket["qty"]             = "";
$row_ticket["consume"]             = "";
$row_ticket["send_to"]             = "";
$row_ticket["create_datetime"] = date('Y-m-d H:i:s');
$date_range = "";
if($action == "edit") {
    $sql_cmd = "select t.* from ticket as t where t.ticket_id = '".$_GET['id']."'";
    $rs = $db->query($sql_cmd);
    $row_ticket = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
}
else{
	$row_ticket["desc"]="<li>本折價券適用於「常春藤網路書城」。</li>
						<li>本折價券可抵用之商品為常春藤網路書城上無註明「本商品不可使用價券」者。</li>
						<li>本折價券不得找零且恕不兌現，折抵金額不再開立統一發票。</li>
						<li>本折價券不得與其它折價券及「熊贈點扣點」同時使用。</li>";
}
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php"); ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                折價券上稿管理
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" method="POST" action="./<?=$action?>.php" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">名稱</label>
                            <div class="col-sm-10 control-text">
                                <?php
                                    if($action == 'add') {
                                ?>
                                <input id="dealer_name" name="name" type="text" class="form-control" placeholder=""
                                       value="<?=$row_ticket["name"]?>">
                                <?php
                                    }
                                    else {
                                        echo $row_ticket["name"];
                                    }
                                ?>

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">啟用時間</label>
                            <div class="col-sm-10 control-text">
                                <?php
                                    if($action == 'add') {
                                ?>
                                <div class="input-group">
                                    <input class="datetimepicker5" name="date_start" type="text" class="form-control pull-right rangepicker" value="<?=$row_ticket['date_start']?>">
                                </div>
                                <?php
                                    }
                                    else {
                                        echo $row_ticket['date_start'];
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">到期時間</label>
                            <div class="col-sm-10 control-text">
                                <?php
                                    if($action == 'add') {
                                ?>
                                <div class="input-group">
                                    <input class="datetimepicker5" name="date_end" type="text" class="form-control pull-right rangepicker" value="<?=$row_ticket['date_end']?>"  autocomplete="on">
                                </div>
                                <?php
                                    }
                                    else {
                                        echo $row_ticket['date_end'];
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="price" class="col-sm-2 control-label">折抵金額</label>
                            <div class="col-sm-10 control-text">
                                <?php
                                    if($action == 'add') {
                                ?>
                                <input id="price" name="price" type="text" class="form-control" placeholder="" value="<?=$row_ticket['price']?>">
                                <?php
                                    }
                                    else {
                                        echo $row_ticket['price'];
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="consume" class="col-sm-2 control-label">單次消費滿</label>
                            <div class="col-sm-10 control-text">
                                <?php
                                    if($action == 'add') {
                                ?>
                                <input id="consume" name="consume" type="text" class="form-control" placeholder="" value="<?=$row_ticket['consume']?>">
                                <?php
                                    }
                                    else {
                                        echo $row_ticket['consume'];
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">注意事項</label>
                            <div class="col-sm-10">
                                <textarea  id="editor" name="editor" class="form-control" placeholder=""><?=$row_ticket["desc"]?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">圖片</label>
                            <div class="col-sm-10">
                                <? if(!empty($row_ticket['pic'])): ?>
                                    <img src="<?=WEBSITE_URL?>upload/<?=$row_ticket["pic"]?>" width='100px'>
                                    <br><?=$row_ticket['pic']?>
                                <? endif ?>
                                <input id="pic" name="pic" type="file" class="form-control" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="price" class="col-sm-2 control-label">ALT</label>
                            <div class="col-sm-10">
                                <input id="alt" name="alt" type="text" class="form-control" placeholder="" value="<?=$row_ticket['alt']?>">
                            </div>
                        </div>
                            <label for="name" class="col-sm-2 control-label">發送對象</label>
                            <div class="col-sm-10">
                                <?php
                                    foreach ($ARRall['send_to'] as $key => $value) {
                                        if($key == 1)
                                            $show_customer = "$('.Customer_IDS').show();";
                                        else
                                            $show_customer = "$('.Customer_IDS').hide();";
                                ?>
                                <label>
                                    <input name="option" type="radio" value="<?=$key?>" onclick="<?=$show_customer?>"><?=$value?>
                                </label>
                                <?php
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="form-group Customer_IDS" style="display:none;">
                            <label for="Customer_IDS" class="col-sm-2 control-label">UID</label>
                            <div class="col-sm-10">
                                <input name="Customer_IDS" type="text" class="form-control" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="f_path" class="col-sm-2 control-label">建立時間</label>
                            <div class="col-sm-10 control-text">
                                <i class="fa fa-clock-o" aria-hidden="true"></i> <?=$row_ticket["create_datetime"]?>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-right">
                        <input type="hidden" name="id" value="<?=$row_ticket['ticket_id']?>">
                        <a href="javascript:history.back()" class="btn btn-danger" style="margin-right: 10px;">取消，並返回上一層</a>
                        <button type="button" class="btn btn-primary" onclick="check_file();">儲存</button>
                        <input type="hidden" name="referer" value="<?=(!empty($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:"")?>">
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>