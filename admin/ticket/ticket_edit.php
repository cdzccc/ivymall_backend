<?php
include("../config.php");

ClassJscript::islogin();
ClassJscript::isadmino("ticket_edit");
$now = date('Y-m-d H:i:s');
$id = filter_input(INPUT_POST, 'id');
$ticket_id = filter_input(INPUT_POST, 'ticket_id');
$status    = filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 1, 'default' => 1)));

$sql_array = array(
    "status" => array("2", checkinput_sql($status, 30)),
);

$sql_cmd = update("ticket_list", array("TicketList_ID", $id), $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
   js_go_back_global("DB_EDIT_ERROR");
   exit;
}else{
   $db->disconnect();
   js_repl_global( "./ticket_list.php?id=".$ticket_id, "EDIT_SUCCESS");
   exit;
}
?>
