<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("sales_data");

$sql_where = "";
$delivery = "";
$payment = "";
$order_status = [];
$goods_type = "";
$s_date = "";
$item_number = "";
if(!empty($_GET['payment']) && $_GET['payment'] != 'all') {
    $sql_where .= " and o.Payment = '".$_GET["payment"]."'";
    $payment = $ARRall['payment'][$_GET['payment']];
}
if(!empty($_GET['delivery']) && $_GET['delivery'] != 'all') {
    $sql_where .= " and o.delivery = '".$_GET["delivery"]."'";
    $delivery = $ARRall['delivery'][$_GET['delivery']];
}

if(!empty($_GET['order_status'])) {
    $sql_where .= " and o.Status in ('".implode("','", $_GET["order_status"])."')";
    foreach ($_GET['order_status'] as $value) {
        $order_status[] = $ARRall['order_status'][$value];
    }
}
if(!empty($_GET['goods_type']) && $_GET['goods_type'] != 'all') {
    $sql_where .= " and g.type = '".$_GET["goods_type"]."'";
    switch ($_GET['goods_type']) {
        case '1':
            $goods_type = "單本";
            break;
        case '2':
            $goods_type = "套書";
            break;
        case '3':
            $goods_type = "雜誌";
            break;
        default:
            $goods_type = "";
            break;
    }
}
if(!empty($_GET['s_date']) && !empty($_GET['e_date'])) {
    $sql_where .= " and o.create_datetime between '".$_GET['s_date']." 00:00:00' and '".$_GET['e_date']." 23:59:59'";
    $s_date = $_GET['s_date']."~".$_GET['e_date'];
}
if(!empty($_GET['item_number']) && $_GET['item_number'] != 'all') {
    $sql_where .= " and g.item_number = '".$_GET["item_number"]."'";
    $item_number = $_GET['item_number'];
}
$sql_cmd = "select 
        g.Goods_Name,
        g.item_number,
        sum(oi.Goods_Qty) as qty, 
        g.type, 
        g.sell_price,
        g.item_group,
        oi.Goods_Price,
        oi.Magazine_item_number,
        sum(o.total_price) as total_price,
        count(o.Order_ID) as total_order,
        sum(if(Payment = 1, o.total_price, 0)) as credit_price,
        sum(if(Payment = 1, 1, 0)) as credit_order,
        sum(if(Payment = 3, o.total_price, 0)) as store_price,
        sum(if(Payment = 3, 1, 0)) as store_order,
        sum(if(Payment = 2, o.total_price, 0)) as delivery_price,
        sum(if(Payment = 2, 1, 0)) as delivery_order
    from `order` as o 
    join `order_item` as oi
        on o.Order_ID = oi.Order_ID
    join goods as g 
        on g.Goods_ID = oi.goodsId
    where o.deleted_at is null ".$sql_where." group by g.Goods_ID, oi.Goods_Price";
$rs_member = $db->query($sql_cmd);
// $rs[] = [
//         "銷售總金額",
//         "訂單總筆數",
//         "線上刷卡金額",
//         "線上刷卡筆數",
//         "超商取貨付款金額",
//         "超商取貨付款筆數",
//         "貨到付款金額",
//         "貨到付款筆數",
//         "重複購買率",
//     ];
$rs[] = ["付款方式",$payment];
$rs[] = ["配送方式",$delivery];
$rs[] = ["訂單狀態",implode("、", $order_status)];
$rs[] = ["商品種類",$goods_type];
$rs[] = ["日期區間",$s_date];
$rs[] = ["商品貨號",$item_number];
$rs[] = [];

$total_price = 0;
$total_order = 0;
$credit_price = 0;
$credit_order = 0;
$store_price = 0;
$store_order = 0;
$delivery_price = 0;
$delivery_order = 0;
$repeat_buy = 0;
$repeat_buy_ratio = 0;
$customer_buy = [];
$rs[] = ["商品貨號", "商品名稱", "商品價格", "銷售數量", "備註"];

while($row = $rs_member->fetchRow(MDB2_FETCHMODE_ASSOC)) {
    //"1. 套書的組成商品，列在此欄位
//2. 雜誌的期數及規格，列在此欄位"                                  
    $remarks = "";
    switch ($row['type']) {
        case '2':
            $item_group = explode(",", $row['item_group']);
            $sql_cmd = "select * from goods where Goods_ID in ('".implode("','", $item_group)."')";
            $rs_goods = $db->query($sql_cmd);
            $goods_name = [];
            while($row_goods = $rs_goods->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                $goods_name[] = $row_goods['Goods_Name'];
            }
            $remarks = implode(",", $goods_name);
            break;
        case '3':
            $row["item_number"] .= " - " . $row["item_number"];
            break;
    }
    $rs[] = ["\t".$row["item_number"], $row["Goods_Name"], $row['Goods_Price'], $row['qty'], $remarks];

    // 銷售總金額
    $total_price += $row['total_price'];
    // 訂單總筆數
    $total_order += $row['total_order'];
    // 線上刷卡金額
    $credit_price += $row['credit_price'];
    // 線上刷卡筆數
    $credit_order += $row['credit_order'];
    // 超商取貨付款金額
    $store_price += $row['store_price'];
    // 超商取貨付款筆數
    $store_order += $row['store_order'];
    // 貨到付款金額
    $delivery_price += $row['delivery_price'];
    // 貨到付款筆數
    $delivery_order += $row['delivery_order'];
}
$rs[] = [];

    // 重複購買率
$sql_cmd = "select 
        Customer_ID,count(Customer_ID) as total
    from `order` as o 
    join `order_item` as oi
        on o.Order_ID = oi.Order_ID
    join goods as g 
        on g.Goods_ID = oi.goodsId
    where o.deleted_at is null ".$sql_where."
    group by Customer_ID
    HAVING total > 1";
$rs_member = $db->query($sql_cmd);
while($row = $rs_member->fetchRow(MDB2_FETCHMODE_ASSOC)) {
    $repeat_buy++;
}

if($repeat_buy > 0 && $total_price > 0)
    $repeat_buy_ratio = round(($repeat_buy/$total_price)*100,2);

$rs[] = ["銷售總金額",$total_price];
$rs[] = ["訂單總筆數",$total_order];
$rs[] = ["線上刷卡金額",$credit_price];
$rs[] = ["線上刷卡筆數 ",$credit_order];
$rs[] = ["超商取貨付款金額",$store_price];
$rs[] = ["超商取貨付款筆數",$store_order];
$rs[] = ["貨到付款金額",$delivery_price];
$rs[] = ["貨到付款筆數 ",$delivery_order];
$rs[] = ["重複購買率 ",$repeat_buy_ratio];
                                                                    
header('Content-Encoding: UTF-8');
header("Content-type: application/x-download");
header("Content-disposition: attachment; filename=站內銷售數據統計". date("Y-m-d") .".csv");
echo arr_to_csv($rs);
?>