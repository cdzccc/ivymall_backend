<?php
include("../config.php");

ClassJscript::islogin();
ClassJscript::isadmino("admin_add");
if(empty($_POST['name'])) {
    js_go_back_global("DATA_EMPTY");
    exit;
}

if (!empty($_POST['pwd']))
{
    if(!preg_match("/^[a-zA-Z0-9]{6,16}$/", $_POST['pwd'])) {
        js_go_back_global("PWD_ERROR");
        exit;
    }
    $pwd_out = hash('sha512',checkinput_sql($_POST['pwd'], 255));
}else{
    js_go_back_global("PASSWORD_BLANK");
    exit;
}
if (!empty($_POST['userid'])) {
    if(!preg_match("/^[a-zA-Z0-9]{4,16}$/", $_POST['userid'])) {
        js_go_back_global("ID_ERROR");
        exit;
    }

    $sql_cmd = "select * from user where delete_at is null and userid = '".$_POST['userid']."'";
    $rs = $db->query($sql_cmd);
    if($rs->numRows() > 0) {
        js_go_back_global("ID_REPEAT");
        exit;
    }
}
else {
    js_go_back_global("USER_BLANK");
    exit;
}

$sql_cmd2 = "select * from priv order by id ASC ";
$rs2 = $db->query($sql_cmd2);
$priv = "";
while($row2 = $rs2->fetchRow(MDB2_FETCHMODE_ASSOC))
{
    if (!empty($_POST[$row2['name']]) && $_POST[$row2['name']]=="y")
	{
  		$priv .= $row2['name'].",";
		if ($_POST[$row2['name']."_add"]=="y")
		{
			$priv .= $row2['name']."_add,";
		}

		if ($_POST[$row2['name']."_edit"]=="y")
		{
			$priv .= $row2['name']."_edit,";
		}

		if ($_POST[$row2['name']."_del"]=="y")
		{
			$priv .= $row2['name']."_del,";
		}
   	}
}
if(empty($priv)) {
    js_go_back_global("DATA_EMPTY");
    exit;
}
$sql_array = array(
    "userid"	=> array("2", checkinput_sql($_POST['userid'], 255)),
    "password" 	=> array("2", $pwd_out),
    "name" 		=> array("2", checkinput_sql($_POST['name'], 50)),
    "date" 		=> array("2", date("Y-m-d H:i:s")),
    "loginlock" => array("2", checkinput_sql($_POST['status'], 5)),
    "priv" 		=> array("2", $priv),
);

$sql_cmd = insert("user", $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
   js_go_back_global("DB_EDIT_ERROR");
   exit;
}else{
    add_log('管理員管理','1');
   $db->disconnect();
   js_repl_global( "./list.php", "ADD_SUCCESS");
   exit;
}
?>
