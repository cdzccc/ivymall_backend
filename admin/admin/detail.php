<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("admin");

if(isset($_GET['action']))
    $action = $_GET['action'];
else
    $action = "add";

$row_user["id"] = "";
$row_user["name"] = "";
$row_user["userid"] = "";
$row_user["priv"] = "";
$row_user["password"] = "";
$row_user["date"] = date("Y-m-d H:i:s");
$row_user["loginlock"] = 0;

if($action == "edit") {
    $sql_cmd = "select * from user where delete_at is null and id = '".checkinput_sql($_GET['id'],11)."'";
    $rs_user = $db->query($sql_cmd);
    $row_user = $rs_user->fetchRow(MDB2_FETCHMODE_ASSOC);
}

$sql_cmd = "select * from priv order by id asc";
$rs_priv = $db->query($sql_cmd);
?>
<!DOCTYPE html>
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <?php include("../view/sidebar.php"); ?>
        <!-- /.sidebar -->
    </aside>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                系統帳號
                
            </h1>
        </section>
        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" method="POST" action="./<?=$action?>.php">
                    <div class="box-body">

                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">人員名稱</label>
                            <div class="col-sm-10">
                                <input id="title" name="name" type="text" class="form-control"
                                       value="<?=$row_user["name"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">帳號</label>
                            <div class="col-sm-10">
                                <input id="title" name="userid" type="text" class="form-control"
                                       value="<?=$row_user["userid"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">密碼</label>
                            <div class="col-sm-10">
                                <input id="title" name="pwd" type="text" class="form-control" placeholder="需修改再填寫">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">狀態</label>
                            <div class="col-sm-10 control-text">
                                <label>
                                    <input name="status" type="radio" <?=($row_user['loginlock'] == 1)?"checked":""?>
                                       value="1">停用
                                </label>
                                <label>
                                    <input name="status" type="radio" <?=($row_user['loginlock'] == 0)?"checked":""?>
                                       value="0">啟用
                                </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">權限</label>
                            <div class="col-sm-10">
                                <table class="table table-bordered">
                                    <tr>
                                        <td>全選1</td>
                                        <td>
                                            <a href="javascript:void(0);" onclick="select_all('');">全選</a>
                                        </td>
                                    </tr>
                                    <?php
                                        while($row = $rs_priv->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                                            ?>
                                            <tr>
                                                <td><?=checkoutput($row['cname'])?><a href="javascript:void(0);" onclick="select_all('<?=$row['name']?>');">(全選)</a></td>
                                                <td>
                                                    <label style="width: 100px;">
                                                        <input class="<?=$row['name']?>" name="<?=$row['name']?>" type="checkbox" value="y"  <? if (preg_match("/(,|^)".$row['name']."(,|$)/", $row_user['priv'])) { echo "checked"; } ?> />
                                                        瀏覽&nbsp;&nbsp;
                                                    </label>
                                                    <label style="width: 100px;">
                                                        <input class="<?=$row['name']?>" name="<?=$row['name']?>_add" type="checkbox" value="y" <? if (preg_match("/(,|^)".$row['name']."_add(,|$)/", $row_user['priv'])) { echo "checked"; } ?> />
                                                        新增&nbsp;&nbsp;
                                                    </label>
                                                    <label style="width: 100px;">
                                                        <input class="<?=$row['name']?>" name="<?=$row['name']?>_edit" type="checkbox" value="y" <? if (preg_match("/(,|^)".$row['name']."_edit(,|$)/", $row_user['priv'])) { echo "checked"; } ?> />
                                                        修改&nbsp;&nbsp;
                                                    </label>
                                                    <label style="width: 100px;">
                                                        <input class="<?=$row['name']?>" name="<?=$row['name']?>_del" type="checkbox" value="y" <? if (preg_match("/(,|^)".$row['name']."_del(,|$)/", $row_user['priv'])) { echo "checked"; } ?> />
                                                        刪除
                                                    </label>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    ?>
                                </table>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="f_path" class="col-sm-2 control-label">建立時間</label>
                            <div class="col-sm-10" style="line-height: 32px;">
                                <i class="fa fa-clock-o" aria-hidden="true"></i> <?=$row_user['date']?>
                            </div>
                        </div>


                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-right">
                        <input type="hidden" name="id" value="<?=$row_user['id']?>">
                        <input name="old_pwd" type="hidden" value="<?=$row_user['password']?>">
                        <a href="javascript:history.back()" class="btn btn-danger" style="margin-right: 10px;">取消，並返回上一層</a>
                        <button type="submit" class="btn btn-primary">儲存</button>
                        <input type="hidden" name="referer" value="<?=(!empty($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:"")?>">
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>