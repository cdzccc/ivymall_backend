<?php
include("../config.php");

ClassJscript::islogin();
ClassJscript::isadmino("admin_edit");
if(empty($_POST['name'])) {
    js_go_back_global("DATA_EMPTY");
    exit;
}
if (!empty($_POST['pwd']))
{
	$pwd_out = hash('sha512',checkinput_sql($_POST['pwd'], 255));
}else{
    $pwd_out = $_POST['old_pwd'];
}

$sql_cmd2 = "select * from priv order by id ASC ";
$rs2 = $db->query($sql_cmd2);
$priv = "";
while($row2 = $rs2->fetchRow(MDB2_FETCHMODE_ASSOC))
{
	if (!empty($_POST[$row2['name']]) && $_POST[$row2['name']]=="y")
	{
  		$priv .= $row2['name'].",";
		if ($_POST[$row2['name']."_add"]=="y")
		{
			$priv .= $row2['name']."_add,";
		}

		if ($_POST[$row2['name']."_edit"]=="y")
		{
			$priv .= $row2['name']."_edit,";
		}

		if ($_POST[$row2['name']."_del"]=="y")
		{
			$priv .= $row2['name']."_del,";
		}
   	}
}

$sql_array = array(
    "userid"	=> array("2", checkinput_sql($_POST['userid'], 255)),
    "password" 	=> array("2", $pwd_out),
    "name" 		=> array("2", checkinput_sql($_POST['name'], 50)),
    "date" 		=> array("2", date("Y-m-d H:i:s")),
    "loginlock" => array("2", checkinput_sql($_POST['status'], 5)),
    "priv" 		=> array("2", $priv),
);

$sql_cmd = update("user", array("id", intval($_POST['id'])), $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
   js_go_back_global("DB_EDIT_ERROR");
   exit;
}else{
    add_log('管理員管理','2');
   $db->disconnect();
    $referer = filter_input(INPUT_POST, 'referer');
    $redirect = "./list.php";
    if(!empty($referer))
        $redirect = $referer;
    js_repl_global( $redirect, "EDIT_SUCCESS");
   exit;
}
?>
