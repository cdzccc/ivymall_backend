<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("store_cvs");
include DOCUMENT_ROOT.'/schedule/store/STORE_CVS/storeCVS.php';
$StoreCVS = new ClassStoreCVS();
$nowdatetime=date("Y-m-d H:i:s");

if ($_FILES['upload']['name'] != "none" && is_uploaded_file($_FILES['upload']['tmp_name']))
{
    //重組檔名
    $name = $_FILES["upload"]["name"];
    $ext = end((explode(".", $name)));
    if(!in_array($ext, ["xml"])) {
        js_go_back_self("未上傳檔案");
        exit;
    }
    $file_prefix = preg_replace("/^(F\d{2})/", "$1", $name);
    $SRPDATA=file($_FILES['upload']['tmp_name']);
    switch ($ext) {
        case 'F01':
            $StoreCVS->F01($SRPDATA);
            break;
        case 'F03':
            $StoreCVS->F03($SRPDATA);
            break;
        case 'F04':
            $StoreCVS->F04($SRPDATA);
            break;
        case 'F05':
            $StoreCVS->F05($SRPDATA);
            break;
        case 'F07':
            $StoreCVS->F07($SRPDATA);
            break;
        case 'F09':
            $StoreCVS->F09($SRPDATA);
            break;
        case 'F11':
            $StoreCVS->F11($SRPDATA);
            break;
        case 'F44':
            $StoreCVS->F44($SRPDATA);
            break;
        case 'F45':
            $StoreCVS->F45($SRPDATA);
            break;
        case 'F61':
            $StoreCVS->F61($SRPDATA);
            break;
        default:
            # code...
            break;
   }                   
}
else {
    js_go_back_self("未上傳檔案");
    exit;
}