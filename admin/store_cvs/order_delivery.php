<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("store_cvs");

$data = filter_input(INPUT_GET, 'store_update');
$Order_ID = filter_input(INPUT_GET, 'id', FILTER_DEFAULT,FILTER_REQUIRE_ARRAY);
// $Order_ID = array_keys($data);
print_r($Order_ID);
print_r($data);
$sql_cmd = "select * from `order`
            where Order_ID in ('". implode("','", $Order_ID) ."')";
$rs = $db->query($sql_cmd);
while ($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
    if($row['store_update'] == $data)
        continue;
    //if($row['store_update'] == "2")
        //continue;
    $sql_cmd = "update `order`
                set store_update = '".$data."'
                where Order_ID = '".$row['Order_ID']."'";
    $rs2 = $db->query($sql_cmd);

    if($row['delivery'] == 1) {
        if($data == 0) {
            $sql_cmd = "delete from store where order_id = '".$row["Order_ID"]."'";
            $db->query($sql_cmd);
            continue;
        }
        if($row["Payment"] == 3) {
            $servicetype = "1";
        }
        else {
            $servicetype = "3";
        }
        preg_match("/^([^(]+)\(#?([^)]+)/", $row['store_name'], $store);
        $storeid = $store[2];
        $storename = $store[1];
        $ShipDate = date("Y-m-d");
        $ReturnDate = date("Y-m-d", strtotime($ShipDate."+7 days"));
        $sql_array = array(
            "crdate"      => array("2", date("Y-m-d H:i:s")),
            "customer_id" => array("3", $row['Customer_ID']),
            "order_id"    => array("3", $row['Order_ID']),
            "storepath"   => array("2", "A"),
            "storeid"     => array("3", $storeid),
            "storename"   => array("2", $storename),
            "storeaddr"   => array("2", $row['store_addr']),
            "servicetype" => array("2", $servicetype),
            "status"      => array("2", 30),
            "ShipDate"    => array("2", $ShipDate),
            "ReturnDate"  => array("2", $ReturnDate)
        );
        $sql_cmd = insert("store", $sql_array);
        $db->query($sql_cmd);
    }
    else if($row['delivery'] == 2) {
        if($data == 0) {
            $sql_cmd = "delete from store_cvs where order_id = '".$row["Order_ID"]."'";
            $db->query($sql_cmd);
            continue;
        }
        if($row["Payment"] == 3) {
            $servicetype = "1";
        }
        else {
            $servicetype = "3";
        }
        preg_match("/^([^(]+)\(#?([^)]+)/", $row['store_name'], $store);
        $storeid = $store[2];
        $storename = $store[1];
        $ShipDate = date("Y-m-d", strtotime($ShipDate."+1 days"));
        $ReturnDate = date("Y-m-d", strtotime($ShipDate."+7 days"));
        $sql_array = array(
            "crdate"      => array("2", date("Y-m-d H:i:s")),
            "customer_id" => array("3", $row['Customer_ID']),
            "order_id"    => array("3", $row['Order_ID']),
            "storepath"   => array("2", "A"),
            "storeid"     => array("3", $storeid),
            "storename"   => array("2", $storename),
            "storeaddr"   => array("2", $row['store_addr']),
            "servicetype" => array("2", $servicetype),
            "status"      => array("2", 30),
            "ShipDate"    => array("2", $ShipDate),
            "ReturnDate"  => array("2", $ReturnDate)
        );
        $sql_cmd = insert("store_cvs", $sql_array);
        $db->query($sql_cmd);
    }
}

js_repl_global( "./list.php", "EDIT_SUCCESS");
