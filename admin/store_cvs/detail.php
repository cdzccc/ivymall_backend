<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("store_cvs");

if(isset($_GET['action']))
    $action = $_GET['action'];
else
    $action = "add";

if($action == "edit") {
    $sql_cmd = "select * from `order` as o
    left join store_cvs as s on o.Order_ID = s.order_id 
    where o.deleted_at is null and o.Order_ID = '".checkinput_sql($_GET['id'],19)."'";
    $rs = $db->query($sql_cmd);
    $row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
}

?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php") ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                便利達康超商訂單查詢
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <form class="form-horizontal" method="POST" action="./<?=$action?>.php" enctype="multipart/form-data">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-header">
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">訂單編號</label>
                            <div class="col-sm-10 control-text">
                                <?=$row["Order_ID"]?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">訂單資訊</h3>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="storeid" class="col-sm-2 control-label">門市編號</label>
                            <div class="col-sm-10">
                                <input name="storeid" type="name" class="form-control" value="<?=$row["storeid"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="storename" class="col-sm-2 control-label">門市名稱</label>
                            <div class="col-sm-10">
                                <input name="storename" type="name" class="form-control" value="<?=$row["storename"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="storeaddr" class="col-sm-2 control-label">門市地址</label>
                            <div class="col-sm-10">
                                <input name="storeaddr" type="name" class="form-control" value="<?=$row["storeaddr"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="store_location" class="col-sm-2 control-label">門市位置</label>
                            <div class="col-sm-10">
                                <select name="store_location">
                                    <option value="0" <?=($row['store_location'] == 0)?"selected":""?>>本島</option>
                                    <option value="1" <?=($row['store_location'] == 1)?"selected":""?>>外島</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label">購買人姓名</label>
                            <div class="col-sm-10">
                                <input name="name" type="name" class="form-control" value="<?=$row["name"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="phone" class="col-sm-2 control-label">購買人電話</label>
                            <div class="col-sm-10">
                                <input name="phone" type="name" class="form-control" value="<?=$row["phone"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="Delivery_Name" class="col-sm-2 control-label">收件人</label>
                            <div class="col-sm-10">
                                <input name="Delivery_Name" type="name" class="form-control" value="<?=$row["Delivery_Name"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="Delivery_Phone" class="col-sm-2 control-label">收件人手機</label>
                            <div class="col-sm-10">
                                <input name="Delivery_Mobile" type="name" class="form-control" value="<?=$row["Delivery_Mobile"]?>">
                            </div>
                        </div>
						<div class="form-group">
                            <label for="Delivery_Phone" class="col-sm-2 control-label">收件人市話</label>
                            <div class="col-sm-10">
                                <input name="Delivery_Phone" type="name" class="form-control" value="<?=$row["Delivery_Phone"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="total_price" class="col-sm-2 control-label">消費金額</label>
                            <div class="col-sm-10">
                                <input name="total_price" type="name" class="form-control" value="<?=$row["total_price"]?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-header">
                        <h3 class="box-title">訂單資訊</h3>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="Delivery_No" class="col-sm-2 control-label">配送編號</label>
                            <div class="col-sm-10">
                                <input name="Delivery_No" type="name" class="form-control" value="<?=$row["Delivery_No"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">上傳狀態</label>
                            <div class="col-sm-10">
                                <?php if($row['store_update'] != 2): ?>
                                <select name="store_update">
                                    <option value="0" <?=($row['store_update'] == 0)?"selected":""?>>尚未轉出</option>
                                    <option value="1" <?=($row['store_update'] == 1)?"selected":""?>>轉出準備中</option>
                                </select>
                                <?php else: ?>
                                    已轉出
								<input type="hidden" name="store_update" value="<?=$row['store_update']?>">
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="crdate" class="col-sm-2 control-label">轉出成功時間</label>
                            <div class="col-sm-10">
                                <?=$row["crdate"]?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="store_time" class="col-sm-2 control-label">貨到物流時間</label>
                            <div class="col-sm-10">
                                <?=$row["store_time"]?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-header">
                        <h3 class="box-title">貨態</h3>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">最新貨態</label>
                            <div class="col-sm-10 control-text">
                                <?=$row["statusstr"]?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">F11 出貨資料回覆</label>
                            <div class="col-sm-10 control-text">
                                <?=$row["F11"]?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">F03 大物流驗收檔</label>
                            <div class="col-sm-10 control-text">
                                <?=$row["F03"]?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">F44 進店即時檔</label>
                            <div class="col-sm-10 control-text">
                                <?=$row["F44"]?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">F04 進店檔</label>
                            <div class="col-sm-10 control-text">
                                <?=$row["F04"]?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">F05 取貨完成檔</label>
                            <div class="col-sm-10 control-text">
                                <?=$row["F05"]?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">F07 大物流驗退檔</label>
                            <div class="col-sm-10 control-text">
                                <?=$row["F07"]?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">F61 訂單預退檔</label>
                            <div class="col-sm-10 control-text">
                                <?=$row["F61"]?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">F09 取消出貨檔</label>
                            <div class="col-sm-10 control-text">
                                <?=$row["F09"]?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">F45 取消代收檔</label>
                            <div class="col-sm-10 control-text">
                                <?=$row["F45"]?>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-right">
                        <input type="hidden" name="id" value="<?=$row['Order_ID']?>">
                        <a href="./list.php" class="btn btn-danger" style="margin-right: 10px;">取消，並返回上一層</a>
                        <button type="submit" class="btn btn-primary">儲存</button>
                        <input type="hidden" name="referer" value="<?=(!empty($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:"")?>">
                    </div>
                    <!-- /.box-footer -->
                </div>
            </form>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>
<script>
    $(document).ready(function() {
        $(document).on('submit','form',function(){
            if($('.editor').val() != "" && $('.editor').val() != undefined && !confirm("客服回覆送出後無法修改，是否確定送出？")) {
                return false;
            }
        });
    })
</script>
</body>
</html>