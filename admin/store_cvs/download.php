<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("store_cvs");
include DOCUMENT_ROOT.'/schedule/store/STORE_CVS/storeCVS.php';

$StoreCVS = new ClassStoreCVS();
$nowdatetime=date("Y-m-d H:i:s");
$filename="F10".$StoreCVS->CVS.$StoreCVS->EC.date("Ymd").".xml";

$Order_ID = filter_input(INPUT_GET, 'id', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$sql_cmd = "SELECT count(*) AS allrow 
    FROM store_cvs AS A 
    LEFT JOIN `order` AS B ON A.order_id=B.Order_ID
    WHERE A.order_id in ('". implode("','", $Order_ID) ."') AND A.status in (30) AND B.delivery = '2' ";
$rs = $db->query($sql_cmd);
$row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
$allrow=$row['allrow'];
if($allrow<=0){
	js_go_back_self("請先轉出訂單，再進行下載");
    exit();
}
    $sql_cmd = "SELECT A.*,B.create_datetime,B.name,B.total_price,B.Delivery_Mobile, B.Delivery_Name
        FROM store_cvs AS A 
        LEFT JOIN `order` AS B ON A.order_id=B.Order_ID
        WHERE A.order_id in ('". implode("','", $Order_ID) ."') AND A.status in (30) AND B.delivery = '2' ";
    $rs = $db->query($sql_cmd); 

$rs = $db->query($sql_cmd);
$data = [];

while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
    $data[] = $row;
}
$xml = $StoreCVS->F10($data, $allrow);

header('Content-type: text/xml');
header('Content-Disposition: attachment; filename="'.$filename.'.xml"');

echo $xml;