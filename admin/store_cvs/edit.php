<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("store_cvs_edit");
$id = filter_input(INPUT_POST, 'id');

$storeid        = filter_input(INPUT_POST, 'storeid');
$storename      = filter_input(INPUT_POST, 'storename');
$storeaddr      = filter_input(INPUT_POST, 'storeaddr');
$store_location = filter_input(INPUT_POST, 'store_location');
$name           = filter_input(INPUT_POST, 'name');
$phone          = filter_input(INPUT_POST, 'phone');
$Delivery_Name  = filter_input(INPUT_POST, 'Delivery_Name');
$Delivery_Phone = filter_input(INPUT_POST, 'Delivery_Phone');
$Delivery_Mobile = filter_input(INPUT_POST, 'Delivery_Mobile');
$total_price    = filter_input(INPUT_POST, 'total_price');
$Delivery_No    = filter_input(INPUT_POST, 'Delivery_No');
$store_update   = filter_input(INPUT_POST, 'store_update');

if(empty($id)) {
    js_go_back_global("NOT_POST");
    exit;
}

if(empty($storeid)
  || empty($storename)
  || empty($storeaddr)
  || empty($name)
  || empty($phone)
  || empty($Delivery_Name)
  || empty($Delivery_Phone) && empty($Delivery_Mobile)
  || empty($total_price)
) {
    $err_msg = "欄位";
    if(empty($storeid))
        $err_field[] = "門市編號";
    if(empty($storename))
        $err_field[] = "門市名稱";
    if(empty($storeaddr))
        $err_field[] = "門市地址";
    if(empty($name))
        $err_field[] = "購買人姓名";
    if(empty($phone))
        $err_field[] = "購買人電話";
    if(empty($Delivery_Name))
        $err_field[] = "收件人";
    if(empty($Delivery_Phone)&&empty($Delivery_Mobile))
        $err_field[] = "收件人手機";
    if(empty($total_price))
        $err_field[] = "消費金額";
    $err_msg .= "'".implode("','", $err_field)."'不可為空值";
    js_go_back_self($err_msg);
    exit;
}

$sql_array = array(
    "store_name"      => array("2", checkinput_sql($storename."(#".$storeid.")", 9999999)),
    "store_addr"      => array("2", checkinput_sql($storeaddr, 500)),
    "store_location"  => array("2", checkinput_sql($store_location,2)),
    "name"            => array("2", checkinput_sql($name, 50)),
    "phone"           => array("2", checkinput_sql($phone, 50)),
    "Delivery_Name"   => array("2", checkinput_sql($Delivery_Name, 9999999)),
    "Delivery_Phone"  => array("2", checkinput_sql($Delivery_Phone, 500)),
    "Delivery_Mobile"  => array("2", checkinput_sql($Delivery_Mobile, 500)),
    "total_price"     => array("2", checkinput_sql($total_price,2)),
    "Delivery_No"     => array("2", checkinput_sql($Delivery_No, 50)),
    "store_update"    => array("2", checkinput_sql($store_update, 50)),
);
$sql_cmd = update("order", array("Order_ID", $id), $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
   js_go_back_global("DB_EDIT_ERROR");
   exit;
}else{
    $sql_cmd = "select * from `store_cvs` where order_id = '".$id."'";
    $rs = $db->query($sql_cmd);
    $row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
    $sql_array = array(
        "storeid"   => array("2", checkinput_sql($storeid, 9999999)),
        "storename" => array("2", checkinput_sql($storename, 500)),
        "storeaddr" => array("2", checkinput_sql($storeaddr,500)),
    );
    if($store_update == 2 && empty($row['store_update_time'])) {
        $sql_array['store_update_time'] = date('Y-m-d H:i:s');
    }
    
    $sql_cmd = update("store_cvs", array("order_id", $id), $sql_array);
    $rs = $db->query($sql_cmd);

    add_log('便利達康超商訂單查詢','2');
    $db->disconnect();
    $referer = filter_input(INPUT_POST, 'referer');
    $redirect = "./list.php";
    if(!empty($referer))
        $redirect = $referer;
    js_repl_global( $redirect, "EDIT_SUCCESS");
    exit;
}
?>
