<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("columncontent_add");
$id = get_id();
$name = filter_input(INPUT_POST, 'name');
$sub_name = filter_input(INPUT_POST, 'sub_name');
$category = filter_input(INPUT_POST, 'category');
$desc = filter_input(INPUT_POST, 'desc');
$content = filter_input(INPUT_POST, 'editor');
$post_time = filter_input(INPUT_POST, 'post_time');
$submitter = filter_input(INPUT_POST, 'submitter');
$author = filter_input(INPUT_POST, 'author');
$author_desc = filter_input(INPUT_POST, 'author_desc');
$author_alt = filter_input(INPUT_POST, 'author_alt');
$alt = filter_input(INPUT_POST, 'alt');
$index_alt = filter_input(INPUT_POST, 'index_alt');
$recommand1 = filter_input(INPUT_POST, 'recommand1');
$recommand2 = filter_input(INPUT_POST, 'recommand2');
$recommand3 = filter_input(INPUT_POST, 'recommand3');
$recommand4 = filter_input(INPUT_POST, 'recommand4');
$position  = filter_input(INPUT_POST, 'position');
$meta_keyword = filter_input(INPUT_POST, 'meta_keyword');
$meta_desc  = filter_input(INPUT_POST, 'meta_desc');

$pic = "";
$index_pic = "";
$media = "";
$author_img = "";

$status    = filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 1, 'default' => 0)));
$sort        = filter_input(INPUT_POST, 'sort', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 99999, 'default' => 0)));

if(empty($name) || empty($category) || empty($content) || empty($post_time) || empty($_FILES['pic']['name'])) {
    $err_msg = "欄位";
    if(empty($name))
        $err_field[] = "中文主標題";
    if(empty($category))
        $err_field[] = "分類";
    if(empty($post_time))
        $err_field[] = "文章日期";
    if(empty($content))
        $err_field[] = "內容";
    if(empty($_FILES['pic']['name']))
        $err_field[] = "列表圖片";
    $err_msg .= "'".implode("','", $err_field)."'不可為空值";
    js_go_back_self($err_msg);
    exit;
}

if ($_FILES['pic']['name'] != "none" && is_uploaded_file($_FILES['pic']['tmp_name']))
{
    //重組檔名
    $type = explode(".", $_FILES['pic']['name']);
    $img = $_FILES['pic']['name'];
    if(file_exists(DOCUMENT_ROOT."/upload/".$img)) {
        //js_go_back_self("列表圖片檔名重複!!");
        //exit;
    }
    @copy($_FILES['pic']['tmp_name'], DOCUMENT_ROOT."/upload/".$img);
}
else {
    $err_msg = "列表圖片";
    $err_msg .= "上傳失敗";
    js_go_back_self($err_msg);
    exit;
}
if ($_FILES['author_pic']['name'] != "none" && is_uploaded_file($_FILES['author_pic']['tmp_name']))
{
    //重組檔名
    $type = explode(".", $_FILES['author_pic']['name']);
    $author_pic = $_FILES['author_pic']['name'];
    if(file_exists(DOCUMENT_ROOT."/upload/".$author_pic)) {
        //js_go_back_self("作者-照片檔名重複!!");
        //exit;
    }
    @copy($_FILES['author_pic']['tmp_name'], DOCUMENT_ROOT."/upload/".$author_pic);
}
else {
    $author_pic = "";
}
if ($_FILES['index_pic']['name'] != "none" && is_uploaded_file($_FILES['index_pic']['tmp_name']))
{
    //重組檔名
    $type = explode(".", $_FILES['index_pic']['name']);
    $index_pic = $_FILES['index_pic']['name'];
    if(file_exists(DOCUMENT_ROOT."/upload/".$index_pic)) {
        //js_go_back_self("首頁列表圖片檔名重複!!");
        //exit;
    }
    @copy($_FILES['index_pic']['tmp_name'], DOCUMENT_ROOT."/upload/".$index_pic);
}
else {
    $index_pic = "";
}

if ($_FILES['media']['name'] != "none" && is_uploaded_file($_FILES['media']['tmp_name']))
{
    //重組檔名
    $type = explode(".", $_FILES['media']['name']);
    $media = $_FILES['media']['name'];
    if(file_exists(DOCUMENT_ROOT."/upload/".$media)) {
        js_go_back_self("音檔檔名重複!!");
        exit;
    }
    @copy($_FILES['media']['tmp_name'], DOCUMENT_ROOT."/upload/".$media);
}
else {
    $media = "";
}


$create_datetime = date("Y-m-d H:i:s");
$create_user     = $_SESSION[SESSION_VARIABLE."_user_id"];
$update_datetime = date("Y-m-d H:i:s");
$update_user     = $_SESSION[SESSION_VARIABLE."_user_id"];

$sql_array = array(
    "id"              => array("2", checkinput_sql($id, 19)),
    "name"            => array("2", checkinput_sql($name, 255)),
    "sub_name"        => array("2", checkinput_sql($sub_name, 255)),
    "category"        => array("2", checkinput_sql($category, 19)),
    "desc"            => array("2", checkinput_sql($desc, 255)),
    "content"         => array("2", checkinput_sql($content, 9999999)),
    "post_time"       => array("2", checkinput_sql($post_time, 30)),
    "author"          => array("2", checkinput_sql($author, 255)),
    "author_desc"     => array("2", checkinput_sql($author_desc, 900)),
    "author_alt"      => array("2", checkinput_sql($author_alt, 200)),
    "alt"             => array("2", checkinput_sql($alt, 200)),
    "index_alt"       => array("2", checkinput_sql($index_alt, 200)),
    "recommand1"      => array("2", checkinput_sql($recommand1, 19)),
    "recommand2"      => array("2", checkinput_sql($recommand2, 19)),
    "recommand3"      => array("2", checkinput_sql($recommand3, 19)),
    "recommand4"      => array("2", checkinput_sql($recommand4, 19)),
    "pic"             => array("2", checkinput_sql($img, 200)),
    "author_pic"      => array("2", checkinput_sql($author_pic, 200)),
    "index_pic"       => array("2", checkinput_sql($index_pic, 200)),
    "media"           => array("2", checkinput_sql($media, 200)),
    "status"          => array("2", checkinput_sql($status, 5)),
    "create_datetime" => array("2", checkinput_sql($create_datetime, 50)),
    "create_user"     => array("2", checkinput_sql($create_user, 50)),
    "update_datetime" => array("2", checkinput_sql($update_datetime, 50)),
    "update_user"     => array("2", checkinput_sql($update_user, 50)),
    "sort"            => array("2", checkinput_sql($sort, 5)),
    "position"        => array("2", checkinput_sql($position, 5)),
    "submitter"       => array("2", checkinput_sql($submitter, 100)),
    "meta_keyword"    => array("2", checkinput_sql($meta_keyword, 200)),
    "meta_desc"       => array("2", checkinput_sql($meta_desc, 200)),
);
$sql_cmd = insert("study_column", $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
   js_go_back_global("DB_ADD_ERROR");
   exit;
}else{
    add_log('學習專欄內容管理','1');
   $db->disconnect();
   js_repl_global( "./list.php", "ADD_SUCCESS");
   exit;
}
?>
