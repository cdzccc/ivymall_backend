<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("columncontent");

if(isset($_GET['action']))
    $action = $_GET['action'];
else
    $action = "add";

$row["id"] = "";
$row["name"] = "";
$row["sub_name"] = "";
$row["category"] = "";
$row["desc"] = "";
$row["content"] = "";
$row["post_time"] = "";
$row["author"] = "";
$row["author_desc"] = "";
$row["author_img"] = "";
$row["author_alt"] = "";
$row["submitter"] = "";
$row["media"] = "";
$row["pic"] = "";
$row["alt"] = "";
$row["index_pic"] = "";
$row["index_alt"] = "";
$row["recommand1"] = "";
$row["recommand2"] = "";
$row["recommand3"] = "";
$row["recommand4"] = "";
$row["status"] = "1";
$row['sort'] = 0;
$row["meta_keyword"] = "";
$row['meta_desc'] = "";

$sql_cmd = "select * from category where Category_CodeGroup = 'COLUMN' and status = 1 and deleted_at is null";
$rs_category = $db->query($sql_cmd);

$sql_cmd = "select * from goods where Status = 1 and deleted_at is null and type in (1,2,3)";
$rs = $db->query($sql_cmd);
$row_goods = [];
while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
    $row_goods[] = $row;
}
if($action == "edit") {
    $sql_cmd = "select * from study_column where deleted_at is null and id = '".checkinput_sql($_GET['id'],19)."'";
    $rs = $db->query($sql_cmd);
    $row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
}

?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php") ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                學習專欄內容管理
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" method="POST" action="./<?=$action?>.php" enctype="multipart/form-data">
                    <div class="box-header">
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">中文主標題</label>
                            <div class="col-sm-10">
                                <input name="name" type="text" class="form-control" value="<?=$row["name"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">英文副標題</label>
                            <div class="col-sm-10">
                                <input name="sub_name" type="text" class="form-control" value="<?=$row["sub_name"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">分類</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="category">
                                    <option value="">請選擇</option>
                                    <?php while($row_category = $rs_category->fetchRow(MDB2_FETCHMODE_ASSOC)) { ?>
                                        <option value="<?=$row_category['Category_Code']?>" <?=($row_category['Category_Code'] == $row['category']?"selected":"")?>><?=$row_category['Category_Name']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">簡述</label>
                            <div class="col-sm-10">
                                <input name="desc" type="text" class="form-control" value="<?=$row["desc"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="post_time" class="col-sm-2 control-label">文章日期</label>
                            <div class="col-sm-10">
                                <input id="post_time" name="post_time" type="text" class="datepicker form-control" value="<?=$row["post_time"]?>">
                            </div>
                        </div>                        
                        <div class="form-group">
                            <label for="author" class="col-sm-2 control-label">作者-姓名</label>
                            <div class="col-sm-10">
                                <input name="author" type="text" class="form-control" value="<?=$row["author"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="author_pic" class="col-sm-2 control-label">作者-照片上傳<br><span class="text-red">建議尺寸: 145x145</span></label>
                            <div class="col-sm-10">
                                <? if(!empty($row['author_pic'])): ?>
                                    <img src="<?=WEBSITE_URL?>upload/<?=$row['author_pic']?>" width="100" class=author_pic>
                                    <br><?=$row['author_pic']?>
                                     &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<a href='javascript:void(0);' class='btn btn-danger' onclick='$(".old_author_pic").val("");$(".author_pic").hide();$(this).hide()'>刪除圖片</a>
                                <? endif ?>
                                <input id="author_pic" name="author_pic" type="file">
                                <input name="old_author_pic" type="hidden" class="old_author_pic" value='<?=$row['author_pic']?>'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="author_alt" class="col-sm-2 control-label">Alt</label>
                            <div class="col-sm-10">
                                <input id="author_alt" name="author_alt" type="text" class="form-control" value="<?=$row["author_alt"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="author_desc" class="col-sm-2 control-label">作者-簡述</label>
                            <div class="col-sm-10">
                                <textarea class="author_desc form-control" name="author_desc" ><?=$row["author_desc"]?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="submitter" class="col-sm-2 control-label">上稿者</label>
                            <div class="col-sm-10">
                                <input name="submitter" type="text" class="form-control" value="<?=$row["submitter"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="media" class="col-sm-2 control-label">音檔上傳</label>
                            <div class="col-sm-10">
                                <? if(!empty($row['media'])): ?>
                                    <audio controls>
                                        <source src="<?=WEBSITE_URL?>upload/<?=$row['media']?>" type="audio/mpeg">
                                        Your browser does not support the audio element.
                                    </audio>
                                    <br><?=$row['media']?>
                                <? endif ?>
                                <input id="media" name="media" type="file"  accept=".mp3">
                                <input name="old_media" type="hidden" value='<?=$row['media']?>'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="pic" class="col-sm-2 control-label">列表圖片上傳<br><span class="text-red">建議尺寸: 540x280</span></label>
                            <div class="col-sm-10">
                                <? if(!empty($row['pic'])): ?>
                                    <img src="<?=WEBSITE_URL?>upload/<?=$row['pic']?>" width="100" class="pic">
                                    <br><?=$row['pic']?>
                                     &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<a href='javascript:void(0);' class='btn btn-danger' onclick='$(".old_pic").val("");$(".pic").hide();$(this).hide()'>刪除圖片</a>
                                <? endif ?>
                                <input id="pic" name="pic" type="file">
                                <input name="old_pic" type="hidden" class="old_pic" value='<?=$row['pic']?>'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="alt" class="col-sm-2 control-label">Alt</label>
                            <div class="col-sm-10">
                                <input id="alt" name="alt" type="text" class="form-control" value="<?=$row["alt"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="index_pic" class="col-sm-2 control-label">首頁列表圖片上傳<br><span class="text-red">建議尺寸: 1920x1080</span></label>
                            <div class="col-sm-10">
                                <? if(!empty($row['index_pic'])): ?>
                                    <img src="<?=WEBSITE_URL?>upload/<?=$row['index_pic']?>" width="100" class="index_pic">
                                    <br><?=$row['index_pic']?>
                                     &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<a href='javascript:void(0);' class='btn btn-danger' onclick='$(".old_index_pic").val("");$(".pic").hide();$(this).hide()'>刪除圖片</a>
                                <? endif ?>
                                <input id="index_pic" name="index_pic" type="file">
                                <input name="old_index_pic" type="hidden" value='<?=$row['index_pic']?>'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="index_alt" class="col-sm-2 control-label">Alt</label>
                            <div class="col-sm-10">
                                <input id="index_alt" name="index_alt" type="text" class="form-control" value="<?=$row["index_alt"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="editor" class="col-sm-2 control-label">內容</label>
                            <div class="col-sm-10">
                                <textarea class="editor" name="editor"><?=$row["content"]?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="recommand1" class="col-sm-2 control-label">相關推薦1</label>
                            <div class="col-sm-10">
                                <select class="select2_category_item form-control" name="recommand1" style="width:100%;">
                                    <option></option>
                                    <?php foreach($row_goods as $goods) { ?>
                                        <option value="<?=$goods['Goods_ID']?>" <?=($goods['Goods_ID'] == $row['recommand1']?"selected":"")?>>[<?=$goods['item_number']?>]<?=$goods['Goods_Name']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="recommand2" class="col-sm-2 control-label">相關推薦2</label>
                            <div class="col-sm-10">
                                <select class="select2_category_item form-control" name="recommand2" style="width:100%;">
                                    <option></option>
                                    <?php foreach($row_goods as $goods) { ?>
                                        <option value="<?=$goods['Goods_ID']?>" <?=($goods['Goods_ID'] == $row['recommand2']?"selected":"")?>>[<?=$goods['item_number']?>]<?=$goods['Goods_Name']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="recommand3" class="col-sm-2 control-label">相關推薦3</label>
                            <div class="col-sm-10">
                                <select class="select2_category_item form-control" name="recommand3" style="width:100%;">
                                    <option></option>
                                    <?php foreach($row_goods as $goods) { ?>
                                        <option value="<?=$goods['Goods_ID']?>" <?=($goods['Goods_ID'] == $row['recommand3']?"selected":"")?>>[<?=$goods['item_number']?>]<?=$goods['Goods_Name']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="recommand4" class="col-sm-2 control-label">相關推薦4</label>
                            <div class="col-sm-10">
                                <select class="select2_category_item form-control" name="recommand4" style="width:100%;">
                                    <option></option>
                                    <?php foreach($row_goods as $goods) { ?>
                                        <option value="<?=$goods['Goods_ID']?>" <?=($goods['Goods_ID'] == $row['recommand4']?"selected":"")?>>[<?=$goods['item_number']?>]<?=$goods['Goods_Name']?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="position" class="col-sm-2 control-label">顯示於首頁</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="position">
                                    <option value="">請選擇</option>
                                    <option value="l" <?=($row["position"] == "l")?"selected":""?>>左</option>
                                    <option value="t" <?=($row["position"] == "t")?"selected":""?>>中</option>
                                    <option value="r" <?=($row["position"] == "r")?"selected":""?>>右</option>
                                </select>

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="sort" class="col-sm-2 control-label">Meta Keywords</label>
                            <div class="col-sm-10">
                                <input id="sort" name="meta_keyword" type="text" class="form-control" value="<?=$row["meta_keyword"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="sort" class="col-sm-2 control-label">Meta Description</label>
                            <div class="col-sm-10">
                                <input id="sort" name="meta_desc" type="text" class="form-control" value="<?=$row["meta_desc"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="sort" class="col-sm-2 control-label">排序</label>
                            <div class="col-sm-10">
                                <input id="sort" name="sort" type="text" class="form-control" value="<?=$row["sort"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">狀態</label>
                            <div class="col-sm-10">
                                <span class="control-text">
                                    <label>
                                        <input name="status" type="radio" <?=($row['status'] == 1)?"checked":""?>
                                           value="1">上架
                                    </label>
                                    <label>
                                        <input name="status" type="radio" <?=($row['status'] == 0)?"checked":""?>
                                           value="0">下架
                                    </label>
                                </span>
                            </div>
                        </div>
                        <?php if($action == "edit") {?>
                        <div class="form-group">
                            <label for="sort" class="col-sm-2 control-label">建立時間</label>
                            <div class="col-sm-10 control-text">
                                <?=$row['create_datetime']?>
                            </div>
                        </div>
                        <?php }?>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-right">
                        <input type="hidden" name="id" value="<?=$row['id']?>">
                        <a href="javascript:history.back()" class="btn btn-danger" style="margin-right: 10px;">取消，並返回上一層</a>
                        <button type="button" class="btn btn-primary" onclick="check_file();">儲存</button>
                        <input type="hidden" name="referer" value="<?=(!empty($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:"")?>">
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>