<?php
    include "../config.php";
$file = fopen("./member.csv", "r");
// header("Content-Type:text/html;charset=big5");
$total = 0;
$error = 0;
$error_account = [];    

$i = 1;
while(!feof($file)){
    $data = __fgetcsv($file);
    if($i == 1 || empty($data[0])) {
        $i++;
        continue;
    }
    $Customer_ID = get_ids();
    $Customer_Email = $data[0];
    $Customer_PWD = $data[1];
    $name = $data[2];
    $birthday = $data[3];
    $gender = $data[4];
    $phone = $data[5];
    $Customer_Email = $data[6];
    $zip = $data[7];
    $addr = $data[8];
    $point = $data[9];
    $epaper = $data[11];
    $hash_key = $data[12];
    $fbLogin = $data[13];
    $googleLogin = $data[14];
    $level = $data[15];
    switch ($gender) {
        case '男':
            $gender = "M";
            break;
        
        default:
            $gender = "F";
            break;
    }
    $epaper1 = 0;
    if(strpos($epaper, "常春藤解析電子報") !== false) {
        $epaper1 = 1;
    }
    $epaper2 = 0;
    if(strpos($epaper, "常春藤生活電子報") !== false) {
        $epaper2 = 1;
    }
    switch ($fbLogin) {
        case '是':
            $fbLogin = 1;
            break;
        default:
            $fbLogin = 0;
            break;
    }
    switch ($googleLogin) {
        case '是':
            $googleLogin = 1;
            break;
        default:
            $googleLogin = 0;
            break;
    }
    switch ($level) {
        case '種子會員':
            $level = 0;
            break;
        case '幼苗會員':
            $level = 1;
            break;
        case '大樹會員':
            $level = 2;
            break;
        case '神木會員':
            $level = 3;
            break;
        default:
            $level = 0;
            break;
    }
    $total++;

    $sql_cmd = "select * from customer where Customer_Mail = '".checkinput_sql($Customer_Email, 255)."'";
    $rs = $db->query($sql_cmd);
    if($rs->numRows() > 0) {
        $error++;
        $error_account[] = $Customer_Email;
        continue;
    }
    $sql_array = array(
        "Customer_ID"             => array("2", checkinput_sql($Customer_ID, 255)),
        "hash_key"                => array("2", checkinput_sql($hash_key, 255)),
        "Customer_Mail"           => array("2", checkinput_sql($Customer_Email, 255)),
        "Customer_Pwd"            => array("2", checkinput_sql("cqt".md5($Customer_PWD), 255)),
        "fbLogin"                 => array("2", checkinput_sql($fbLogin, 255)),
        "googleLogin"             => array("2", checkinput_sql($googleLogin, 255)),
        "status"                  => array("2", checkinput_sql(intval(1), 255)),
        "enabled"                 => array("2", checkinput_sql(1, 255)),
        "birthday"                => array("2", checkinput_sql($birthday, 255)),
        "epaper1"                 => array("2", checkinput_sql($epaper1, 19)),
        "epaper2"                 => array("2", checkinput_sql($epaper2, 255)),
        "Contact_Email"           => array("2", checkinput_sql($Customer_Email, 200)),
        "email_verify"            => array("2", checkinput_sql(1, 30)),
        "level"                   => array("2", checkinput_sql($level, 255)),
        "level_sdate"             => array("2", checkinput_sql("", 200)),
        "level_edate"             => array("2", checkinput_sql("", 200)),
        "Customer_Name"           => array("2", checkinput_sql($name, 200)),
        "gender"                  => array("2", checkinput_sql($gender, 200)),
        "Customer_Phone"          => array("2", checkinput_sql($phone, 19)),
        "Customer_City"           => array("2", checkinput_sql(substr($addr,0,9), 19)),
        "Customer_Area"           => array("2", checkinput_sql(substr($addr,9,9), 19)),
        "Customer_Addr"           => array("2", checkinput_sql(substr($addr,18), 50)),
        "remarks"                 => array("2", checkinput_sql("", 30)),
        "Customer_Contact_Person" => array("2", checkinput_sql($name, 30)),
    );
    $sql_cmd = insert("customer", $sql_array);

    $rs = $db->query($sql_cmd);
    $pear = new PEAR();

    if ($pear->isError($rs)) {
        $error++;
        $error_account[] = $Customer_Email;
        continue;
    }

    if($point > 0) {
        $point_summary_id = get_id();
        $sql_array = array(
            "id"          => array("2", $point_summary_id),
            "Customer_ID" => array("2", $Customer_ID),
            "in"          => array("2", $point),
            "out"         => array("2", 0),
            "total"       => array("2", $point),
        );
        $sql_cmd = insert("point_summary", $sql_array);
        $rs2 = $db->query($sql_cmd);

        $point_list_id = get_id();
        $create_datetime = date("Y-m-d H:i:s");
        $enabled_date = date("Y-m-d H:i:s");
        $mode = 3;
        $type = 1;
        $sql_array = array(
            "id"           => array("2", checkinput_sql($point_list_id, 19)),
            "Customer_ID"  => array("2", checkinput_sql($Customer_ID, 200)),
            "point"        => array("2", checkinput_sql($point, 200)),
            "mode"         => array("2", checkinput_sql($mode , 45)),
            "type"         => array("2", checkinput_sql($type,2)),
            "datetime"     => array("2", checkinput_sql($create_datetime, 50)),
            "Order_ID"     => array("2", checkinput_sql("", 50)),
            "left"         => array("2", $point),
            "enabled_date" => array("2", checkinput_sql($enabled_date, 50)),
            "status"       => array("2", checkinput_sql(1, 50)),
            "end_date"     => array("2", checkinput_sql("", 50)),
        );
        $sql_cmd = insert("point_list", $sql_array);
        $db->query($sql_cmd);

    }
}
echo "共匯入".$total."筆資料<br>";
echo "失敗".$error."筆資料<br>";
echo "失敗帳號：<br>";
echo implode(",", $error_account);


function __fgetcsv(&$handle, $length = null, $d = ",", $e = '"') {
 $d = preg_quote($d);
 $e = preg_quote($e);
 $_line = "";
 $eof=false;
 while ($eof != true) {
 $_line .= (empty ($length) ? fgets($handle) : fgets($handle, $length));
 $itemcnt = preg_match_all('/' . $e . '/', $_line, $dummy);
 if ($itemcnt % 2 == 0){
 $eof = true;
 }
 }
 
 $_csv_line = preg_replace('/(?: |[ ])?$/', $d, trim($_line));
 
 $_csv_pattern = '/(' . $e . '[^' . $e . ']*(?:' . $e . $e . '[^' . $e . ']*)*' . $e . '|[^' . $d . ']*)' . $d . '/';
 preg_match_all($_csv_pattern, $_csv_line, $_csv_matches);
 $_csv_data = $_csv_matches[1];
 
 for ($_csv_i = 0; $_csv_i < count($_csv_data); $_csv_i++) {
 $_csv_data[$_csv_i] = preg_replace("/^" . $e . "(.*)" . $e . "$/s", "$1", $_csv_data[$_csv_i]);
 $_csv_data[$_csv_i] = str_replace($e . $e, $e, $_csv_data[$_csv_i]);
 $_csv_data[$_csv_i] = mb_convert_encoding( $_csv_data[$_csv_i],"utf-8","big5");
 }
 
 return empty ($_line) ? false : $_csv_data;
}