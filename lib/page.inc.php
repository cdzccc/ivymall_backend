<?php
/******************************************************************
    File Name       : page.inc.php
    Version         : 2.1
    Date            : 2009/03/04
    Modifier Date   : 2014/09/01
    Copyright       : Alex Chen
    Description     : page 物件檔
    Program Manager : Alex Chen
    Program Modifier: Jason Shih
********************************************************************/
//傳入總筆數,分頁數
class page
{
	var $page_array = array();

	//設定分頁
	function set_page( $total, $page_count )
	{
        if(!empty($_GET['ppc']))
            $page_count = $_GET['ppc'];
        #取餘數
        $check = $total % $page_count;
        $pages = $total / $page_count;
        $star = 0;
        $end  = $page_count;
        //如果有餘數則要多加一頁
        if ( $check ) { $pages=$pages+1; }

        /* 如果頁數為0則將頁數設為1頁 by Jason 2014/09/01 */
        if ($pages == 0) {
            $pages = 1;
        }
        for ($i=1;$i<=$pages;$i++)
        {
            $this->page_array[$i] = "limit $star,$page_count";
            $star = $end;
            $end = $end+$page_count;
        }
        // print_r($this->page_array);die;
        return $this->page_array;
	}


	function multi($num , $perpage, $curr_page, $mpurl, $css, $val)
	{
        $query_string = $_GET;
        unset($query_string['ppc']);
        $query_string = http_build_query($query_string);

        $query_string = (empty($query_string))?"":$query_string."&";
        $selected1 = '';
        $selected10 = '';
        $selected30 = '';
        $selected50 = '';
        $selected100 = '';
        $selected200 = '';
        if(!empty($_GET['ppc'])) {
            $perpage = $_GET['ppc'];
            if ($perpage == 1) {
                $selected1= ' selected';
            }elseif ($perpage == 10) {
                $selected10= ' selected';
            }elseif ($perpage == 30) {
                $selected30= ' selected';
            }elseif ($perpage == 50) {
                $selected50= ' selected';
            }elseif ($perpage == 100) {
                $selected100= ' selected';
            }elseif ($perpage == 200) {
                $selected100= ' selected';
            }
        }
        else
            $selected10 = ' selected';

	    $multipage = "<div class=\"dataTables_paginate paging_simple_numbers\" id=\"example2_paginate\">
                <ul class=\"pagination\" style='width:100%;'>";
        $multipage .= '<div class="col-sm-5"><div class="dataTables_info" role="status" aria-live="polite">
                    全部 '.$num.' 筆資料 / 每頁顯示
                    <select name="ppc" id="ppc" onchange="location.replace(\''.$_SERVER['PHP_SELF'].'?'.$query_string.'ppc=\' + this.value);">
                        <option value="1"'.$selected1.'>1</option>
                        <option value="10"'.$selected10.'>10</option>
                        <option value="30"'.$selected30.'>30</option>
                        <option value="50"'.$selected50.'>50</option>
                        <option value="100"'.$selected100.'>100</option>
                        <option value="200"'.$selected200.'>200</option>
                    </select>
                    筆
                </div></div>';
	   if ($num > $perpage)
	   {
			$page  =  10 ;
			$offset  =  2 ;

			$pages  =   ceil ( $num   /   $perpage );
			$from   =   $curr_page   -   $offset ;
			$to   =   $curr_page   +   $page   -   $offset   -   1 ;

			if ( $page   >   $pages )
			{
				$from   =   1 ;
				$to   =   $pages ;
			}else{
				if ( $from   <   1 )
				{
					$to   =   $curr_page   +   1   -   $from ;
					$from   =   1 ;
					if (( $to   -   $from )  <   $page   &&  ( $to   -   $from )  <   $pages )
					{
						$to   =   $page ;
					}
				}elseif ( $to   >   $pages )
				{
					$from   =   $curr_page   -   $pages   +   $to ;
					$to   =   $pages ;
					if (( $to   -   $from )  <   $page   &&  ( $to  -  $from )  <   $pages )
					{
						  $from   =   $pages   -   $page   +   1 ;
					}
				}
			}

			//判斷有無css
			if ($css)
			{
				$style = "class=\"".$css."\"";
			}
			else
            {
                $style = "";
            }
            if($from == $curr_page)
            {
                $disabled = "disabled";
            }
            else {
                $disabled = "";
            }
			$multipage .= "<li class=\"paginate_button previous ".$disabled."\" id=\"example2_previous\">
                        <a href=\"".$mpurl."?num=".($curr_page-1).'&'.$val."\" aria-controls=\"example2\" data-dt-idx=\"0\" tabindex=\"0\">Previous</a>
                    </li>";
			for ( $i = $from ;  $i   <=   $to ;  $i ++ )
			{
				if ( $i == $curr_page )
				{
				    $active = "active";
				}else{
				    $active = "";
				}
                $multipage .="<li class=\"paginate_button ".$active."\" id=\"example2_previous\">
                        <a href=\"".$mpurl."?num=".$i.'&'.$val."\"  aria-controls=\"example2\" data-dt-idx=\"0\" tabindex=\"0\"> $i </a>
                        </li>";
            }
           if($to == $curr_page)
           {
               $disabled = "disabled";
           }
           else {
               $disabled = "";
           }

           $multipage .= "<li class=\"paginate_button previous ".$disabled."\" id=\"example2_previous\">
                        <a href=\"".$mpurl."?num=".($curr_page+1).'&'.$val."\" aria-controls=\"example2\" data-dt-idx=\"0\" tabindex=\"0\">Next</a>
                    </li>";

           $multipage .= "</ul>
            </div>";

        }

		return  $multipage ;
	}
	function pager($pages,$num,$url,$val) {
        $count = count( $pages );
        for ($j=1;$j<=$count;$j++)
        {
            $pager['PAGE_LIST'][$j] = $url."?num=".$j.$val;
        }
        $prev = $num-1;
        $next = $num+1;

        $pager['PREV_PAGE']  = "#";
        if ($num!=1)
        {
            $pager['PREV_PAGE'] = $url."?num=$prev$val";
        }

        $pager['NEXT_PAGE']  = "#";
        if ($num!=$count)
        {
            $pager['NEXT_PAGE'] = $url."?num=$next$val";
        }

        $pager['FIRST_PAGE'] = $url."?num=1$val";
        $pager['END_PAGE'] = $url."?num=$count$val";
        $pager['CURR_PAGE'] = $num;
        $pager['TOTAL_PAGE'] = $count;
        return $pager;
    }
}
?>