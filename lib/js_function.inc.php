<?
#回上一頁函數(使用自己的訊息函數)
function js_go_back( $message ) 
{
  GLOBAL $MESSAGE_INC;
  include_once("$MESSAGE_INC");
  echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
        <script language=\"javascript\">
          alert(\"$message_array[$message]\") ;
          window.history.back() ;
        </script>
  " ;
}

#重新讀取函數(使用自己的訊息函數)
function js_repl( $url, $message )
{
  GLOBAL $MESSAGE_INC;
  include_once("$MESSAGE_INC");
  echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
        <script language=\"javascript\">
          alert( \"$message_array[$message]\" ) ;
          window.location.replace(\"$url\");
        </script>
  " ;
}

#回上一頁函數(使用通用的訊息函數)
function js_go_back_global( $message,$lang = "tw" ) 
{
  GLOBAL $MESSAGE_GLOBAL_INC;
  include_once("$MESSAGE_GLOBAL_INC");
  if ($lang == "en") {
    $alert_msg = $message_global_array_en[$message];
  }
  else {
    $alert_msg = $message_global_array[$message];
  }
  echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
        <script language=\"javascript\">
          alert(\"$alert_msg\") ;
          window.history.back() ;
        </script>
  " ;
}

#重新讀取函數(使用自己的訊息函數)
function js_repl_global( $url, $message,$lang="tw" )
{
  GLOBAL $MESSAGE_GLOBAL_INC;
  include_once("$MESSAGE_GLOBAL_INC");
  if ($lang == "en") {
    $alert_msg = $message_global_array_en[$message];
  }
  else {
    $alert_msg = $message_global_array[$message];
  }
  echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
        <script language=\"javascript\">
          alert(\"$alert_msg\") ;
          window.location.replace(\"$url\");
        </script>
  " ;
}


function js_top_global( $url, $message )
{
  GLOBAL $MESSAGE_GLOBAL_INC;
  include_once("$MESSAGE_GLOBAL_INC");
  echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
  		<script language=\"javascript\">
          alert( \"$message_global_array[$message]\" ) ;
          window.top.location.href = '$url';
        </script>
  " ;
}
#回上一頁函數
function js_go_back_self( $message,$lang = "tw" ) 
{
  echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
        <script language=\"javascript\">
          alert(\"$message\") ;
          window.history.back() ;
        </script>
  " ;
}
#回上一頁函數
function js_go_back_width_post_self( $message) 
{
  echo '<form id="myForm" action="./detail.php" method="post">';
    foreach ($_POST as $a => $b) {
        if(is_array($b)) {
          foreach($b as $key=> $item) {
            echo '<input type="hidden" name="'.htmlentities($a).'[]" value="'.htmlentities($item).'">';
          }
        }
        else {
          echo '<input type="hidden" name="'.htmlentities($a).'" value="'.htmlentities($b).'">';
        }
    }
  echo '</form>';
  echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
        <script language=\"javascript\">
          alert(\"$message\") ;
          document.getElementById('myForm').submit();
        </script>
  " ;
}
#重新讀取函數
function js_repl_self( $url, $message )
{
  GLOBAL $MESSAGE_INC;
  include_once("$MESSAGE_INC");
  echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
        <script language=\"javascript\">
          alert( \"$message\" ) ;
          window.location.replace(\"$url\");
        </script>
  " ;
}

#檢查email 函數
function check_mail( $email )
{
  #檢查格式
  if ( preg_match("/^[a-zA-Z0-9-_~.]+@[a-zA-Z0-9-_~.]+\.[a-zA-Z]+$/", $email) )
  {
    return true ;
  }
  else {
    return false ;
  }
}
#檢查email 函數
function check_pwd( $pwd )
{
  #檢查格式
  if (preg_match("/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,12}$/", $pwd) )
  {
    return true ;
  }
  else {
    return false ;
  }
}

?>