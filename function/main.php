<?php
    //判断远程文件 
    function check_remote_file_exists($url) { 
        $curl = curl_init($url);
        // 不取回数据 
        curl_setopt($curl, CURLOPT_NOBODY, true);
        // 发送请求 
        $result = curl_exec($curl);
        $found = false;
        // 如果请求没有发送失败
        if ($result !== false) {
            // 再检查http响应码是否为200
            $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            if ($statusCode == 200) {
                $found = true; 
            } 
        } 
        curl_close($curl); 
        return $found; 
    }
    function check_sql_injection() {
        global $_SERVER;
        if (preg_match("/select|union/i", $_SERVER["QUERY_STRING"])) {
            $file_name = str_replace("/", "_", $_SERVER["PHP_SELF"]);
            $file_name = str_replace(".php", "", $file_name);
            $log = new Logging();
            $log->lcfile("SqlInjection",$file_name);
            $log->lwrite('REMOTE_ADDR : '.$_SERVER["REMOTE_ADDR"]);
            $log->lwrite('SERVER_NAME : '.$_SERVER["SERVER_NAME"]);
            $log->lwrite('REQUEST_URI : '.$_SERVER["REQUEST_URI"]);
            $log->lwrite('');
            $log->lclose();
            sleep(1);
        }
    }
    function curl($url,$method,$data) {
        switch ($method) {
            case 'post':
                $curlPost = $data;
                break;
            case 'get':
                $url .= "?".$data;
                $curlPost = "";
                break;
            default:
                $curlPost = "";
        }

        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL,$url);
        curl_setopt( $ch, CURLOPT_HEADER, 0);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt( $ch, CURLOPT_POST, 1);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $curlPost);
        curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST,0);
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER,0);
        $result = curl_exec($ch);
        $curl_getinfo = curl_getinfo($ch);
        $rtn["result"] = $result;
        $rtn["curl_getinfo"] = $curl_getinfo;
        curl_close($ch);
        return $rtn;
    }
    function curling($url, $postData = null, $header = null, $opts = null) 
    {
        $request_header = array(
                "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36",
                "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"
        );
        
        $parameter = array(
                CURLOPT_URL     => $url,
                CURLOPT_HEADER  => False,  // get response header
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_FOLLOWLOCATION => True, // 有轉頁的話則跟著轉頁
        );
        
        if(!empty($header)){
            $request_header = array_merge($request_header,$header); // 有自訂 header 則 append
        }
        
        $parameter[CURLOPT_HTTPHEADER] = $request_header;
        
        if(!empty($opts)){
            $parameter = $parameter + $opts; // 有自訂其他參數則 append
        }
        if (!empty($postData)) {
            $parameter[CURLOPT_POST] = true;
            $parameter[CURLOPT_POSTFIELDS] = $postData;
        }
        
        $curl = curl_init();
        curl_setopt_array($curl, $parameter);
        $html = curl_exec($curl);
        // $http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE); // 這可以拿到 http status code
        
        curl_close($curl);
        
        return $html;
    }
    /** 
     * Truncates text.
     *
     * Cuts a string to the length of $length and replaces the last characters
     * with the ending if the text is longer than length.
     *
     * modify by Jason Shih at 2016-01-20
     * support Chinese characters
     * 
     * @param string $text String to truncate.
     * @param integer $length Length of returned string, including ellipsis.
     * @param string $ending Ending to be appended to the trimmed string.
     * @param boolean $exact If false, $text will not be cut mid-word
     * @param boolean $considerHtml If true, HTML tags would be handled correctly
     * @return string Trimmed string.
     */
    function truncate($text, $length = 100, $ending = '...', $exact = true, $considerHtml = false) {
        if ($considerHtml) {
        // if the plain text is shorter than the maximum length, return the whole text
        if (mb_strlen(preg_replace('/<.*?>/', '', $text),"UTF-8") <= $length) {
            return $text;
        }

        // splits all html-tags to scanable lines
        preg_match_all('/(<.+?>)?([^<>]*)/s', $text, $lines, PREG_SET_ORDER);

        $total_length = mb_strlen($ending,"UTF-8");
        $open_tags = array();
        $truncate = '';

        foreach ($lines as $line_matchings) {
            // if there is any html-tag in this line, handle it and add it (uncounted) to the output
            if (!empty($line_matchings[1])) {
            // if it’s an “empty element” with or without xhtml-conform closing slash (f.e.)
            if (preg_match('/^<(\s*.+?\/\s*|\s*(img|br|input|hr|area|base|basefont|col|frame|isindex|link|meta|param)(\s.+?)?)>$/is', $line_matchings[1])) {
                // do nothing
                // if tag is a closing tag (f.e.)
            } else if (preg_match('/^<\s*\/([^\s]+?)\s*>$/s', $line_matchings[1], $tag_matchings)) {
                // delete tag from $open_tags list
                $pos = array_search($tag_matchings[1], $open_tags);
                if ($pos !== false) {
                    unset($open_tags[$pos]);
                }
                // if tag is an opening tag (f.e. )
            } else if (preg_match('/^<\s*([^\s>!]+).*?>$/s', $line_matchings[1], $tag_matchings)) {
                // add tag to the beginning of $open_tags list
                array_unshift($open_tags, strtolower($tag_matchings[1]));
            }
            // add html-tag to $truncate’d text
            $truncate .= $line_matchings[1];
        }

        // calculate the length of the plain text part of the line; handle entities as one character
        $content_length = mb_strlen(preg_replace('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|&#x[0-9a-f]{1,6};/i', ' ', $line_matchings[2]),"UTF-8");
        if ($total_length+$content_length > $length) {
            // the number of characters which are left
            $left = $length - $total_length;
            $entities_length = 0;
            // search for html entities
            if (preg_match_all('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|&#x[0-9a-f]{1,6};/i', $line_matchings[2], $entities, PREG_OFFSET_CAPTURE)) {
                // calculate the real length of all entities in the legal range
                foreach ($entities[0] as $entity) {
                    if ($entity[1]+1-$entities_length <= $left) {
                        $left--;
                        $entities_length += mb_strlen($entity[0],"UTF-8");
                    } else {
                        // no more characters left
                        break;
                    }
                }
            }
            $truncate .= mb_substr($line_matchings[2], 0, $left+$entities_length,"UTF-8");
            // maximum lenght is reached, so get off the loop
            break;
            } else {
                $truncate .= $line_matchings[2];
                $total_length += $content_length;
            }

            // if the maximum length is reached, get off the loop
            if($total_length >= $length) {
                break;
                }
            }
        } else {
            if (mb_strlen($text,"UTF-8") <= $length) {
                return $text;
            } else {
                $truncate = mb_substr($text, 0, $length - mb_strlen($ending,"UTF-8"),"UTF-8");
            }
        }

        // if the words shouldn't be cut in the middle...
        if (!$exact) {
            // ...search the last occurance of a space...
            $spacepos = strrpos($truncate, ' ');
            if (isset($spacepos)) {
                // ...and cut the text in this position
                $truncate = mb_substr($truncate, 0, $spacepos,"UTF-8");
            }
        }

        // add the defined ending to the text
        $truncate .= $ending;

        if($considerHtml) {
            // close all unclosed html-tags
            foreach ($open_tags as $tag) {
                $truncate .= '';
            }
        }

        return $truncate;

    }
    function location($address) {
        // get location
        $curlPost = "";
        $url = "http://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($address)."&sensor=false";

        $storer = curl_init();
        curl_setopt($storer, CURLOPT_URL,$url);
        curl_setopt($storer, CURLOPT_HEADER, 0);
        curl_setopt($storer, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($storer, CURLOPT_POST, 1);
        curl_setopt($storer, CURLOPT_POSTFIELDS, $curlPost);
        $location_result = curl_exec($storer);
        curl_close($storer);    
        $location_result = json_decode($location_result,true);
        $Location["Latitude"] = 0;
        $Location["Longitude"] = 0;
        if (!empty($location_result["results"]["geometry"]["location"]["lat"])) {
            $Location["Latitude"] = $location_result["results"]["geometry"]["location"]["lat"];
        }
        if (!empty($location_result["results"]["geometry"]["location"]["lng"])) {
            $Location["Longitude"] = $location_result["results"]["geometry"]["location"]["lng"];
        }
        return $Location;
    }
    function get_id() {
        list($usec, $sec) = explode(" ", microtime());
        $msec=substr(($usec*1000000),0,5);
        $id   = date("YmdHis").$msec;
        return $id;
    }
    function get_ids() {
        list($usec, $sec) = explode(" ", microtime());
        $msec=substr(($usec*1000000),0,3);
        $id   = date("YmdHis").$msec;
        return $id;
    }
    function order_status($status) {
        //1:下單成功/2:取消/3:出貨中/4:已到貨
        $msg = array(
            1 => "下單成功",
            2 => "取消",
            3 => "出貨中",
            4 => "已到貨",
            5 => "退貨中",
            6 => "退貨成功",
        );
        if(isset($msg[$status]))
            return $msg[$status];
        else
            return "";
    }
    function payment($payment) {
        $msg = array(
            1 => "轉帳",
            2 => "信用卡",
        );
        if(isset($msg[$payment]))
            return $msg[$payment];
        else
            return "";
    }

    function add_log($item,$action) {
        global $db;
        $create_datetime = date("Y-m-d H:i:s");
        $create_user     = $_SESSION[SESSION_VARIABLE."_user_id"];
        $sql_array = array(
            "item"            => array("2", $item),
            "action"          => array("2", $action),
            "create_datetime" => array("2", checkinput_sql($create_datetime, 50)),
            "create_user"     => array("2", checkinput_sql($create_user, 50)),
        );
        $sql_cmd = insert("action_log", $sql_array);
        $rs = $db->query($sql_cmd);
    }
    function arr_to_csv($data) {
        $rs = "\xEF\xBB\xBF";
        if(is_array($data)) {
            foreach ($data as $key => $row) {
                $rs .= "\"".implode("\",\"", $row)."\"\r\n";
            }
        }
        return $rs;
    }
    function dateDiff($startTime, $endTime) {
        $start = strtotime($startTime);
        $end = strtotime($endTime);
        $timeDiff = $end - $start;
        return floor($timeDiff / (60 * 60 * 24));
    }

    function level_up_gift($level, $Customer_ID) {
        global $db;
        $date_start = date("Y-m-d");
        $date_end = date("Y-m-d",strtotime("+ 90 days"));
        $create_datetime = date("Y-m-d H:i:s");
        switch($level) {
            case "2":
                $price = "100";
            break;
            case "3":
                $price = "150";
            break;
            default:
                $price = "0";

        }
        if($price > 0) {

        }
        $sql_array = array(
            "Ticket_ID"       => array("2", ""),
            "CODE"            => array("2", ""),
            "Customer_ID"     => array("2", checkinput_sql($Customer_ID, 50)),
            "date_end"        => array("2", checkinput_sql($date_end, 50)),
            "date_start"      => array("2", checkinput_sql($date_start, 50)),
            "status"          => array("2", 1),
            "mode"          => array("2", 2),
            "create_datetime" => array("2", checkinput_sql($create_datetime, 50)),
            "price"           => array("2", $price),
            "used"           => array("2", 0),
            "name"           => array("2", "升等禮 - ".$price."元"),
        );
        $sql_cmd = insert("ticket_list", $sql_array);
        $db->query($sql_cmd);
        
        $log->lwrite('Customer_ID - '.$Customer_ID);
        $log->lwrite('name - '."升等禮 - ".$price."元");
    }
?>