<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("email_info_edit");
$id = filter_input(INPUT_POST, 'id');

$name   = filter_input(INPUT_POST, 'name');
$email  = filter_input(INPUT_POST, 'email');
$option = filter_input(INPUT_POST, 'option', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$status = filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 1, 'default' => 0)));


if(empty($id)) {
    js_go_back_global("NOT_POST");
    exit;
}

if(empty($name) || empty($option)) {
    js_go_back_global("DATA_EMPTY");
    exit;
}

if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    js_go_back_global("EMAIL_ERROR");
    exit;
}


$update_datetime = date("Y-m-d H:i:s");
$update_user     = $_SESSION[SESSION_VARIABLE."_user_id"];

$sql_array = array(
    "name"            => array("2", checkinput_sql($name, 200)),
    "email"           => array("2", checkinput_sql($email, 200)),
    "option"          => array("2", checkinput_sql(implode(",", $option) , 45)),
    "status"          => array("2", checkinput_sql($status,2)),
    "update_datetime" => array("2", checkinput_sql($update_datetime, 50)),
    "update_user"     => array("2", checkinput_sql($update_user, 50)),
);
$sql_cmd = update("mail_info", array("id", $id), $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
   js_go_back_global("DB_EDIT_ERROR");
   exit;
}else{
    add_log('聯絡我們收信人管理','2');
   $db->disconnect();
   js_repl_global( "./list.php", "EDIT_SUCCESS");
   exit;
}
?>
