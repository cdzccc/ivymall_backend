<?php
include "../../config.php";
    /* 點數取消 
        規則：「熊贈點」使用期限為365天
        執行時間：每日
    */
    $log = new Logging();
    $category = "schedule";
    $filename = "point_cancel";
    $log->lcfile($category,$filename);
    $log->lwrite('開始執行 - '.date('Y-m-d H:i:s'));

    $year = date("Y",strtotime("-2 year"));
    $sql_cmd = "select * from point_summary where total > 0";
    $rs = $db->query($sql_cmd);
    $log->lwrite('總筆數 - '.$rs->numRows());
    while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
        // 撈取一年前取得的點數
        echo $sql_cmd = "select sum(p.point) as total 
            from point_list as p
            left join `order` as o on p.Order_ID = o.Order_ID
            where p.Customer_ID = '".$row['Customer_ID']."' 
                and p.type = 1 
                and (
                    p.datetime <= '".date("Y-m-d H:i:s", strtotime(" -1 years "))."' 
                    or p.end_date <= '".date("Y-m-d H:i:s")."' 
                )
                and (o.Status is null or o.Status != 9)
         ";
        $rs_list = $db->query($sql_cmd);
        $point_get = $rs_list->fetchRow(MDB2_FETCHMODE_ASSOC)["total"];
        // 比對一年前取得的點數大於總使用的點數時，判定為有未使用的點數
        $log->lwrite('ID - '.$row['Customer_ID']);
        $log->lwrite('get - '.$point_get);
        $log->lwrite('used - '.$row['out']);
		
		//扣掉重複的
		echo $sql_cmd_type2 = "select sum(point) as total_type2
            from point_list
            where Customer_ID = '".$row['Customer_ID']."' 
                and type = 2
                and (
                    datetime <= '".date("Y-m-d H:i:s", strtotime(" -1 years "))."' 
                    or end_date <= '".date("Y-m-d H:i:s")."' 
                )";
        $rs_list_type2 = $db->query($sql_cmd_type2);
        $point_type2 = $rs_list_type2->fetchRow(MDB2_FETCHMODE_ASSOC)["total_type2"];
		
		echo "<br>point_get:".$point_get."<br>";
		echo "<br>row['out']:".$row['out']."<br>";
		echo "<br>point_type2:".$point_type2."<br>";
        if($point_get > $row['out']) {
            $point_cancel = $point_get - $row['out'] - $point_type2;
            $point_out = $row['out'] + $point_cancel;
            $point_total =  $row['total'] - $point_cancel;
			$point_total = $point_total < 0 ? 0 : $point_total;
			if($point_cancel!=0){
				$sql_cmd = "update point_summary set `out` = ".$point_out.",total = ".$point_total." where Customer_ID = '".$row['Customer_ID']."'";
				$db->query($sql_cmd);
				$point_list_id = get_id();
				$sql_array = array(
					"id"           => array("2", $point_list_id),
					"Customer_ID"  => array("2", $row['Customer_ID']),
					"point"        => array("2", $point_cancel),
					"type"         => array("2", 2),
					"datetime"     => array("2", date("Y-m-d H:i:s")),
					//"enabled_date"     => array("2", date("Y-m-d H:i:s")),
					//"end_date"     => array("2", date("Y-m-d H:i:s")),
					"Order_ID"     => array("2", ""),
					"left"         => array("2", $point_total),
					"mode"         => array("2", "7"),
					"status"       => array("2", "1"),
				);
				$sql_cmd = insert("point_list", $sql_array);
				$rs2 = $db->query($sql_cmd);
			}
        }
    }