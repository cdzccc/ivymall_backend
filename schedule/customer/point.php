<?php
include "../../config.php";
    $log = new Logging();
    $category = "schedule";
    $filename = "point";
    $log->lcfile($category,$filename);
    $log->lwrite('開始執行 - '.date('Y-m-d H:i:s'));
    $now = date("Y-m-d H:i:s");
    $sql_cmd = "select * from point_list where status = 0 and type = 1 and enabled_date <= '".$now."'";
    $rs = $db->query($sql_cmd);
    while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
        $sql_cmd = "select * from point_summary where Customer_ID = '".$row['Customer_ID']."'";
        $rs_summary = $db->query($sql_cmd);
        $point = $row['point'];
        if($rs_summary->numRows() > 0) {
            $row_point = $rs_summary->fetchRow(MDB2_FETCHMODE_ASSOC);
            $point_total = $row_point['total'] + $point;
            $point_in = $row_point['in'] + $point;
            $point_out = $row_point['out'];

            $sql_cmd = "update point_summary set `in` = ".$point_in.", `out` = ".$point_out.", total = ".$point_total." where Customer_ID = '".$row['Customer_ID']."'";
            $rs2 = $db->query($sql_cmd);
        }
        else {
            $point_summary_id = get_id();
            $sql_array = array(
                "id"          => array("2", $point_summary_id),
                "Customer_ID" => array("2", $row['Customer_ID']),
                "in"          => array("2", $point),
                "out"         => array("2", 0),
                "total"       => array("2", $point),
            );
            $sql_cmd = insert("point_summary", $sql_array);
            $point_total = $point;
            $rs2 = $db->query($sql_cmd);
        }
        $sql_cmd = "update point_list set status = 1, `left` = '".$point_total."' where id = '".$row['id']."'";
        $db->query($sql_cmd);
    }