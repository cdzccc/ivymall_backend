<?php
include "../../config.php";
    /* 生日禮
        規則：當月壽星給予點數
        執行時間：每月一號
    */
	 $log = new Logging();
    $category = "schedule";
    $filename = "birthday";
    $log->lcfile($category,$filename);
    $log->lwrite('開始執行 - '.date('Y-m-d H:i:s'));
	
	
    $sql_cmd = "SELECT * from customer WHERE DATE_FORMAT(birthday, '%m') = DATE_FORMAT(now(), '%m')";
    $rs = $db->query($sql_cmd);
    $date_start = date("Y-m-d");
    $date_end = date("Y-m-d",strtotime("+ 90 days"));
    $create_datetime = date("Y-m-d H:i:s");
    while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
        switch($row['level']) {
            case "0":
            case "1":
                $price = "50";
            break;
            case "2":
                $price = "100";
            break;
            case "3":
                $price = "200";
            break;

        }
        $sql_array = array(
            "Ticket_ID"       => array("2", ""),
            "CODE"            => array("2", ""),
            "Customer_ID"     => array("2", checkinput_sql($row['Customer_ID'], 50)),
            "date_end"        => array("2", checkinput_sql($date_end, 50)),
            "date_start"      => array("2", checkinput_sql($date_start, 50)),
            "status"          => array("2", 1),
            "mode"          => array("2", 2),
            "create_datetime" => array("2", checkinput_sql($create_datetime, 50)),
            "price"           => array("2", $price),
            "used"           => array("2", 0),
            "name"           => array("2", $price."元 生日折價券"),
        );
        $sql_cmd = insert("ticket_list", $sql_array);
        $db->query($sql_cmd);
		
		$log->lwrite('Customer_ID - '.$row['Customer_ID']);
        $log->lwrite('name - '.$price."元 生日折價券");
		
		
		if (!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/", $row['Customer_Mail'])) {
			$account=""; 
		}else{
			$account=$row['Customer_Mail'];
		}

        $datas = [
            "title"    => "生日好禮通知函",
            "type"     => 6,
            "mail"     => $account,
            "point"    => $price,
            "date_end" => $date_end
        ];
        ClassMail::send_mail($datas);
    }