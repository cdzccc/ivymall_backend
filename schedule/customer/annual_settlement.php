<?php
include "../../config.php";
    /* 
        種子會員 - 完成註冊即成為常春藤會員，且獲得入會禮 50 元折價券
        幼苗會員 - 有效消費次數1次以上
        大樹會員 - 每年度累積有效消費金額達1,500元以上
        神木會員 - 每年度累積有效消費金額達4,500元以上

        • 消費統計期間：前年度的 12 月 1 日至當年度 11 月 30 日
        • 年度等級公佈日：每年 1 月 1 日
        • 等級有效期間：年度公布日起至當年度 12 月 31 日止
\    */
	/*$sql_cmd = "SELECT o.Customer_ID, 
                    count(o.Customer_ID) as total_order, 
                    sum(o.total_price) as  total_price,
                    c.level
                from `order` as o 
                join customer as c
                    on o.Customer_ID = c.Customer_ID
                WHERE 
                    o.create_datetime between (
                        DATE_FORMAT(DATE_SUB(curdate(), INTERVAL 1 YEAR),'%Y-12-01')
                        and DATE_FORMAT(curdate(),'%Y-11-30')
                    )
                    and o.status in (7,8) 
                group by o.Customer_ID";*/
	
	//會員降等
	echo $sql_cmd = "select * from customer where level_edate = '".date("Y")."-12-31 23:59:59"."'";
	$rs = $db->query($sql_cmd);
	while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
		//當年度沒有升過等的資料才進行續等或降等
		echo $row['level_edate']."----".date("Y")."-12-31 23:59:59";
		if($row['level_edate']==date("Y")."-12-31 23:59:59"){
			echo "****";
			$new_level = get_new_level($row['level'], $row['total_order'], $row['year_consumption_b']);
			update_customer_level($row['Customer_ID'], $new_level);
		}
    }
	
						
    /*echo $sql_cmd = "SELECT o.Customer_ID, 
                    count(o.Customer_ID) as total_order, 
                    sum(o.total_price) as  total_price,
                    c.level,
					c.level_edate
                from `order` as o 
                join customer as c
                    on o.Customer_ID = c.Customer_ID
                WHERE 
                    o.create_datetime between '".(date("Y")-1)."-12-01'
                        and '".(date("Y"))."-11-30'
                    and o.status in (7,8) 
					and c.level >=2
                group by o.Customer_ID";
    $rs = $db->query($sql_cmd);
    while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
		//當年度沒有升過等的資料才進行續等或降等
		echo $row['level_edate']."----".date("Y")."-12-31 23:59:59";
		if($row['level_edate']==date("Y")."-12-31 23:59:59"){
			echo "****";
			$new_level = get_new_level($row['level'], $row['total_order'], 0);
			update_customer_level($row['Customer_ID'], $new_level);
		}
    }*/

    function get_new_level($level, $order, $total_price) {
        if($total_price >= '4500') {
            $new_level = 3;
        }
        else if($total_price >= '1500') {
            $new_level = 2;
        }
        else {
            $new_level = 1;
        }

        return $new_level;
    }
	
    function update_customer_level($Customer_ID, $level) {
        global $db;
		if($level>=2)
			$sql_cmd = "update customer set level = '".$level."',level_sdate = '".date("Y-m-d H:i:s")."', `level_edate` = '".(date("Y")+1)."-12-31 23:59:59' where Customer_ID = '".$Customer_ID."'";
        else
			$sql_cmd = "update customer set level = '".$level."',level_sdate = '".date("Y-m-d H:i:s")."', `level_edate` = '' where Customer_ID = '".$Customer_ID."'";
		$db->query($sql_cmd);
    }