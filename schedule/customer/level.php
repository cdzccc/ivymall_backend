<?php
include "../../config.php";
    /* 
        種子會員 - 完成註冊即成為常春藤會員，且獲得入會禮 50 元折價券
        幼苗會員 - 有效消費次數1次以上
        大樹會員 - 每年度累積有效消費金額達1,500元以上
        神木會員 - 每年度累積有效消費金額達4,500元以上

        • 消費統計期間：前年度的 12 月 1 日至當年度 11 月 30 日
        • 年度等級公佈日：每年 1 月 1 日
        • 等級有效期間：年度公布日起至當年度 12 月 31 日止
        • 提早升等：若於每月結算日前達成升等條件，將於次月 1 日更新會員等級，異動均以「會員專區」內呈現為準。
    */
    echo $sql_cmd = "SELECT o.Customer_ID, 
                    count(o.Customer_ID) as total_order, 
                    sum(o.total_price) as  total_price,
                    c.level
                from `order` as o 
                join customer as c
                    on o.Customer_ID = c.Customer_ID
                WHERE 
                    o.create_datetime between '".(date("Y")-1)."-12-01'
                        and '".(date("Y"))."-11-30'
                    and o.status in (7,8) 
                group by o.Customer_ID";
	/*$sql_cmd = "SELECT o.Customer_ID, 
                    count(o.Customer_ID) as total_order, 
                    sum(o.total_price) as  total_price,
                    c.level
                from `order` as o 
                join customer as c
                    on o.Customer_ID = c.Customer_ID
                WHERE 
                    o.create_datetime between (
                        DATE_FORMAT(DATE_SUB(curdate(), INTERVAL 1 YEAR),'%Y-12-01')
                        and DATE_FORMAT(curdate(),'%Y-11-30')
                    )
                    and o.status in (7,8) 
                group by o.Customer_ID";
				*/
    $rs = $db->query($sql_cmd);
    while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
        $new_level = get_new_level($row['level'], $row['total_order'], $row['total_price']);
		
		echo $new_level;
		echo $row['level'];
		if($new_level>$row['level']){

			update_customer_level($row['Customer_ID'], $new_level);
            level_up_gift($new_level, $row['Customer_ID']);
		}
    }

    function get_new_level($level, $order, $total_price) {
        if($total_price >= '4500') {
            $new_level = 3;
        }
        else if($total_price >= '1500') {
            $new_level = 2;
        }
        else if($order > 0) {
            $new_level = 1;
        }
        return max($level,$new_level);
    }
    function update_customer_level($Customer_ID, $level) {
        global $db;
        $sql_cmd = "update customer set level = '".$level."',level_sdate = '".date("Y-m-d H:i:s")."', `level_edate` = '".(date("Y")+1)."-12-31 23:59:59' where Customer_ID = '".$Customer_ID."'";
		
        $db->query($sql_cmd);
    }