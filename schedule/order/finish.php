<?php
include "../../config.php";
    /* 
        訂單狀態7,8
        7日後給予點數，票券
    */
    $sql_cmd = "select a.*,b.Customer_Mail, b.level from `order` as a
        join customer as b on a.Customer_ID = b.Customer_ID
        where a.deleted_at is null and a.Status in (7,8) and a.delivery_at between ('".date("Y-m-d 00:00:00", strtotime("-7 days"))."' and '".date("Y-m-d 23:59:59", strtotime("-7 days"))."') and a.is_finish = 0";
    $rs = $db->query($sql_cmd);
    $create_datetime = date("Y-m-d H:i:s");

    while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
        if($row["point_get"] > 0) {
            $sql_cmd = "select * from point_summary where Customer_ID = '".$row['Customer_ID']."'";
            $rs_summary = $db->query($sql_cmd);
            $point = $row["point_get"];
            if($rs_summary->numRows() > 0) {
                $row_point = $rs_summary->fetchRow(MDB2_FETCHMODE_ASSOC);
                $point_total = $row_point['total'] + $point;
                $point_in = $row_point['in'] + $point;
                $point_out = $row_point['out'];
                $sql_cmd = "update point_summary set `in` = ".$point_in.", `out` = ".$point_out.", total = ".$point_total." where Customer_ID = '".$row['Customer_ID']."'";
                $rs2 = $db->query($sql_cmd);
            }
            else {
                $point_summary_id = get_id();
                $sql_array = array(
                    "id"          => array("2", $point_summary_id),
                    "Customer_ID" => array("2", $row['Customer_ID']),
                    "in"          => array("2", $point),
                    "out"         => array("2", 0),
                    "total"       => array("2", $point),
                );
                $sql_cmd = insert("point_summary", $sql_array);
                $rs2 = $db->query($sql_cmd);
                $row_point['total'] = 0;
            }
            $point_list_id = get_id();
            $enabled_date = date("Y-m-d H:i:s");
            $end_date = date("Y-m-d H:i:s", strtotime("+1 years"));
            $sql_array = array(
                "id"           => array("2", checkinput_sql($point_list_id, 19)),
                "Customer_ID"  => array("2", checkinput_sql($row['Customer_ID'], 200)),
                "point"        => array("2", checkinput_sql($row["point_get"], 200)),
                "mode"         => array("2", checkinput_sql(2 , 45)),
                "type"         => array("2", checkinput_sql(1,2)),
                "datetime"     => array("2", checkinput_sql($create_datetime, 50)),
                "Order_ID"     => array("2", checkinput_sql("", 50)),
                "left"         => array("2", $row_point['total']+$point),
                "enabled_date" => array("2", checkinput_sql($enabled_date, 50)),
                "status"       => array("2", checkinput_sql(1, 50)),
                "end_date"     => array("2", checkinput_sql($end_date, 50)),
                "level"        => array("2", checkinput_sql($row['level'], 50)),
            );
            $sql_cmd = insert("point_list", $sql_array);
            $db->query($sql_cmd);
        }
        $sql_cmd = "select * from goods_event 
            where 
                type = 6 
                and value1 >= ".$row['total_price']." 
                and status = 1 
                and sdate <= '".$row['create_datetime']."' 
                and edate >= '".$row['create_datetime']."'
                order by value1 desc limit 1";
        $rs_event = $db->query($sql_cmd);
        if($rs_event->numRows() > 0) {
            $row_event = $rs_event->fetchRow(MDB2_FETCHMODE_ASSOC);
            $sql_cmd = "select * from ticket where ticket_id = '".$row_event['ticket_id']."'";
            $rs_ticket = $db->query($sql_cmd);
            $row_ticket = $rs_ticket->fetchRow(MDB2_FETCHMODE_ASSOC);
            $sql_array = array(
                "Ticket_ID"       => array("2", checkinput_sql($row_ticket['ticket_id'], 19)),
                "CODE"            => array("2", ""),
                "Customer_ID"     => array("2", checkinput_sql($row['Customer_ID'], 50)),
                "price"           => array("2", checkinput_sql($row_ticket['price'], 5)),
                "date_start"      => array("2", checkinput_sql($row_ticket['date_start'], 50)),
                "date_end"        => array("2", checkinput_sql($row_ticket['date_end'], 50)),
                "status"          => array("2", 1),
                "create_datetime" => array("2", checkinput_sql($create_datetime, 50)),
            );
            $sql_cmd = insert("ticket_list", $sql_array);
            $db->query($sql_cmd);
        }

        //首購
        $sql_cmd = "select * from `order` where Status in (8,12,13) and Order_ID != '".$row['Order_ID']."' and Customer_ID = '".$row['Customer_ID']."'";
        $rs = $db->query($sql_cmd);
        if($rs->numRows() == 0) {
            $sql_cmd = "select * from point_summary where Customer_ID = '".$row['Customer_ID']."'";
            $rs_summary = $db->query($sql_cmd);
            $point_get = 30;
            $row_point = $rs_summary->fetchRow(MDB2_FETCHMODE_ASSOC);
            $point_total = $row_point['total'] + $point_get;
            $point_in = $row_point['in'] + $point_get;
            $point_out = $row_point['out'];
            $sql_cmd = "update point_summary set `in` = ".$point_in.", `out` = ".$point_out.", total = ".$point_total." where Customer_ID = '".$row['Customer_ID']."'";
            $rs2 = $db->query($sql_cmd);
            $point_list_id = get_id();
            $enabled_date = date("Y-m-d H:i:s");
            $end_date = date("Y-m-d H:i:s", strtotime("+1 years"));
            $sql_array = array(
                "id"           => array("2", checkinput_sql($point_list_id, 19)),
                "Customer_ID"  => array("2", checkinput_sql($row['Customer_ID'], 200)),
                "point"        => array("2", checkinput_sql($row["point_get"], 200)),
                "mode"         => array("2", checkinput_sql(1, 45)),
                "type"         => array("2", checkinput_sql(1, 1)),
                "datetime"     => array("2", checkinput_sql(date("Y-m-d H:i:s"), 50)),
                "Order_ID"     => array("2", checkinput_sql($row['Order_ID'], 50)),
                "left"         => array("2", $row_point['total']+$point_get),
                "enabled_date" => array("2", checkinput_sql($enabled_date, 50)),
                "status"       => array("2", checkinput_sql(1, 50)),
                "end_date"     => array("2", checkinput_sql($end_date, 50)),
                "level"        => array("2", checkinput_sql($row['level'], 50)),
            );
            $sql_cmd = insert("point_list", $sql_array);
            $db->query($sql_cmd);
        }

        $sql_cmd = "update `customer` set order_count = (order_count + 1), year_consumption = (year_consumption + ".$row['total_price']."),total_consumption = (total_consumption + ".$row['total_price'].") where Customer_ID = '".$row['Customer_ID']."'";
        $db->query($sql_cmd);
		$sql_cmd = "update `order` set is_finish = '1' where Order_ID = '".$row['Order_ID']."'";
        $db->query($sql_cmd);
    }