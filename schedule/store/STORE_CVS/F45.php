<?php
    include "../../../config.php"; 
    $nowdate=date("Y-m-d");
    $nowdatetime=date("Y-m-d H:i:s");
    include DOCUMENT_ROOT."/class/class-ftp-implicit-ssl-tls.php";
    $StoreCVS = new ClassStoreCVS();
    $FTP_Implicit_SSL = new FTP_Implicit_SSL($StoreCVS->store_ftpuser, $StoreCVS->store_ftppass, $StoreCVS->store_ftpserver, $StoreCVS->store_port, '', true);
    $StoreCVS = new ClassStoreCVS();
    $filename="F45".$StoreCVS->EC.$StoreCVS->CVS.str_replace('-','',$nowdate).".xml";

    $fplog=fopen('./log/storelog.log','a');
    // if((!$ftp_connid) || (!$ftp_logins)){
    //     fwrite($fplog,$nowdatetime.'(F45)連接到超商ftp伺服器失敗!'."\r\n");
    // }else{
    //     ftp_pasv($ftp_connid,true);
    //     ftp_chdir($ftp_connid,'');
        $ftp_upload=$FTP_Implicit_SSL->download("/F45/".$filename,'.');
        if($ftp_upload==false){
            fwrite($fplog,$nowdatetime.'(F45)F45檔案下載失敗!'."\r\n");
        }else{
            $doc = new DOMDocument();
            $content = file_get_contents('./F45/'.$filename);
            $doc->loadXML($content);
            $books = $doc->getElementsByTagName( "F45CONTENT" );
            foreach( $books as $book ){
                $ShipmentNos = $book->getElementsByTagName( "ODNO" );
                $ShipmentNo = $ShipmentNos->item(0)->nodeValue;
                $RTDTs = $book->getElementsByTagName( "RTDT" );
                $RTDT = $RTDTs->item(0)->nodeValue;
                $sql_array['status']= array("2",intval(70));
                $sql_array['F45']= array("3","實際取貨代收日期：".date("Y-m-d",strtotime($RTDT)));
                $sql_cmd = update("store_cvs", array("id", intval($ShipmentNo)), $sql_array);
                $rs = $db->query($sql_cmd);

                $sql_cmd = "select * from store_cvs where id = '".intval($ShipmentNo)."'";
                $rs = $db->query($sql_cmd);
                $row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);

                $sql_cmd = "select * from `order` where Order_ID = '".$row['order_id']."'";
                $rs_order = $db->query($sql_cmd);
                $row_order = $rs_order->fetchRow(MDB2_FETCHMODE_ASSOC);
                // 到店發mail
                $datas = [
                    "title"   => "商品到店",
                    "type"    => 3,
                    "mail"    => $row_order['email'],
                ];
                ClassMail::send_mail($datas);

                $sql_cmd = "update `order` set status = 5 where Order_ID = '".$row['order_id']."'";
                $db->query($sql_cmd);
            }
            fwrite($fplog,$nowdatetime.'(F45)F45檔案下載解析成功!'."\r\n");
        }
    // }
    // ftp_close($ftp_connid); //断开
    Fclose($fplog);