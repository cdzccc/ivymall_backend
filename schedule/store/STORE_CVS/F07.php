﻿<?php
    include "../../../config.php"; 
    $nowdate=date("Y-m-d");
    $nowdatetime=date("Y-m-d H:i:s");
    include './storeCVS.php';
    include DOCUMENT_ROOT."/class/class-ftp-implicit-ssl-tls.php";
    $StoreCVS = new ClassStoreCVS();
    $FTP_Implicit_SSL = new FTP_Implicit_SSL($StoreCVS->store_ftpuser, $StoreCVS->store_ftppass, $StoreCVS->store_ftpserver, $StoreCVS->store_port, '', true);
    $filename="F07".$StoreCVS->ECNO.$StoreCVS->CVS.str_replace('-','',$nowdate).".xml";

    $fplog=fopen('./log/storelog.log','a');
    // if((!$ftp_connid) || (!$ftp_logins)){
    //     fwrite($fplog,$nowdatetime.'(F07)連接到超商ftp伺服器失敗!'."\r\n");
    // }else{
    //     ftp_pasv($ftp_connid,true);
    //     ftp_chdir($ftp_connid,'');
        $ftp_upload=$FTP_Implicit_SSL->download("/F07/".$filename,'.');
        if($ftp_upload==false){
            fwrite($fplog,$nowdatetime.'(F07)F07檔案下載失敗!'."\r\n");
        }else{
            $isError = false;
            $content = "便利達康回覆：以下訂單已退回至大智通物流中心。<br>";
            $doc = new DOMDocument();
            $content = file_get_contents('./F07/'.$filename);
            $doc->loadXML($content);
            $books = $doc->getElementsByTagName( "F07CONTENT" );
            foreach( $books as $book ){
                $ShipmentNos = $book->getElementsByTagName( "ODNO" );
                $ShipmentNo = $ShipmentNos->item(0)->nodeValue;
                $RET_Ms = $book->getElementsByTagName( "RET_M" );
                $RET_M = $RET_Ms->item(0)->nodeValue;
                $RTDCDTs = $book->getElementsByTagName( "RTDCDT" );
                $RTDCDT = $RTDCDTs->item(0)->nodeValue;

                $sql_array['status']= array("2",intval(140));
                $sql_array['statusstr']= array("3","[".$RET_M."]".$StoreCVS->store_DOCerror($RET_M));
                $sql_array['F07']= array("3","代碼：".$RET_M."\n描述：".$StoreCVS->store_DOCerror($RET_M)."\n大物流實際退日：".date("Y-m-d",strtotime($RTDCDT)));
                $sql_cmd = update("store_cvs", array("id", intval($ShipmentNo)), $sql_array);
                $rs = $db->query($sql_cmd);

                $sql_cmd = "select * from store_cvs where id = '".intval($ShipmentNo)."'";
                $rs = $db->query($sql_cmd);
                
                $isError = true;
                $content .= "訂單編號：".$row['order_id']."<br>大物流實際驗退日：".$RTDCDT."<br>退貨原因：".$RET_M."：".$StoreCVS->store_DOCerror($RET_M);

            }
            if($isError) {
                $sql_cmd = "select * from var where type = 'CVS_FTP'";
                $rs = $db->query($sql_cmd);
                $email = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)['value'];
				$email = explode(",",$email);
				foreach($email as $value) {
					$datas = [
						"title"   => "超商退回大物流回報通知",
						"content" => $content,
						"type"    => 99,
						"mail"    => $value,
					];
					ClassMail::send_mail($datas);
				}
            }

            fwrite($fplog,$nowdatetime.'(F07)F07檔案下載解析成功!'."\r\n");
        }
    // }
    // ftp_close($ftp_connid); //断开
    Fclose($fplog);