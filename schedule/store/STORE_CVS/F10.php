<?php
    include "../../../config.php"; 
    $nowdate=date("Y-m-d");
    $nowdatetime=date("Y-m-d H:i:s");
    include './storeCVS.php';
    $StoreCVS = new ClassStoreCVS();
    $sql_cmd = "SELECT count(*) AS allrow 
        FROM store_cvs AS A 
        LEFT JOIN `order` AS B ON A.order_id=B.Order_ID
        WHERE 
            ShipDate='".$nowdate."' AND 
            A.status in (30) AND 
            B.delivery = '2' ";
    $rs = $db->query($sql_cmd);
    $row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
    $allrow=$row['allrow'];
    if($allrow<=0){
        // exit();
    }
    if($allrow>0){

		echo $sql_cmd = "SELECT A.*,B.create_datetime,B.name,B.total_price,B.Delivery_Mobile, B.Delivery_Name
			FROM store_cvs AS A 
			LEFT JOIN `order` AS B ON A.order_id=B.Order_ID
			WHERE 
				ShipDate='".$nowdate."' AND 
				A.status in (30) AND 
				B.delivery = '2' ";
		$rs = $db->query($sql_cmd); 
		
		$filename="F10".$StoreCVS->CVS.$StoreCVS->ECNO.date("YmdHis", strtotime($nowdatetime)).".xml";
		$xmlfile="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
		$xmlfile.="<ORDER_DOC>\n";
			while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
					if (!empty($row["Delivery_Mobile"])) {
						$mobile = substr($row["Delivery_Mobile"],-3);
					}
					else {
						$mobile = "";
					}
					$xmlfile.="<ORDER>\n";
					$xmlfile.="<ECNO>".$StoreCVS->ECNO."</ECNO>\n";
					$xmlfile.="<ODNO>".substr(($row['id']+100000000000),1,11)."</ODNO>\n";
					$xmlfile.="<STNO>".$row['storeid']."</STNO>\n";
					$xmlfile.="<AMT>".$row['total_price']."</AMT>\n";
					$xmlfile.="<CUTKNM><![CDATA[".$row['Delivery_Name']."]]></CUTKNM>\n";
					$xmlfile.="<CUTKTL>".$mobile."</CUTKTL>\n";
					$xmlfile.="<PRODNM>0</PRODNM>\n";
					$xmlfile.="<ECWEB><![CDATA[常春藤網路書城]]></ECWEB>\n";
					$xmlfile.="<ECSERTEL>02-2331-7600</ECSERTEL>\n";
					$xmlfile.="<REALAMT>".$row['total_price']."</REALAMT>\n";
					$xmlfile.="<TRADETYPE>".$row['servicetype']."</TRADETYPE>\n";
					$xmlfile.="<SERCODE>".$StoreCVS->SERCODE."</SERCODE>\n";
					$xmlfile.="<EDCNO>".$StoreCVS->EDCNO."</EDCNO>\n";
				$xmlfile.="</ORDER>\n";
				// update id
				echo $sql_cmd = "update `order` set Delivery_No = '".substr(($row['id']+100000000000),1,11)."',store_update = '2', Status = '4'   where Order_ID = '".$row['order_id']."'";
				$db->query($sql_cmd);
				$sql_cmd = "update `store_cvs` set store_update_time = '".date("Y-m-d H:i:s")."' where Order_ID = '".$row['order_id']."'";
				$db->query($sql_cmd);
			}
			$xmlfile.="<ORDERCOUNT>\n";
				$xmlfile.="<TOTALS>".$allrow."</TOTALS>\n";
			$xmlfile.="</ORDERCOUNT>\n";
		$xmlfile.="</ORDER_DOC>";

		$fp=fopen('./F10/'.$filename,"w");

		if(fwrite($fp,$xmlfile)){
			$xml = $StoreCVS->sendXmlOverPost($xmlfile);
			$sql_cmd = "select * from var where type = 'CVS_FTP'";
			$rs = $db->query($sql_cmd);
			$email = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)['value'];
			$email = explode(",",$email);
			if(empty($xml)) {
				foreach($email as $value) {
					$datas = [
						"title"   => "未正確上傳超商檔案至便利達康",
						"content" => "本日超商XML檔案「".$filename.".xml」未正確上傳至便利達康，請檢查FTP服務是否正常。",
						"type"    => 99,
						"mail"    => $value,
					];
					ClassMail::send_mail($datas);
				}
			}
			else {
				foreach($email as $value) {
					$datas = [
						"title"   => "超商檔案XML正確上傳至便利達康",
						"content" => "本日超商XML檔案「".$filename.".xml」已正確上傳至便利達康。",
						"type"    => 99,
						"mail"    => $value,
					];
				}
				ClassMail::send_mail($datas);
				$content = preg_replace("/.*?(<.*)$/m", "$1", $xml->ORDERS_ADDResult);
				$filename="F11".$StoreCVS->ECNO.$StoreCVS->CVS.date("YmdHis", strtotime($nowdatetime)).".xml";
				$fp=fopen('./F11/'.$filename,"w");
				fwrite($fp,$content);
				$doc = new DOMDocument();
				$doc->loadHtml('<?xml encoding="utf-8" ?>' . $content);
				$books = $doc->getElementsByTagName( "err_order" );
				$content = "便利達康已回覆以下訂單資料發生錯誤，請重新對該訂單進行「重新出貨」動作。<br>";
				$isError = false;
				foreach( $books as $book ){
					$ShipmentNos = $book->getElementsByTagName( "odno" );
					$ShipmentNo = $ShipmentNos->item(0)->nodeValue;
					$ERRCODEs = $book->getElementsByTagName( "errcode" );
					$ERRCODE = $ERRCODEs->item(0)->nodeValue;
					$ERRDESCs = $book->getElementsByTagName( "errdesc" );
					$ERRDESC = $ERRDESCs->item(0)->nodeValue;
					$sql_array['status']= array("2",intval(100));
					$sql_array['statusstr']= array("3","[".$ERRCODE."]".$ERRDESC);
					$sql_array['F11']= array("3","代碼：".$ERRCODE."\n描述：".$ERRDESC);
					$sql_cmd = update("store_cvs", array("id", intval($ShipmentNo)), $sql_array);
					$rs = $db->query($sql_cmd);
					$sql_cmd = "select * from store_cvs where id = '".intval($ShipmentNo)."'";
					$rs = $db->query($sql_cmd);
					$row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
					$content .= "訂單編號：".$row['order_id']."<br>錯誤代碼：".$ERRCODE."<br>錯誤說明：".$ERRDESC;
				}
				if($isError) {
					foreach($email as $value) {
						$datas = [
							"title"   => "便利達康解析訂單檔發生錯誤",
							"content" => $content,
							"type"    => 99,
							"mail"    => $value,
						];
						ClassMail::send_mail($datas);
					}
				}
				$sql_cmd="UPDATE store_cvs SET status='".intval('40')."',statusstr='' WHERE ShipDate='".$nowdate."' AND status='".intval('30')."'";
				$rs = $db->query($sql_cmd);
			}
		}
		// echo $xml->ORDERS_ADDResult;
	}