﻿<?php
    include "../../../config.php"; 
    $nowdate=date("Y-m-d");
    $nowdatetime=date("Y-m-d H:i:s");
    include './storeCVS.php';
    include DOCUMENT_ROOT."/class/class-ftp-implicit-ssl-tls.php";
    $StoreCVS = new ClassStoreCVS();
    $FTP_Implicit_SSL = new FTP_Implicit_SSL($StoreCVS->store_ftpuser, $StoreCVS->store_ftppass, $StoreCVS->store_ftpserver, $StoreCVS->store_port, '', true);
    $filename="F04".$StoreCVS->ECNO.$StoreCVS->CVS.str_replace('-','',$nowdate).".xml";
    $fplog=fopen('./log/storelog.log','a');
    // if((!$ftp_connid) || (!$ftp_logins)){
    //     fwrite($fplog,$nowdatetime.'(F04)連接到超商ftp伺服器失敗!'."\r\n");
    // }else{
    //     ftp_pasv($ftp_connid,true);
    //     ftp_chdir($ftp_connid,'');
        $ftp_upload=$FTP_Implicit_SSL->download("/F04/".$filename,'.');
        if($ftp_upload==false){
            fwrite($fplog,$nowdatetime.'(F04)F04檔案下載失敗!'."\r\n");
        }else{
            $doc = new DOMDocument();
            $content = file_get_contents('./F04/'.$filename);
            $doc->loadXML($content);
            $books = $doc->getElementsByTagName( "F04CONTENT" );
            foreach( $books as $book ){
                $ShipmentNos = $book->getElementsByTagName( "ODNO" );
                $ShipmentNo = $ShipmentNos->item(0)->nodeValue;
                $DCSTDTs = $book->getElementsByTagName( "DCSTDT" );
                $DCSTDT = $DCSTDTs->item(0)->nodeValue;
                $sql_array['status']= array("2",intval(70));
                $sql_array['statusstr']= array("3","");
                $sql_array['F04']= array("3","實際進店日期：".date("Y-m-d",strtotime($DCSTDT)));
                $sql_cmd = update("store_cvs", array("id", intval($ShipmentNo)), $sql_array);
                $rs = $db->query($sql_cmd);

                $sql_cmd = "select * from store_cvs where id = '".intval($ShipmentNo)."'";
                $rs = $db->query($sql_cmd);
                $row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);

                $sql_cmd = "select * from `order` where Order_ID = '".$row['order_id']."'";
                $rs_order = $db->query($sql_cmd);
                $row_order = $rs_order->fetchRow(MDB2_FETCHMODE_ASSOC);
                // 到店發mail
                if($row_order['Payment'] == 1)
                    $row_order['total_price'] = 0;
                $datas = [
                    "title"             => "常春藤網路書城-商品已到超商門市通知",
                    "type"              => 5,
                    "mail"              => $row_order['email'],
                    "order_id"          => $row_order['Order_ID'],
                    "order_create_date" => $row_order['create_datetime'],
                    "store"             => $row['storename'],
                    "store_addr"        => $row['storeaddr'],
                    "take_time"         => date("Y-m-d", strtotime($StoreTime." +7 days")),
                    "deivery"           => $row_order['Delivery_No'],
                    "name"              => $row_order['Delivery_Name'],
                    "price"             => $row_order['total_price'],
                ];
                ClassMail::send_mail($datas);
				
				
				$mobile = preg_replace("/^0/", "886", $row_order['Delivery_Phone']);
				$content = "常春藤通知：配編".$row_order['Order_ID']."（$".$row_order['total_price']."元）已送達".$row['storename']."門市，請於".date("Y-m-d", strtotime($StoreTime." +7 days"))."前攜帶證件取貨，謝謝。";
				$client = new infobip\api\client\SendSingleTextualSms(
					new infobip\api\configuration\BasicAuthConfiguration('Ivyeducation','Ivy23317600')
				);
				$requestBody = new infobip\api\model\sms\mt\send\textual\SMSTextualRequest();
				$requestBody->setFrom('cqt');
				$requestBody->setTo($mobile);
				$requestBody->setText($content);
				$response = $client->execute($requestBody);
				
				
		

                $sql_cmd = "update `order` set status = 5 where Order_ID = '".$row['order_id']."'";
                $db->query($sql_cmd);
            }
            fwrite($fplog,$nowdatetime.'(F04)F04檔案下載解析成功!'."\r\n");
        }
		
		$sql_cmd = "select * from `order` as o
			left join store_cvs as s on o.Order_ID = s.order_id 
			where s.status = '70'";
		$rs = $db->query($sql_cmd);
		while ($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
			$f04_date = explode("實際進店日期：",$row['F04'])[1];
			print_r($f04_date);
            if(date("Y-m-d", strtotime("-3 day")) == $f04_date) {
				$mobile = preg_replace("/^0/", "886", $row['Delivery_Mobile']);
				$content = "常春藤通知：配編".$row_order['Order_ID']."（$".$row_order['total_price']."元）已送達".$row['storename']."門市，請於".date("Y-m-d", strtotime($StoreTime." +7 days"))."前攜帶證件取貨，謝謝。";
				$client = new infobip\api\client\SendSingleTextualSms(
					new infobip\api\configuration\BasicAuthConfiguration('Ivyeducation','Ivy23317600')
				);
				$requestBody = new infobip\api\model\sms\mt\send\textual\SMSTextualRequest();
				$requestBody->setFrom('cqt');
				$requestBody->setTo($mobile);
				$requestBody->setText($content);
				$response = $client->execute($requestBody);
                //echo "4天";
            }
			if(date("Y-m-d", strtotime("-7 day")) == $f04_date) {
				$mobile = preg_replace("/^0/", "886", $row['Delivery_Mobile']);
				$content = "常春藤通知：配編".$row_order['Order_ID']."（$".$row_order['total_price']."元）已送達".$row['storename']."門市，請於".date("Y-m-d", strtotime($StoreTime." +7 days"))."前取貨，若已領取請忽略本通知，謝謝。";
				$client = new infobip\api\client\SendSingleTextualSms(
					new infobip\api\configuration\BasicAuthConfiguration('Ivyeducation','Ivy23317600')
				);
				$requestBody = new infobip\api\model\sms\mt\send\textual\SMSTextualRequest();
				$requestBody->setFrom('cqt');
				$requestBody->setTo($mobile);
				$requestBody->setText($content);
				$response = $client->execute($requestBody);
                //echo "7天";
            }
        }
    // }
    // ftp_close($ftp_connid); //断开
    Fclose($fplog);