<?php
class ClassStoreCVS {
    Public static $upload_url="https://cvsweb.cvs.com.tw/webservice/service.asmx?wsdl";
    Public $store_ftpserver='cvsftp.cvs.com.tw';
    Public $store_ftpuser='364';
    Public $store_ftppass=';yk[X;GXA\9`*Z66';
    Public $store_port='8821';
    Public $CVS='CVS';
    public $EC = 'D07'; // 物流代碼
    Public $ECNO='364'; //網站代號
    public $SERCODE = "990"; // 代收代號
    public $EDCNO = "";

    private $db = '';

    public function __construct() {
        $this->db = $GLOBALS['db'];
    }


    public static function sendXmlOverPost($xml) {
        $url = "https://cvsweb.cvs.com.tw/webservice/service.asmx?wsdl";
        $client = new SoapClient($url);

        $xmlr = new SimpleXMLElement($xml);
        $params = new stdClass();
        $params->xmlStr = $xmlr->asXML();
        $result = $client->ORDERS_ADD($params);
        return $result;
    }
    public function F01($SRPDATA) {

    }
    public function F03($SRPDATA) {
        $doc = new DOMDocument();
        $doc->loadHTML($SRPDATA);
        $books = $doc->getElementsByTagName( "F03CONTENT" );
        foreach( $books as $book ){
            $ShipmentNos = $book->getElementsByTagName( "ODNO" );
            $ShipmentNo = $ShipmentNos->item(0)->nodeValue;
            $DOCDATEs = $book->getElementsByTagName( "DOCDATE" );
            $DOCDATE = $DOCDATEs->item(0)->nodeValue;
            $sql_array['F03']= array("3","處理日期時間".date("Y-m-d",strtotime($DOCDATE)));
            $sql_array['statusstr']= array("3","");
            $sql_array['store_time']= array("3",date("Y-m-d",strtotime($DOCDATE)));
            $sql_cmd = update("store_cvs", array("id", intval($ShipmentNo)), $sql_array);
            $rs = $this->db->query($sql_cmd);
        }

    }
    public function F04($SRPDATA) {
        $doc = new DOMDocument();
        $doc->loadHTML($SRPDATA);
        $books = $doc->getElementsByTagName( "F04CONTENT" );
        foreach( $books as $book ){
            $ShipmentNos = $book->getElementsByTagName( "ODNO" );
            $ShipmentNo = $ShipmentNos->item(0)->nodeValue;
            $DCSTDTs = $book->getElementsByTagName( "DCSTDT" );
            $DCSTDT = $DCSTDTs->item(0)->nodeValue;
            $sql_array['status']= array("2",intval(70));
            $sql_array['statusstr']= array("3","");
            $sql_array['F04']= array("3","實際進店日期：".date("Y-m-d",strtotime($DCSTDT)));
            $sql_cmd = update("store_cvs", array("id", intval($ShipmentNo)), $sql_array);
            $rs = $this->db->query($sql_cmd);

            $sql_cmd = "select * from store_cvs where id = '".intval($ShipmentNo)."'";
            $rs = $this->db->query($sql_cmd);
            $row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);

            $sql_cmd = "select * from `order` where Order_ID = '".$row['order_id']."'";
            $rs_order = $this->db->query($sql_cmd);
            $row_order = $rs_order->fetchRow(MDB2_FETCHMODE_ASSOC);
            // 到店發mail
            $datas = [
                "title"   => "商品到店",
                "type"    => 3,
                "mail"    => $row_order['email'],
            ];
            ClassMail::send_mail($datas);

            $sql_cmd = "update `order` set status = 5 where Order_ID = '".$row['order_id']."'";
            $this->db->query($sql_cmd);
        }
    }
    public function F05($SRPDATA) {
        $doc = new DOMDocument();
        $doc->loadHTML($SRPDATA);
        $books = $doc->getElementsByTagName( "F05CONTENT" );
        foreach( $books as $book ){
            $ShipmentNos = $book->getElementsByTagName( "ODNO" );
            $ShipmentNo = $ShipmentNos->item(0)->nodeValue;
            $RTDTs = $book->getElementsByTagName( "RTDT" );
            $RTDT = $RTDTs->item(0)->nodeValue;
            $TKDTs = $book->getElementsByTagName( "TKDT" );
            $TKDT = $TKDTs->item(0)->nodeValue;
            $sql_array['status']= array("2",intval(80));
            $sql_array['statusstr']= array("3","完工");
            $sql_array['F05']= array("3","實際取貨代收日期：".date("Y-m-d",strtotime($RTDT))."\n結帳基準日期：".date("Y-m-d",strtotime($TKDT)));
            $sql_cmd = update("store_cvs", array("id", intval($ShipmentNo)), $sql_array);
            $rs = $this->db->query($sql_cmd);

            $sql_cmd = "select * from store_cvs where id = '".intval($ShipmentNo)."'";
            $rs = $this->db->query($sql_cmd);
            
            $row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
            $sql_cmd = "update `order` set status = 7 where Order_ID = '".$row['order_id']."'";
            $this->db->query($sql_cmd);
        }
    }
    public function F07($SRPDATA) {
        $doc = new DOMDocument();
        $doc->loadHTML($SRPDATA);
        $books = $doc->getElementsByTagName( "F07CONTENT" );
        foreach( $books as $book ){
            $ShipmentNos = $book->getElementsByTagName( "ODNO" );
            $ShipmentNo = $ShipmentNos->item(0)->nodeValue;
            $RET_Ms = $book->getElementsByTagName( "RET_M" );
            $RET_M = $RET_Ms->item(0)->nodeValue;
            $RTDCDTs = $book->getElementsByTagName( "RTDCDT" );
            $RTDCDT = $RTDCDTs->item(0)->nodeValue;

            $sql_array['status']= array("2",intval(140));
            $sql_array['statusstr']= array("3","[".$RET_M."]".$this->store_DOCerror($RET_M));
            $sql_array['F07']= array("3","代碼：".$RET_M."\n描述：".$this->store_DOCerror($RET_M)."\n大物流實際退日：".date("Y-m-d",strtotime($RTDCDT)));
            $sql_cmd = update("store_cvs", array("id", intval($ShipmentNo)), $sql_array);
            $rs = $this->db->query($sql_cmd);

            $sql_cmd = "select * from store_cvs where id = '".intval($ShipmentNo)."'";
            $rs = $this->db->query($sql_cmd);
            
            $isError = true;
            $content .= "訂單編號：".$row['order_id']."<br>大物流實際驗退日：".$RTDCDT."<br>退貨原因：".$RET_M."：".$this->store_DOCerror($RET_M);

        }
        if($isError) {
            $sql_cmd = "select * from var where type = 'CVS_FTP'";
            $rs = $this->db->query($sql_cmd);
            $email = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)['value'];
            $datas = [
                "title"   => "超商退回大物流回報通知",
                "content" => $content,
                "type"    => 99,
                "mail"    => $email,
            ];
            ClassMail::send_mail($datas);
        }

    }
    public function F09($SRPDATA) {
        $doc = new DOMDocument();
        $doc->loadHTML($SRPDATA);
        $books = $doc->getElementsByTagName( "F09CONTENT" );
        foreach( $books as $book ){
            $ShipmentNos = $book->getElementsByTagName( "ODNO" );
            $ShipmentNo = $ShipmentNos->item(0)->nodeValue;
            $RET_Ms = $book->getElementsByTagName( "RET_M" );
            $RET_M = $RET_Ms->item(0)->nodeValue;
            $RET_Rs = $book->getElementsByTagName( "RET_R" );
            $RET_R = $RET_Rs->item(0)->nodeValue;
            $RTDCDTs = $book->getElementsByTagName( "RTDCDT" );
            $RTDCDT = $RTDCDTs->item(0)->nodeValue;

            $sql_array['status']= array("2",intval(140));
            $sql_array['statusstr']= array("3","[".$RET_M."]".$this->store_DOCerror($RET_M));
            $sql_array['F09']= array("3","取消類型代碼：".$RET_M."\n取消類型描述：".$this->store_DOCerror($RET_M)."\n取消原因代碼：".$RET_R."\n取消原因描述：".$this->store_DOCerror($RET_R));
            $sql_cmd = update("store_cvs", array("id", intval($ShipmentNo)), $sql_array);
            $rs = $this->db->query($sql_cmd);

            $sql_cmd = "select * from store_cvs where id = '".intval($ShipmentNo)."'";
            $rs = $this->db->query($sql_cmd);
            
            $isError = true;
            $content .= "訂單編號：".$row['order_id']."<br>取消類型：".$RET_M."：".$this->store_DOCerror($RET_M);

        }
        if($isError) {
            $sql_cmd = "select * from var where type = 'CVS_FTP'";
            $rs = $this->db->query($sql_cmd);
            $email = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)['value'];
            $datas = [
                "title"   => "便利達康取消出貨回報",
                "content" => $content,
                "type"    => 99,
                "mail"    => $email,
            ];
            ClassMail::send_mail($datas);
        }
    }
    public function F11($SRPDATA) {
        $doc = new DOMDocument();
        $doc->loadHTML($SRPDATA);
        $books = $doc->getElementsByTagName( "ERR_ORDER" );
        $content = "便利達康已回覆以下訂單資料發生錯誤，請重新對該訂單進行「重新出貨」動作。<br>";
        $isError = false;
        foreach( $books as $book ){
            $ShipmentNos = $book->getElementsByTagName( "ODNO" );
            $ShipmentNo = $ShipmentNos->item(0)->nodeValue;
            $ERRCODEs = $book->getElementsByTagName( "ERRCODE" );
            $ERRCODE = $ERRCODEs->item(0)->nodeValue;
            $ERRDESCs = $book->getElementsByTagName( "ERRDESC" );
            $ERRDESC = $ERRDESCs->item(0)->nodeValue;
            $sql_array['status']= array("2",intval(100));
            $sql_array['statusstr']= array("3","[".$ERRCODE."]".$ERRDESC);
            $sql_array['F11']= array("3","代碼：".$ERRCODE."\n描述：".$ERRDESC);
            $sql_cmd = update("store_cvs", array("id", intval($ShipmentNo)), $sql_array);
            $rs = $this->db->query($sql_cmd);
            $sql_cmd = "select * from store_cvs where id = '".intval($ShipmentNo)."'";
            $rs = $this->db->query($sql_cmd);
            $row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
            $content .= "訂單編號：".$row['order_id']."<br>錯誤代碼：".$ERRCODE."<br>錯誤說明：".$ERRDESC;
        }
        if($isError) {
            $datas = [
                "title"   => "便利達康解析訂單檔發生錯誤",
                "content" => $content,
                "type"    => 99,
                "mail"    => $email,
            ];
            ClassMail::send_mail($datas);
        }

    }
    public function F44($SRPDATA) {
        $doc = new DOMDocument();
        $doc->loadHTML($SRPDATA);
        $books = $doc->getElementsByTagName( "F44CONTENT" );
        foreach( $books as $book ){
            $ShipmentNos = $book->getElementsByTagName( "ODNO" );
            $ShipmentNo = $ShipmentNos->item(0)->nodeValue;
            $DCSTDTs = $book->getElementsByTagName( "DCSTDT" );
            $DCSTDT = $DCSTDTs->item(0)->nodeValue;
            $sql_array['status']= array("2",intval(70));
            $sql_array['statusstr']= array("3","");
            $sql_array['F44']= array("3","實際進店日期：".date("Y-m-d",strtotime($DCSTDT)));
            $sql_cmd = update("store_cvs", array("id", intval($ShipmentNo)), $sql_array);
            $rs = $this->db->query($sql_cmd);

            $sql_cmd = "select * from store_cvs where id = '".intval($ShipmentNo)."'";
            $rs = $this->db->query($sql_cmd);
            $row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);

            $sql_cmd = "select * from `order` where Order_ID = '".$row['order_id']."'";
            $rs_order = $this->db->query($sql_cmd);
            $row_order = $rs_order->fetchRow(MDB2_FETCHMODE_ASSOC);
            // 到店發mail
            $datas = [
                "title"   => "商品到店",
                "type"    => 3,
                "mail"    => $row_order['email'],
            ];
            ClassMail::send_mail($datas);
            
            $sql_cmd = "update `order` set status = 5 where Order_ID = '".$row['order_id']."'";
            $this->db->query($sql_cmd);
        }
    }
    public function F45($SRPDATA) {
        $doc = new DOMDocument();
        $doc->loadHTML($SRPDATA);
        $books = $doc->getElementsByTagName( "F45CONTENT" );
        foreach( $books as $book ){
            $ShipmentNos = $book->getElementsByTagName( "ODNO" );
            $ShipmentNo = $ShipmentNos->item(0)->nodeValue;
            $RTDTs = $book->getElementsByTagName( "RTDT" );
            $RTDT = $RTDTs->item(0)->nodeValue;
            $sql_array['status']= array("2",intval(70));
            $sql_array['F45']= array("3","實際取貨代收日期：".date("Y-m-d",strtotime($RTDT)));
            $sql_cmd = update("store_cvs", array("id", intval($ShipmentNo)), $sql_array);
            $rs = $this->db->query($sql_cmd);

            $sql_cmd = "select * from store_cvs where id = '".intval($ShipmentNo)."'";
            $rs = $this->db->query($sql_cmd);
            $row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);

            $sql_cmd = "select * from `order` where Order_ID = '".$row['order_id']."'";
            $rs_order = $this->db->query($sql_cmd);
            $row_order = $rs_order->fetchRow(MDB2_FETCHMODE_ASSOC);
            // 到店發mail
            $datas = [
                "title"   => "商品到店",
                "type"    => 3,
                "mail"    => $row_order['email'],
            ];
            ClassMail::send_mail($datas);

            $sql_cmd = "update `order` set status = 5 where Order_ID = '".$row['order_id']."'";
            $this->db->query($sql_cmd);
        }
    }
    public function F61($SRPDATA) {
        $doc = new DOMDocument();
        $doc->loadHTML($SRPDATA);
        $books = $doc->getElementsByTagName( "F61CONTENT" );
        foreach( $books as $book ){
            $ShipmentNos = $book->getElementsByTagName( "ODNO" );
            $ShipmentNo = $ShipmentNos->item(0)->nodeValue;
            $RTDTs = $book->getElementsByTagName( "RTDT" );
            $RTDT = $RTDTs->item(0)->nodeValue;

            $sql_array['status']= array("2",intval(150));
            $sql_array['DCReturnDate']= array("3",preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1-$2-$3",$RTDT));
            $sql_array['F61']= array("3","預計退貨日：".preg_replace("/(\d{4})(\d{2})(\d{2})/", "$1-$2-$3",$RTDT));
            $sql_cmd = update("store_cvs", array("id", intval($ShipmentNo)), $sql_array);
            $rs = $this->db->query($sql_cmd);

            $sql_cmd = "select * from store_cvs where id = '".intval($ShipmentNo)."'";
            $rs = $this->db->query($sql_cmd);
        }

    }
    public function F10($data, $allrow) {
        $xmlfile="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
        $xmlfile.="<ORDER_DOC>\n";
            foreach ($data as $key => $row) {
                    if (!empty($row["Delivery_Mobile"])) {
                        $mobile = substr($row["Delivery_Mobile"],-3);
                    }
                    else {
                        $mobile = "";
                    }
                    $xmlfile.="<ORDER>\n";
                    $xmlfile.="<ECNO>".$this->ECNO."</ECNO>\n";
                    $xmlfile.="<ODNO>".substr(($row['id']+100000000000),1,11)."</ODNO>\n";
                    $xmlfile.="<STNO>".$row['storeid']."</STNO>\n";
                    $xmlfile.="<AMT>".$row['total_price']."</AMT>\n";
                    $xmlfile.="<CUTKNM><![CDATA[".$row['Delivery_Name']."]]></CUTKNM>\n";
                    $xmlfile.="<CUTKTL>".$mobile."</CUTKTL>\n";
                    $xmlfile.="<PRODNM>0</PRODNM>\n";
                    $xmlfile.="<ECWEB><![CDATA[常春藤網路書城]]></ECWEB>\n";
                    $xmlfile.="<ECSERTEL>02-2331-7600</ECSERTEL>\n";
                    $xmlfile.="<REALAMT>".$row['total_price']."</REALAMT>\n";
                    $xmlfile.="<TRADETYPE>".$row['servicetype']."</TRADETYPE>\n";
                    $xmlfile.="<SERCODE>".$this->SERCODE."</SERCODE>\n";
                    $xmlfile.="<EDCNO>".$this->EDCNO."</EDCNO>\n";
                $xmlfile.="</ORDER>\n";
                // update id
                $sql_cmd = "update `order` set Delivery_No = '".substr(($row['id']+100000000000),1,11)."' where Order_ID = '".$row['order_id']."'";
                $this->db->query($sql_cmd);
            }
            $xmlfile.="<ORDERCOUNT>\n";
                $xmlfile.="<TOTALS>1</TOTALS>\n";
            $xmlfile.="</ORDERCOUNT>\n";
        $xmlfile.="</ORDER_DOC>";
		return $xmlfile;
    }
    public function store_DOCerror($errorid){
        switch(trim($errorid)){
            case 'T1': $errorstr='系統取消，EC 自行在後台取消';break;//ETA
            case 'D1': $errorstr='大物流取消';break;//ETA
            case 'S1': $errorstr='小物流取消';break;//ETA
            case 'N1': $errorstr='門市取消';break;//ETA
            case 'T00': $errorstr='正常驗退';break;//ETA
            case 'T01': $errorstr='閉店、整修、無路線路順';break;//ETA
            case 'T02': $errorstr='無進貨資料';break;//ETA
            case 'T03': $errorstr='條碼錯誤';break;//ETA
            case 'T04': $errorstr='條碼重複';break;//ETA
            case 'T05': $errorstr='貨物進店後發生異常提早退貨';break;//ETA
            case 'T06': $errorstr='超過三十天系統自動取消';break;//ETA
            case 'T07': $errorstr='EC 自行取消';break;//ETA
            case 'T08': $errorstr='超才';break;//ETA
            case 'D01': $errorstr='大物流遺失';break;//EIN
            case 'D02': $errorstr='未送貨到小物流';break;//EIN
            case 'D04': $errorstr='大物流包裝不良(滲漏)';break;//EIN
            case 'S03': $errorstr='小物流遺失';break;//EIN
            case 'S04': $errorstr='路線刪單';break;//EIN
            case 'S06': $errorstr='小物流破損';break;//EIN
            case 'S07': $errorstr='門市反應商品包裝不良(滲漏)';break;//EIN
            case 'N05': $errorstr='門市遺失';break;//EIN
            default: $errorstr=$errorid;break;
        }
        return $errorstr;
    }
    public function qrcode($data) {
        $store_company = preg_replace("/(^\w{1}).*?/", "$1", $data['STNO']);
        switch ($store_company) {
            case 'F':
                $store_company = "2";
                break;
            case 'L':
                $store_company = "5";
                break;
            case 'K':
                $store_company = "6";
                break;
            default:
                $store_company = "";
                break;
        }
        $rs = [
            "B1", // 類型
            "                  ", // 寄件物寄件條碼
            "         ", // 寄件物寄件條碼
            "                  ", // 寄件物寄件條碼
            "               ", // 寄件物寄件條碼
            $this->delivery_code($data), // 取件物流碼
            $store_company, // 回收物流商
            $data['storeid'], // 取件店號
            $data['RSNO'], // 路線路順(理貨 用)
            $data['EQPID'], // 設備代碼
            "0", // 溫層別
            $this->first_code($data), // 取貨條碼一
            $this->second_code($data), // 取貨條碼二
            "             ", // 備用1
            "          ", // 備用2
        ];
        return implode($rs, "||");
    }

    public function first_code($data) {
        $service = "";
        return $code = $this->ECNO.substr(($data['id']+100000000000),1,3).$service;
    }
    public function second_code($data) {
        $code = substr(($data['id']+100000000000),-8).$data['servicetype'].substr(($data['total_price']+100000),-5);
        $tmp = str_split($code);
        $check1 = 0;
        $check2 = 0;
        foreach($tmp as $key => $value) {
            if($key % 2 == 0) {
                $check1 += $value;
            }
            else {
                $check2 += $value;
            }
        }
        $check1 = $check1 % 11;
        switch ($check1) {
            case '10':
                $check1 = 1;
                break;
        }
        $check2 = $check2 % 11;
        switch ($check2) {
            case '0':
                $check2 = 8;
                break;
            case '10':
                $check2 = 9;
                break;
        }
        return $code.$check1.$check2;
    }
    // 大物流條碼
    public function delivery_code($data) {
        $tran = [
            0 => "0",
            1 => "1",
            2 => "2",
            3 => "3",
            4 => "4",
            5 => "5",
            6 => "6",
            7 => "7",
            8 => "8",
            9 => "9",
            10 => "A",
            11 => "B",
            12 => "C",
            13 => "D",
            14 => "E",
            15 => "F",
            16 => "G",
            17 => "H",
            18 => "I",
            19 => "J",
            20 => "K",
            21 => "L",
            22 => "M",
            23 => "N",
            24 => "O",
            25 => "P",
            26 => "Q",
            27 => "R",
            28 => "S",
            29 => "T",
            30 => "U",
            31 => "V",
            32 => "W",
            33 => "X",
            34 => "Y",
            35 => "Z",
            36 => "-",
            37 => ".",
            38 => "SP",
            39 => "$",
            40 => "/",
            41 => "+",
            42 => "%",
        ];
        //$store_company = preg_replace("/(^\w{1}).*?/", "$1", $data['storeid']);
        $store_company = substr($data['storeid'],0,1);
        switch ($store_company) {
            case 'F':
                $store_company = "1";
                break;
            case 'L':
                $store_company = "2";
                break;
            case 'K':
                $store_company = "3";
                break;
            default:
                $store_company = "";
                break;
        }
        $code = $store_company.$this->ECNO.'00'.substr(($data['id']+100000000000),1,11);

        $check = 0;
        $tmp = str_split($code);
        foreach($tmp as $key => $value) {
            $check += $value;
        }
        $check = $check % 43;
        $check = $tran[$check];
		
        return $code.$check;
    }
}