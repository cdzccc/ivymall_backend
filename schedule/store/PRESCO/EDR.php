<?php 
include "../../../config.php"; 
	$nowdate=date("Y-m-d");
	$nowdatetime=date("Y-m-d H:i:s");
	//$nowdate='2013-07-31';
	//$nowdatetime='2013-07-31 12:33:42';
	include './storefun.php';
	$filename=$store_ParentId.$store_eshopid.str_replace('-','',$nowdate).'01';
	$ftp_connid=ftp_connect($store_ftpserver,$store_port);
	$ftp_logins= ftp_login($ftp_connid, $store_ftpuser, $store_ftppass);
	$fplog=fopen('./log/storelog.log','a');
	if((!$ftp_connid) || (!$ftp_logins)){
		fwrite($fplog,$nowdatetime.'(EDR)連接到超商ftp伺服器失敗!'."\r\n");
	}else{
		ftp_pasv($ftp_connid,true);
		ftp_chdir($ftp_connid,'EDR');
		$ftp_upload=@ftp_get($ftp_connid,'./EDR/'.$filename.'.edr',$filename.'.edr', FTP_BINARY);
		if($ftp_upload==false){
			fwrite($fplog,$nowdatetime.'(EDR)EDR檔案下載失敗!'."\r\n");
		}else{
			$isError = false;
			$content = "統一數網回覆：以下訂單消費者未取貨，已退回至大智通物流中心。<br>";
			$doc = new DOMDocument();
			$doc->load('./EDR/'.$filename.$PPSid.'.edr');
			$books = $doc->getElementsByTagName( "DCReturnAdvice" );
			foreach( $books as $book ){
				$ShipmentNos = $book->getElementsByTagName( "ShipmentNo" );
				$ShipmentNo = $ShipmentNos->item(0)->nodeValue;
				$DCReturnCodes = $book->getElementsByTagName( "DCReturnCode" );
				$DCReturnCode = $DCReturnCodes->item(0)->nodeValue;
				$DCReturnNames = $book->getElementsByTagName( "DCReturnName" );
				$DCReturnName = $DCReturnNames->item(0)->nodeValue;
				$DCReturnDates = $book->getElementsByTagName( "DCReturnDate" );
				$DCReturnDate = $DCReturnDates->item(0)->nodeValue;
				$sql_array['status']= array("2",intval(140));
				$sql_array['statusstr']= array("3","[".$DCReturnCode."]".$DCReturnName);
				$sql_array['DCReturnDate']= array("3",$DCReturnDate);
				$sql_array['EDR']= array("3","代碼：".$DCReturnCode."\n描述：".$DCReturnName."DC預定退貨日：".$DCReturnDate);
				$sql_cmd = update("store", array("id", intval($ShipmentNo)), $sql_array);
				$rs = $db->query($sql_cmd);
				$isError = true;
				$sql_cmd = "select * from store where id = '".intval($ShipmentNo)."'";
				$rs = $db->query($sql_cmd);
				$row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
				$content .= "訂單編號：".$row['order_id']."<br>退貨驗收日期：".$DCReturnDate."<br>退貨驗收代碼：".$DCReturnCode."<br>退貨驗收說明：".$DCReturnName;
			}
			if($isError) {
				$sql_cmd = "select * from var where type = 'PRESCO_FTP'";
				$rs = $db->query($sql_cmd);
				$email = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)['value'];
				$email = explode(",",$email);
				
				foreach($email as $value) {
					$datas = [
						"title"   => "超商退回大智通回報通知",
						"content" => $content,
						"type"    => 99,
						"mail"    => $email,
					];
					ClassMail::send_mail($value);
				}
			}

			fwrite($fplog,$nowdatetime.'(EDR)EDR檔案下載解析成功!'."\r\n");
		}
	}
 	ftp_close($ftp_connid); //断开
	Fclose($fplog);
?>
