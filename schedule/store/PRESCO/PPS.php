<?php 
include "../../../config.php"; 
	$nowdate=date("Y-m-d");
	$nowdatetime=date("Y-m-d H:i:s");
//	$nowdate='2013-08-18';
//	$nowdatetime='2013-08-18 12:33:42';
	include './storefun.php';
	$filename=$store_ParentId.$store_eshopid.str_replace('-','',$nowdate);
	$ftp_connid=ftp_connect($store_ftpserver,$store_port);
	$ftp_logins= ftp_login($ftp_connid, $store_ftpuser, $store_ftppass);
	$fplog=fopen('./log/storelog.log','a');
	if((!$ftp_connid) || (!$ftp_logins)){
		fwrite($fplog,$nowdatetime.'(PPS)連接到超商ftp伺服器失敗!'."\r\n");
	}else{
		ftp_pasv($ftp_connid,true);
		ftp_chdir($ftp_connid,'PPS');
		// for($i=1;;$i++){
		// }
		$PPSid=date("H");
		// $PPSid="05";
		fwrite($fplog,$nowdatetime.'(PPS)File name'.'./PPS/'.$filename.$PPSid.'.PPS'."\r\n");
		// if(!is_file('./PPS/'.$filename.$PPSid.'.PPS')){
		// 	break;
		// }
		$ftp_upload=@ftp_get($ftp_connid,'./PPS/'.$filename.$PPSid.'.PPS',$filename.$PPSid.'.PPS', FTP_BINARY);
		if($ftp_upload==false){
			fwrite($fplog,$nowdatetime.'(PPS)PPS檔案下載失敗!'."\r\n");
		}else{
			$doc = new DOMDocument();
			$doc->load('./PPS/'.$filename.$PPSid.'.PPS');
			$books = $doc->getElementsByTagName( "PPS" );
			$isError = false;
			$content = "統一數網回覆：大智通物流配送至7-11門市時，發生以下問題。<br>";
			foreach( $books as $book ){
				$ShipmentNos = $book->getElementsByTagName( "ShipmentNo" );
				$ShipmentNo = $ShipmentNos->item(0)->nodeValue;
				$StoreTypes = $book->getElementsByTagName( "StoreType" );
				$StoreType = $StoreTypes->item(0)->nodeValue;
				$StoreDates = $book->getElementsByTagName( "StoreDate" );
				$StoreDate = $StoreDates->item(0)->nodeValue;
				$StoreTimes = $book->getElementsByTagName( "StoreTime" );
				$StoreTime = $StoreTimes->item(0)->nodeValue;
				unset($sql_array['statusstr']);

				$sql_cmd = "select * from store where id = '".intval($ShipmentNo)."'";
				$rs = $db->query($sql_cmd);
				$row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
				if($StoreType=='101'){
					$sql_array['status']= array("2",intval(70));
					$sql_array['statusstr']= array("3","[".$StoreType."]".store_DOCerror($StoreType));
					$sql_array['PPS']= array("3","代碼：".$StoreType."\n描述：".store_DOCerror($StoreType)."到店日期：".$StoreDate."到店時間：".$StoreTime);
					$sql_cmd = "update `order` set status = 5 where Order_ID = '".$row['order_id']."'";
					$db->query($sql_cmd);
					$sql_cmd = "select * from `order` where Order_ID = '".$row['order_id']."'";
					$rs_order = $db->query($sql_cmd);
					$row_order = $rs_order->fetchRow(MDB2_FETCHMODE_ASSOC);
					// 到店發mail
					if($row_order['Payment'] == 1)
						$row_order['total_price'] = 0;
					$sql_cmd = "select * from store where id = '".ntval($ShipmentNo)."'";
					$rs_store = $db->query($sql_cmd);
					$row_store = $rs_store->fetchRow(MDB2_FETCHMODE_ASSOC);
			    	$datas = [
						"title"             => "常春藤網路書城-商品已到超商門市通知",
						"type"              => 5,
						"mail"              => $row_order['email'],
						"order_id"          => $row_order['Order_ID'],
						"order_create_date" => $row_order['create_datetime'],
						"store"             => $row_store['storename'],
						"store_addr"        => $row_store['storeaddr'],
						"take_time"         => date("Y-m-d", strtotime($StoreTime." +7 days")),
						"deivery"           => $row_order['Delivery_No'],
						"name"              => $row_order['Delivery_Name'],
						"price"             => $row_order['total_price'],
				    ];
				    ClassMail::send_mail($datas);
				}else{
					$sql_array['status']= array("2",intval(130));
					$sql_array['statusstr']= array("3","[".$StoreType."]".store_DOCerror($StoreType));
					$sql_array['PPS']= array("3","代碼：".$StoreType."\n描述：".store_DOCerror($StoreType)."到店日期：".$StoreDate."到店時間：".$StoreTime);

					$isError = true;

					$content .= "訂單編號：".$row['order_id']."<br>到店狀態碼：".$StoreType."<br>到店狀態說明：".store_DOCerror($StoreType);
					if($StoreType == "201") {
				    	$datas = [
							"title"             => "常春藤網路書城-商品已到超商門市通知",
							"type"              => 7,
							"mail"              => $row['email'],
							"order_id"          => $row['Order_ID'],
							"order_create_date" => $row['create_datetime'],
							"cancel_reason"     => "[".date('Y-m-d H:i:s')."] 未於期限內至指定門市取貨，已取消訂單",
					    ];
					    ClassMail::send_mail($datas);						
					}
				}
				$sql_cmd = update("store", array("id", intval($ShipmentNo)), $sql_array);
				$rs = $db->query($sql_cmd);
			}
			if($isError) {
				$sql_cmd = "select * from var where type = 'PRESCO_FTP'";
				$rs = $db->query($sql_cmd);
				$email = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)['value'];
				$email = explode(",",$email);
				
				foreach($email as $value) {
					$datas = [
						"title"   => "商品到店錯誤回報",
						"content" => $content,
						"type"    => 99,
						"mail"    => $value,
					];
					ClassMail::send_mail($datas);
				}
			}
			fwrite($fplog,$nowdatetime.'(PPS)PPS檔案下載解析成功!'."\r\n");
		}
	}
 	ftp_close($ftp_connid); //断开
	Fclose($fplog);
?>
