<?php
include "../../../config.php"; 
	$nowdate=date("Y-m-d");
	$nowdatetime=date("Y-m-d H:i:s");
//	$nowdate='2013-09-07';
//	$nowdatetime='2013-09-07 05:30:00';
	echo $sql_cmd = "SELECT count(*) AS allrow 
		FROM store AS A 
		LEFT JOIN `order` AS B ON A.order_id=B.Order_ID
		WHERE ShipDate='".$nowdate."' AND A.status in (30) AND B.delivery = '1' ";
	$rs = $db->query($sql_cmd);
	$row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
	$allrow=$row['allrow'];
	if($allrow<=0){
		exit();
	}
	$sql_cmd = "SELECT A.*,B.create_datetime,B.name,B.total_price,B.Delivery_Mobile, B.Delivery_Name
		FROM store AS A 
		LEFT JOIN `order` AS B ON A.order_id=B.Order_ID
		WHERE ShipDate='".$nowdate."' AND A.status in (30) AND B.delivery = '1' ";
	$rs = $db->query($sql_cmd);	
	
	include './storefun.php';
	$filename=$store_ParentId.$store_eshopid.str_replace('-','',$nowdate).'01';
	$xmlfile="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
	$xmlfile.="<OrderDoc>\n";
		$xmlfile.="<DocHead>\n";
			$xmlfile.="<DocNo>".$filename."</DocNo>\n";//文件編號
			$xmlfile.="<DocDate>".$nowdate."</DocDate>\n";//文件日期
			$xmlfile.="<ParentId>".$store_ParentId."</ParentId>\n";//母廠商編號
		$xmlfile.="</DocHead>\n";
		$xmlfile.="<DocContent>\n";
$i=0;
while(($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC))){
	$i++;
	if (!empty($row["Delivery_Mobile"])) {
		$mobile = "0000000".substr($row["Delivery_Mobile"],-3);
	}
	else {
		$mobile = "";
	}
			$xmlfile.="<Order>\n";
				$xmlfile.="<EshopId>".$store_eshopid."</EshopId >\n";//子廠商代號
				$xmlfile.="<OPMode>".$row['storepath']."</OPMode>\n";//通路A=7-eleven H=hilife
				//$xmlfile.="<EshopOrderNo>".substr(($row['order_id']+100000000000),1,11)."</EshopOrderNo>\n";//訂單序號
				$xmlfile.="<EshopOrderNo>".$row['order_id']."</EshopOrderNo>\n";//訂單序號
				$xmlfile.="<EshopOrderDate>".substr($row['create_datetime'],0,10)."</EshopOrderDate>\n";//訂單日期
				$xmlfile.="<ServiceType>".$row['servicetype']."</ServiceType>\n";//1取貨付款3取貨不付款
				$xmlfile.="<ShopperName>".$row['name']."</ShopperName>\n";//購買人姓名
				$xmlfile.="<ShopperPhone></ShopperPhone>\n";//購買人電話(空)
				$xmlfile.="<ShopperMobilPhone></ShopperMobilPhone>\n";//購買人行動(空)
				$xmlfile.="<ShopperEmail></ShopperEmail>\n";//購買人信箱(空)
				$xmlfile.="<ReceiverName>".$row['Delivery_Name']."</ReceiverName>\n";//收貨人姓名(空)
				$xmlfile.="<ReceiverPhone></ReceiverPhone>\n";//收貨人電話(空)
				//因應統一超商考量避免7-11店配包裹因同名同姓誤領，擬增加手機末三碼資訊
				//1.應為10 碼數字；不足10 位，左邊補0，若是手機末三碼為182，則帶入0000000182
				//2.若無提供手機末三碼資料，請帶空值
				$xmlfile.="<ReceiverMobilPhone>".$row["Delivery_Mobile"]."</ReceiverMobilPhone>\n";//收貨人行動(空)
				$xmlfile.="<ReceiverEmail></ReceiverEmail>\n";//收貨人信箱(空)
				$xmlfile.="<ReceiverIDNumber></ReceiverIDNumber>\n";//購買人身分證(空)
				$xmlfile.="<OrderAmount>".$row['total_price']."</OrderAmount>\n";
				$xmlfile.="<OrderDetail>\n";
					$xmlfile.="<ProductId></ProductId>\n";//商品編號(空)
					$xmlfile.="<ProductName></ProductName>\n";//商品名稱(空)
					$xmlfile.="<Quantity></Quantity>\n";//數量(空)
					$xmlfile.="<Unit></Unit>\n";//單位(空)
					$xmlfile.="<UnitPrice></UnitPrice>\n";//單價(空)
				$xmlfile.="</OrderDetail>\n";
				$xmlfile.="<ShipmentDetail>\n";
					$xmlfile.="<ShipmentNo>".substr(($row['id']+60450001),0,8)."</ShipmentNo>\n";//出貨單編號
					$xmlfile.="<ShipDate>".date("Y-m-d",strtotime("+1 day",strtotime($row['ShipDate'])))."</ShipDate>\n";//出貨日期
					$xmlfile.="<ReturnDate>".$row['ReturnDate']."</ReturnDate>\n";//沒領退貨日
					if($allrow==$i){$YN='Y';}else{$YN='N';}
					$xmlfile.="<LastShipment>".$YN."</LastShipment>\n";//是否為最後一筆出貨單
					$xmlfile.="<ShipmentAmount>".$row['total_price']."</ShipmentAmount>\n";//訂單金額
					$xmlfile.="<StoreId>".$row['storeid'] ."</StoreId>\n";//門市代號
					$xmlfile.="<EshopType>".$store_EshopType."</EshopType>\n";//商平行泰代碼04
				$xmlfile.="</ShipmentDetail>\n";
			$xmlfile.="</Order>\n";
            // update id
            $sql_cmd = "update `order` set Delivery_No = '".$store_eshopid.substr(($row['id']+60450001),0,8)."',store_update = '2', Status = '4' where Order_ID = '".$row['order_id']."'";
            $db->query($sql_cmd);
			$sql_cmd = "update `store` set store_update_time = '".date("Y-m-d H:i:s")."' where Order_ID = '".$row['order_id']."'";
            $db->query($sql_cmd);
}
		$xmlfile.="</DocContent>\n";
	$xmlfile.="</OrderDoc>";
if(is_file('./SIN/'.$filename.'.xml')){ 
	for($i=1;$i<=99;$i++){
		if(!is_file('./SIN/'.$filename.'_bk'.$i.'.xml')){
 			  copy('./SIN/'.$filename.'.xml', './SIN/'.$filename.'_bk'.$i.'.xml');
			break;
		}
	}
}

$sql_cmd = "select * from var where type = 'PRESCO_FTP'";
$rs = $db->query($sql_cmd);
$email = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)['value'];
$email = explode(",",$email);

$fp=fopen('./SIN/'.$filename.'.xml',"w");
if(fwrite($fp,$xmlfile)){
	$ftp_connid=ftp_connect($store_ftpserver,$store_port);
	$ftp_logins= ftp_login($ftp_connid, $store_ftpuser, $store_ftppass);
	if((!$ftp_connid) || (!$ftp_logins)){
	    $datas = [
	        "title"   => "未正確上傳超商檔案至統一數網",
	        "content" => "本日超商XML檔案「".$filename.".xml」未正確上傳至統一數網，請檢查FTP服務是否正常。",
	        "type"    => 99,
	        "mail"    => $email,
	    ];
	    ClassMail::send_mail($datas);

	}else{
		ftp_pasv($ftp_connid,true);
		ftp_chdir($ftp_connid,'SIN');
		$ftp_upload=ftp_put($ftp_connid,$filename.'.xml','./SIN/'.$filename.'.xml', FTP_BINARY);
 		ftp_close($ftp_connid); //断开
		
		foreach($email as $value) {
			$datas = [
				"title"   => "超商檔案XML正確上傳至統一數網",
				"content" => "本日超商XML檔案「".$filename.".xml」已正確上傳至統一數網。",
				"type"    => 99,
				"mail"    => $value,
			];
			ClassMail::send_mail($datas);
		}
	}
}else{
    $datas = [
        "title"   => "未正確上傳超商檔案至統一數網",
        "content" => "本日超商XML檔案「".$filename.".xml」未正確上傳至統一數網，請檢查FTP服務是否正常。",
        "type"    => 99,
        "mail"    => $email,
    ];
    ClassMail::send_mail($datas);
}
?>
