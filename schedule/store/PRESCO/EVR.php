<?php 
include "../../../config.php"; 
	$nowdate=date("Y-m-d");
	$nowdatetime=date("Y-m-d H:i:s");
	//$nowdate='2013-07-31';
	//$nowdatetime='2013-07-31 12:33:42';
	include './storefun.php';
	$filename=$store_ParentId.$store_eshopid.str_replace('-','',$nowdate).'01';
	$ftp_connid=ftp_connect($store_ftpserver,$store_port);
	$ftp_logins= ftp_login($ftp_connid, $store_ftpuser, $store_ftppass);
	$fplog=fopen('../log/storelog.log','a');
	if((!$ftp_connid) || (!$ftp_logins)){
		fwrite($fplog,$nowdatetime.'(EVR)連接到超商ftp伺服器失敗!'."\r\n");
	}else{
		ftp_pasv($ftp_connid,true);
		ftp_chdir($ftp_connid,'EVR');
		$ftp_upload=@ftp_get($ftp_connid,'./EVR/'.$filename.'.EVR',$filename.'.EVR', FTP_BINARY);
		if($ftp_upload==false){
			fwrite($fplog,$nowdatetime.'(EVR)EVR檔案下載失敗!'."\r\n");
		}else{
			$isError = false;
			$content = "統一數網回覆：以下訂單已從大智通物流中心退回。<br>";
			$doc = new DOMDocument();
			$doc->load('./EDR/'.$filename.$PPSid.'.edr');
			$books = $doc->getElementsByTagName( "DCReturnAdvice" );
			foreach( $books as $book ){
				$ShipmentNos = $book->getElementsByTagName( "ShipmentNo" );
				$ShipmentNo = $ShipmentNos->item(0)->nodeValue;
				$DCRetCodes = $book->getElementsByTagName( "DCRetCode" );
				$DCRetCode = $DCRetCodes->item(0)->nodeValue;
				$DCRetNames = $book->getElementsByTagName( "DCRetName" );
				$DCRetName = $DCRetNames->item(0)->nodeValue;
				$DCRetDates = $book->getElementsByTagName( "DCRetDate" );
				$DCRetDate = $DCRetDates->item(0)->nodeValue;
				$sql_array['status']= array("2",intval(150));
				$sql_array['statusstr']= array("3","[".$DCRetCode."]".$DCRetName);
				$sql_array['DCReturnDate']= array("3",$DCRetDate);
				$sql_array['EDR']= array("3","代碼：".$DCRetCode."\n描述：".$DCRetName."廠退日期：".$DCRetDate);
				$sql_cmd = update("store", array("id", intval($ShipmentNo)), $sql_array);
				$rs = $db->query($sql_cmd);
				$isError = true;
				$sql_cmd = "select * from store where id = '".intval($ShipmentNo)."'";
				$rs = $db->query($sql_cmd);
				$row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
				$content .= "訂單編號：".$row['order_id']."<br>廠退日期：".$DCRetDate."<br>廠退狀態代碼：".$DCRetCode."<br>廠退狀態說明：".$DCRetName;
			}
			if($isError) {
				$sql_cmd = "select * from var where type = 'PRESCO_FTP'";
				$rs = $db->query($sql_cmd);
				$email = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)['value'];
				$email = explode(",",$email);
				
				foreach($email as $value) {
					$datas = [
						"title"   => "大智通廠退回報通知",
						"content" => $content,
						"type"    => 99,
						"mail"    => $email,
					];
					ClassMail::send_mail($value);
				}
			}
			fwrite($fplog,$nowdatetime.'(EVR)EVR檔案下載解析成功!'."\r\n");
		}
	}
 	ftp_close($ftp_connid); //断开
	Fclose($fplog);
?>
