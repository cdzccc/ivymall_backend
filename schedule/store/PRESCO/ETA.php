<?php 
include "../../../config.php"; 
	$nowdate=date("Y-m-d");
	$nowdatetime=date("Y-m-d H:i:s");
//	$nowdate='2013-08-20';
//	$nowdatetime='2013-08-20 12:33:42';
	include './storefun.php';
	$filename=$store_ParentId.$store_eshopid.str_replace('-','',$nowdate);
	$ftp_connid=ftp_connect($store_ftpserver,$store_port);
	$ftp_logins= ftp_login($ftp_connid, $store_ftpuser, $store_ftppass);
	$fplog=fopen('./log/storelog.log','a');
	if((!$ftp_connid) || (!$ftp_logins)){
		fwrite($fplog,$nowdatetime.'(ETA)連接到超商ftp伺服器失敗!'."\r\n");
	}else{
		ftp_pasv($ftp_connid,true);
		ftp_chdir($ftp_connid,'ETA');
		for($i=1;;$i++){
			$ETAid=substr($i+100,1,2);
			if(!is_file('./ETA/'.$filename.$ETAid.'.ETA')){
				break;
			}
		}
		$ftp_upload=@ftp_get($ftp_connid,'./ETA/'.$filename.$ETAid.'.ETA',$filename.$ETAid.'.ETA', FTP_BINARY); 
		if($ftp_upload==false){
			fwrite($fplog,$nowdatetime.'(ETA)ETA檔案下載失敗!'."\r\n");
		}else{
			$doc = new DOMDocument();
			$doc->load('./ETA/'.$filename.$ETAid.'.ETA');
			$books = $doc->getElementsByTagName( "ETAToEshop" );
			$isError = false;
			$content = "統一數網回覆以下訂單資料無法出貨至大智通物流中心。<br>";
			foreach( $books as $book ){
				$ShipmentNos = $book->getElementsByTagName( "ShipmentNo" );
				$ShipmentNo = $ShipmentNos->item(0)->nodeValue;
				$ReplyCodes = $book->getElementsByTagName( "ReplyCode" );
				$ReplyCode = $ReplyCodes->item(0)->nodeValue;
				$ReplyDetails = $book->getElementsByTagName( "ReplyDetail" );
				$ReplyDetail = $ReplyDetails->item(0)->nodeValue;
				unset($sql_array['statusstr']);
				if($ReplyCode=='02001'){
					$sql_array['status']= array("2",intval(50));
					$sql_array['statusstr']= array("3","[".$ReplyCode."]".$ReplyDetail);
					$sql_array['ETA']= array("3","代碼：".$ReplyCode."\n描述：".$ReplyDetail);
				}else{
					$isError = true;
					$sql_array['status']= array("2",intval(110));
					$sql_array['statusstr']= array("3","[".$ReplyCode."]".$ReplyDetail);
					$sql_array['ETA']= array("3","代碼：".$ReplyCode."\n描述：".$ReplyDetail);
					$sql_cmd = "select * from store where id = '".intval($store_id)."'";
					$rs = $db->query($sql_cmd);
					$row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
					$content .= "訂單編號：".$row['order_id']."<br>回應代碼：".$ReplyCode."<br>回應詳細內容：".$ReplyDetails;
				}
				$sql_cmd = "select * from `order` where Delivery_No = '".$ShipmentNo."' and delivery = 1";
				$rs = $db->query($sql_cmd);
				if($rs->numRows() > 0) {
					$order = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);

					$sql_cmd = update("store", array("order_id", $order['Order_ID']), $sql_array);
					$rs = $db->query($sql_cmd);
				}
			}
			if($isError) {
				$sql_cmd = "select * from var where type = 'PRESCO_FTP'";
				$rs = $db->query($sql_cmd);
				$email = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)['value'];
				$email = explode(",",$email);
				
				foreach($email as $value) {
					$datas = [
						"title"   => "ETAToEshop出貨門市路線錯誤回報",
						"content" => $content,
						"type"    => 99,
						"mail"    => $value,
					];
					ClassMail::send_mail($datas);
				}
			}
			fwrite($fplog,$nowdatetime.'(ETA)ETA檔案下載解析成功!'."\r\n");
		}
	}
 	ftp_close($ftp_connid); //断开
	Fclose($fplog);
?>
