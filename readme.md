#php version: 7.2
####php.ini配置
short_open_tag=On;

#資料庫
####mysql

#程式設定
將config.php.sample複製，另存為config.php
修改網址及資料庫的連線資訊

##安裝 Bower 管理套件
#####透過 Node.js 的 npm 工具來安裝
    $ npm install bower -g
####Bower 套件相依性
#####套件安裝
######
    $ bower install
#####使用套件
* bootstrap 3.3.7
* bootstrap-datepicker 1.7.1
* bootstrap-daterangepicker 2.1.27
* bootstrap-timepicker 0.5.2
* ckeditor 4.8.0
* font-awesome 4.7.0
* ionicons 2.0.1
* jquery 3.3.1
* jquery-ui 1.11.4
* moment 2.20.1


#排程
####會員相關
#####會員生日禮
    $ /schedule/customer/birthday.php
#####點數啟用處理
    $ /schedule/customer/point.php
#####過期點數處理
    $ /schedule/customer/point_cancel.php
#####會員升級
    $ /schedule/customer/level.php
#####年度結算
    $ /schedule/customer/annual_settlement.php
####訂單相關
#####訂單完成給點及票券
    $ /schedule/order/finish.php
#####上傳發票資訊
    $ /schedule/invoice/upload.php
#####下載發票資訊
    $ /schedule/invoice/download.php

####統一數網
#####出貨資料拋檔
    $ /schedule/store/PRESCO/SIN.php
#####出貨資料報錯m
    $ /schedule/store/PRESCO/SRP.php
#####修正:出貨日期,配編,門市代碼
    $ /schedule/store/PRESCO/SUP.php
#####修正訂單資料處理回覆
    $ /schedule/store/PRESCO/SURP.php
#####預定出貨資料（出貨門市路線）回應
    $ /schedule/store/PRESCO/ETA.php
#####DC 進貨驗收回檔
    $ /schedule/store/PRESCO/EIN.php
#####商品到(離)店檔
    $ /schedule/store/PRESCO/PPS.php
#####門市銷帳檔
    $ /schedule/store/PRESCO/ESP.php
#####預定退貨資料
    $ /schedule/store/PRESCO/ERT.php
#####DC 退貨驗收回檔
    $ /schedule/store/PRESCO/EDR.php
#####廠退
    $ /schedule/store/PRESCO/EVR.php

####便利達康
#####出貨資料拋檔
    $ /schedule/store/STORE_CVS/F10.php
#####XML店鋪資料檔
    $ /schedule/store/STORE_CVS/F01.php
#####大物流驗收檔
    $ /schedule/store/STORE_CVS/F03.php
#####進店即時檔
    $ /schedule/store/STORE_CVS/F44.php
#####進店檔
    $ /schedule/store/STORE_CVS/F04.php
#####取貨完成檔
    $ /schedule/store/STORE_CVS/F05.php
#####大物流驗退檔
    $ /schedule/store/STORE_CVS/F07.php
#####訂單預退檔
    $ /schedule/store/STORE_CVS/F61.php
#####取消出貨檔
    $ /schedule/store/STORE_CVS/F09.php
#####取消代收檔
    $ /schedule/store/STORE_CVS/F45.php
