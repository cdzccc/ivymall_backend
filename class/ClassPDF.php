<?php
    $root = preg_replace("/(.*?)\/(admin|schedule|tools).*/", "$1", $_SERVER['SCRIPT_FILENAME']);

    require $root.'/vendor/autoload.php';
    require $root.'/class/Archive/Zip.php';

    use Spipu\Html2Pdf\Html2Pdf;
    class ClassPDF{ 
        function create_order_list($data) {
            $content = "";
            $file_list = [];
            foreach ($data as $key => $value) {
                $content = file_get_contents("./pdf/Orderlist.html");

                $content = str_replace('@data_email', $value['Customer_Mail'], $content);
                $content = str_replace('@data_level', $value['level'], $content);
                $content = str_replace('@data_order_id', $value['Order_ID'], $content);
                $content = str_replace('@order_createdate', $value['create_datetime'], $content);

                // 收件人資訊
                $url = "http://zipcode.mosky.tw/api/find?address=".urlencode($value['city'].$value['area'].$value['address']);
                $zipcode = json_decode(curling($url))->result;
                $content = str_replace('@data_name', $value['name'], $content);
                $content = str_replace('@data_phone', $value['phone'], $content);
                $address = $zipcode.$value['city'].$value['area'].$value['address'];
                $content = str_replace('@data_addr', implode("<br>", self::utf8_str_split($address, 20)), $content);

                $url = "http://zipcode.mosky.tw/api/find?address=".urlencode($value['Delivery_City'].$value['Delivery_Area'].$value['Delivery_Addr']);
                $zipcode = json_decode(curling($url))->result;
                $content = str_replace('@data_delivery_name', $value['Delivery_Name'], $content);
                $content = str_replace('@data_delivery_mobile', $value['Delivery_Mobile'], $content);
                $content = str_replace('@data_delivery_phone', $value['Delivery_Phone'], $content);
                $address = $zipcode.$value['Delivery_City'].$value['Delivery_Area'].$value['Delivery_Addr'];
                $content = str_replace('@data_delivery_addr', implode("<br>", self::utf8_str_split($address, 20)), $content);


                // 取貨資訊
                switch ($value['delivery']) {
                    case '1':
                    case '2':
                        $content = str_replace('@data_store_type', "取貨門市", $content);
                        $content = str_replace('@data_store_name', $value['store_name'], $content);
                        $content = str_replace('@data_store_addr_name', "門市地址：", $content);
                        $content = str_replace('@data_store_addr', $value['store_addr'], $content);

                        break;
                    case '3':
                        switch ($value['Delivery_time']) {
                            case '1':
                                $data_store_name = "不指定";
                                break;
                            case '1':
                                $data_store_name = "13:00前";
                                break;
                            case '1':
                                $data_store_name = "14:00至18:00";
                                break;
                            default:
                                $data_store_name = "不指定";
                                break;
                        }
                        $content = str_replace('@data_store_type', "希望收件時段:", $content);
                        $content = str_replace('@data_store_name', $data_store_name, $content);
                        $content = str_replace('@data_store_addr_name', "", $content);
                        $content = str_replace('@data_store_addr', "", $content);
                        break;
                }

                // 發票
                $i_info = self::i_type($value);
                $content = str_replace('@data_i_info', $i_info, $content);

                // 金額
                $content = str_replace('@data_note', $value['Note'], $content);
                $content = str_replace('@data_ticket', $value['discount_ticket'], $content);
                $content = str_replace('@data_point', $value['discount_ticket'], $content);
                $content = str_replace('@data_event', $value['discount_customer'], $content);
                $content = str_replace('@data_discount_p', $value['discount_purchase'], $content);
                $content = str_replace('@data_discount_off', $value['discount_off'], $content);
                $content = str_replace('@data_delivery_price', $value['Delivery_Price'], $content);
                $content = str_replace('@data_total_price', number_format($value['total_price']), $content);
                $index = 1;
                $item = "";
                foreach($value['item'] as $item_row) {
                    $item .= self::product_list();
                    $item = str_replace('@data_goods_index', $index, $item);
                    $item = str_replace('@data_goods_item_number', $item_row['item_number'], $item);

                    switch ($item_row['type']) {
                        case '1':
                            $goods_name = $item_row['Goods_Name'];
                            break;
                        case '2':
                            $goods_name = $item_row['Goods_Name'];
                            $goods_name .= "<br>套書詳細內容：<br>";
                            foreach ($item_row['item_group'] as $item_group) {
                                $goods_name .= "(".$item_group['item_number'].")".$item_group['Goods_Name']."<br>";
                            }
                            break;
                        case '3':
                            $goods_name = $item_row['Goods_Name'];
                            break;
                    }
                    $item = str_replace('@data_goods_name', $goods_name, $item);

                    switch ($item_row['type']) {
                        case '1':
                            $item = str_replace('@data_goods_remark', "", $item);
                            break;
                        case '2':
                            $item = str_replace('@data_goods_remark', "", $item);
                            break;
                        case '3':
                            $remark = $item_row['goods_item']['Name']."<br>起始月份：".$item_row['start_month']."<br>起始期數：".$item_row['start_periods'];
                            $item = str_replace('@data_goods_remark', $remark, $item);
                            break;
                    }
                    $item = str_replace('@data_goods_price', number_format($item_row['Goods_Price']), $item);
                    $item = str_replace('@data_goods_qty', $item_row['Goods_Qty'], $item);
                    $item = str_replace('@data_goods_total_price', number_format($item_row['Total_Price']), $item);
                    if(!empty($item_row['gift_item'])) {
                        $item .= self::product_list();
                        $item = str_replace('@data_goods_index', "", $item);
                        $item = str_replace('@data_goods_item_number', $item_row['gift_item']['item_number'], $item);
                        $item = str_replace('@data_goods_name', $item_row['gift_item']['name'], $item);
                        $item = str_replace('@data_goods_remark', "贈品", $item);
                        $item = str_replace('@data_goods_price', "0", $item);
                        $item = str_replace('@data_goods_qty', $item_row['gift_number'], $item);
                        $item = str_replace('@data_goods_total_price', "0", $item);
                    }
                }
                $content = str_replace('@data_goods_row', $item, $content);
                $html2pdf = new Html2Pdf('P', 'A4', 'en');
                $html2pdf->setDefaultFont("cid0jp");
                $html2pdf->writeHTML($content);
                $html2pdf->output(DOCUMENT_ROOT.'/admin/orders/pdf_file/'.$value['Order_ID'].'.pdf', 'F');
                $file_list[] = DOCUMENT_ROOT.'/admin/orders/pdf_file/'.$value['Order_ID'].'.pdf';
            }
            // 建立 Archive_Zip 物件，及定義檔案名稱
            $archive_file_name = "order_".date("Ymd").".zip";
            $zipfile = New Archive_Zip($archive_file_name);

            // 建立 zip 檔案
            $p_params = array('remove_path' => DOCUMENT_ROOT.'/admin/orders/pdf_file/');
            $zipfile->create($file_list,$p_params);
            
            header("Content-type: application/zip"); 
            header("Content-Disposition: attachment; filename=$archive_file_name"); 
            header("Pragma: no-cache"); 
            header("Expires: 0"); 
            readfile($archive_file_name);
            foreach($file_list as $file) {
                unlink($file);
            }
            unlink($archive_file_name);

            exit;
        }

        public function create_store_711_label($data) {
            $content = "";
            foreach ($data as $key => $value) {
                $content = file_get_contents("./label.html");
                $content = str_replace('@data_note', $value['note'], $content);
                $content = str_replace('@data_name', $value['name'], $content);
                $content = str_replace('@data_store_id', $value['store_id'], $content);
                $content = str_replace('@data_store_name', $value['store_name'], $content);
                $content = str_replace('@data_chars_content', $value['chars'], $content);
                $content = str_replace('@data_chars_img', $value['chars'], $content);
                $content = str_replace('@data_ShipDate', $value['ShipDate'], $content);
                $content = str_replace('@data_ReturnDate', $value['ReturnDate'], $content);
                $content = str_replace('@data_order_id', $value['order_id'], $content);
                $width_in_mm = 85; 
                $height_in_mm = 85;
                $html2pdf = new Html2Pdf('P', [$width_in_mm,$height_in_mm], 'en', true, 'UTF-8', array(0, 0, 0, 0));
                // $html2pdf->setDefaultFont("kaiu", '', '', true);

                $html2pdf->writeHTML($content);
                $html2pdf->output(DOCUMENT_ROOT.'/admin/store/pdf_file/'.$value['order_id'].'.pdf', 'F');
                $file_list[] = DOCUMENT_ROOT.'/admin/store/pdf_file/'.$value['order_id'].'.pdf';
            }
            // 建立 Archive_Zip 物件，及定義檔案名稱
            $archive_file_name = "store_".date("Ymd").".zip";
            $zipfile = New Archive_Zip($archive_file_name);

            // 建立 zip 檔案
            $p_params = array('remove_path' => DOCUMENT_ROOT.'/admin/store/pdf_file/');
            $zipfile->create($file_list,$p_params);
            
            header("Content-type: application/zip"); 
            header("Content-Disposition: attachment; filename=$archive_file_name"); 
            header("Pragma: no-cache"); 
            header("Expires: 0"); 
            readfile($archive_file_name);
            foreach($file_list as $file) {
                unlink($file);
            }
            unlink($archive_file_name);

            exit;
        }
        public function create_store_cvs_label($data) {
            $content = "";
            foreach ($data as $key => $value) {
                $content = file_get_contents("./label.html");
                $content = str_replace('@data_note1', $value['note'], $content);
                $content = str_replace('@data_name', $value['name'], $content);
                $content = str_replace('@data_store_id', $value['store_id'], $content);
                $content = str_replace('@data_store_name', $value['store_name'], $content);
                $content = str_replace('@data_ShipDate', $value['ShipDate'], $content);
                $content = str_replace('@data_ReturnDate', $value['ReturnDate'], $content);
                $content = str_replace('@data_order_id', $value['order_id'], $content);
                $content = str_replace('@data_area', $value['area'], $content);
                $content = str_replace('@data_store_company', $value['store_company'], $content);
                $content = str_replace('@data_qr_content', $value['qr_content'], $content);
                $content = str_replace('@data_EQPID', $value['EQPID'], $content);
                $content = str_replace('@data_RSNO', $value['RSNO'], $content);
                $content = str_replace('@data_EC', $value['EC'], $content);
                $content = str_replace('@data_note2', $value['note2'], $content);
                $content = str_replace('@data_mobile', $value['mobile'], $content);
                $content = str_replace('@data_barcode1', $value['barcode1'], $content);
                $content = str_replace('@data_barcode2', $value['barcode2'], $content);
                $content = str_replace('@data_barcode3', $value['barcode3'], $content);
                $width_in_mm = 80; 
                $height_in_mm = 125;
                $html2pdf = new Html2Pdf('P', [$width_in_mm,$height_in_mm], 'en', true, 'UTF-8', array(0, 0, 0, 0));
                // $html2pdf->setDefaultFont("biao_kaiti");
                $html2pdf->writeHTML($content);
                $html2pdf->output(DOCUMENT_ROOT.'/admin/store_cvs/pdf_file/'.$value['order_id'].'.pdf', 'F');
                // 建立 Archive_Zip 物件，及定義檔案名稱
                $file_list[] = DOCUMENT_ROOT.'/admin/store_cvs/pdf_file/'.$value['order_id'].'.pdf';
            }

            $archive_file_name = "store_cvs_".date("Ymd").".zip";
            $zipfile = New Archive_Zip($archive_file_name);

            // 建立 zip 檔案
            $p_params = array('remove_path' => DOCUMENT_ROOT.'/admin/store_cvs/pdf_file/');
            $zipfile->create($file_list,$p_params);
            
            header("Content-type: application/zip"); 
            header("Content-Disposition: attachment; filename=$archive_file_name"); 
            header("Pragma: no-cache"); 
            header("Expires: 0"); 
            readfile($archive_file_name);
            foreach($file_list as $file) {
                unlink($file);
            }
            unlink($archive_file_name);

            exit;
        }
        function product_list() {
            return '<tr>
                        <td colspan="7" height="1" style="border-bottom: 1px solid #898989;"></td>
                    </tr>
                    <tr>
                        <td align="center" bgcolor="#fffaf0" style="font-size:12px;">@data_goods_index</td>
                        <td bgcolor="#fffaf0" style="font-size:12px;">@data_goods_item_number</td>
                        <td bgcolor="#fffaf0" style="font-size:12px;">@data_goods_name</td>
                        <td bgcolor="#fffaf0" style="font-size:12px;">@data_goods_remark</td>
                        <td bgcolor="#fffaf0" style="font-size:12px;">$ @data_goods_price</td>
                        <td bgcolor="#fffaf0" style="font-size:14px;">@data_goods_qty</td>
                        <td bgcolor="#fffaf0" style="font-size:14px;">$ @data_goods_total_price</td>
                    </tr>';
        }
        function i_type($data) {
            switch ($data['i_type']) {
                case '1':
                    $content = '<tr>
                                    <th align="left" style="font-size:12px;font-weight:300;" scope="col">捐贈發票</th>
                                    <th colspan="4" align="left" style="font-size:12px;font-weight:300;" scope="col"></th>
                                </tr>';
                    break;
                case '2':
                    $content = '<tr>
                                    <th bgcolor="#ECEFF3" style="font-size:12px;" scope="col">&nbsp;</th>
                                    <th align="left" style="font-size:12px;font-weight:300;" scope="col">電子發票</th>
                                    <th colspan="4" align="left" style="font-size:12px;font-weight:300;" scope="col">（您已選擇若電子發票中獎，將以掛號寄紙本發票至購買人/收件人）</th>
                                </tr>';
                    break;
                case '3':
                    $content = '<tr>
                                    <th bgcolor="#ECEFF3" style="font-size:12px;" scope="col">&nbsp;</th>
                                    <th align="left" style="font-size:12px;font-weight:300;" scope="col">&nbsp;</th>
                                    <th align="left" style="font-size:12px;font-weight:300;" scope="col">統編：</th>
                                    <th align="left" style="font-size:12px;font-weight:300;" scope="col">@data_i_tax_id_number</th>
                                    <th align="left" style="font-size:12px;font-weight:300;" scope="col">抬頭：</th>
                                    <th align="left" style="font-size:12px;font-weight:300;" scope="col">@data_i_title</th>
                                </tr>';
                    break;
            }

            $content .= '<tr>
                <th bgcolor="#ECEFF3" style="font-size:12px;" scope="col">&nbsp;</th>
                <th align="left" style="font-size:12px;font-weight:300;" scope="col">&nbsp;</th>
                <th align="left" style="font-size:12px;font-weight:300;" scope="col">統編：</th>
                <th align="left" style="font-size:12px;font-weight:300;" scope="col">@data_i_tax_id_number</th>
                <th align="left" style="font-size:12px;font-weight:300;" scope="col">抬頭：</th>
                <th align="left" style="font-size:12px;font-weight:300;" scope="col">@data_i_title</th>
            </tr>';
            $content = str_replace('@data_i_tax_id_number', $data['i_tax_id_number'], $content);
            $content = str_replace('@data_i_title', $data['i_title'], $content);
            return $content;
        }
        function utf8_str_split($str, $split_len = 1)
        {
            if (!preg_match('/^[0-9]+$/', $split_len) || $split_len < 1)
                return FALSE;
         
            $len = mb_strlen($str, 'UTF-8');
            if ($len <= $split_len)
                return array($str);
         
            preg_match_all('/.{'.$split_len.'}|[^\x00]{1,'.$split_len.'}$/us', $str, $ar);
         
            return $ar[0];
        }
    }
?>