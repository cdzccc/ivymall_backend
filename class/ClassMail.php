<?php
require_once(DOCUMENT_ROOT."/class/phpmailer/class.phpmailer.php");
class ClassMail { 		
	/* 主旨、寄件人可自行設定 */
	static function send_mail(array $datas){
        $mg = [
            'from_email' => 'admin@ivy2.begonia-design.com.tw',
            'password'   => '^6wCez36',
        ];

        // $url = "https://" . $mg['version'] . $mg['domain'] . "/messages";
        $html = self::MailBody($datas);

        if ($html != "fail") {
            $mail= new PHPMailer();
            $mail->IsSMTP();//設定使用SMTP方式寄信
            $mail->IsHTML(true);//設定郵件內容為HTML
            $mail->SMTPAuth=true;//設定SMTP需要驗證
            // $mail->SMTPtSecure="ssl";//Gmail的SMTP主機需要使用SSL連線
            $mail->Host="ivy2.begonia-design.com.tw";//Gamil的SMTP主機
            $mail->Port=25;//Gamil的SMTP主機的SMTP埠位為465埠。
            $mail->CharSet="utf-8";//設定郵件編碼
            $mail->Encoding="base64";//設定信件編碼，大部分郵件工具都支援此編碼方式
            $mail->Username=$mg['from_email'];//設定驗證帳號

            $mail->Password = $mg['password'];//設定驗證密碼

            $mail->From="service@ivy.com.tw";//設定寄件者信箱
            
            $mail->FromName="常春藤網路書城";//設定寄件者姓名
            $mail->Subject=$datas['title'];//設定郵件標題
            $mail->Body=$html;//設定郵件內容
            $mail->AddAddress($datas['mail'],$datas['mail']);//設定收件者郵件及名稱
            $temp=$mail->Send();
            if(!$temp) {
                $temp = $mail->ErrorInfo;
            }
            /* write log */
            $log = new Logging();
            $category = "ClassMail";
            if ($temp == 1) {
                $filename = "success";
            }
            else {
                $filename = "fail";
            }
            $log->lcfile($category,$filename);
            $log->lwrite('Email:'.$datas['mail']);
            $log->lwrite('Subject:'.$datas['title']);
            $log->lwrite('Result:'.$temp);

            $log->lclose();
            /* end write log */
            // var_dump($temp);
            // exit;
            return $temp;
        }

        return "false";
	}
            // Get Mail Body
    private static function MailBody(array $data)
    {
        switch ($data['type']) {
            case "1":
                $data['content'] = str_replace(array("\n", "\r"), '', $data['content']);
                $mail_body = $data['content'];
                return $mail_body;
                break;
            case "2"://貨到通知管理
                $mail_body = file_get_contents(DOCUMENT_ROOT."/class/mailbody/ArrivalNotice.html");

                $mail_body = preg_replace("/\@data\_email/i", $data['mail'], $mail_body);
                $mail_body = preg_replace("/\@data\_goods/i", $data['goods_name'], $mail_body);
                $mail_body = preg_replace("/\@link\_goods/i", $data['link_goods'], $mail_body);

                return $mail_body;
                break;
            case "3"://超商到店通知管理
                $mail_body = file_get_contents(DOCUMENT_ROOT."/class/mailbody/ArrivalStore.html");

                return $mail_body;
                break;
            case "4"://訂單問題回覆通知
                $mail_body = file_get_contents(DOCUMENT_ROOT."/class/mailbody/order-QA.html");
                $mail_body = preg_replace("/\@data\_email/i", $data['mail'], $mail_body);
                $mail_body = preg_replace("/\@data\_order_id/i", $data['Order_ID'], $mail_body);
                $mail_body = preg_replace("/\@data\_question/i", $data['question'], $mail_body);
                $mail_body = preg_replace("/\@data\_answer/i", $data['answer'], $mail_body);

                return $mail_body;
                break;
            case "5"://超商到店通知管理
                $mail_body = file_get_contents(DOCUMENT_ROOT."/class/mailbody/order-StoreArrival.html");
                $mail_body = preg_replace("/\@data\_email/i", $data['mail'], $mail_body);
                $mail_body = preg_replace("/\@data\_order_id/i", $data['order_id'], $mail_body);
                $mail_body = preg_replace("/\@data\_order_create_date/i", $data['order_create_date'], $mail_body);
                $mail_body = preg_replace("/\@data\_store/i", $data['store'], $mail_body);
                $mail_body = preg_replace("/\@data\_store_addr/i", $data['store_addr'], $mail_body);
                $mail_body = preg_replace("/\@data\_take_time/i", $data['take_time'], $mail_body);
                $mail_body = preg_replace("/\@data\_deivery/i", $data['deivery'], $mail_body);
                $mail_body = preg_replace("/\@data\_name/i", $data['name'], $mail_body);
                $mail_body = preg_replace("/\@data\_price/i", $data['price'], $mail_body);

                return $mail_body;
                break;
            case "6"://生日禮
                $mail_body = file_get_contents(DOCUMENT_ROOT."/class/mailbody/birthday.html");
                $mail_body = preg_replace("/\@data\_email/i", $data['mail'], $mail_body);
                $mail_body = preg_replace("/\@data\_point/i", $data['point'], $mail_body);
                $mail_body = preg_replace("/\@data\_e_date/i", $data['e_date'], $mail_body);

                return $mail_body;
                break;
            case "7"://取消訂單
                $mail_body = file_get_contents(DOCUMENT_ROOT."/class/mailbody/order-Cancel.html");
                $mail_body = preg_replace("/\@data\_email/i", $data['mail'], $mail_body);
                $mail_body = preg_replace("/\@data\_order_id/i", $data['order_id'], $mail_body);
                $mail_body = preg_replace("/\@data\_order_create_date/i", $data['order_create_date'], $mail_body);
                $mail_body = preg_replace("/\@data\_cancel_reason/i", $data['cancel_reason'], $mail_body);

                return $mail_body;
                break;

            case "99"://自定義訊息
                $mail_body = $data['content'];
                return $mail_body;
                break;
        }
    }
}
?>