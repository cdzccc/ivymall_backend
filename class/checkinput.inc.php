<?php
//正確取出字串
function cutstr($string, $length)
{
    preg_match_all("/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|\xe0[\xa0-\xbf][\x80-\xbf]|[\xe1-\xef][\x80-\xbf][\x80-\xbf]|\xf0[\x90-\xbf][\x80-\xbf][\x80-\xbf]|[\xf1-\xf7][\x80-\xbf][\x80-\xbf][\x80-\xbf]/", $string, $info);  
        for($i=0; $i<count($info[0]); $i++) {

                $wordscut .= $info[0][$i];

                $j = ord($info[0][$i]) > 127 ? $j + 2 : $j + 1;

                if ($j > $length - 3) {
                     return $wordscut;
                }
       }
       return join('', $info[0]);
}

function utf8_str_split($str, $split_len = 1)
{
    if (!preg_match('/^[0-9]+$/', $split_len) || $split_len < 1)
        return FALSE;
 
    $len = mb_strlen($str, 'UTF-8');
    if ($len <= $split_len)
        return array($str);
 
    preg_match_all('/.{'.$split_len.'}|[^\x00]{1,'.$split_len.'}$/us', $str, $ar);
 
    return $ar[0];
}

//檢查輸入
function checkinput($str, $num) 
{
    global $db;
 	$str = substr(trim($str), 0, $num);
    // 去除斜杠
    if (get_magic_quotes_gpc()) {
    $str = stripslashes($str);
    }
    // 如果不是数字则加引号
    if (!is_numeric($str)) {
    // $str = mysql_real_escape_string($str);
        $str = $db->quote($str, 'text');
    }
	return $str;
}

function checkinput_sql($str, $num) 
{
 	$str = substr(trim($str), 0, $num);
	return $str;
}

//utf 字串分割
function utf_checkinput($str, $num) 
{
 	$num = $num * 3;
	$str = addslashes(cutstr(trim($str), $num));
	return $str;
}

function utf_checkinput_sql($str, $num) 
{
 	$num = $num * 3;
	$str = cutstr(trim($str), $num);
	return $str;
}

//輸出
function checkoutput($str) 
{
 	$str = strip_tags(stripslashes(trim($str)));
	return $str;
}

function checkoutput_html($str) 
{
 	$str = stripslashes(trim($str));
	return $str;
}
?>