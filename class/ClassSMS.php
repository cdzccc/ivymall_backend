<?php
class ClassSMS{
    private static $smsHost="api.every8d.com";
    private static $sendSMSUrl="http://api.every8d.com/API21/HTTP/sendSMS.ashx";
    private static $getCreditUrl="http://api.every8d.com/API21/HTTP/getCredit.ashx";
    private static $batchID="";
    private static $credit=0.0;
    private static $processMsg="";
    private static $userID="0963828173";
    private static $password="51xueLeifeng";

    /*傳送參數到sms主機取得資料 ([遠端網址],[傳送參數])*/
    private static function httpPost($url, $postData){
        $result="";
        $res="";
        $length=strlen($postData);
        $fp=fsockopen(self::$smsHost,80,$errno,$errstr);
        if ($fp) {
            # code...
            $header="POST ".$url." HTTP/1.0\r\n";
            $header.="Content-Type: application/x-www-form-urlencoded; charset=utf-8\r\n";
            $header.="Content-Length: ".$length."\r\n\r\n";
            $header.=$postData."\r\n";
            fputs($fp,$header,strlen($header));
            while(!feof($fp)){
                $res.=fgets($fp,1024);
            }
            // echo "success";
        }
        else {
            // echo "fail";
            // fsockopen 連線失敗
        }
        fclose($fp);
        $strArray=explode("\r\n\r\n",$res);
        $result=$strArray[1];
        return $result;
    }

    /*取得帳號餘額 */
    static function getCredit(){
        $postDataString="UID=".self::$userID."&PWD=".self::$password;
        $resultString=self::httpPost(self::$getCreditUrl,$postDataString);
        // if(substr($resultString,0,1)=="-"){
        // 	self::$processMsg=$resultString;
        // 	return false;
        // }else{
        // 	self::$credit=$resultString;
        // 	return true;
        // }
        return $resultString;
    }

    /* 傳送簡訊 ([帳號],[密碼],[主旨],[內容],[門號],[發送時間])
        主旨=簡訊後台標題,發給客戶不會發送主旨
        門號=接收人手機,格式+886912345678或0912345678
        發送時間=[空值]立刻發送 [YYYYMMDDhhmnss]指定時間 */
    private static function sendSMS($subject,$content,$mobile,$sendTime){
        $postDataString="UID=".self::$userID;
        $postDataString.="&PWD=".self::$password;
        $postDataString.="&SB=".$subject;
        $postDataString.="&MSG=".$content;
        $postDataString.="&DEST=".$mobile;
        $postDataString.="&ST=".$sendTime;
        $resultString=self::httpPost(self::$sendSMSUrl,$postDataString);
        if(substr($resultString,0,1)=="-"){
            // echo $resultString;
            self::$processMsg=$resultString;
            return false;
        }else{
            $strArray=explode(",",$resultString);
            self::$credit=$strArray[0];
            self::$batchID=$strArray[4];
            return true;
        }
    }
}
?>
