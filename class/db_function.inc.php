<?php
function getDB($dsn)
{
//    $DB = new DB;
	// 建立資料庫連結
    $ClassDB = new DB();
    $db = $ClassDB->connect($dsn);
	if ($ClassDB->isError($db))
	{
		header("Content-Type: text/plain; charset=utf-8");
		die ($db->getMessage());
	}
	$db->query("SET NAMES 'utf8'");
	//時區設定
	date_default_timezone_set('Asia/Taipei');
	return $db;
}
function getMDB2($dsn,$option = false)
{
	// 建立資料庫連結
    $ClassDB = new MDB2();
    $db = MDB2::connect($dsn,['debug'=>true]);

	if (MDB2::isError($db))
	{
		header("Content-Type: text/plain; charset=utf-8");
		die ($db->getMessage());
	}
	$db->query("SET NAMES 'utf8'");
	//時區設定
	date_default_timezone_set('Asia/Taipei');
	return $db;
}
//加入資料庫函數
function insert($table, $sql_array)
{
	$sql_cmd = "insert into `".$table."` (";
	$sql = "";
	//組合欄位
	$i=1;
	 // while(list($index, $data) = each($sql_array)) 
	 foreach($sql_array as $index=>$data)
	 {
	 	if ($i==1)
		{
			$i_out = "";
			$i=2;
		}else{
			$i_out = ", ";
		}
		
		$sql_cmd .= $i_out."`".$index."`";
		
		//輸出post 
		switch ($data[0])
		{
			case "1":
				$sql .= $i_out.$data[1];
				break;
			case "2":
				$sql .= $i_out."'".addslashes($data[1])."'";
				break;
			case "3":
				$sql .= $i_out."'".$data[1]."'";
				break;
		}
		
		
	 }
	
	 $sql_cmd .= ") values (";	
	 $sql .=")";
	return $sql_cmd.$sql;
}


//更新資料庫函數
function update($table, $id, $sql_array)
{
	$sql_cmd = "update `".$table."` set ";
	//組合欄位
	$i=1;
	 foreach($sql_array as $index=>$data)
	 {
	 	if ($i==1)
		{
			$i_out = "";
			$i=2;
		}else{
			$i_out = ", ";
		}
		
		//輸出post 
		switch ($data[0])
		{
			case "1":
				$sql_cmd .= $i_out."`".$index."` = ".$data[1];
				break;
			case "2":
				$sql_cmd .= $i_out."`".$index."` = '".addslashes($data[1])."'";
				break;
			case "3":
				$sql_cmd .= $i_out."`".$index."` = '".$data[1]."'";
				break;
		}
		
		
	 }
	
    $sql_cmd .= " where ".$id[0]." = '".$id[1]."'";	
	return $sql_cmd;
}
?>