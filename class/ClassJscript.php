<?php
	class ClassJscript{
		static function islogin(){//沒有登入
			if(!isset($_SESSION[SESSION_VARIABLE."_user_id"]) || $_SESSION[SESSION_VARIABLE."_user_id"] == "" || strlen($_SESSION[SESSION_VARIABLE."_user_id"]) == 0){
				if(preg_match("/^\/store/", $_SERVER["REQUEST_URI"])) {
					if(!empty($_GET['token'])) {
						global $db;
						$sql_cmd = "select * from dealer where token = '".checkinput_sql($_GET['token'], 128)."'";
						$rs = $db->query($sql_cmd);
						if($rs->numRows() == 0) {
							echo "<script>location.href='".WEBSITE_URL."store/login.php"."'</script>";
							exit();
						}
						else {
							$row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
					        $_SESSION[SESSION_VARIABLE."_user_s"] = checkoutput($row['Name']);
					        $_SESSION[SESSION_VARIABLE."_user_id"] = $row['Dealer_ID'];
						}
					}
					else {
						echo "<script>location.href='".WEBSITE_URL."store/login.php"."'</script>";
						exit();
					}
						// self::alert_go('您已超過登入時間, 或是您尚未登入!!',WEBSITE_URL.'store/login.php');
				}
				else {
					echo "<script>location.href='".WEBSITE_URL."admin/login.php"."'</script>";
					exit();
					// self::alert_go('您已超過登入時間, 或是您尚未登入!!',WEBSITE_URL.'admin/login.php');
				}
			}
		}
		static function isadmino($modname){//沒有權限
			$priv_s_array=explode(",",$_SESSION[SESSION_VARIABLE."_priv_s"]);
			// print_r($_SESSION) ;die;
			if (!in_array($modname,$priv_s_array)){
				self::alert_go('您沒有此功能的權限',WEBSITE_URL.'admin/index.php');
			}
		}
		static function user_token_check($token) {
			global $db;
			//檢查參數有無存在
			if (empty($token)){
			    $return['status'] = "failed";
			    $return['message'] = "token empty";
			    echo json_encode($return);
			    exit;
			}
			$sql_cmd = "select * from `member` where token='".checkinput_sql($token, 128)."'";
			$rs = $db->query($sql_cmd);
			if($rs->numRows() == 0) {
			    $return['status'] = "failed";
			    $return['message'] = "token error";
			    echo json_encode($return);
			    exit;
			}
			return $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
		}
		private static function alert_go($str,$url){
			echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
				<script language="javascript">
					alert("'.$str.'");';
			if(empty($url)){
				echo 'history.go(-1);';
			}else{
				echo 'window.location.replace("'.$url.'");';
			}
			echo '</script>';
			exit();
		}

	}
?>