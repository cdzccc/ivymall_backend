<?php
    class Logging {
        // declare log file and file pointer as private properties
        private $log_file, $fp;

        // set/create log file by category and name
        public function lcfile($category,$filename) {
            if( PHP_OS == "Linux") {
                $log_path = DOCUMENT_ROOT."/logs";
                $slash = "/";
            }
            else if ( PHP_OS == "Darwin") {
                $log_path = DOCUMENT_ROOT."/logs";
                $slash = "/";

            }
            else {
                $log_path = DOCUMENT_ROOT."/logs";
                $slash = "/";
            }
            $log_path.= $slash.$category;
            if (!is_dir($log_path)) {
                $old = umask(0);
                mkdir($log_path,0777);
                umask($old);
            }
            $log_path .= $slash.$filename;
            if (!is_dir($log_path)) {
                $old = umask(0);
                mkdir($log_path,0777);
                umask($old);
            }
            $path = $log_path.$slash.$filename.date("Ymd").".log";
            $this->log_file = $path;
        }

        // set log file (path and name)
        public function lfile($path) {
            $this->log_file = $path;
        }
        // write message to the log file
        public function lwrite($message) {
            // if file pointer doesn't exist, then open log file
            if (!is_resource($this->fp)) {
                $this->lopen();
            }
            // define script name
            $script_name = pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME);
            // define current time and suppress E_WARNING if using the system TZ settings
            // (don't forget to set the INI setting date.timezone)
            $time = @date('[d/M/Y:H:i:s]');
            // write current time, script name and message to the log file
            if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
                fwrite($this->fp, "$time ($script_name) $message\r\n");
            }
            else {
                fwrite($this->fp, "$time ($script_name) $message\n");
            }
        }
        // close log file (it's always a good idea to close a file when you're done with it)
        public function lclose() {
            fclose($this->fp);
        }
        // open log file (private method)
        private function lopen() {
            // in case of Windows set default log file
            if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
                $log_file_default = 'c:/php/logfile.txt';
            }
            // set default log file for Linux and other systems
            else {
                $log_file_default = '/tmp/logfile.txt';
            }
            // define log file from lfile method or use previously set default
            $lfile = $this->log_file ? $this->log_file : $log_file_default;
            // open log file for writing only and place file pointer at the end of the file
            // (if the file does not exist, try to create it)
            if (!file_exists($lfile)) {
                $this->fp = fopen($lfile, "w") or die("Unable to open $lfile!");
                // fclose($myfile);
            }
            else {
                $this->fp = fopen($lfile, 'a') or exit("Can't open $lfile!");
                
            }
        }
    }
?>