<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("product");
$sql_where = "";

$option  = filter_input(INPUT_GET, 'option');
$status = filter_input(INPUT_GET, 'status');

if(!empty($option))
    $sql_where .= " and `option` = '".$option."'";
if(in_array($status, ["0","1"]))
    $sql_where .= " and status = '".checkinput_sql($status,3)."'";

//取出總筆數
$sql_cmd = "select count(*) from goods_additional where deleted_at is null ".$sql_where." ";
$rs = $db->query($sql_cmd);
$row = $rs->fetchRow();
$total = $row[0];

$mypage = new page;
$pages = $mypage->set_page($total, $PAGE_NUM);

//設定分頁的初始值
if (empty($_GET['num']))
{
    $num = 1;
}else{
    $num = intval($_GET['num']);
}

$sql_cmd = "select * from goods_additional where deleted_at is null ".$sql_where." order by create_datetime desc ".$pages[$num];
$rs_link  = $db->query($sql_cmd);
$tmp = $_GET;
unset($tmp['num']);
$val = http_build_query($tmp);
$page = $mypage->multi($total , $PAGE_NUM, $num, "./list.php", "", $val);
?><!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php"); ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                加購活動上稿管理
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box">
                <div class="box-header">
                    <form action="./list.php" method="GET">
                        <div style="float:left; display:inline-block; width:180px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">活動管別</button>
                                </div>
                                <select class="form-control" name="option">
                                    <option value="all">全部</option>
                                    <?php
                                        foreach($ARRall['goods_category'] as $key => $goods_category) {
                                            $selected = "";
                                            if($key == $option)
                                                $selected = "selected";
                                            echo "<option value='".$key."' ".$selected.">".$goods_category."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div style="float:left; display:inline-block; width:180px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">狀態</button>
                                </div>
                                <select class="form-control" name="status">
                                    <option value="all">全部</option>
                                    <option value="1" <?=($status == "1")?"selected":""?>>上架</option>
                                    <option value="0" <?=($status == "0")?"selected":""?>>下架</option>
                                </select>
                            </div>
                        </div>
                        <a class="btn btn-danger btn-sm" href="./list.php">清除</a>
                        <button type="submit" class="btn btn-success btn-sm">送出</button>
                        <a class="pull-right btn btn-success btn-sm" href="./detail.php?action=add">新增</a>
                    </form>
                </div>
                <div class="box-body no-padding">
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                <th>活動館別</th>
                                <th>參與商品</th>
                                <th>上架時間</th>
                                <th>下架時間</th>
                                <th>狀態</th>
                                <th>操作</th>
                            </tr>
                            <?php
                            if($rs_link->numRows() > 0) {
                            while($row = $rs_link->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                            ?>
                            <tr onClick="iCheckThisRow(this);">
                                <th><?=(empty($row['option']))?"全部":$ARRall['goods_category'][$row['option']]?></th>
                                <th><input type="button" value="參與商品" onclick="location.href='/admin/product_additional_list/list.php?additional_id=<?=$row['id']?>'"></th>
                                <th><?=$row['sdate']?></th>
                                <th><?=$row['edate']?></th>
                                <th><?=($row['status']==0)?"下架":"上架"?></th>
                                <th>
                                    <a class="btn btn-xs btn-primary" href="./detail.php?action=edit&id=<?=$row['id']?>">編輯</a>
                                    <a class="btn btn-xs btn-danger" href="javascript:void(0);" onclick="if(confirm('是否確認刪除？')){location.href='./delete.php?id=<?=$row["id"]?>';}">刪除</a>
                                </th>
                            </tr>
                            <?php }} ?>
                        </tbody>
                    </table>
                </div>
            </div>
		<?=$page?>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>