<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("category_edit");
$Category_Code = filter_input(INPUT_POST, 'Category_Code');

$Category_Name  = filter_input(INPUT_POST, 'Category_Name');
if(empty($Category_Code)) {
    js_go_back_global("NOT_POST");
    exit;
}
if(empty($Category_Name)) {
    js_go_back_global("DATA_EMPTY");
    exit;
}
$update_datetime = date("Y-m-d H:i:s");
$update_user     = $_SESSION[SESSION_VARIABLE."_user_id"];


$sql_array = array(
    "Category_Name"   => array("2", checkinput_sql($Category_Name, 45)),
    "update_datetime" => array("2", checkinput_sql($update_datetime, 50)),
    "update_user"     => array("2", checkinput_sql($update_user, 50)),
);
$sql_cmd = update("category",array("Category_Code", $Category_Code), $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
   js_go_back_global("DB_EDIT_ERROR");
   exit;
}else{
   $db->disconnect();
   js_repl_global( "./list.php", "EDIT_SUCCESS");
   exit;
}
?>
