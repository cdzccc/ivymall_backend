<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("category");
if(isset($_GET['action']))
    $action = $_GET['action'];
else
    $action = "add";

$row_delivery["delivery_id"] = "";
$row_delivery["name"] = "";
$row_delivery["price"] = "";

if($action == "edit") {
    $sql_cmd = "select * from category where Category_Code = '".checkinput_sql($_GET['Category_Code'],19)."'";
    $rs_category = $db->query($sql_cmd);
    $row_category = $rs_category->fetchRow(MDB2_FETCHMODE_ASSOC);
}

?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php") ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                分類管理
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" method="POST" action="./<?=$action?>.php">
                    <div class="box-header">
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">對應單元</label>
                            <div class="col-sm-10">
                                <select name="Category_Name">
                                    <?php
                                        foreach ($ARRall['goods_category'] as $key => $value) {
                                    ?>
                                    <option value="<?=$value?>" <?=($row_category["Category_Name"] == $value)?"checked":""?>><?=$value?></option>
                                    <?php
                                        }
                                    ?>
                                    
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-right">
                        <input type="hidden" name="Category_Code" value="<?=$row_category["Category_Code"]?>">
                        <a href="./list.php" class="btn btn-danger" style="margin-right: 10px;">取消，並返回上一層</a>
                        <button type="submit" class="btn btn-primary">儲存</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>