<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("category_add");
$Category_Code = get_id();

$Category_Name  = filter_input(INPUT_POST, 'Category_Name');

if(empty($Category_Name)) {
    js_go_back_global("DATA_EMPTY");
    exit;
}
$create_datetime = date("Y-m-d H:i:s");
$create_user     = $_SESSION[SESSION_VARIABLE."_user_id"];
$update_datetime = date("Y-m-d H:i:s");
$update_user     = $_SESSION[SESSION_VARIABLE."_user_id"];


$sql_array = array(
    "Category_Code"      => array("2", checkinput_sql($Category_Code, 19)),
    "Category_CodeGroup" => array("2", "Goods_Category"),
    "Category_Name"      => array("2", checkinput_sql($Category_Name, 45)),
    "Desc"               => array("2", checkinput_sql($Category_Name, 45)),
    "Status"             => array("2", 1),
    "create_datetime"    => array("2", checkinput_sql($create_datetime, 50)),
    "create_user"        => array("2", checkinput_sql($create_user, 50)),
    "update_datetime"    => array("2", checkinput_sql($update_datetime, 50)),
    "update_user"        => array("2", checkinput_sql($update_user, 50)),
);
$sql_cmd = insert("category", $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
   js_go_back_global("DB_EDIT_ERROR");
   exit;
}else{
   $db->disconnect();
   js_repl_global( "./list.php", "ADD_SUCCESS");
   exit;
}
?>
