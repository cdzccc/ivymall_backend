<?php
include("../config.php");
ClassJscript::islogin();
ClassJscript::isadmino("about_history_edit");
$id = filter_input(INPUT_POST, 'id');

$name      = filter_input(INPUT_POST, 'name');
$sub_name  = filter_input(INPUT_POST, 'sub_name');
$desc2     = filter_input(INPUT_POST, 'editor');
$desc      = filter_input(INPUT_POST, 'alt');
$url       = filter_input(INPUT_POST, 'url');
$post_time = filter_input(INPUT_POST, 'post_time');
$status    = filter_input(INPUT_POST, 'status', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 1, 'default' => 0)));
$sort        = filter_input(INPUT_POST, 'sort', FILTER_VALIDATE_INT, array('options' => array('min_range' => 0, 'max_range' => 99999, 'default' => 0)));
$old_pic       = filter_input(INPUT_POST, 'old_pic');

if(empty($id)) {
    js_go_back_global("NOT_POST");
    exit;
}
var_dump($_POST);
if(empty($name) || empty($desc2) || empty($post_time)) {
    js_go_back_global("DATA_EMPTY");
    exit;
}

if ($_FILES['pic']['name'] != "none" && is_uploaded_file($_FILES['pic']['tmp_name']))
{
    //重組檔名
    $type = explode(".", $_FILES['pic']['name']);
    $img = date("YmdHis")."_m.".$type[1];
    @copy($_FILES['pic']['tmp_name'], DOCUMENT_ROOT."/upload/".$img);
}
else {
    $img = $old_pic;
}

$update_datetime = date("Y-m-d H:i:s");
$update_user     = $_SESSION[SESSION_VARIABLE."_user_id"];

$sql_array = array(
    "name"            => array("2", checkinput_sql($name, 255)),
    "sub_name"        => array("2", checkinput_sql($sub_name, 255)),
    "Category"        => array("2", "History"),
    "desc"            => array("2", checkinput_sql($desc, 100)),
    "desc2"           => array("2", checkinput_sql($desc2, 2000)),
    "post_time"       => array("2", checkinput_sql($post_time, 30)),
    "status"          => array("2", checkinput_sql($status, 5)),
    "pic"             => array("2", checkinput_sql($img, 200)),
    "update_datetime" => array("2", checkinput_sql($update_datetime, 50)),
    "update_user"     => array("2", checkinput_sql($update_user, 50)),
    "sort"            => array("2", checkinput_sql($sort, 5)),
);
$sql_cmd = update("article", array("id", $id), $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
   js_go_back_global("DB_EDIT_ERROR");
   exit;
}else{
   $db->disconnect();
   js_repl_global( "./list.php", "EDIT_SUCCESS");
   exit;
}
?>
