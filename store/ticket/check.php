<?php
include("../config.php");
ClassJscript::islogin();

$sql_where = "";
$code = filter_input(INPUT_GET, 'code');

if(!empty($code)) {
    $sql_where .= " and CODE = '".checkinput_sql($code,8)."'";
}
//取出總筆數
$sql_cmd = "select count(*) from ticket_list as tl
join ticket as t on t.ticket_id = tl.Ticket_ID where t.Dealer_ID = '".$_SESSION[SESSION_VARIABLE."_user_id"]."'".$sql_where;
$rs = $db->query($sql_cmd);
$row = $rs->fetchRow();
$total = $row[0];

$mypage = new page;
$pages = $mypage->set_page($total, $PAGE_NUM);

//設定分頁的初始值
if (empty($_GET['num']))
{
    $num = 1;
}else{
    $num = intval($_GET['num']);
}


$sql_cmd = "select t.*,tl.CODE,tl.create_datetime as tl_time, tl.status as tl_status,tl.TicketList_ID from ticket_list as tl
join ticket as t on t.ticket_id = tl.Ticket_ID where t.Dealer_ID = '".$_SESSION[SESSION_VARIABLE."_user_id"]."'".$sql_where."  order by TicketList_id desc ".$pages[$num];
$rs_dealer  = $db->query($sql_cmd);
$tmp = $_GET;
unset($tmp['num']);
$val = http_build_query($tmp);
$page = $mypage->multi($total , $PAGE_NUM, $num, "./check.php", "", $val);
?>
<!DOCTYPE html>
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <?php include("../view/sidebar.php"); ?>
        <!-- /.sidebar -->
    </aside>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                優惠券核銷
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box">
                <div class="box-header">
                    <form action="./check.php" method="GET">
                        <div style="float:left; display:inline-block; width:320px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">序號</button>
                                </div>
                                <input type="text" name="code" class="form-control input-sm"
                                       placeholder="" value="">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-danger btn-sm">搜尋</button>
                    </form>
                </div>
                <div class="box-body no-padding text-center">
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                <th>名稱</th>
                                <th>使用期間</th>
                                <th>兌換點數</th>
                                <th>創建時間</th>
                                <th>狀態</th>
                                <th>操作</th>
                            </tr>
                            <?php
                            if($rs_dealer->numRows() > 0) {
                            while($row = $rs_dealer->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                            ?>
                            <tr onClick="iCheckThisRow(this);">
                                <th><?=$row['name']?></th>
                                <th><?=$row['date_start']." - ".$row['date_end']?></th>
                                <th><?=$row['point']?></th>
                                <th><?=$row['tl_time']?></th>
                                <th><?=($row['tl_status']==1)?"可核銷":"已核銷"?></th>
                                <th>
                                    <? if($row['tl_status'] == 1):?>
                                        <a class="btn btn-xs btn-primary fancybox_iframe" href="./input.php?id=<?=$row['TicketList_ID']?>">核銷</a>
                                    <? endif ?>
                                </th>
                            </tr>
                            <?php
                            } }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <?=$page?>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>
    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>