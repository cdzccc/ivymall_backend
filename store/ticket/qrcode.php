<?php
include("../config.php");
ClassJscript::islogin();

$sql_where = "";

$price = filter_input(INPUT_GET, 'price');
if(!empty($price)) {
    $sql_cmd = "select * from dealer where Dealer_ID = '".$_SESSION[SESSION_VARIABLE."_user_id"]."'";
    $rs = $db->query($sql_cmd);
    $row_setting = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
    $point = floor($price*$row_setting['BONUS']/100);
    $ticketList_ID = get_id();
    $CODE = get_rand();

    $sql_array = array(
        "TicketList_ID"   => array("2", checkinput_sql($ticketList_ID, 19)),
        "Ticket_ID"       => array("2", "qrcode"),
        "CODE"            => array("2", $CODE),
        "Customer_ID"     => array("2", $_SESSION[SESSION_VARIABLE."_user_id"]),
        "status"          => array("2", 1),
        "point"           => array("2", $point),
        "price"           => array("2", $price),
        "create_datetime" => array("2", date("Y-m-d H:i:s")),
    );
    $sql_cmd = insert("ticket_list", $sql_array);
    $rs = $db->query($sql_cmd);
    $qr_content = $CODE.",".$row_setting['u_number'].",".$price;
    $url = "https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=".$qr_content;
}
function get_rand() {
    $random=8;
    $randoma = '';
    for ($i=1;$i<=$random;$i=$i+1) {
        $a=rand(65,90);
        $b=chr($a);
        $randoma=$randoma.$b;
    }
    return $randoma;
}
?>
<!DOCTYPE html>
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <?php include("../view/sidebar.php") ?>
        <!-- /.sidebar -->
    </aside>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                優惠券管理
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box">
                <div class="box-header">
                    <form action="./qrcode.php" method="GET">
                        <div style="float:left; display:inline-block; width:320px; padding-right:15px;">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">請輸入金額</button>
                                </div>
                                <input type="text" name="price" class="form-control input-sm"
                                       placeholder="" value="">
                            </div>
                        </div>
                        <button class="btn btn-info btn-sm">產生qrcode</button>
                    </form>
                </div>
                <div class="box-body no-padding text-center">
                    <? if(!empty($price)): ?>
                    <img src="<?=$url?>">
                    <? endif ?>
                </div>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>
    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>