<!-- ./wrapper -->
<link href="/bower_components/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css"/>

<link href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" rel="stylesheet"
type="text/css"/>

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="/bower_components/jquery/dist/jquery.min.js"></script>
<script src="/bower_components/jquery-ui/jquery-ui.min.js"></script>
<script src="/bower_components/moment/moment.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/bower_components/ckeditor/ckeditor.js"></script>
<script src="/bower_components/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="/bower_components/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script src="/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0-beta.3/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script>

<!-- AdminLTE App -->
<script src="/admin/dist/js/adminlte.min.js"></script>
<script type="text/javascript" src="/admin/dist/js/jquery.twzipcode.js"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
<script>
    function select_all(class_name) {
        var checked = 1;
        $("."+class_name).each(function() {
            if($(this).prop("checked") == false) {
                checked = 0;
            }
        });
        if(checked == 1) {
            $("."+class_name).prop("checked", false);
        }
        else {
            $("."+class_name).prop("checked", true);

        }
    }
    function add_variant() {
        var add_variant = $(".add-variant .form-group").clone();
        $(".add-variant_btn").before(add_variant);
        $('.box-body .select2_category_item').select2({
            theme: "classic",
            tags: true,
            allowClear: true
        });
        change_variant();
        if($('.box-body .select2_category_item').length > 1) {
            $(".box-body .remove_variant").show();
        }
        else {
            $(".box-body .remove_variant").hide();
        }
    }
    function change_variant() {
        $(".variant").each(function() {
            $(this).closest( ".form-group" ).find("select.select2_category_item").attr("name","option["+$(this).val()+"][]");
        });
    }
    function remove_variant(e) {
        e.closest(".form-group").remove();
        setItem();
        if($('.box-body .select2_category_item').length > 1) {
            $(".box-body .remove_variant").show();
        }
        else {
            $(".box-body .remove_variant").hide();
        }
    }
    function setItem() {
        var item = [];
        $(".box-body .select2_category_item").each(function() {
           var array = $(this).val();
            if(item.length == 0) {
                item = array;
            }
            else {
                var tmp = [];
                var k = 0;
                for(var i = 0;i< item.length; i++) {
                    for(var j = 0;j < array.length; j++) {
                        tmp[k] = item[i] + " , " + array[j];
                        k++;
                    }
                }
                item = tmp;
            }
        });
        $(".category").each(function() {
            var check = false;
            for(var i = 0;i< item.length; i++) {
                if ($(this).val() == item[i]) {
                    check = true;
                }
            }
            if(!check) {
                $(this).parents( "tr" ).remove();
            }
        });

        if(item.length > 0) {
            $("table.product").hide();
            $("table.product_item").show();
        }
        else {
            $("table.product").show();
            $("table.product_item").hide();
        }
        var html;
        for(var i = 0;i< item.length; i++) {
            var check = false;
            $(".category").each(function() {
                if($(this).val() == item[i]) {
                    check = true;
                }
            });
            if(!check) {
                html += "<tr>" +
                    "<td>" + item[i] + "</td>" +
                    "<td><input type='text' name='price[]'><input class='category' name='item_category[]' type='hidden' value='"+ item[i] +"'></td>" +
                    "<td><input type='text' name='qty[]'></td>" +
                    "</tr>";
            }
        }
        $(".table tbody").append(html);
    }
    $(document).ready(function() {
        $('#twzipcode').twzipcode({
            'countyName': 'Dealer_Address_City',     // 預設為 county
            'districtName': 'Dealer_Address_Township', // 預設為 district
            'zipcodeName': 'zipcode',   // 預設為 zipcode
            'readonly': true,
        });
        $('.fancybox_iframe').fancybox({
            frameWidth: 400,
            type: 'iframe',
            padding: 0
        });

        $('.rangepicker').daterangepicker({
            timePicker: true,
            timePickerIncrement: 5,
            timePicker24Hour: true,
            locale: {
                format: 'YYYY-MM-DD HH:mm',
                applyLabel: "套用",
                cancelLabel: "取消",
                daysOfWeek: ["日", "一", "二", "三", "四", "五", "六"],
                monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
            }
        });
        $('.datetimepicker').daterangepicker({
            // format: 'YYYY-MM-DD HH:mm:ss'
            timePicker: true,
            timePickerIncrement: 10,
            singleDatePicker: true,
            autoUpdateInput: false,
            locale: {
                format: 'YYYY-MM-DD HH:mm',
                applyLabel: "套用",
                cancelLabel: "取消",
                daysOfWeek: ["日", "一", "二", "三", "四", "五", "六"],
                monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
            }
        });
        $('.datetimepicker').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('YYYY-MM-DD HH:mm'));
        });

        $('.datetimepicker').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
        $('.daterange').daterangepicker({
            autoUpdateInput: false,
            locale: {
                format: 'YYYY-MM-DD',
                applyLabel: "套用",
                cancelLabel: "取消",
                daysOfWeek: ["日", "一", "二", "三", "四", "五", "六"],
                monthNames: ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"],
            }
        });
        $('.daterange').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('YYYY-MM-DD') + ' - ' + picker.endDate.format('YYYY-MM-DD'));
        });
        $('.daterange').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });
        $('.rangepicker').on('cancel.daterangepicker', function(ev, picker) {
            //do something, like clearing an input
            $('.rangepicker').val('');
        });
        $('.rangepicker').val('');
        CKEDITOR.replace('desc');
        var $select2 = $(".select2_dealer").select2({
            theme: "classic",
            tags: false
        });

        $select2.val('<?=(!empty($select2_json))?$select2_json:""?>').trigger("change");
        $('.select2_ajax_category').select2({
            theme: "classic",
        });
        $('.box-body .select2_category_item').select2({
            theme: "classic",
            tags: true,
            allowClear: true
        });
    });
</script>
<style>
    #twzipcode div,#twzipcode input {
        display: inline-block;
    }
</style>
