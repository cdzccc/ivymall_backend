<section class="sidebar">
    <!-- Sidebar Menu -->
    <ul class="sidebar-menu" data-widget="tree">
        <li class="header">HEADER</li>
        <!-- Optionally, you can add icons to the links -->
        <li><a href="/store/dealer/detail.php"><i class="fa fa-user"></i> <span>商家資料</span></a></li>
        <li><a href="/store/product/list.php"><i class="fa fa-link"></i> <span>商品管理</span></a></li>
        <li><a href="/store/orders/list.php"><i class="fa fa-list-alt"></i> <span>訂單管理</span></a></li>
        <li><a href="/store/ticket/qrcode.php"><i class="fa fa-qrcode"></i> <span>qrcode</span></a></li>
        <li><a href="/store/ticket/check.php"><i class="fa fa-ticket"></i> <span>優惠券核銷</span></a></li>
        <li class="treeview menu-open">
            <a href="#">
                <i class="fa fa-list-alt"></i>
                <span>點數管理</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu" style="display: block;">
                <li><a href="/store/point/list.php?type=1"><i class="fa fa-plus"></i>得點資料</a></li>
                <li><a href="/store/point/list.php?type=2"><i class="fa fa-minus"></i>抵點資料</a></li>
            </ul>
        </li>
    </ul>
    <!-- /.sidebar-menu -->
</section>

