<?php
include("../config.php");
ClassJscript::islogin();

if(isset($_GET['action']))
    $action = $_GET['action'];
else
    $action = "add";

$row_dealer["Dealer_ID"] = "";
$row_dealer["dealer_name"] = "";
$row_dealer["Name"] = "";
$row_dealer["phone"] = "";
$row_dealer["email"] = "";
$row_dealer["userid"] = "";
$row_dealer["password"] = "";
$row_dealer["create_datetime"] = date("Y-m-d H:i:s");
$row_dealer["address"] = "";
$row_dealer["address_city"] = "";
$row_dealer["address_township"] = "";
$row_dealer["thumb"] = "";
$row_dealer["u_number"] = "";

$sql_cmd = "select * from dealer where Dealer_ID = ".$_SESSION[SESSION_VARIABLE."_user_id"];
$rs_dealer = $db->query($sql_cmd);
$row_dealer = $rs_dealer->fetchRow(MDB2_FETCHMODE_ASSOC);
$sql_cmd = "select * from delivery";
$rs_delivery = $db->query($sql_cmd);
$row_dealer["delivery_id"] = explode(",", $row_dealer["delivery_id"]);

?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php"); ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                廠商管理
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" method="POST" action="./edit.php" enctype="multipart/form-data">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">商家名稱</label>
                            <div class="col-sm-10">
                                <input id="dealer_name" name="dealer_name" type="text" class="form-control" placeholder=""
                                       value="<?=$row_dealer["dealer_name"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">帳號</label>
                            <div class="col-sm-10">
                                <input id="userid" name="userid" type="text" class="form-control" <?=($action=="edit")?"readonly":""?>
                                       value="<?=$row_dealer["userid"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">密碼</label>
                            <div class="col-sm-10">
                                <input id="pwd" name="pwd" type="password" class="form-control" placeholder="<?=(!empty($row_dealer['pwd']))?"已設定密碼，":""?>需修改再填寫">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">姓名</label>
                            <div class="col-sm-10">
                                <input id="name" name="name" type="text" class="form-control" placeholder="" value="<?=$row_dealer["Name"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">email</label>
                            <div class="col-sm-10">
                                <input id="email" name="email" type="text" class="form-control" placeholder=""
                                       value="<?=$row_dealer["email"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">電話</label>
                            <div class="col-sm-10">
                                <input id="phone" name="phone" type="text" class="form-control" placeholder=""
                                       value="<?=$row_dealer["phone"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">地址</label>
                            <div class="col-sm-10">
                                <div id="twzipcode">
                                    <div data-role="zipcode"
                                         data-name="zipcode"
                                         data-value=""
                                         data-style="zipcode-style form-control" class="col-sm-2 row">
                                    </div>
                                    <div data-role="county"
                                         data-name="Dealer_Address_City"
                                         data-value="<?=$row_dealer['address_city']?>"
                                         data-style="city-style form-control" class="col-sm-2 row">
                                    </div>
                                    <div data-role="district"
                                         data-name="Dealer_Address_Township"
                                         data-value="<?=$row_dealer['address_township']?>"
                                         data-style="district-style form-control" class="col-sm-2 row">
                                    </div>
                                    <div class="col-sm-8 row">
                                        <input name="Dealer_Address" type="text"  class="form-control" value="<?=$row_dealer['address']?>"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">封面圖片</label>
                            <div class="col-sm-10">
                                <?php if(!empty($row_dealer['thumb'])){?>
                                    <a href="<?=WEBSITE_URL."upload/".$row_dealer['thumb']?>" target="_blank">(圖片)</a>
                                <?php } ?>
                                <input id="thumb" name="thumb" type="file" class="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">商家簡介</label>
                            <div class="col-sm-10">
                                <textarea class="editor" name="desc"><?=$row_dealer["Dealer_Desc"]?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">出貨方式</label>
                            <div class="col-sm-10">
                                <?php while($row = $rs_delivery->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                                    $checked = "";
                                    if(in_array($row['delivery_id'], $row_dealer['delivery_id']))
                                        $checked = "checked";
                                    echo "<input name='delivery[]' type='checkbox' value='".$row['delivery_id']."' ".$checked.">".$row['name']."(NT ".$row['price'].")";
                                } ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="u_number" class="col-sm-2 control-label">統一編號</label>
                            <div class="col-sm-10">
                                <input id="u_number" name="u_number" type="text" class="form-control" placeholder=""
                                       value="<?=$row_dealer["u_number"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="UUID" class="col-sm-2 control-label">UUID</label>
                            <div class="col-sm-10">
                                <input id="UUID" name="UUID" type="text" class="form-control" placeholder=""
                                       value="<?=$row_dealer["UUID"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="Major" class="col-sm-2 control-label">Major</label>
                            <div class="col-sm-10">
                                <input id="Major" name="Major" type="text" class="form-control" placeholder=""
                                       value="<?=$row_dealer["Major"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="Minor" class="col-sm-2 control-label">Minor</label>
                            <div class="col-sm-10">
                                <input id="Minor" name="Minor" type="text" class="form-control" placeholder=""
                                       value="<?=$row_dealer["Minor"]?>">
                            </div>
                        </div>
                        <? if($action == "edit"): ?>
                        <div class="form-group">
                            <label for="f_path" class="col-sm-2 control-label">剩餘點數</label>
                            <div class="col-sm-10" style="line-height: 32px;">
                                <input type="text" class="form-control" placeholder=""
                                       value="<?=$row_dealer["point"]?>">
                            </div>
                        </div>
                        <? endif ?>
                        <div class="form-group">
                            <label for="f_path" class="col-sm-2 control-label">建立時間</label>
                            <div class="col-sm-10" style="line-height: 32px;">
                                <i class="fa fa-clock-o" aria-hidden="true"></i> <?=$row_dealer["create_datetime"]?>
                            </div>
                        </div>


                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-right">
                        <input type="hidden" name="id" value="<?=$row_dealer['Dealer_ID']?>">
                        <input name="old_pwd" type="hidden" value="<?=$row_dealer['pwd']?>">
                        <input name="old_img" type="hidden" value="<?=$row_dealer['thumb']?>">
                        <button type="button" class="btn btn-primary" onclick="check_file(data_form);">儲存</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>