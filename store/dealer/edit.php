<?php
include("../config.php");

ClassJscript::islogin();
if (!empty($_POST['pwd']))
{
	$pwd_out = hash('sha512',$_POST['pwd']);
}else{
    $pwd_out = $_POST['old_pwd'];
}
$Name                    = $_POST["name"];
$email                   = $_POST["email"];
$phone                   = $_POST["phone"];
$dealer_name             = $_POST["dealer_name"];
$address                 = $_POST["Dealer_Address"];
$Dealer_Address_City     = $_POST["Dealer_Address_City"];
$Dealer_Address_Township = $_POST["Dealer_Address_Township"];
$update_datetime         = date("Y-m-d H:i:s");
$update_user             = $_SESSION[SESSION_VARIABLE."_user_id"];
$email                   = $_POST["email"];
$bank_account_name       = "";
$bank_account            = "";
$bank_account_code       = "";
$pwd 					 = $pwd_out;
$delivery                = filter_input(INPUT_POST, 'delivery', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
$desc                    = filter_input(INPUT_POST, 'desc');
$UUID                    = filter_input(INPUT_POST, 'UUID');
$Major                   = filter_input(INPUT_POST, 'Major');
$Minor                   = filter_input(INPUT_POST, 'Minor');
$u_number                = filter_input(INPUT_POST, 'u_number');

$local_info = location($address);
$LATITUDE   = $local_info["Latitude"];
$LONGITUDE  = $local_info["Longitude"];

if ($_FILES['thumb']['name'] != "none" && is_uploaded_file($_FILES['thumb']['tmp_name']))
{
    //重組檔名
    $type = explode(".", $_FILES['thumb']['name']);
    $img = date("YmdHis").".".$type[1];
    @copy($_FILES['thumb']['tmp_name'], DOCUMENT_ROOT."/upload/".$img);
}
else {
    $img = $_POST['old_img'];
}


$sql_array = array(
    "Name"             => array("2", checkinput_sql($Name, 50)),
    "phone"            => array("2", checkinput_sql($phone, 10)),
    "dealer_name"      => array("2", checkinput_sql($dealer_name, 50)),
    "address"          => array("2", checkinput_sql($address, 100)),
    "address_city"     => array("2", checkinput_sql($Dealer_Address_City, 100)),
    "address_township" => array("2", checkinput_sql($Dealer_Address_Township, 100)),
    "LATITUDE"         => array("2", checkinput_sql($LATITUDE, 20)),
    "LONGITUDE"        => array("2", checkinput_sql($LONGITUDE, 20)),
    "update_datetime"  => array("2", checkinput_sql($update_datetime, 50)),
    "update_user"      => array("2", checkinput_sql($update_user, 50)),
    "email"            => array("2", checkinput_sql($email, 150)),
    "pwd"              => array("2", checkinput_sql($pwd, 255)),
    "thumb"            => array("2", checkinput_sql($img, 255)),
    "delivery_id"      => array("2", checkinput_sql(implode(",", $delivery), 255)),
    "Dealer_Desc"      => array("2", checkinput_sql($desc, 3000)),
    "UUID"             => array("2", checkinput_sql($UUID, 150)),
    "Major"            => array("2", checkinput_sql($Major, 255)),
    "Minor"            => array("2", checkinput_sql($Minor, 255)),
    "u_number"         => array("2", checkinput_sql($u_number, 10)),
);

$sql_cmd = update("dealer", array("Dealer_ID", $_POST['id']), $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
    js_go_back_global("DB_EDIT_ERROR");
    exit;
}else{
    $db->disconnect();
    js_repl_global( "./detail.php", "EDIT_SUCCESS");
    exit;
}
?>
