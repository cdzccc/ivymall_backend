<?php
include("../config.php");
ClassJscript::islogin();
$sql_where = "";
$order_by = " order by datetime desc ";
if(!empty($_GET['type'])) {
    $sql_where .= " and p.type = '".$_GET['type']."'";
    $typename = ($_GET['type'] == 1)?"得點":"抵點";
}
if(!empty($_GET['daterange'])) {
    $daterange = explode(" - ", $_GET['daterange']);
    $sql_where .= " and o.create_datetime between '".$daterange[0]." 00:00:00' and '".$daterange[1]." 23:59:59'";
}
if(!empty($_GET['Status'])) {
    $sql_where .= " and o.Status = '".$_GET["Status"]."'";
}
if(!empty($_GET['Order_ID'])) {
    $sql_where .= " and p.Order_ID = '".$_GET['Order_ID']."'";
}
if(!empty($_GET['type'])) {
    $sql_where .= " and p.type = '".$_GET['type']."'";
}
if(!empty($_GET['daterange'])) {
    $daterange = explode(" - ", $_GET['daterange']);
    $sql_where .= " and p.datetime between '".$daterange[0]." 00:00:00' and '".$daterange[1]." 23:59:59'";
}
if(!empty($_GET['mode'])) {
    $sql_where .= " and p.mode = '".$_GET["mode"]."'";
}
if(!empty($_GET['dealer'])) {
    $sql_where .= " and d.dealer_name = '".$_GET["dealer"]."'";
}


//取出總筆數
$sql_cmd = "select count(*) from point_list as p
left join `order` as o on o.Order_ID = p.Order_ID
left join `dealer` as d on d.Dealer_ID = o.Dealer_ID
left join `ticket` as t on t.ticket_id = p.Ticket_ID
left join `ticket_list` as tl on tl.create_datetime = p.datetime 
left join (select d.Dealer_ID,tl.CODE from ticket_list as tl join dealer as d on tl.Customer_ID = d.Dealer_ID) as tl_c on p.Ticket_ID = tl_c.CODE
left join (select d.Dealer_ID,i.invoice from invoice as i join dealer as d on i.u_number = d.u_number) as i on i.invoice = p.Ticket_ID
where (o.Dealer_ID = '".$_SESSION[SESSION_VARIABLE."_user_id"]."' 
    or t.Dealer_ID = '".$_SESSION[SESSION_VARIABLE."_user_id"]."'
    or tl_c.Dealer_ID = '".$_SESSION[SESSION_VARIABLE."_user_id"]."'
    or i.Dealer_ID = '".$_SESSION[SESSION_VARIABLE."_user_id"]."'
    or p.Ticket_ID = '".$_SESSION[SESSION_VARIABLE."_user_id"]."') 
    ".$sql_where." ";
$rs = $db->query($sql_cmd);
$row = $rs->fetchRow();
$total = $row[0];

$mypage = new page;
$pages = $mypage->set_page($total, $PAGE_NUM);

//設定分頁的初始值
if (empty($_GET['num']))
{
    $num = 1;
}else{
    $num = intval($_GET['num']);
}

$sql_cmd = "select p.*,o.Total_Price,tl.price as price,p.price as store_price,if(o.Dealer_ID is null,t.Dealer_ID,o.Dealer_ID) as Dealer_ID from point_list as p
left join `order` as o on o.Order_ID = p.Order_ID
left join `dealer` as d on d.Dealer_ID = o.Dealer_ID
left join `ticket` as t on t.ticket_id = p.Ticket_ID
left join `ticket_list` as tl on tl.create_datetime = p.datetime
left join (select d.Dealer_ID,tl.CODE from ticket_list as tl join dealer as d on tl.Customer_ID = d.Dealer_ID) as tl_c on p.Ticket_ID = tl_c.CODE
left join (select d.Dealer_ID,i.invoice from invoice as i join dealer as d on i.u_number = d.u_number) as i on i.invoice = p.Ticket_ID
 where (o.Dealer_ID = '".$_SESSION[SESSION_VARIABLE."_user_id"]."' 
    or t.Dealer_ID = '".$_SESSION[SESSION_VARIABLE."_user_id"]."'
    or tl_c.Dealer_ID = '".$_SESSION[SESSION_VARIABLE."_user_id"]."'
    or i.Dealer_ID = '".$_SESSION[SESSION_VARIABLE."_user_id"]."'
    or p.Ticket_ID = '".$_SESSION[SESSION_VARIABLE."_user_id"]."')
     ".$sql_where.$order_by;

if(!empty($_GET['Excel'])) {
    $Excel = true;
}else{
    $sql_cmd .= $pages[$num];
}



$rs = $db->query($sql_cmd);

while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
    if(!empty($row['Dealer_ID'])) {
        $sql_cmd = "select * from dealer where Dealer_ID = '".$row['Dealer_ID']."'";
        $rs2 = $db->query($sql_cmd);
        $row2 = $rs2->fetchRow(MDB2_FETCHMODE_ASSOC);
        $row['dealer_name'] = $row2['dealer_name'];
        if($row['mode'] == 'ticket') {
            $row['Total_Price'] = $row['price'];
        }
    }
    else if($row['mode'] == 'qrcode') {
        $sql_cmd = "select d.dealer_name,tl.price from ticket_list as tl join dealer as d on tl.Customer_ID = d.Dealer_ID where tl.CODE = '".$row['Ticket_ID']."'";
        $rs2 = $db->query($sql_cmd);
        $row2 = $rs2->fetchRow(MDB2_FETCHMODE_ASSOC);
        $row['dealer_name'] = $row2['dealer_name'];
        $row['Total_Price'] = $row2['price'];
    }
    else if($row['mode'] == 'invoice') {
        $sql_cmd = "select d.dealer_name,i.price from invoice as i join dealer as d on i.u_number = d.u_number where i.invoice = '".$row['Ticket_ID']."'";
        $rs2 = $db->query($sql_cmd);
        $row2 = $rs2->fetchRow(MDB2_FETCHMODE_ASSOC);
        $row['dealer_name'] = $row2['dealer_name'];
        $row['Total_Price'] = $row2['price'];
    }
    else if(in_array($row['mode'], ['store_bonus', 'store_discount'])) {
        $sql_cmd = "select * from dealer where Dealer_ID = '".$row['Ticket_ID']."'";
        $rs2 = $db->query($sql_cmd);
        $row2 = $rs2->fetchRow(MDB2_FETCHMODE_ASSOC);
        $row['dealer_name'] = $row2['dealer_name'];
        $row['Total_Price'] = $row['store_price'];
    }
    else {
        $row['dealer_name'] = "";
    }
    switch ($row['mode']) {
        case 'bonus':
            $row['mode'] = "網路消費";
            break;
        case 'discount':
            $row['mode'] = "訂單折抵";
            break;
        case 'ticket':
            $row['mode'] = "優惠券";
            // $
            break;
        case 'qrcode':
            $row['mode'] = "商家QR";
            break;
        case 'invoice':
            $row['mode'] = "發票";
            break;
        case 'store_bonus':
            $row['mode'] = "店面消費";
            break;
        case 'store_discount':
            $row['mode'] = "店面折抵";
            break;
        case 'cancel':
            $row['mode'] = "點數到期";
            break;
    }
    $row_point[] = $row;
    
}

if($Excel) {
    require_once '../../class/Classes/PHPExcel.php';
    $data =$row_point;
    //var_dump($row);exit;
    
    if($_GET['type']=="1"){
        
        $objPHPExcel=new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1','訂單編號')
        ->setCellValue('B1',$typename.'日期')
        ->setCellValue('C1','商家')
        ->setCellValue('D1','消費金額')
        ->setCellValue('E1','點數')
        ->setCellValue('F1','方式');
        
        $i=2;
        
        foreach($data as $k=>$v){
            
            $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$i,$v['Order_ID']." ")
            ->setCellValue('B'.$i,$v['datetime'])
            ->setCellValue('C'.$i,$v['dealer_name'])
            ->setCellValue('D'.$i,$v['Total_Price'])
            ->setCellValue('E'.$i,$v['point'])
            ->setCellValue('F'.$i,$v['mode']);
            $i++;
        }
        
    }else if($_GET['type']=="2"){
        
        $objPHPExcel=new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1','訂單編號')
        ->setCellValue('B1',$typename.'日期')
        ->setCellValue('C1','商家')
        ->setCellValue('D1','消費金額')
        ->setCellValue('E1','點數')
        ->setCellValue('F1','縣府點數')
        ->setCellValue('G1','方式');
        
        $i=2;
        
        foreach($data as $k=>$v){
            
            $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$i,$v['Order_ID']." ")
            ->setCellValue('B'.$i,$v['datetime'])
            ->setCellValue('C'.$i,$v['dealer_name'])
            ->setCellValue('D'.$i,$v['Total_Price'])
            ->setCellValue('E'.$i,$v['point'])
            ->setCellValue('F'.$i,$v['gov_point'])
            ->setCellValue('G'.$i,$v['mode']);
            $i++;
        }
    }
    
    $objPHPExcel->getActiveSheet()->setTitle($typename.'資料統計表');
    $objPHPExcel->setActiveSheetIndex(0);
    $objPHPExcel -> getActiveSheet() -> getColumnDimension(PHPExcel_Cell::stringFromColumnIndex(0)) -> setAutoSize(true);
    $objPHPExcel -> getActiveSheet() -> getColumnDimension(PHPExcel_Cell::stringFromColumnIndex(1)) -> setAutoSize(true);
    $filename=urlencode($typename.'資料統計表').'_'.date('Y-m-dHis');
    
    
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
    header('Cache-Control: max-age=0');
    header ('Pragma: public');
    $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
    $objWriter->save('php://output');
    exit;
}
// 商家list
$sql_cmd = "select * from dealer where delete_at is null";
$rs = $db->query($sql_cmd);
while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
    $row_dealer[] = $row;
}
//var_dump($row_dealer);exit;
$tmp = $_GET;
unset($tmp['num']);
$val = http_build_query($tmp);
$page = $mypage->multi($total , $PAGE_NUM, $num, "./list.php", "", "&".$val);
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php") ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                點數管理
                <small><?=($_GET['type'] == 1)?"得點資料":"抵點資料"?></small>
            </h1>
        </section>
        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box">
                <div class="box-header">
                    <form action="./list.php" method="GET" class="form-inline" id="form">
                    <input type="hidden" name="type" value="<?=(!empty($_GET['type']))?$_GET['type']:""?>">
                        <div class="form-group" style="margin-bottom: 5px">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">訂單編號</button>
                                </div>
                                <input type="text" name="Order_ID" class="form-control input-sm"
                                       placeholder="訂單編號" value="<?=(!empty($_GET['Order_ID']))?$_GET['Order_ID']:""?>">
                            </div>
                        </div>
                        <div  class="form-group" style="margin-bottom: 5px">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">商家</button>
                                </div>                                
                                <select name="dealer" id="mode" class="form-control input-sm">
                                    <option value="0">全部</option>
                                    <? foreach($row_dealer as $item): ?>
                                        <option value="<?=$item['dealer_name']?>" <?=(!empty($_GET['dealer']) && $_GET['dealer'] === $item['dealer_name'])?"selected":""?>><?=$item['dealer_name']?></option>
                                    <? endforeach ?>
                                </select>
                            </div>
                        </div>
                        <div  class="form-group" style="margin-bottom: 5px">
                            <div class="input-group">
                                <div class="input-group-btn">
                                	<button type="button" class="btn btn-primary btn-sm"><i class="fa fa-calendar"></i></button>                                    
                                </div>
                                <input type="text" class="daterange" name="daterange" class="form-control pull-right input-sm" style="height: 30px; padding: 5px 10px;" id="reservation"
                                       placeholder="創建時間" value="<?=(!empty($_GET['daterange']))?$_GET['daterange']:""?>">
                            </div>
                        </div>                        
                        
                        <div class="form-group" style="margin-bottom: 5px">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">方式</button>
                                </div>
                                <select name="mode" id="mode" class="form-control input-sm">
                                    <option value="0">全部</option>
                                    <option value="bonus" <?=(!empty($_GET['mode']) &&$_GET['mode'] == 'bonus')?"selected":""?>>網路消費</option>
                                    <option value="discount" <?=(!empty($_GET['mode']) &&$_GET['mode'] == 'discount')?"selected":""?>>訂單折抵</option>
                                    <option value="ticket" <?=(!empty($_GET['mode']) &&$_GET['mode'] == 'ticket')?"selected":""?>>優惠券</option>
                                    <option value="qrcode" <?=(!empty($_GET['mode']) &&$_GET['mode'] == 'qrcode')?"selected":""?>>商家QR</option>
                                    <option value="invoice" <?=(!empty($_GET['mode']) &&$_GET['mode'] == 'invoice')?"selected":""?>>發票</option>
                                </select>
                            </div>
                        </div>
                        <!--
                        <div  class="form-group" style="margin-bottom: 5px">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">下載Excel:</button>
                                </div>
                               <select name="Excel" id="Excel" class="form-control input-sm">
                                    <option value="0">否</option>
                                    <option value="1">是</option>
                                </select>
                            </div>
                        </div>
                          -->
                        
                         <div  class="form-group" style="margin-bottom: 5px">                                              
                          <a href="javascript:window.open('./list.php?Excel=1&'+$('#form').serialize());" class="btn btn-info btn-sm">導出excel</a>
                        </div>
                        <div  class="form-group" style="margin-bottom: 5px">
                        <a class="btn btn-danger btn-sm" href="./list.php">清除</a>
                        <button type="submit" class="btn btn-success btn-sm">送出</button>
                        </div>
                    </form>
                </div>
                <div class="box-body no-padding">
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                <th>訂單編號</th>
                                <th><?=($_GET['type'] == 1)?"得點日期":"抵點日期"?></th>
                                <th>商家</th>
                                <th>消費金額</th>
                                <th>點數</th>
                                <? if(!empty($_GET['type']) && $_GET['type'] == 2):?>
                                    <th>縣府點數</th>
                                <? endif ?>
                                <th>方式</th>
                            </tr>
                            <?php  if($row_point) {   ?>
                            <?php foreach($row_point as $key=>$item) :?>

                            <tr onClick="iCheckThisRow(this);">
                                <th><?=$item['Order_ID']?></th>
                                <th><?=$item['datetime']?></th>
                                <th><?=$item['dealer_name']?></th>
                                <th><?=$item['Total_Price']?></th>
                                <th><?=$item['point']?></th>
                                <? if(!empty($_GET['type']) && $_GET['type'] == 2): ?>
                                    <th><?=$item['gov_point']?></th>
                                <? endif ?>
                                <th><?=$item['mode']?></th>
                            </tr>
                            <? endforeach ?>
                            <?php }  ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <?=$page?>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>