<?php
include "../config.php";
//檢查來源
//$myurl = WEBSITE_URL."admin/login.php";
//if(strncmp( @$_SERVER['HTTP_REFERER'], $myurl, strlen($myurl))){
//    unset($_POST);
//    js_go_back_global("POST_URL_ERROR");
//    exit;
//}
//檢查是否用POST
if ($_SERVER['REQUEST_METHOD']!="POST"){
    js_go_back_global("NOT_POST");
    exit;
}
//檢查參數有無存在
if (empty($_POST['username'])){
    js_go_back_global("USER_BLANK");
    exit;
}

if (empty($_POST['pwd'])){
    js_go_back_global("PASSWORD_BLANK");
    exit;
}
//先查詢有無此帳號
$sql_cmd = "select * from `dealer` where delete_at is null and userid='".checkinput_sql($_POST['username'], 255)."'";
$rs = $db->query($sql_cmd);
$row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
$log = new Logging();

// $log->lfile('D:/LoLoPoint/logs/CIF_log.log');
$log->lcfile('WIUP','admin_login');

// write message to the log file
$log->lwrite('login ID   : '.$_POST['username']);
$log->lwrite('REMOTE_ADDR: '.$_SERVER['REMOTE_ADDR']);
if(!$row['Dealer_ID']){
    $log->lwrite('result     : '."ID_ERROR");
    js_go_back_global("ID_ERROR");
    exit;
}else{
    if($row['pwd'] != hash('sha512',$_POST['pwd'])){
        //登入錯誤session 設定
        $log->lwrite('result     : '."PWD_ERROR");
        js_go_back_global("PWD_ERROR");
        exit;
    }else{
        $log->lwrite('result     : '."LOGIN_YES");
        //登入完成把變數存入session中
        unset($_SESSION['loginlock_s']);
        $_SESSION[SESSION_VARIABLE."_user_s"] = checkoutput($row['Name']);
        $_SESSION[SESSION_VARIABLE."_user_id"] = $row['Dealer_ID'];
        $rs->free();
        $db->disconnect();
        js_repl_global(WEBSITE_URL.'store/', "LOGIN_YES");
    }
    $log->lclose();
}
?>
