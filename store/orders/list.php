<?php
include("../config.php");
ClassJscript::islogin();
$sql_where = "";
$order_by = " order by create_datetime desc ";

$Excel = false;
if(!empty($_GET['title'])) {
    $sql_where .= " and concat(o.Delivery_Name,m.Customer_Mail,o.Delivery_Phone) like '%".$_GET["title"]."%'";
}
if(!empty($_GET['daterange'])) {
    $daterange = explode(" - ", $_GET['daterange']);
    $sql_where .= " and o.create_datetime between '".$daterange[0]." 00:00:00' and '".$daterange[1]." 23:59:59'";
}
if(!empty($_GET['Status'])) {
    $sql_where .= " and o.Status = '".$_GET["Status"]."'";
}
if(!empty($_GET['Excel'])) {
    $Excel = true;
}

//取出總筆數
$sql_cmd = "select count(*) from `order` as o join member as m on m.Customer_ID = o.Customer_ID where delete_at is null and o.Dealer_ID = '".$_SESSION[SESSION_VARIABLE."_user_id"]."' ".$sql_where." ";
$rs = $db->query($sql_cmd);
$row = $rs->fetchRow();
$total = $row[0];

$mypage = new page;
if($Excel) {
    $pages = "";
}else {
    $pages = $mypage->set_page($total, $PAGE_NUM);
}

//設定分頁的初始值
if (empty($_GET['num']))
{
    $num = 1;
}else{
    $num = intval($_GET['num']);
}


$sql_cmd = "select o.* from `order` as o join member as m on m.Customer_ID = o.Customer_ID where delete_at is null and o.Dealer_ID = '".$_SESSION[SESSION_VARIABLE."_user_id"]."' ".$sql_where.$order_by.$pages[$num];
$rs_order = $db->query($sql_cmd);
if($Excel) {
    require_once '../../class/Classes/PHPExcel.php';
    while ($row = $rs_order->fetchRow(MDB2_FETCHMODE_ASSOC)) {
        $commoditylist = "";
        if(!empty($row['Order_ID'])) {
            
            $sql_cmd = "SELECT OI.*,GI.Name as Goods_Item_Name,G.Goods_Name from order_item as OI
            join goods_item as GI
            on OI.Goods_Item_ID = GI.Goods_Item_ID
            join goods as G
            on G.Goods_ID = GI.Goods_ID
             where OI.Order_ID = '".$row['Order_ID']."'";
            //$rs_order_item = $db->query($sql_cmd);
            $rs2 = $db->query($sql_cmd);
            
            while($row2 = $rs2->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                
                $commoditylist=$commoditylist." 名稱：".$row2['Goods_Name']."      規格：".$row2['Goods_Item_Name']."      價格：".$row2['Goods_Price']."      數量：".$row2['Goods_Qty']."\r\n";
                
            }
            $row['commoditylist'] = $commoditylist;
            
        }
        $data[] = $row;
    }
    
    //print_r($data);
    // echo "<br>";
    //echo $row[Order_ID];
    //EXIT;
    //$data =$rs_order;
    $objPHPExcel=new PHPExcel();
    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1','訂單編號')
    ->setCellValue('B1','郵箱')
    ->setCellValue('C1','姓名')
    ->setCellValue('D1','行動電話')
    ->setCellValue('E1','配送地址')
    ->setCellValue('F1','留言')
    ->setCellValue('G1','購買商品')
    ->setCellValue('H1','折扣')
    ->setCellValue('I1','運費')
    ->setCellValue('J1','總金額')
    ->setCellValue('K1','訂單狀態')
    ->setCellValue('L1','宅配單號')
    ->setCellValue('M1','綠界付款單號')
    ->setCellValue('N1','付款狀態')
    ->setCellValue('O1','發票資訊')
    ->setCellValue('P1','備註')
    ->setCellValue('Q1','創建時間');
    
    $i=2;
    
    foreach($data as $k=>$v){
        $objPHPExcel->getActiveSheet()->getStyle('G'.$i)->getAlignment()->setWrapText(true);
        //$objPHPExcel->getActiveSheet()->getStyle('H'.$i)->getAlignment()->setWrapText(true)
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A'.$i,$v['Order_ID']." ")
        ->setCellValue('B'.$i,$v['Customer_Mail']." ")
        ->setCellValue('C'.$i,$v['Delivery_Name']." ")
        ->setCellValue('D'.$i,$v['Delivery_Phone']." ")
        ->setCellValue('E'.$i,$v['Delivery_City']."-".$v['Delivery_Area']."-".$v['Delivery_Addr'])
        ->setCellValue('F'.$i,$v['Note']." ")
        ->setCellValue('G'.$i,$v['commoditylist'])
        ->setCellValue('H'.$i,$v['Total_PM']." ")
        ->setCellValue('I'.$i,$v['Delivery_Price']." ")
        ->setCellValue('J'.$i,$v['Total_Price']-$v['Total_PM']+$v['Delivery_Price']." ")
        ->setCellValue('K'.$i,($v['Status']==1)?"下單成功":(($v['Status']==2)?"取消":(($v['Status']==3)?"出貨中":(($v['Status']==4)?"已到貨":""))))
        ->setCellValue('L'.$i,$v['Delivery_No']." ")
        ->setCellValue('M'.$i,$v['trading_id']." ")
        ->setCellValue('N'.$i,($v['Pay_Status'] == 'y')?"已付款":"未付款")
        ->setCellValue('O'.$i,"抬頭：".$v['i_title']."      統編：".$v['i_number'])
        ->setCellValue('P'.$i,$v['Remark'])
        ->setCellValue('Q'.$i,$v['create_datetime']);
        $i++;
    }
    
    $objPHPExcel->getActiveSheet()->setTitle('訂單信息統計表');
    $objPHPExcel->setActiveSheetIndex(0);
    $objPHPExcel -> getActiveSheet() -> getColumnDimension(PHPExcel_Cell::stringFromColumnIndex(0)) -> setAutoSize(true);
    $objPHPExcel -> getActiveSheet() -> getColumnDimension(PHPExcel_Cell::stringFromColumnIndex(1)) -> setAutoSize(true);
    $objPHPExcel -> getActiveSheet() -> getColumnDimension(PHPExcel_Cell::stringFromColumnIndex(2)) -> setAutoSize(true);
    $objPHPExcel -> getActiveSheet() -> getColumnDimension(PHPExcel_Cell::stringFromColumnIndex(3)) -> setAutoSize(true);
    $objPHPExcel -> getActiveSheet() -> getColumnDimension(PHPExcel_Cell::stringFromColumnIndex(4)) -> setAutoSize(true);
    $objPHPExcel -> getActiveSheet() -> getColumnDimension(PHPExcel_Cell::stringFromColumnIndex(6)) -> setAutoSize(true);
    $filename=urlencode('訂單信息統計表').'_'.date('Y-m-dHis');
    
    
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
    header('Cache-Control: max-age=0');
    header ('Pragma: public');
    $objWriter=PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
    $objWriter->save('php://output');
    exit;  
}

$tmp = $_GET;
unset($tmp['num']);
$val = http_build_query($tmp);
$page = $mypage->multi($total , $PAGE_NUM, $num, "./list.php", "", $val);
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php") ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                訂單管理
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box">
                <div class="box-header">
                    <form action="./list.php" method="GET" class="form-inline" id="form">
                       <div  class="form-group" style="margin-bottom: 5px">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">搜尋</button>
                                </div>
                                <input type="text" name="title" class="form-control input-sm"
                                       placeholder="姓名/電話/email" value="<?=(!empty($_GET['title']))?$_GET['title']:""?>">
                            </div>
                        </div>
                        <div  class="form-group" style="margin-bottom: 5px">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-calendar"></i></button>
                                </div>
                                <input type="text" class="daterange" name="daterange" class="form-control pull-right input-sm"  style="height: 30px; padding: 5px 10px;"  id="reservation"
                                       placeholder="創建時間" value="<?=(!empty($_GET['daterange']))?$_GET['daterange']:""?>">
                            </div>
                        </div>
                        <div  class="form-group" style="margin-bottom: 5px">                        
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">訂單狀態</button>
                                </div>
                                <select name="Status" id="Status" class="form-control input-sm">
                                    <option value="0">全部</option>
                                    <option value="1" <?=(!empty($_GET['Status']) &&$_GET['Status'] == 1)?"selected":""?>>下單成功</option>
                                    <option value="2" <?=(!empty($_GET['Status']) &&$_GET['Status'] == 2)?"selected":""?>>取消</option>
                                    <option value="3" <?=(!empty($_GET['Status']) &&$_GET['Status'] == 3)?"selected":""?>>出貨中</option>
                                    <option value="4" <?=(!empty($_GET['Status']) &&$_GET['Status'] == 4)?"selected":""?>>已到貨</option>
                                </select>
                            </div>
                        </div>
                        <!--
                        <div  class="form-group" style="margin-bottom: 5px">                                              
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-sm">下載Excel：</button>
                                </div>
                                <select name="Excel" id="Excel" class="form-control input-sm">
                                    <option value="0">否</option>
                                    <option value="1">是</option>
                                </select>
                            </div>
                        </div>
                          -->
                         <div  class="form-group" style="margin-bottom: 5px">                                              
                          <a href="javascript:window.open('./list.php?Excel=1&'+$('#form').serialize());" class="btn btn-info btn-sm">導出excel</a>
                        </div>
                        <div  class="form-group" style="margin-bottom: 5px">
                            <a class="btn btn-danger btn-sm" href="./list.php">清除</a>
                            <button type="submit" class="btn btn-success btn-sm">送出</button>
                        </div>
                    </form>
                </div>
                <div class="box-body no-padding" style="overflow: scroll;">
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                <th>訂單編號</th>
                                <th>姓名</th>
                                <th>金額</th>
                                <th>狀態</th>
                                <th>支付狀態</th>
                                <th>創建時間</th>
                                <th>操作</th>
                            </tr>
                            <?php while($row = $rs_order->fetchRow(MDB2_FETCHMODE_ASSOC)) {?>

                            <tr onClick="iCheckThisRow(this);">
                                <th><?=$row['Order_ID']?></th>
                                <th><?=$row['Delivery_Name']?></th>
                                <th><?=number_format($row["Total_Price"]-$row['Total_PM'])?></th>
                                <th><?=order_status($row['Status'])?></th>
                                <th><?=($row['Pay_Status'] == 'y')?"已付款":"未付款"?></th>
                                <th><?=$row['create_datetime']?></th>
                                <th>
                                    <a class="btn btn-xs btn-primary" href="./detail.php?Order_ID=<?=$row['Order_ID']?>">編輯</a>
                                    <a class="btn btn-xs btn-danger" href="javascript:void(0);" onclick="if(confirm('是否確認刪除？')){location.href='./delete.php?Order_ID=<?=$row['Order_ID']?>';}">刪除</a>
                                </th>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
 	<?=$page?>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>