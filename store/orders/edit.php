<?php
include("../config.php");
ClassJscript::islogin();

$Order_ID       = filter_input(INPUT_POST, 'Order_ID');
$Delivery_Name  = filter_input(INPUT_POST, 'Delivery_Name');
$Delivery_Phone = filter_input(INPUT_POST, 'Delivery_Phone');
$Delivery_City  = filter_input(INPUT_POST, 'Delivery_City');
$Delivery_Area  = filter_input(INPUT_POST, 'Delivery_Area');
$Delivery_Addr  = filter_input(INPUT_POST, 'Delivery_Addr');
$Total_PM       = filter_input(INPUT_POST, 'Total_PM');
$Status         = filter_input(INPUT_POST, 'Status');
$Delivery_No    = filter_input(INPUT_POST, 'Delivery_No');
$i_title        = filter_input(INPUT_POST, 'i_title');
$i_number       = filter_input(INPUT_POST, 'i_number');
$Remark         = filter_input(INPUT_POST, 'Remark');

if(empty($Order_ID)) {
    js_go_back_global("NOT_POST");
    exit;
}

$sql_cmd = "SELECT * from `order` where Order_ID = '".$Order_ID."'";
$rs = $db->query($sql_cmd);
$row_order = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);
$sql_cmd = "SELECT * from `order_item` where Order_ID = '".$Order_ID."'";
$rs = $db->query($sql_cmd);
while ($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
    $row_order_item[] = $row;
}
$Total_Price = 0;
foreach ($row_order_item as $key => $value) {
    $Total_Price += $value['Goods_Qty']*$value['Goods_Price'];
}

$sql_array = array(
    "Delivery_Name"  => array("2", checkinput_sql($Delivery_Name,50)),
    "Delivery_Phone" => array("2", checkinput_sql($Delivery_Phone,10)),
    "Delivery_City"  => array("2", checkinput_sql($Delivery_City,9)),
    "Delivery_Area"  => array("2", checkinput_sql($Delivery_Area,9)),
    "Delivery_Addr"  => array("2", checkinput_sql($Delivery_Addr,50)),
    "Total_PM"       => array("2", checkinput_sql($Total_PM, 5)),
    "Status"         => array("2", checkinput_sql($Status,2)),
    "Delivery_No"    => array("2", checkinput_sql($Delivery_No,20)),
    "i_title"        => array("2", checkinput_sql($i_title,50)),
    "i_number"       => array("2", checkinput_sql($i_number,10)),
    "Remark"         => array("2", checkinput_sql($Remark,255)),
    "Total_Price"    => array("2", checkinput_sql($Total_Price,5)),
);
$sql_cmd = update("order", array("Order_ID", $Order_ID), $sql_array);
$rs = $db->query($sql_cmd);
$pear = new PEAR();
if ($pear->isError($rs))
{
    js_go_back_global("DB_EDIT_ERROR");
    exit;
}else{
    // 訂單轉付款成功得點
    if($row_order['Pay_Status'] == 'n' && $Pay_Status == 'y') {
        // 紅利得點
        $sql_cmd = "select * from dealer where Dealer_ID = '".$row_order['Dealer_ID']."'";
        $rs2 = $db->query($sql_cmd);
        $row_setting = $rs2->fetchRow(MDB2_FETCHMODE_ASSOC);
        if($row_setting['BONUS'] != 0) {
            $get_point = floor(($row_order['Total_Price'] - $row_order['total_pm'])/100*$row_setting['BONUS']);
            $sql_cmd = "select * from point_summary where Customer_ID = '".$row_order['Customer_ID']."'";
            $rs2 = $db->query($sql_cmd);
            if($rs2->numRows() > 0) {
                $row_point = $rs2->fetchRow(MDB2_FETCHMODE_ASSOC);
                $point_total = $row_point['total'] + $get_point;
                $point_in = $row_point['in'] + $get_point;
                $sql_cmd = "update point_summary set `in` = ".$point_in.",total = ".$point_total." where Customer_ID = '".$row_order['Customer_ID']."'";
                $rs2 = $db->query($sql_cmd);
            }
            else {
                $point_summary_id = get_id();
                $sql_array = array(
                    "id"          => array("2", $point_summary_id),
                    "Customer_ID" => array("2", $row_order['Customer_ID']),
                    "in"          => array("2", $get_point),
                    "out"         => array("2", 0),
                    "total"       => array("2", $get_point),
                );
                $sql_cmd = insert("point_summary", $sql_array);
                $rs2 = $db->query($sql_cmd);
                $row_point['total'] = 0;
            }
            $point_list_id = get_id();
            $sql_array = array(
                "id"          => array("2", $point_list_id),
                "Customer_ID" => array("2", $row_order['Customer_ID']),
                "point"       => array("2", $get_point),
                "type"        => array("2", 1),
                "datetime"    => array("2", date("Y-m-d H:i:s")),
                "Order_ID"    => array("2", $row_order['Order_ID']),
                "left"        => array("2", $row_point['total']+$get_point),
                "mode"        => array("2", "bonus"),
            );
            $sql_cmd = insert("point_list", $sql_array);
            $rs2 = $db->query($sql_cmd);
        }
    }

    if($row_order["Status"] != $Status && in_array($Status, [2,6])) {
        //加回庫存
        $sql_cmd = "select * from order_item where Order_ID = '".checkinput_sql($Order_ID, 19)."'";
        $rs = $db->query($sql_cmd);
        while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            $sql_cmd = "update goods_item set Qty = (Qty+".$row['Goods_Qty'].") where Goods_Item_ID = '".$row['Goods_Item_ID']."'";
            $db->query($sql_cmd);
        }
        if($row_order['Pay_Status'] == 'y') {
            //歸還點數
            $sql_cmd = "select * from point_list where Order_ID = '".checkinput_sql($Order_ID, 19)."' and Customer_ID = '".$row_order['Customer_ID']."'";
            $rs = $db->query($sql_cmd);
            while($row = $rs->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                $sql_cmd = "select * from point_summary where Customer_ID = '".$row_order['Customer_ID']."'";
                $rs_summary = $db->query($sql_cmd);
                $row_summary = $rs_summary->fetchRow(MDB2_FETCHMODE_ASSOC);
                if($row['type'] == 1) {
                    $type = 2;
                    $row_summary['out'] += $row['point'];
                    $row_summary['total'] -= $row['point'];
                }
                else {
                    $type = 1;
                    $row_summary['in'] += $row['point'];
                    $row_summary['total'] += $row['point'];
                }
                $sql_cmd = "update point_summary set `out` = ".$row_summary['out'].", `in` = ".$row_summary['in'].", total = ".$row_summary['total']." where Customer_ID = '".$row_order['Customer_ID']."'";
                $db->query($sql_cmd);
                $point_list_id = get_id();
                $left = $row['left'];
                $sql_array = array(
                    "id"          => array("2", $point_list_id),
                    "Customer_ID" => array("2", $row_order['Customer_ID']),
                    "point"       => array("2", $row['point']),
                    "type"        => array("2", $type),
                    "datetime"    => array("2", date("Y-m-d H:i:s")),
                    "Order_ID"    => array("2", checkinput_sql($Order_ID, 19)),
                    "left"        => array("2", $row_summary['total']),
                );
                $sql_cmd = insert("point_list", $sql_array);
                $db->query($sql_cmd);

            }
        }
        // 銷售量紀錄
        foreach ($row_order_item as $key => $value) {
            $sql_cmd = "update goods_item set Goods_Sell_Count = (Goods_Sell_Count+".$value['Goods_Qty'].") where GoodsItem_ID = '".$value['Goods_Item_ID']."'";
            $rs = $db->query($sql_cmd);
            $sql_cmd = "update goods set sell_count = (sell_count + ".$value['Goods_Qty'].") where Goods_ID = '".$value['Goods_ID']."'";
            $rs = $db->query($sql_cmd);
        }
    }
    $Log_Action = array();
    if($row_order["Deliver_Name"] != $Delivery_Name || $row_order["Delivery_Phone"] != $Delivery_Phone || $row_order["Delivery_City"] != $Delivery_City || $row_order["Delivery_Area"] != $Delivery_Area || $row_order["Delivery_Addr"] != $Delivery_Addr) {
        $Log_Action[] = "送貨資訊修改";
    }
    if($row_order["Total_PM"] != $Total_PM) {
        $Log_Action[] = "折扣修改";
    }
    if($row_order["Status"] != $Status) {
        $Log_Action[] = "訂單狀態修改：".order_status($row_order["Status"])."=>".order_status($Status);
    }
    if($row_order["i_title"] != $i_title || $row_order["i_number"] != $i_number) {
        $Log_Action[] = "發票資訊修改";
    }
    if($row_order["Remark"] != $Remark) {
        $Log_Action[] = "備註修改";
    }

    $Order_Log_ID = get_id();
    $sql_array = array(
        "id"       => array("2", $Order_Log_ID),
        "Order_ID" => array("2", checkinput_sql($Order_ID,19)),
        "Action"   => array("2", implode(",", $Log_Action)),
        "datetime" => array("2", date("Y-m-d H:i:s")),
        "user"     => array("2", $_SESSION[SESSION_VARIABLE."_user_id"]),
    );
    $sql_cmd = insert("order_log", $sql_array);
    $rs = $db->query($sql_cmd);

    $db->disconnect();
    js_repl_global( "./detail.php?Order_ID=".$Order_ID, "EDIT_SUCCESS");
    exit;
}

