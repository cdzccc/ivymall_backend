<?php
include("../config.php");
ClassJscript::islogin();
$select2_json = "";
$action = "edit";
$Order_ID = filter_input(INPUT_GET, 'Order_ID');
if(empty($Order_ID)) {
    js_go_back_global("NOT_POST");
    exit;
}
$sql_cmd = "select o.*,m.Customer_Mail from `order` as o join member as m on o.Customer_ID = m.Customer_ID
         where o.delete_at is null and o.Order_ID = '".checkinput_sql($Order_ID,19)."'";
$rs = $db->query($sql_cmd);
$row_order = $rs->fetchRow(MDB2_FETCHMODE_ASSOC);

$sql_cmd = "SELECT OI.*,GI.Name as Goods_Item_Name,G.Goods_Name from order_item as OI
            join goods_item as GI
            on OI.Goods_Item_ID = GI.Goods_Item_ID
            join goods as G
            on G.Goods_ID = GI.Goods_ID
             where OI.Order_ID = '".$Order_ID."'";
$rs_order_item = $db->query($sql_cmd);

?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<?php include('../view/metalink.php'); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <!-- Main Header -->
    <?php include('../view/header.php'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->

        <?php include("../view/sidebar.php") ?>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                訂單管理
                
            </h1>
        </section>

        <!-- Main content -->
        <section class="content container-fluid">
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal" method="POST" action="./<?=$action?>.php">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">訂單編號</label>
                            <div class="col-sm-10">
                                <?=$row_order['Order_ID']?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">email</label>
                            <div class="col-sm-10">
                                <?=$row_order['Customer_Mail']?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">收件人姓名</label>
                            <div class="col-sm-10">
                                <input id="Delivery_Name" name="Delivery_Name" type="text" class="form-control" placeholder=""
                                       value="<?=$row_order['Delivery_Name']?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">行動電話</label>
                            <div class="col-sm-10">
                                <input id="Delivery_Phone" name="Delivery_Phone" type="text" class="form-control" placeholder=""
                                       value="<?=$row_order['Delivery_Phone']?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">配送地址</label>
                            <div class="col-sm-10">
                                <div id="twzipcode">
                                    <div data-role="zipcode"
                                         data-name="zipcode"
                                         data-value=""
                                         data-style="zipcode-style form-control" class="col-sm-2 row">
                                    </div>
                                    <div data-role="county"
                                         data-name="Delivery_City"
                                         data-value="<?=$row_order['Delivery_City']?>"
                                         data-style="city-style form-control" class="col-sm-2 row">
                                    </div>
                                    <div data-role="district"
                                         data-name="Delivery_Area"
                                         data-value="<?=$row_order['Delivery_Area']?>"
                                         data-style="district-style form-control" class="col-sm-2 row">
                                    </div>
                                    <div class="col-sm-8 row">
                                        <input name="Delivery_Addr" type="text" class="form-control"  value="<?=$row_order['Delivery_Addr']?>"/>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">留言</label>
                            <div class="col-sm-10">
                                <?=$row_order['Note']?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">購買商品</label>
                            <div class="col-sm-10">
                                <table class="table table-bordered">
                                    <tr>
                                        <td>名稱</td>
                                        <td>規格</td>
                                        <td>價格</td>
                                        <td>數量</td>
                                        <td>小計</td>
                                    </tr>
                                    <?php 
                                        $sum = "";
                                        while($row_order_item = $rs_order_item->fetchRow(MDB2_FETCHMODE_ASSOC)) {
                                            $sum += $row_order_item["Goods_Price"]*$row_order_item["Goods_Qty"];
                                    ?>
                                    <tr>
                                        <td><?=$row_order_item["Goods_Name"]?></td>
                                        <td><?=$row_order_item["Goods_Item_Name"]?></td>
                                        <td><?=number_format($row_order_item["Goods_Price"])?></td>
                                        <td><?=$row_order_item["Goods_Qty"]?></td>
                                        <td><?=number_format($row_order_item["Goods_Price"]*$row_order_item["Goods_Qty"])?></td>
                                    </tr>
                                    <?php } ?>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>總計</td>
                                        <td><?=$sum?></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="f_path" class="col-sm-2 control-label">折扣</label>
                            <div class="col-sm-10" style="line-height: 32px;">
                                <input id="Total_PM" name="Total_PM" type="text" class="form-control" placeholder=""
                                       value="<?=$row_order["Total_PM"]?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="f_path" class="col-sm-2 control-label">運費</label>
                            <div class="col-sm-10" style="line-height: 32px;">
                                <?=number_format($row_order['Delivery_Price']);?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="f_path" class="col-sm-2 control-label">總金額</label>
                            <div class="col-sm-10" style="line-height: 32px;">
                                <?=number_format($row_order["Total_Price"]-$row_order["Total_PM"]+$row_order['Delivery_Price']);?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="f_path" class="col-sm-2 control-label">訂單狀態</label>
                            <div class="col-sm-10" style="line-height: 32px;">
                                <label><input type="radio" name="Status" value="1" <?=($row_order["Status"]=="1")?"checked":"";?>>下單成功</label>
                                <label><input type="radio" name="Status" value="2" <?=($row_order["Status"]=="2")?"checked":"";?>>取消</label>
                                <label><input type="radio" name="Status" value="3" <?=($row_order["Status"]=="3")?"checked":"";?>>出貨中</label>
                                <label><input type="radio" name="Status" value="4" <?=($row_order["Status"]=="4")?"checked":"";?>>已到貨</label>
                                <label><input type="radio" name="Status" value="5" <?=($row_order["Status"]=="5")?"checked":"";?>>退貨中</label>
                                <label><input type="radio" name="Status" value="6" <?=($row_order["Status"]=="6")?"checked":"";?>>退貨成功</label>
                            </div>
                        </div>
                        <? if(in_array($row_order["Status"], [5,6])) :?>
                        <div class="form-group">
                            <label for="f_path" class="col-sm-2 control-label">退貨人姓名</label>
                            <div class="col-sm-10" style="line-height: 32px;">
                                <?=$row_order["Return_Name"]?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="f_path" class="col-sm-2 control-label">退貨人手機</label>
                            <div class="col-sm-10" style="line-height: 32px;">
                                <?=$row_order["Return_Phone"]?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="f_path" class="col-sm-2 control-label">退貨地址</label>
                            <div class="col-sm-10" style="line-height: 32px;">
                                <?=$row_order["Return_City"].$row_order["Return_Area"].$row_order["Return_Addr"]?>
                            </div>
                        </div>
                        <? endif ?>
                        <div class="form-group">
                            <label for="f_path" class="col-sm-2 control-label">宅配單號</label>
                            <div class="col-sm-10" style="line-height: 32px;">
                                <input id="Delivery_No" name="Delivery_No" type="text" class="form-control" placeholder=""
                                       value="<?=$row_order["Delivery_No"]?>">

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="f_path" class="col-sm-2 control-label">綠界付款單號</label>
                            <div class="col-sm-10" style="line-height: 32px;">
                                <?=($row_order["trading_id"]);?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="f_path" class="col-sm-2 control-label">付款狀態</label>
                            <div class="col-sm-10" style="line-height: 32px;">
                                <?=($row_order["Pay_Status"]=="y")?"已付款":"未付款";?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="f_path" class="col-sm-2 control-label">發票資訊</label>
                            <div class="col-sm-10" style="line-height: 32px;">
                                <div  class="col-sm-6">
                                    抬頭：<input id="i_title" name="i_title" type="text" class="form-control" placeholder=""
                                       value="<?=$row_order["i_title"]?>">
                                </div>
                                <div  class="col-sm-6">
                                    統編：<input id="i_number" name="i_number" type="text" class="form-control" placeholder=""
                                       value="<?=$row_order["i_number"]?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="f_path" class="col-sm-2 control-label">備註</label>
                            <div class="col-sm-10" style="line-height: 32px;">
                                <input id="Remark" name="Remark" type="text" class="form-control" placeholder=""
                                       value="<?=$row_order["Remark"]?>">

                            </div>
                        </div>

                        <div class="form-group">
                            <label for="f_path" class="col-sm-2 control-label">建立時間</label>
                            <div class="col-sm-10" style="line-height: 32px;">
                                <i class="fa fa-clock-o" aria-hidden="true"></i> <?=$row_order["create_datetime"]?>
                            </div>
                        </div>


                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-right">
                        <input type="hidden" name="Order_ID" value="<?=$row_order['Order_ID']?>">
                        <a href="./list.php" class="btn btn-danger" style="margin-right: 10px;">取消，並返回上一層</a>
                        <button type="submit" class="btn btn-primary">儲存</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php include('../view/footer.php'); ?>

    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<?php include('../view/js_css_include.php'); ?>

</body>
</html>