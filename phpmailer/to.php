<?php 
include "../config.website.inc";
include_once "../appapi/config_medoo.php";
include_once "./toMail.php";
$username = filter_input(INPUT_POST, 'username');
$useremail = filter_input(INPUT_POST, 'useremail');
$usertel = filter_input(INPUT_POST, 'usertel');
$usertex = filter_input(INPUT_POST, 'usertext');

//檢查是否用POST
if ($_SERVER['REQUEST_METHOD'] != "POST"){
    js_go_back_global("NOT_POST");
    exit;
}

$username = filter_input(INPUT_POST, 'username');
$useremail = filter_input(INPUT_POST, 'useremail');
$usertel = filter_input(INPUT_POST, 'usertel');
$usertext = filter_input(INPUT_POST, 'usertext');

//檢查參數有無存在
if (empty($username) || empty($usertel) || empty($useremail)|| empty($usertext)){
    //js_go_back_self($zhconverter->convert("姓名不可空白", $Language_ID));
    echo Response::json(100,"請補全資料",null);
    exit;
}
$email = Mail::toMail("【金門商城】聯絡我們","ad@open-life.com","【金門商城】聯絡我們","<p>姓名：$username</p><p>E-mail：$useremail</p><p>電話：$usertel</p><p>說明文字：$usertext</p>");
if($email){
    echo Response::json(200,"發送成功",null);   
}
else{
   
    echo Response::json(100,"發送失敗，請稍後重試",null);
}


?>